    -------------------------------------------------------------------------
     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    -------------------------------------------------------------------------

	SPPAS resources
	
    -------------------------------------------------------------------------

    This is a set of resources files in order to use the automatic annotations
	of SPPAS. Resources for French and English languages are distributed in the
	SPPAS package. Resources for the support of other languages are hosted in a
	repository and have to be installed. So... How to get more language 
	resources?
	
		1. Install them with the program sppas/bin/preinstall.py; or
		2. Install them with the GUI "Page Annotate"; or
		3. Download and install them manually:
			• Repository hosting the resources: https://www.ortolang.fr 
			• Direct access: https://hdl.handle.net/11403/sppasresources
			• Owner: Brigitte Bigi contact@sppas.org

	 
	Some of the annotations like Face Detection or Face Sights, are also 
	requiring models which have to be installed in the same way.
	
	See the resources documentation for details about the installation and
	about the license of each resource file.

    -------------------------------------------------------------------------

Author: B. Bigi
Version: 1.2
License: LGPL, v3
Copyright (c) 2019-2022 Brigitte Bigi, Laboratoire Parole et Langage, France.


Changes in version 1.2:
=======================

- Different numbers for consonant: we now use the ones of the English cued speech
- Use letters for vowels instead of numbers

Consonnants are:

1. /p/, /d/, /Z/
2. /k/, /v/, /z/
3. /s/, /R\/
4. /b/, /n/, /H/
5. /m/, /t/, /f/
6. /l/, /S/, /J/, /w/
7. /g/
8. /j/, /N/

Vowels are:

- cheek bone /9~/, /2/
- side /A\/, /o/, /9/
- mouth /i/, /O~/, /a~/
- chin /E/, /u/, /O/
- throat /y/, /e/, /e~/


Changes in version 1.3:
=======================

Neutral position is 'N' and neutral shape is '0'.


Changes in version 1.4:
=======================

- cueConfigfra.txt bug correction: 9~ (-un) is 't' and "e~" (-in) is 'b'




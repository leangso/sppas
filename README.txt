    -------------------------------------------------------------------------
     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    * this software is governed by GPL version 3 or any later version;
    * documentations of this software are governed by GNU Free Documentation
      License, version 1.3;
    * samples of this software are under the terms of the Creative Commons
      Attribution-NonCommercial-ShareAlike 4.0 International Public License;
    * both plugins and resources of this software have individual licenses
      (see their documentation).

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see http://www.gnu.org/licenses/.

    -------------------------------------------------------------------------

    SPPAS - the automatic annotation and analyses of speech, is a "Research
    Software". It is a scientific computer software package written and
    maintained by Brigitte Bigi, researcher in Computer Science at the
    Laboratoire Parole et Langage, in Aix-en-Provence, France.

    * SPPAS main page: <https://sppas.org/>
    * SPPAS package: <https://sourceforge.net/projects/sppas>
    * Author home page: https://sppas.org/bigi/
    * Author ORCID: https://orcid.org/0000-0003-1834-6918

    -------------------------------------------------------------------------

    By using SPPAS, you agree to cite a reference in your publications.
    Any publication arising from the use of this software, including but
    not limited to academic journal and conference publications, technical
    reports and manuals, must cite at least one of the two following works
    or any of the references listed in the documentation:

        Brigitte BIGI (2015).
        SPPAS - Multi-lingual Approaches to the Automatic Annotation of Speech.
        the Phonetician - International Society of Phonetic Sciences,
        ISSN 0741-6164, Number 111-112 / 2015-I-II, pages 54-69.

        Brigitte Bigi (2016).
        A Phonetization Approach for the Forced-Alignment Task in SPPAS.
        Lecture Notes in Computer Science, vol 9561. Springer, Cham.
        https://doi.org/10.1007/978-3-319-43808-5_30

    -------------------------------------------------------------------------

    Help?

    - Tutorials, documentation and FAQ are available on the main web site
    - Explore the 'documentation' folder of the SPPAS package

    -------------------------------------------------------------------------

    * Any and all constructive comments are welcome: `contact@sppas.org`
    * Declare an issue or propose a plugin: `develop@sppas.org`
    * Software Heritage Identifier (SWHIG): swh:1:dir:6b7dc4f1610d6b1c3eefdd5cce320ac21611dc13
    * Clone the package with git: https://git.code.sf.net/p/sppas/code

    -------------------------------------------------------------------------

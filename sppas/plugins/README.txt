    -------------------------------------------------------------------------
     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    -------------------------------------------------------------------------

	SPPAS plugins

    -------------------------------------------------------------------------

    This is a set of plugins of SPPAS in order to add new features. The
    following 3 actions can be performed:
    1. Installing a plugin;
    2. Applying a plugin on some data;
    3. Removing a plugin.

    There's various solution to perform these actions:

    - unzip a new plugin into this folder to install it;
    - launch the GUI, select the "Plugins" page, click "Install" or "Delete"
      button, and/or click the plugin name and follow instructions;
    - use the CLI, with the sppas/bin program "plugin.py".

    Each plugin has its own license: see the plugin folder.

    -------------------------------------------------------------------------

"""
:filename: sppas.__init__.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Import all source packages.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

Import all packages of the SPPAS API, located in the "src" package.
Do not import python programs of bin and scripts nor the user interfaces.

"""


class sppasPythonError(Exception):
    """:ERROR 9999: Deprecated Python: SPPAS requires 3.6+.

    """

    def __init__(self):
        self._status = 9999
        self.parameter = ":ERROR 9999: Invalid Python. SPPAS requires 3.6+."

    def __str__(self):
        return repr(self.parameter)

    def get_status(self):
        return self._status

    status = property(get_status, None)

# ---------------------------------------------------------------------------


import sys
if sys.version_info < (3, 5):
    raise sppasPythonError

from .src import *

# ---------------------------------------------------------------------------

# sg is an instance of sppasGlobalSettings() defined in config package.
# The information are loaded from the sppasmeta.json external config file.
__version__ = sg.__version__
__name__ = sg.__name__
__author__ = sg.__author__
__docformat__ = sg.__docformat__

# -*- coding: UTF-8 -*-
"""
:filename: sppas.scripts.sppasseg.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Full automatic process of speech segmentation.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

Preamble:
=========

The audio file must be MONO, e.g. with only one channel. It can be 16,000Hz
or more but not less. It can be 16bits or 24bits (not tested) but 32bits can
be randomly problematic.

The transcribed file (if given) must be utf-8.

Any filename -- it includes its path, should only be US-ASCII characters
without whitespace: if not, it can randomly cause problems under Windows.

Description:
============

It's a three automatic steps process:

1. Prepare the data:

    - Nothing is done if the ortho. transcription is already time-aligned into
      the IPUs;
    - Fill in IPUs if the ortho. transcription is already available but not
      time-aligned. Notice that it can't work if the recording protocol was
      not very very very strict: it works only if the number of sounding
      segments EXACTLY matches the given number of transcribed segments.
    - Search for IPUs and auto. transcribe with the STT DeepSpeech. It
      requires installing DeepSpeech in the currently used Python and to give
      the path of the models. Get it here (DeepSpeech + English model):
      https://github.com/mozilla/DeepSpeech/releases/
      or here for another language:
      https://discourse.mozilla.org/t/links-to-pretrained-models/62688

      The function "stt" can be modified in order to replace DeepSpeech by
      another STT system. It could also be modified in order to use the API
      of DeepSpeech (much faster) rather than launching a process.

2. Perform Speech Segmentation;

    - no option to configure the annotations. They are all fixed internally!
    - can estimate an intensity coefficient;
    - can predict silent pauses (the ones into the IPUs).

3. Format the output (if asked).

    - can map the phoneme into another set (IPA, Praat, ...) if a mapping
      table is given. It's a file with two columns, both separated by a
      whitespace: 1st is SPPAS phoneme, 2nd is expected phoneme.
    - can create a data structure with all the results and write into a JSON
      file.

Example of use:
==============

    $SPPAS/.sppaspyenv~/bin/python3 $SPPAS/sppas/scripts/sppasseg.py
       -w ./audio_files/some_mono_audio.wav
       -l eng
       --sil
       --rms
       --norm
       -e .TextGrid
       -m ./DeepSpeechModels/

"""

import codecs
import logging
import sys
import shutil
import os
import json
from argparse import ArgumentParser
try:
    import deepspeech
    STT = True
except:
    STT = False

PROGRAM = os.path.abspath(__file__)
SPPAS = os.path.dirname(os.path.dirname(os.path.dirname(PROGRAM)))
sys.path.append(SPPAS)

from sppas.src.config import lgs
lgs.set_log_level(50)

from sppas.src.config import sg
from sppas.src.config import separators
from sppas.src.config import symbols
from sppas.src.config import paths
from sppas.src.config import Process
from sppas.src.calculus import fmean
from sppas.src.calculus import lstdev
from sppas.src.utils import sppasFileUtils
from sppas.src.anndata.anndataexc import AnnDataTypeError
from sppas.src.anndata import sppasTranscription
from sppas.src.anndata import sppasTier
from sppas.src.anndata import sppasMedia
from sppas.src.anndata import sppasTag
from sppas.src.anndata import sppasLabel
from sppas.src.anndata import sppasTrsRW
from sppas.src.anndata.aio.aioutils import serialize_labels
import sppas.src.audiodata.aio
from sppas.src.audiodata import sppasAudioPCM
from sppas.src.annotations.annotationsexc import EmptyInputError
from sppas.src.annotations.annotationsexc import NoChannelInputError
from sppas.src.annotations.annotationsexc import AudioChannelError
from sppas.src.annotations import sppasFindTier
from sppas.src.annotations import sppasSearchIPUs
from sppas.src.annotations import sppasFillIPUs
from sppas.src.annotations import sppasTextNorm
from sppas.src.annotations import sppasPhon
from sppas.src.annotations import sppasAlign
from sppas.src.annotations.Align.models.tiermapping import sppasMappingTier
from sppas.src.annotations.RMS.irms import IntervalsRMS
from sppas.src.annotations import SppasFiles

# ---------------------------------------------------------------------------
# Constants

SIL_PHON = list(symbols.phone.keys())[list(symbols.phone.values()).index("silence")]

# ---------------------------------------------------------------------------


class SPPASAutoIPUs(object):
    """Prepare the data to be used by SPPAS automatic segmentation.

    DeepSpeech is used for the Speech-To-Text task when no
    orthographic transcription is given. Any other system
    could be used...
    You only have to change the 'stt' command!

    """

    def __init__(self):
        """Create a SPPAS Automatic IPUs segmentation.

        """
        # One of them is used to segment speech:
        self.__ann_fillipus = sppasFillIPUs(log=None)
        self.__ann_searchipus = sppasSearchIPUs(log=None)
        # the following parameter is language dependent (at least 0.200 for
        # French, or 0.250 for English). Default is 0.200:
        self.__ann_searchipus.set_min_sil(0.400)
        # the following parameters depend on the audio quality (the worse
        # the higher). Default is 0.020:
        self.__ann_searchipus.set_shift_start(0.040)
        self.__ann_searchipus.set_shift_end(0.040)

    # -----------------------------------------------------------------------

    @staticmethod
    def stt(audio_file, models_dir):
        """Automatic Speech-To-Text.

        :param audio_file: (str) File name of a mono- 16kHz audio file.
        :param models_dir: (str) DeepSpeech models directory.
        :return: (str) The obtained (or guessed!) orthographic transcription

        """
        # Get deepspeech executable full path. Here, we expect it to be at the
        # same location than the currently used python.
        py_dir = os.path.dirname(sys.executable)
        if "deepspeech" not in os.listdir(py_dir):
            raise OSError("deepspeech not found in {:s}".format(py_dir))
        ds = os.path.join(py_dir, "deepspeech")

        # Get deepspeech model filename & the optional HIGHLY RECOMMENDED SCORER
        modelname = None
        scorername = None
        for filename in os.listdir(models_dir):
            if filename.endswith("-models.pbmm"):
                modelname = os.path.join(models_dir, filename)
            if filename.endswith("-models.scorer"):
                scorername = os.path.join(models_dir, filename)
        if modelname is None:
            raise OSError("No model found in {:s}".format(models_dir))

        # =======================================
        # Launch STT on the given IPU audio file
        # =======================================
        command = "{:s} ".format(ds)
        command += "--audio {:s} ".format(audio_file)
        command += "--model {:s} ".format(modelname)
        if scorername is not None:
            command += "--scorer {:s} ".format(scorername)

        process = Process()
        process.run(command)
        err = process.error()
        message = process.out()

        return message

    # -----------------------------------------------------------------------

    def ipussegments(self, input_files):
        """Perform IPUs segmentation fully automatically.

        :param input_files: List of required and optional input files (audio + text)
        :return: (sppasTier)

        """
        input_channel, input_tier, models_dir = self.get_inputs(input_files)

        if input_tier is None:
            # search for the IPUs and execute STT on each one
            if STT is False:
                raise IOError("No transcription is given and the STT system "
                              "is not installed.")
            if models_dir is None:
                raise IOError("No models directory was found for DeepSpeech...")
            return self.__do_search_and_stt(input_channel, models_dir)

        elif "raw" in input_tier.get_name().lower():
            # fill in the ipus with the orthographic transcription
            return self.__ann_fillipus.convert(input_channel, input_tier)

        else:
            return input_tier

    # -----------------------------------------------------------------------

    def __do_search_and_stt(self, input_channel, models_dir):
        """Search for ipus and transcribe with an stt..."""
        tier = self.__ann_searchipus.convert(input_channel)
        tier.set_name("TransSTT")
        framerate = input_channel.get_framerate()

        # -----------------------------------------------------------------------
        # Split the audio file into IPUs and transcribe automatically
        # -----------------------------------------------------------------------
        # Create a working directory
        working_dir = sppasFileUtils().set_random()
        os.mkdir(working_dir)

        # Browse through the IPUs in order to create its ortho transcription
        for i, ann in enumerate(tier):

            # is an IPU?
            text = serialize_labels(ann.get_labels(), separator="_", empty="", alt=False)
            if len(text) == 0 or ann.get_best_tag().is_silence():
                continue

            # get localization information
            begin = ann.get_lowest_localization().get_midpoint()
            end = ann.get_highest_localization().get_midpoint()

            # create audio output of the IPU
            fn_i = os.path.join(working_dir, "ipu_{:04d}.wav".format(i+1))
            extracter = input_channel.extract_fragment(int(begin*framerate), int(end*framerate))
            audio_out = sppasAudioPCM()
            audio_out.append_channel(extracter)
            sppas.src.audiodata.aio.save(fn_i, audio_out)

            # Launch STT on the given IPU audio file
            # =======================================
            txt = self.stt(fn_i, models_dir)
            ann.append_label(sppasLabel(sppasTag(txt)))

        # Make some cleaning: delete the working dir
        shutil.rmtree(working_dir)

        return tier

    # -----------------------------------------------------------------------

    def get_inputs(self, input_files):
        """Return the channel and the tier with ipus.

        :param input_files: (list)
        :raise: NoTierInputError
        :return: (sppasChannel, sppasTier)

        """
        # Get the tier and the channel
        audio_ext = SppasFiles.get_outformat_extensions("AUDIO")
        annot_ext = SppasFiles.get_outformat_extensions("ANNOT")
        tier = None
        channel = None
        models_dir = None
        audio_filename = ""
        for filename in input_files:
            if filename is None:
                continue
            if os.path.isdir(filename):
                models_dir = filename
                continue

            fn, fe = os.path.splitext(filename)

            if channel is None and fe in audio_ext:
                audio_speech = sppas.src.audiodata.aio.open(filename)
                n = audio_speech.get_nchannels()
                if n != 1:
                    audio_speech.close()
                    raise AudioChannelError(n)
                idx = audio_speech.extract_channel()
                channel = audio_speech.get_channel(idx)
                audio_filename = filename
                audio_speech.close()

            elif tier is None and fe in annot_ext:
                parser = sppasTrsRW(filename)
                trs_input = parser.read()
                # a raw transcription or an already aligned one is expected.
                tier = sppasFindTier.transcription(trs_input)

        # Check input tier
        if tier is not None:

            if "raw" in tier.get_name().lower() and tier.is_point() is False:
                raise AnnDataTypeError(tier.get_name(), 'PointTier')
            if tier.is_empty() is True:
                raise EmptyInputError(tier.get_name())

            # Check input channel
            if channel is None:
                raise NoChannelInputError

            # Set the media to the input tier
            extm = os.path.splitext(audio_filename)[1].lower()[1:]
            media = sppasMedia(os.path.abspath(audio_filename), mime_type="audio/"+extm)
            tier.set_media(media)

        return channel, tier, models_dir

# ---------------------------------------------------------------------------


class SPPASAutoSeg(object):
    """Full process of speech segmentation.

    With no intermediate results, the process is fully automatic and there's
    no way to check each step...

    The orthographic transcription is supposed to already be time-aligned at
    the IPUs level. Then, Speech Segmentation is:

        1- Text Normalization
        2- Phonetization
        3- Alignment

    """

    def __init__(self, lang):
        """Create a SPPAS Auto Segmenter.

        :param lang: (str) Language code in iso format.

        """
        self.__ann_norm = sppasTextNorm(log=None)
        vocab = os.path.join(paths.resources, "vocab", lang + ".vocab")
        self.__ann_norm.load_resources(vocab, lang=lang)
        self.__ann_norm.set_occ_dur(False)

        self.__ann_phon = sppasPhon(log=None)
        prons = os.path.join(paths.resources, "dict", lang + ".dict")
        self.__ann_phon.load_resources(prons)

        self.__ann_align = sppasAlign(log=None)
        acm = os.path.join(paths.resources, "models", "models-" + lang)
        self.__ann_align.load_resources(acm)

        self.__lang = lang
        self.predict_sil = False

    # -----------------------------------------------------------------------

    def phonsegments(self, audio_file, tier_iputrs):
        """Perform speech segmentation fully automatically.

        Audio is:
            - mono -- an exception is raised if not;
            - 16KHz -- if more, the audio is copied and converted by SPPAS to 16kHz;
            - 16 bits -- 24 works too -- but Python audio library randomly works on 32bits.

        :param audio_file: (str) Audio filename
        :param tier_iputrs: (sppasTier or None) A tier with the transcribed IPUs (if any)
        :return: (sppasTranscription)

        """
        # Normalize the transcription inside the ipus
        tier_faked_tokens, tier_std_tokens, tier_custom = self.__ann_norm.convert(tier_iputrs)

        # Phonetize the normalized text
        tier_phon = self.__ann_phon.convert(tier_faked_tokens)
        if self.predict_sil is True:
            self.__add_phon_sil(tier_phon)

        # Forced-alignment to fix the time-aligned phonemes and tokens
        workdir = sppasAlign.fix_workingdir(audio_file)
        try:
            tier_phna, tier_toka, tier_pron = self.__ann_align.convert(tier_phon, tier_std_tokens, tier_faked_tokens, audio_file, workdir)
            tier_phna.set_media(tier_iputrs.get_media())
        except:
            shutil.rmtree(workdir)
            raise

        # RMS estimator
        audio_speech = sppas.src.audiodata.aio.open(audio_file)
        idx = audio_speech.extract_channel()
        input_channel = audio_speech.get_channel(idx)
        audio_speech.close()

        tier_rms = self.__rms(tier_phna, input_channel)
        tier_rms.set_media(tier_iputrs.get_media())

        # Create the output transcription object
        trs = self.__create_trs(input_channel.get_framerate(), audio_file)
        trs.append(tier_iputrs)
        trs.append(tier_phna)
        trs.append(tier_toka)
        trs.append(tier_rms)
        return trs

    # -----------------------------------------------------------------------

    def __rms(self, tier, input_channel):
        """RMS value of each interval in the given tier. """
        rms = IntervalsRMS(input_channel)
        all_values = list()
        tier_rms = sppasTier("Intensity")
        for ann in tier:
            labels = ann.get_labels()
            if labels[0].get_best().is_silence() is True:
                rms_tag = sppasTag(0, "int")
                tier_rms.create_annotation(ann.get_location().copy(), sppasLabel(rms_tag))
                continue

            # Localization of the current annotation
            begin = ann.get_lowest_localization()
            end = ann.get_highest_localization()

            # Estimate all RMS values during this ann
            rms.estimate(begin.get_midpoint(), end.get_midpoint())

            # The global RMS of the fragment between begin and end
            rms_tag = sppasTag(rms.get_rms(), "int")
            tier_rms.create_annotation(ann.get_location().copy(), sppasLabel(rms_tag))
            all_values.append(rms.get_rms())

        avg = fmean(all_values)
        stdev = lstdev(all_values)
        for ann in tier_rms:
            labels = ann.get_labels()
            rms = labels[0].get_best().get_typed_content()
            if rms == 0:
                continue
            if rms < avg-stdev:
                ann.set_labels([sppasLabel(sppasTag(1, "int"))])
            elif rms > avg+stdev:
                ann.set_labels([sppasLabel(sppasTag(3, "int"))])
            else:
                ann.set_labels([sppasLabel(sppasTag(2, "int"))])

        return tier_rms

    # -----------------------------------------------------------------------

    def __add_phon_sil(self, tier_phon):
        """Add an alternative silence at the end of each variant."""
        for ann in tier_phon:
            # the current labels of this annotation (a phonetized ipu)
            labels = ann.get_labels()
            # the new list of labels of this phonetized ipu
            new_labels = list()
            # each tag is a pronunciation variant of a token
            for label in labels:
                # to not add an optional silence at the end of an IPU
                is_last_label = True if label is labels[-1] else False
                # all tags: with or without ending by a silence
                all_tags = list()
                # each tag is now 2 different variants
                for tag, score in label:
                    if tag is not None:
                        all_tags.append(tag)
                        if tag.is_silence() is False and is_last_label is False:
                            new_tag = tag.copy()
                            content = new_tag.get_content()
                            content = content + separators.phonemes + SIL_PHON
                            new_tag.set_content(content)
                            all_tags.append(new_tag)

                new_labels.append(sppasLabel(all_tags))
            ann.set_labels(new_labels)

    # -----------------------------------------------------------------------

    def __create_trs(self, framerate, filename):
        """Create a sppasTranscription and return it."""
        trs_output = sppasTranscription("SPPASAutoSeg")
        trs_output.set_meta('annotation_result_of', filename)
        trs_output.set_meta("media_sample_rate", str(framerate))
        trs_output.set_meta('language_iso', "iso639-3")
        trs_output.set_meta('language_code_0', self.__lang)
        trs_output.set_meta('language_name_0', "Undetermined")
        trs_output.set_meta('language_url_0', "https://iso639-3.sil.org/code/"+self.__lang)
        return trs_output

# ---------------------------------------------------------------------------


class FormatTrsSeg(object):
    """Format speech segmentation output.

    """

    def __init__(self, map_table=None):
        self.data = {
            'phonemes': {},
            'silence': {},
            'words': {},
            'expressions': {}}

        # Base energy value
        self.base_energy = 0.75

        # Energy modulation coeff from SPPAS intensity coeff
        self.mod_energy = 0.20

        # Mapping table of phonemes
        self.__mapping = sppasMappingTier(map_table)
        self.__mapping.set_keep_miss(True)  # keep unknown entries as given
        self.__mapping.set_miss_symbol("")  # not used!
        self.__mapping.set_delimiters([])   # will use longest matching

    # -----------------------------------------------------------------------

    def reset(self):
        """Reset the data."""
        self.data = {
            'phonemes': {},
            'silence': {},
            'words': {},
            'expressions': {}}

    # -----------------------------------------------------------------------

    def trs_phn_to_map(self, trs):
        """Map the phonemes of the phonemes tier of the given trs.

        :param trs: (sppasTranscription)

        """
        # Map the phonemes of the tier "PhonAlign"
        tier_phon = trs.find("PhonAlign")
        new_tier = self.__mapping.map_tier(tier_phon)
        new_tier.set_name(tier_phon.get_name())

        # Replace the current phon tier by the mapped one
        trs.pop(trs.get_tier_index_id(tier_phon.get_id()))
        trs.append(new_tier)

    # -----------------------------------------------------------------------

    def trs_to_data_norm(self, trs):
        """Fill in the data normalized structure from the given trs.

        :param trs: (sppasTranscription)

        """
        tier_rms = trs.find("Intensity")
        tier_phon = trs.find("PhonAlign")
        tier_tok = trs.find("TokensAlign")
        for a in tier_phon:
            start = round(a.get_location().get_best().get_begin().get_midpoint(), 3)
            end = round(a.get_location().get_best().get_end().get_midpoint(), 3)
            phn = serialize_labels(a.get_labels(), " ")
            ra = tier_rms.find(start, end, overlaps=False)
            if ra is not None:
                value = ra[0].get_labels()[0].get_best().get_typed_content()
                if value == 0:
                    energy = 0
                else:
                    energy = self.base_energy + ((value-2) * self.mod_energy)
            else:
                energy = self.base_energy
            # ok. set to the dict.
            self.data['phonemes'][start] = (phn, start, end, energy)

        for a in tier_tok:
            start = round(a.get_location().get_best().get_begin().get_midpoint(), 3)
            end = round(a.get_location().get_best().get_end().get_midpoint(), 3)
            tok_tag = a.get_best_tag(0)
            if tok_tag.is_silence() or tok_tag.is_pause():
                self.data['silence'][start] = (start, end, end-start)
            else:
                self.data['words'][start] = (tok_tag.get_content(), start, end)

# ---------------------------------------------------------------------------


if __name__ == "__main__":

    # -----------------------------------------------------------------------
    # Verify and extract args:
    # -----------------------------------------------------------------------

    parser = ArgumentParser(
        usage="%(prog)s -i text_file -w audio_file -e lang [options]",
        description="Fully automatic speech segmentation",
        epilog="This program is part of {:s} version {:s}. {:s}.".format(
            sg.__name__, sg.__version__, sg.__copyright__)
    )

    # Add arguments for input/output files
    # ------------------------------------

    group_io = parser.add_argument_group('Files')

    group_io.add_argument(
        "-w",
        required=True,
        metavar="file",
        help='Input audio file name (.wav).')

    group_io.add_argument(
        "-i",
        required=False,
        metavar="file",
        help='Input transcription file name (trs within IPUs or simple txt).')

    group_io.add_argument(
        "-m",
        metavar="file",
        required=False,
        help='Directory of DeepSpeech models (if no transcription).')

    group_io.add_argument(
        "-l",
        metavar="lang",
        required=True,
        help='Iso of one of the available languages.')

    group_io.add_argument(
        "-e",
        metavar=".ext",
        default=SppasFiles.get_default_extension("ANNOT"),
        choices=SppasFiles.get_outformat_extensions("ANNOT"),
        help='Output annotation file extension. One of: {:s}'
             ''.format(" ".join(SppasFiles.get_outformat_extensions("ANNOT"))))

    group_io.add_argument(
        "--map",
        metavar="file",
        required=False,
        help='A phoneme mapping table (2 columns with a whitespace separator).')

    parser.add_argument(
        "--sil",
        action='store_true',
        help="Enable the prediction of a short silence after each token")

    parser.add_argument(
        "--rms",
        action='store_true',
        help="Add the intensity in a [0-3] range scale")

    parser.add_argument(
        "--norm",
        action='store_true',
        help="Normalize and print the output on stdout")

    parser.add_argument(
        "--quiet",
        action='store_true',
        help="Disable verbosity")

    # Force to print help if no argument is given then parse
    # ------------------------------------------------------

    if len(sys.argv) <= 1:
        sys.argv.append('-h')

    args = parser.parse_args()

    # Redirect all messages to a quiet logging
    # ----------------------------------------
    if not args.quiet:
        lgs.set_log_level(15)
    lgs.stream_handler()

    # Check if either -i or -m is given but not both
    # ----------------------------------------------
    if STT is False and args.m:
        print("The option -m can only be used if DeepSpeech is available.")
        sys.exit(1)

    if args.i and args.m:
        print("The -i option and the -m option are both mutually exclusives.")
        sys.exit(1)

    if not (args.i or args.m):
        print("Either the -i option or the -m option is required.")
        sys.exit(1)

    # ----------------------------------------------------------------------------
    # Annotations are running here
    # ----------------------------------------------------------------------------
    fn, fe = os.path.splitext(args.w)

    # Prepare the data: create an ortho. transcription into IPUs
    ipus = SPPASAutoIPUs()
    tier_ipus = ipus.ipussegments([args.w, args.i, args.m])
    media = sppasMedia(os.path.abspath(args.w), mime_type="audio/" + fe)
    tier_ipus.set_media(media)

    # Perform the full speech segmentation process
    seg = SPPASAutoSeg(args.l)
    if args.sil:
        # predict the silent pauses into IPUs
        seg.predict_sil = True
    trs = seg.phonsegments(args.w, tier_ipus)

    # ----------------------------------------------------------------------------
    # Result output
    # ----------------------------------------------------------------------------
    # Map the phonemes if a mapping table is given
    if args.map:
        norm = FormatTrsSeg(map_table=args.map)
        norm.trs_phn_to_map(trs)
    else:
        norm = FormatTrsSeg(map_table=None)

    # Normalize the output if the option is enabled
    if args.norm:
        # Fill in a data structure from the objects
        norm.trs_to_data_norm(trs)
        # Write the resulting dictionary into a JSON file
        output_file = fn + "-sppasseg.json"
        with codecs.open(output_file, 'w', "UTF-8") as f:
            json.dump(norm.data, f, indent=4, separators=(',', ': '))
        if not args.quiet:
            logging.info(norm.data)

    # Write result into an annotated file with the given extension (default: xra)
    output_file = fn + "-sppasseg" + args.e
    parser = sppasTrsRW(output_file)
    parser.write(trs)

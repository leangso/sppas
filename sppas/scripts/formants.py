# -*- coding: UTF-8 -*-
"""
:filename: sppas.scripts.sppasseg.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Use Praat and Parsel-mouth to estimate formants.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import sys
import os
import logging
from argparse import ArgumentParser
try:
    import parselmouth
except Exception as e:
    print("Parselmouth import failed. Not installed? Parselmouth also requires Praat.")
    print("Try: python3 -m pip install praat-parselmouth")
    print(str(e))
    sys.exit(1)

PROGRAM = os.path.abspath(__file__)
SPPAS = os.path.dirname(os.path.dirname(os.path.dirname(PROGRAM)))
sys.path.append(SPPAS)

from sppas.src.config import lgs
lgs.set_log_level(50)

import sppas.src.audiodata.aio
from sppas.src.anndata import sppasTrsRW
from sppas.src.anndata import sppasTranscription
from sppas.src.anndata import sppasLabel
from sppas.src.anndata import sppasTag
from sppas.src.anndata import serialize_labels
from sppas.src.annotations import SppasFiles
from sppas.src.annotations import sppasFindTier

# ----------------------------------------------------------------------------
# Verify and extract args:
# ----------------------------------------------------------------------------

parser = ArgumentParser(usage="{:s} -i file".format(os.path.basename(PROGRAM)),
                        description="... a program to "
                                    "estimate formants of phonemes.")

parser.add_argument("-i",
                    metavar="file",
                    required=True,
                    help='Input audio file name.')

parser.add_argument("-e",
                    metavar="ext",
                    required=False,
                    default=".xra",
                    help='File extension for the tracks (default:.xra).')

parser.add_argument("--quiet",
                    action='store_true',
                    help="Disable the verbosity")

if len(sys.argv) <= 1:
    sys.argv.append('-h')

args = parser.parse_args()

# ----------------------------------------------------------------------------
# Load input data

# Open the audio and check it
try:
    audio = sppas.src.audiodata.aio.open(args.i)
except Exception as e:
    print(str(e))
    sys.exit(1)
if audio.get_nchannels() > 1:
    print('This script only supports mono audio files.')
    sys.exit(1)
audio.close()
logging.debug("Audio file is valid.")

#
sound = parselmouth.Sound(args.i)

# fix the annotated data filename
filename = os.path.splitext(args.i)[0]
phonemes = None
for ext in SppasFiles.get_informat_extensions("ANNOT"):
    phonemes = filename + "-palign" + ext
    if os.path.exists(phonemes):
        break
    else:
        phonemes = None
if phonemes is None:
    print('No annotated data corresponding to the audio file {:s}.'
          ''.format(args.i))
    sys.exit(1)
logging.debug("Annotated file with phonemes was found: {:s}.".format(phonemes))

# Load annotated data
try:
    parser = sppasTrsRW(phonemes)
    trs_input = parser.read()
    tier = sppasFindTier.aligned_phones(trs_input)
except Exception as e:
    print(str(e))
    sys.exit(1)
logging.debug("Annotated file with phonemes is valid.".format(phonemes))

# ----------------------------------------------------------------------------
# Prepare output
trs = sppasTranscription("Praat-Formants")
t1 = trs.create_tier("F1")
t2 = trs.create_tier("F2")
t3 = trs.create_tier("F3")
t4 = trs.create_tier("F4")

for ann in tier:

    # is a phoneme? if yes, extract the text content!
    text = serialize_labels(ann.get_labels(), separator="_", empty="", alt=False)
    if len(text) == 0 or not ann.get_best_tag().is_speech():
        continue

    # get localization information
    begin = ann.get_lowest_localization().get_midpoint()
    end = ann.get_highest_localization().get_midpoint()

    # manipulation = parselmouth.praat.call(sound, "To Manipulation", 0.01, 50, 650)
    # pitch_tier = parselmouth.praat.call(manipulation, "Extract pitch tier")
    # print(pitch_tier)

    formant = parselmouth.praat.call(sound, "To Formant (burg)", 0.01, 5, 5500, 0.025, 50.)
    # args are:
    #   Time step: 0.0 (=auto)
    #   Max. Number of formants: 5.0   ##### -->>> IS CHANGING SIGNIFICANTLY THE RESULTS.
    #   Formant ceiling (Hz): 5500.0 (= adult female)
    #   Window length (s): 0.025
    #   Pre-emphasis from (Hz): 50.0

    formant1 = parselmouth.praat.call(formant, "Get mean", 1, begin, end, 'hertz')
    formant2 = parselmouth.praat.call(formant, "Get mean", 2, begin, end, 'hertz')
    formant3 = parselmouth.praat.call(formant, "Get mean", 3, begin, end, 'hertz')
    formant4 = parselmouth.praat.call(formant, "Get mean", 4, begin, end, 'hertz')

    if not args.quiet:
        print("{:.3f} {:.3f} {:s} {:.2f} {:.2f} {:.2f} {:.2f}"
              "".format(begin, end, text, formant1, formant2, formant3, formant4))

    t1.create_annotation(ann.get_location().copy(),
                         [sppasLabel(sppasTag(round(formant1, 2), "float"))])
    t2.create_annotation(ann.get_location().copy(),
                         [sppasLabel(sppasTag(round(formant2, 2), "float"))])
    t3.create_annotation(ann.get_location().copy(),
                         [sppasLabel(sppasTag(round(formant3, 2), "float"))])
    t4.create_annotation(ann.get_location().copy(),
                         [sppasLabel(sppasTag(round(formant4, 2), "float"))])

# ----------------------------------------------------------------------------
# Write result into an annotated file with the given extension (default: xra)
output_file = filename + "-formants" + args.e
parser = sppasTrsRW(output_file)
parser.write(trs)

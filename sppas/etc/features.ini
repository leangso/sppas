; features.ini
; Configure the installation of other programs that SPPAS could use.
;
; Use [Section] to add a new feature of SPPAS requiring dependencies.
;
; The features corresponds to 3 types of installation:
;      - deps: package dependencies
;      - lang: linguistic resources
;      - annot: annotation resources
;
;
; For type = deps:
;
; * Attributes starting with "req_" concern system package dependencies.
;   Only unix-based systems can have package requirements. If they don't,
;   the corresponding req_ attribute is missing or empty or is "nil".
;     - req_deb: apt package manager
;     - req_ios: brew package manager
;   Other package managers are not implemented yet.
;
; * Attribute 'pip' concerns installation of Pypis packages:
;     - use whitespace to separate packages
;     - use ':' to separate the package name (at left) and the
;       version constraints (at right)
;
; * Attribute 'pip_alt' is for the installation of alternative Pypis
;   packages, in case the regular ones failed to be installed.
;
; * Attribute 'pip_opt' is for passing options to pip install system.
;
; * Attribute "cmd_" is a command to be executed for a given system.
;
; deb/ios/rpm/win
;

; ---------------------------------------------------------------------------
; deps
; ---------------------------------------------------------------------------

[wxpython]
type = deps
brief = The Graphical User Interface toolkit for the Python language
desc = This feature allows to interact with SPPAS in graphical mode, with the Graphical User Interface. It will install wxPython, the open source cross-platform GUI toolkit for the Python language. https://wxpython.org/
enable = true
req_deb = make:>;0.0 gcc:>;0.0 libgtk-3-dev:>;0.0 libwebkitgtk-dev:>;0.0 libwebkitgtk-3.0-dev:>;0.0 libgstreamer-gl1.0-0:>;0.0 freeglut3:>;0.0 freeglut3-dev:>;0.0 python-gst-1.0:>;0.0 python3-gst-1.0:>;0.0 libglib2.0-dev:>;0.0 ubuntu-restricted-extras:>;0.0 libgstreamer-plugins-base1.0-dev:>;0.0
req_rpm = @development-tools:>;0.0 dpkg:>;0.0 dpkg-devel:>;0.0 freeglut:>;0.0 freeglut-devel:>;0.0 gstreamer-plugins-bad-free:>;0.0 gstreamer-plugins-bad-free-devel:>;0.0 gstreamer-plugins-base:>;0.0 gstreamer-plugins-base-devel:>;0.0 gstreamer1-plugins-bad-free:>;0.0 gstreamer1-plugins-bad-free-devel:>;0.0 gstreamer1-plugins-base:>;0.0 gstreamer1-plugins-base-devel:>;0.0 gtk3:>;0.0 gtk3-devel:>;0.0 libjpeg:>;0.0 libjpeg-devel:>;0.0 libnotify:>;0.0 libnotify-devel:>;0.0 libSM:>;0.0 libSM-devel:>;0.0 libtiff:>;0.0 libtiff-devel:>;0.0 SDL:>;0.0 SDL-devel:>;0.0 webkitgtk4:>;0.0 webkitgtk4-devel:>;0.0 webkitgtk4-jsc:>;0.0 webkitgtk4-jsc-devel:>;0.0 webkitgtk4-plugin-process-gtk2:>;0.0
pip = wxpython:>4.0
pip_opt = -f https://wxpython.org/Phoenix/snapshot-builds/


[julius]
type = deps
brief = CSR Engine Julius
desc = This feature is used by the automatic annotation 'Alignment'. It allows to get time-alignment at phone and token levels. It will install the Open-Source Large Vocabulary CSR Engine Julius. https://julius.osdn.jp/en_index.php
enable = false
req_deb = julius:>;4.5
req_rpm = julius:>;4.5
req_ios = julius:>;4.5
cmd_win = python.exe $SPPAS\\sppas\\bin\\juliusdownload.py


[video]
type = deps
brief = Images and videos
desc = This feature allows SPPAS to deal with images and videos. The installation of numpy (BSD license), opencv (Apache2 license) and mediapipe (Apache2 license) libraries is required to enable some of the automatic annotations, including face detection and face landmark. See https://numpy.org/, https://opencv.org/ and https://mediapipe.dev/.
enable = false
pip = numpy:==1.19.3 opencv-python-headless:>4.4 mediapipe:>0.8


[audioplay]
type = deps
brief = Play audio files
desc = This feature allows to play sounds in the Edit page of the SPPAS GUI. It will install an audio library: either simpleaudio (MIT Licensed) or pyaudio (MIT Licensed - https://people.csail.mit.edu/hubert/pyaudio/) depending on the OS and the version of Python.
enable = true
pip = simpleaudio:>1.0
pip_alt = PyAudio:>0.1
req_deb = make:>;0.0 gcc:>;0.0 libasound2-dev:>;0.0 libportaudio2:>;0.0 portaudio19-dev:>;0.0
req_rpm = @development-tools:>;0.0 alsa-lib-devel:>;0.0
req_ios = portaudio:>;1


; ---------------------------------------------------------------------------
; annot. The name of the section is the 'id' of the annotation.
; ---------------------------------------------------------------------------

[facedetect]
type = annot
brief = Face detection
desc = Download the models to automatically detect faces in an image (Download: 5.3Mb). Requires video feature.
enable = false


[facemark]
type = annot
brief = Face landmarks
desc = Download the models to automatically detect 68 landmarks on a face in an image (Download: 106Mb). Requires video feature.
enable = false


[cuedspeech]
type = annot
brief = Cued Speech
desc = Download the rules and data to automatically generate Cued Speech key codes. Supported language is French only. (Download: 11.0Mb)
enable = false


; ---------------------------------------------------------------------------
; lang. The name of the section is the iso of the language.
; ---------------------------------------------------------------------------

[ben]
type = lang
brief = Bengali
desc = Support of Bengali Language. It includes a vocabulary, a pronunciation dictionary, and an acoustic model. (Download: 0.7Mb)
enable = false

[cat]
type = lang
brief = Catalan
desc = Support of Catalan Language. It includes a vocabulary, a pronunciation dictionary, and an acoustic model. (Download: 1.0Mb)
enable = false

[cmn]
type = lang
brief = Mandarin Chinese
desc = Support of Mandarin Chinese Language. It includes a vocabulary, a pronunciation dictionary, and an acoustic model. (Download: 1.4Mb)
enable = false

[deu]
type = lang
brief = German
desc = Support of German Language. It includes a vocabulary, a pronunciation dictionary, and an acoustic model. (Download: 4.5Mb)
enable = false

[eng]
type = lang
brief = English
desc = Support of English Language. It includes a vocabulary, a pronunciation dictionary, and an acoustic model. (Download: 1.2Mb)
enable = false

[fra]
type = lang
brief = French
desc = Support of French Language. It includes a vocabulary, a pronunciation dictionary, an acoustic model, syllabification rules, etc. (Download: 5.5Mb)
enable = false

[fraquebec]
type = lang
brief = Quebec French
desc = Support of Quebec French Language. It includes a pronunciation dictionary, and an acoustic model. (Download: 0.4Mb)
enable = false

[ita]
type = lang
brief = Italian
desc = Support of Italian Language. It includes a vocabulary, a pronunciation dictionary, an acoustic model and syllabification rules. (Download: 5.7Mb)
enable = false

[jpn]
type = lang
brief = Japanese
desc = Support of Japanese Language. It includes a vocabulary, a pronunciation dictionary, and an acoustic model. (Download: 0.7Mb)
enable = false

[kor]
type = lang
brief = Korean
desc = Support of Korean Language (under development). It includes a vocabulary, a pronunciation dictionary, and an acoustic model. (Download: 0.1Mb)
enable = false

[nan]
type = lang
brief = Min Nan
desc = Support of Min Nan Language. It includes a vocabulary, a pronunciation dictionary, and an acoustic model. (Download: 0.1Mb)
enable = false

[pcm]
type = lang
brief = Naija
desc = Support of Naija Language. It includes a vocabulary, a pronunciation dictionary, and an acoustic model. (Download: 0.5Mb)
enable = false

[pes]
type = lang
brief = Persian
desc = Support of Persian Language. It includes an acoustic model. (Download: 0.3Mb)
enable = false

[pol]
type = lang
brief = Polish language
desc = Support of Polish Language. It includes a vocabulary, a pronunciation dictionary, an acoustic model and rules for syllabification. (Download: 3.9Mb)
enable = false

[por]
type = lang
brief = Portuguese
desc = Support of Portuguese Language (under development). It includes a vocabulary, a pronunciation dictionary, an acoustic model. (Download: 0.4Mb)
enable = false

[spa]
type = lang
brief = Spanish
desc = Support of Spanish Language. It includes a vocabulary, a pronunciation dictionary, an acoustic model. (Download: 0.3Mb)
enable = false

[vie]
type = lang
brief = Vietnamese
desc = Support of Vietnamese Language. It includes a vocabulary. (Download: 6.5Mb)
enable = false

[yue]
type = lang
brief = Cantonese
desc = Support of Cantonese Language. It includes a vocabulary, a pronunciation dictionary, an acoustic model. (Download: 6.5Mb)
enable = false


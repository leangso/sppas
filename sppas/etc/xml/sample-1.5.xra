<?xml version="1.0" encoding="UTF-8"?>
<Document
		author="Brigitte Bigi"
		date="2021-10-12"
		format="1.5">

	<Metadata>
			<Entry key="created">2017-03-06</Entry>
			<Entry key="license">GPL v3</Entry>
	</Metadata>

	<Media id="m1" url="file:///file.wav" mimetype="audio/wav">
			<Tier id="t2"/>
			<Content>
			the media source file could also be embedded here
			</Content>
	</Media>

	<Media id="m2" url="file:///file.avi" mimetype="video/avi">
		<Metadata>
            <Entry key="samplerate">25</Entry>
            <Entry key="resolution">320x200</Entry>
            <Entry key="timeorigin">0.32</Entry>
            <Entry key="display">true</Entry>
		</Metadata>
		<Tier id="t1"/>
	</Media>

	<Media id="m3" url="file:///file.wav" />

	<Tier id="t1" tiername="Intonation">
		<Metadata>
            <Entry key="author">first name</Entry>
            <Entry key="expert">2</Entry>
		</Metadata>
		<Annotation score="0.555">
            <Location>
                <Point score="1.0" midpoint="0.1234" radius="0.0001" />
            </Location>
            <Label>
                <Tag score="0.8" type="str">le label supposé de cette annotation</Tag>
                <Tag score="0.2" type="str">le texte alternatif de cette annotation</Tag>
            </Label>
		</Annotation>
		<Annotation>
            <Location>
                <Point score="1.0" midpoint="0.2345" radius="0.0005" />
            </Location>
            <Label>
                <Tag type="str">H*</Tag>
            </Label>
		</Annotation>
	</Tier>

	<Tier id="t2" tiername="TokensAlign">
		<Annotation>
            <Location>
                <Interval>
                    <Begin midpoint="0.1234" />
                    <End midpoint="0.3600" radius="0.005" />
                </Interval>
            </Location>
            <Label>
                    <Tag>le</Tag>
            </Label>
		</Annotation>
		<Annotation>
            <Location>
                <Interval>
                    <Begin midpoint="0.3600" />
                    <End midpoint="0.8700" />
                </Interval>
            </Location>
            <Label>
                <Tag score="0.9">label</Tag>
                <Tag score="0.1" />
            </Label>
		</Annotation>
		<Annotation>
            <Location>
                <Interval>
                        <Begin midpoint="0.6000" radius="0.005" />
                        <End midpoint="0.9340" radius="0.0001" />
                </Interval>
            </Location>
            <Label>
                <Tag>supposé</Tag>
            </Label>
		</Annotation>
	</Tier>

	<Tier id="t3" tiername="IPU">
		<Annotation>
            <Location>
                <Interval score="0.8">
                    <Begin midpoint="0.1234" radius="0.001" />
                    <End midpoint="0.2340" radius="0.001" />
                </Interval>
                <Interval score="0.2">
                    <Begin midpoint="0.3456" radius="0.001" />
                    <End midpoint="0.7890" radius="0.001" />
                </Interval>
            </Location>
            <Label>
                <Tag score="-0.632">le texte supposé</Tag>
                <Tag score="-0.568">un texte alternatif</Tag>
            </Label>
		</Annotation>
	</Tier>

	<Tier id="t4" tiername="Tokens">
		<Annotation>
            <Location>
                <Interval score="0.8">
                    <Begin midpoint="0.1234" radius="0.001" />
                    <End midpoint="0.2340" radius="0.001" />
                </Interval>
                <Interval score="0.2">
                    <Begin midpoint="0.3456" radius="0.001" />
                    <End midpoint="0.7890" radius="0.001" />
                </Interval>
            </Location>
            <Label key="001">
                <Tag>le</Tag>
            </Label>
            <Label key="002">
                <Tag>texte</Tag>
            </Label>
            <Label key="003">
                <Tag>supposé</Tag>
            </Label>
		</Annotation>
	</Tier>

	<Tier id="t5" tiername="Phones">
		<Annotation>
            <Location>
                <Interval score="0.8">
                    <Begin midpoint="0.1234" radius="0.001" />
                    <End midpoint="0.2340" radius="0.001" />
                </Interval>
                <Interval score="0.2">
                    <Begin midpoint="0.3456" radius="0.001" />
                    <End midpoint="0.7890" radius="0.001" />
                </Interval>
            </Location>
            <Label key="001">
                <Tag score="0.5">l</Tag>
                <Tag score="0.5">l-@</Tag>
            </Label>
            <Label key="002">
                <Tag score="0.5">t-E-k-s-t</Tag>
                <Tag score="0.5">t-E-k-s-t-@</Tag>
            </Label>
            <Label key="003">
                <Tag>s-y-p-o-z-e</Tag>
            </Label>
		</Annotation>
	</Tier>

	<Tier id="t6" tiername="FaceDetection">
		<Metadata>
            <Entry key="description">Coordinates of the detected faces.</Entry>
		</Metadata>
		<Annotation id="faces_img4">
            <Location>
                <Interval>
                    <Begin midpoint="0.120" />
                    <End midpoint="0.160" />
                </Interval>
            </Location>
            <Label>
                <Tag type="rect" score="0.889">(127,232,318,324,10)</Tag>
            </Label>
		</Annotation>
		<Annotation id="faces_img8">
            <Location>
                <Interval>
                    <Begin midpoint="0.320" />
                    <End midpoint="0.360" />
                </Interval>
            </Location>
            <Label>
                <Tag type="rect" score="0.678">(123,240,320,324,10)</Tag>
                <Tag type="rect" score="0.245">(353,350,120,224,10)</Tag>
            </Label>
		</Annotation>
	</Tier>

	<Tier id="t7" tiername="VowelFacePoints">
		<Metadata>
            <Entry key="description">Coordinates of the vowel in images of Cued Speech.</Entry>
            <Entry key="label1">the top finger</Entry>
            <Entry key="label2">the wrist</Entry>
		</Metadata>
		<Annotation id="av1">
            <Location>
                <Interval>
                    <Begin midpoint="0.120" />
                    <End midpoint="0.160" />
                </Interval>
            </Location>
            <Label>
                <Tag type="point">(234,402,12)</Tag>
            </Label>
            <Label>
                <Tag type="point">(256,802)</Tag>
            </Label>
		</Annotation>
		<Annotation id="av2">
            <Location>
                <Interval>
                    <Begin midpoint="0.320" />
                    <End midpoint="0.400" />
                </Interval>
            </Location>
            <Label>
                <Tag type="point">(265,420,9)</Tag>
            </Label>
            <Label>
                <Tag type="point">(260,820)</Tag>
            </Label>
		</Annotation>
	</Tier>

	<Hierarchy>
            <Link type="TimeAlignment" from="t2" to="t3" />
			<Link type="TimeAssociation" from="t2" to="t4" />
			<Link type="TimeAssociation" from="t4" to="t5" />
	</Hierarchy>

	<Vocabulary id="v0" description="un short description">
        <Entry description="details about this specific controlled entry">
            le
        </Entry>
        <Entry type="str">
            label
        </Entry>
        <Entry>
            supposé
        </Entry>
        <Tier id="t2"/>
	</Vocabulary>
	
	<Vocabulary id="intensity" description="a vocabulary with integers as entries">
		<Entry description="low intensity level." type="int">0</Entry>
		<Entry description="medium intensity level." type="int">1</Entry>
		<Entry description="high intensity level." type="int">2</Entry>
	</Vocabulary>

</Document>


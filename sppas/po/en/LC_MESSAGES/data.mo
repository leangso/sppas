��          �   %   �      0     1     ?     M     [     i     w     �     �     �     �     �     �     �     �     �                    -     ;     I     W     e  0  s     �  9   �  8   �  7   .  )   f  3   �     �  (   �  &     ,   (  4   U     �  :   �  B   �  D   (  C   m  D   �  9   �  <   0  ;   m  <   �  9   �  ,    	                                                                                                       
          	              :ERROR 2000:  :ERROR 2005:  :ERROR 2010:  :ERROR 2015:  :ERROR 2020:  :ERROR 2025:  :ERROR 2050:  :ERROR 2060:  :ERROR 2061:  :ERROR 2062:  :ERROR 2070:  :ERROR 2080:  :ERROR 2330:  :ERROR 2332:  :ERROR 2334:  :ERROR 2336:  :ERROR 2338:  :ERROR 2610:  :ERROR 2620:  :ERROR 3610:  :ERROR 3620:  :ERROR 3630:  :ERROR 3640:  Project-Id-Version: SPPAS
Report-Msgid-Bugs-To: contact@sppas.org
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Brigitte Bigi <develop@sppas.org>
Language-Team: 
MIME-Version: 1.0
Language: en_US
X-Generator: Poedit 2.2.1
 No audio file defined. Audio type error: not supported file format {extension}.  {filename} opening, reading or writing error: {message}. No data or corrupted data in the audio file {filename}. {number} is not a right index of channel. From {value1} to {value2} is not a proper interval. No channel defined. Channels have not the same sample width. Channels have not the same frame rate. Channels have not the same number of frames. Invalid sample width. Expect 1, 2 or 4. Got {value}. Invalid framerate. Got {value}. The value {} can't be superior to the one of the image {}. The width value {} can't be superior to the width {} of the image. The height value {} can't be superior to the height {} of the image. The x-axis value {} can't be superior to the width {} of the image. The y-axis value {} can't be superior to the height {} of the image. Image of file {filename} can't be read by opencv library. Image of file {filename} can't be written by opencv library. Video of file {filename} can't be opened by opencv library. Video of file {filename} can't be written by opencv library. Video of file {filename} can't be read by OpenCV library. A video has already locked the video stream. 
��          �      |      �     �     �               )     7     E     S     a     o     }     �     �     �     �     �     �     �     �     �     	  7    1   O  (   �  -   �     �  "   �  "     ;   <  <   x     �  1   �  R   �  @   L  )   �  8   �  /   �  B      )   c  0   �  0   �  0   �  .         
                     	                                                                       :ERROR 9010:  :ERROR 9012:  :ERROR 9014:  :ERROR 9020:  :ERROR 9030:  :ERROR 9032:  :ERROR 9034:  :ERROR 9036:  :ERROR 9040:  :ERROR 9060:  :ERROR 9062:  :ERROR 9064:  :ERROR 9110:  :ERROR 9120:  :ERROR 9160:  :ERROR 9162:  :ERROR 9164:  :ERROR 9180:  :ERROR 9182:  :ERROR 9184:  :ERROR 9186:  Project-Id-Version: SPPAS
Report-Msgid-Bugs-To: contact@sppas.org
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Brigitte Bigi <brigitte.bigi@gmail.com>
Language-Team: 
MIME-Version: 1.0
Language: en_US
X-Generator: Poedit 1.8.11
 Name {!s:s} does not match a file or a directory. Name {!s:s} does not match a valid file. Name {!s:s} does not match a valid directory. {:s} has no attribute '{:s}'. '{:s}' does not match root '{:s}'. '{:s}' does not match with '{:s}'. '{:s}' cant be added because it already exists in the list. '{:s}' cant be removed because it doesn't exist in the list. {!s:s} is locked. An identifier must contain at least 2 characters. Identifier '{ident}' is not valid. It should be between 2 and 12 ASCII-characters. Attribute value '{value}' can't be converted into type '{type}'. Unknown extension '{:s}' for a workspace. No workspace file is matching the workspace name '{:s}'. A workspace with name {:s} is already existing. It is not allowed to export a workspace with the same name '{:s}'. Workspace with name '{:s}' was not found. It is not allowed to export the Blank workspace. It is not allowed to delete the Blank workspace. It is not allowed to rename the Blank workspace. It is not allowed to save the Blank workspace. 
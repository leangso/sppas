��            )   �      �     �     �     �     �     �     �     �                    -     ;     I     W     e     s     �     �     �     �     �     �     �     �     �     �               "    /  (   L  *   u     �  $   �  '   �  ;     5   C  :   y  '   �  A   �  J     j   i  m   �  !   B  $   d  '   �  "   �     �  :   �  :   +	  (   f	     �	  "   �	      �	     �	      
     %
  *   <
     g
                                                                                                     	                      
              :ERROR 0001:  :ERROR 0100:  :ERROR 0200:  :ERROR 0300:  :ERROR 0310:  :ERROR 0320:  :ERROR 0330:  :ERROR 0340:  :ERROR 0400:  :ERROR 0510:  :ERROR 0520:  :ERROR 0530:  :ERROR 0540:  :ERROR 0600:  :ERROR 0610:  :ERROR 0620:  :ERROR 0650:  :ERROR 0660:  :ERROR 0670:  :ERROR 0680:  :ERROR 0977:  :INFO 0510:  :INFO 0520:  :INFO 0530:  :INFO 0540:  :INFO 0550:  :INFO 0560:  :INFO 0570:  :INFO 0580:  Project-Id-Version: SPPAS
Report-Msgid-Bugs-To: contact@sppas.org
PO-Revision-Date: 
Last-Translator: Brigitte Bigi <develop@sppas.org>
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.2.2
 The following error occurred: {message}. {!s:s} is not of the expected type '{:s}'. Invalid index value {:d}. Invalid value '{!s:s}' for '{!s:s}'. Expected a positive value. Got {value}. Min value {min_value} is bigger than max value {max_value}. Value {value} out of range [{min_value},{max_value}]. List index {value} out of range [{min_value},{max_value}]. Invalid key '{!s:s}' for data '{!s:s}'. Installation procedure reported error(s) and/or warnings: {error} Feature {name} is not enabled; its installation should be processed first. The package {package} can't be imported. The installation of the feature {name} should be processed first. The package {package} is not up-to-date. The re-installation of the feature {name} should be processed first. No such file or directory: {name} Unknown extension for filename {:s}. The directory {dirname} does not exist. File '{:s}' can't be open or read. File '{:s}' can't be saved. Files with extension '{:s}' are not supported for reading. Files with extension '{:s}' are not supported for writing. The input field '{:s}' is not completed. Installation of '{name}'. '{name}' not available on your OS. Install of '{name}' is disabled. '{name}' is installed. Installation of '{name}' failed. Installation finished. '{name}' does not match any valid package. No system command is processed. 
��    .      �  =   �      �     �     �               %     2     ?     L     Y     f     s     �     �     �     �     �     �     �     �     �     �                    )     6     C     P     ]     j     w     �     �     �     �     �     �     �     �     �  "   �     �          
  	      �   *  �   �  D   �  6      �   W  �   	     �	     �	  "   �	  "   �	  A   
      _
  -   �
     �
      �
     �
       .     }   J  g   �  �   0  D   �          #  )   1  D   [  h   �  �   	  s   �  !   $  *   F  +   q     �     �     �     �     �     �     �     �     �  "        $     1     6  	   L                         ,      %      #   
         $   .       )                 !                            '          "      &                +                                	   -   *          (                  :INFO 0500:  :INFO 0502:  :INFO 0504:  :INFO 0506:  :INFO 0508:  :INFO 0510:  :INFO 0512:  :INFO 0514:  :INFO 0520:  :INFO 0524:  :INFO 0530:  :INFO 0534:  :INFO 0540:  :INFO 0544:  :INFO 0550:  :INFO 0551:  :INFO 0552:  :INFO 0554:  :INFO 0555:  :INFO 0556:  :INFO 0558:  :INFO 0560:  :INFO 0561:  :INFO 0562:  :INFO 0563:  :INFO 0564:  :INFO 0565:  :INFO 0566:  :INFO 0568:  :INFO 0570:  :INFO 0573:  :INFO 0580:  Back Cancel Confirm exit? Description Exit Feature Features Install InstallWizard of {soft} {version}. Installation Next Page ({page}/{total}) View logs Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.2.2
 SPPAS is a research software package developed by Brigitte Bigi, CNRS researcher at 'Laboratoire Parole et Langage', Aix-en-Provence, France.

The InstallWizard will install on your computer other programs SPPAS is requiring. Please read the following license agreement. Click 'Next' to accept. Check the boxes in the list below to enable a feature: Click Install button to start the installation.

If you want to review or change any of your installation settings, click 'Back'. Click 'Cancel' to exit the wizard without installing. Before clicking 'Exit' button, share your experience of this installer with the author:
Click on the 'Send feedback' button of the 'Log window'. Installation of '{name}' See the log file for details. Installation of external programs. '{name}' not available on your OS. Download linguistic resources for language-dependent annotations. Install of '{name}' is disabled. Download resources to enable the annotations. '{name}' is installed. All installations and downloads. Installation of '{name}' failed Welcome & conditions Welcome to the InstallWizard proposed by SPPAS SPPAS offers open source cross-platform, customizable automatic annotation and analysis solutions for audio and video media.  The InstallWizard will install on your computer other programs SPPAS is using for some of its features. SPPAS is a research software package developed by Brigitte Bigi. She is a researcher at CNRS, 'Laboratoire Parole et Langage', Aix-en-Provence, France. I undertake herewith to cite a reference of SPPAS in my publications Installation finished License terms Accept license terms of SPPAS source code this software is governed by GNU GPL version 3 or any later version; the documentations of this software package are governed by GNU Free Documentation License, version 1.3; the samples of this software package are under the terms of the CC-By-NC-4.0 - Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License; both the plugins and resources of this software package have individual licenses. Please, read their documentation. I accept the terms of the license '{name}' does not match any valid package. Installation procedure of external programs No system command is processed. Back Cancel Confirm exit? Description Exit Feature Features Install InstallWizard of {soft} {version}. Installation Next Page ({page}/{total}) View logs 
��          �   %   �      0     1     ?     M     [     i     w     �     �     �     �     �     �     �     �     �                    -     ;     I     W     e  0  s  &   �  B   �  m     E   |  3   �  =   �  $   4  8   Y  5   �  '   �  H   �  ?   9  _   y  B   �  D     C   a  D   �  N   �  R   9	  K   �	  Q   �	  N   *
  8   y
                                                                                                       
          	              :ERROR 2000:  :ERROR 2005:  :ERROR 2010:  :ERROR 2015:  :ERROR 2020:  :ERROR 2025:  :ERROR 2050:  :ERROR 2060:  :ERROR 2061:  :ERROR 2062:  :ERROR 2070:  :ERROR 2080:  :ERROR 2330:  :ERROR 2332:  :ERROR 2334:  :ERROR 2336:  :ERROR 2338:  :ERROR 2610:  :ERROR 2620:  :ERROR 3610:  :ERROR 3620:  :ERROR 3630:  :ERROR 3640:  Project-Id-Version: SPPAS
Report-Msgid-Bugs-To: contact@sppas.org
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Brigitte Bigi <develop@sppas.org>
Language-Team: 
MIME-Version: 1.0
Language: fr_FR
X-Generator: Poedit 2.2.1
 Aucun fichier audio n'a été défini. Les fichiers audio d'extension {extension} ne sont pas supportés. L'erreur suivante s'est produite à l'ouverture, la lecture ou l'écriture du fichier {filename} : {message}. Le fichier audio {filename} est vide ou ses données sont corrompues. Aucun canal d'index {number} dans le fichier audio. De {value1} à {value2} ne constitue pas un intervale valide. Aucun canal audio n'a été défini. Les canaux n'ont pas les mêmes tailles d'échantillons. Les canaux n'ont pas le même taux d'échantillonage. Les canaux ne font pas la même taille. La taille d'échantillon {value} est invalide. Il est attendu 1, 2 ou 4. {value} n'est pas une valeur correcte de taux d'echantillonage. La valeur {} ne peut pas être supérieure à celle de l'image {} à laquelle elle se réfère. The width value {} can't be superior to the width {} of the image. The height value {} can't be superior to the height {} of the image. The x-axis value {} can't be superior to the width {} of the image. The y-axis value {} can't be superior to the height {} of the image. Le fichier d'image {filename} n'a pas pu être lu par la bibliothèque opencv. Le fichier d'image {filename} n'a pas pu être écrit par la bibliothèque opencv. La librairie OpenCV n'a pas réussi à ouvrir le fichier vidéo {filename}. La librairie OpenCV n'a pas réussi à écrire dans le fichier vidéo {filename}. La librairie OpenCV n'a pas pu lire la vidéo à partir du fichier {filename}. Une autre vidéo a déjà ouvert et verrouillé le flux. 
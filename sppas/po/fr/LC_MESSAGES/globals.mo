��          �   %   �      p     q          �     �     �     �     �     �     �     �     �               '     5     C     Q     _     m     {     �     �     �     �     �     �     �    �  .   �  )   .     X  0   w  G   �  `   �  J   Q  H   �  1   �  <     g   T  �   �  �   =  (   �  "   �  F   	  B   Y	  G   �	  0   �	     
  A   .
  ,   p
     �
  '   �
     �
  +   �
  /                                                                      
                                     	                    :ERROR 0001:  :ERROR 0100:  :ERROR 0200:  :ERROR 0300:  :ERROR 0310:  :ERROR 0320:  :ERROR 0330:  :ERROR 0340:  :ERROR 0400:  :ERROR 0510:  :ERROR 0520:  :ERROR 0530:  :ERROR 0540:  :ERROR 0610:  :ERROR 0620:  :ERROR 0650:  :ERROR 0670:  :ERROR 0680:  :ERROR 0977:  :INFO 0510:  :INFO 0520:  :INFO 0530:  :INFO 0540:  :INFO 0550:  :INFO 0560:  :INFO 0580:  :INFO 570:  Project-Id-Version: SPPAS
Report-Msgid-Bugs-To: contact@sppas.org
PO-Revision-Date: 
Last-Translator: Brigitte Bigi <develop@sppas.org>
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.2.2
 L'erreur suivante a été relevée: {message}. {!s:s} n'est pas du type attendu: '{:s}'. Valeur d'index invalide: {:d}. La valeur '{!s:s}' est incorrecte pour '{!s:s}'. Une valeur positive était attendue, alors que {value} a été donnée. La valeur minimale donnée {min_value} est supérieur à la valeur maximale donnée {max_value}. La valeur {value} est en dehors de l'intervalle [{min_value},{max_value}]. L'index {value} est en dehors de l'intervalle [{min_value},{max_value}]. Clé inconnue '{!s:s}' pour les données '{!s:s}' L'installation a induit des erreurs et/ou warnings: {error}. La fonctionnalité {name} n'est pas activée; son installation est requise avant de pouvoir l'utiliser. Le package {package} n'a pas pu être importé. L'installation de la fonctionnalité {name} est requise pour pouvoir l'utiliser. Le package {package} nécessite une mise à jour. Une ré-installation de la fonctionnalité {name} est requise pour pouvoir l'utiliser. L'extension du fichier {:s} est inconnue Le dossier {dirname} n'existe pas. Le fichier '{:s}' n'a pas pu être ouvert ou n'a pas pu être chargé. Les fichiers avec l'extension '{:s}' ne peuvent pas être ouverts. Les fichiers avec l'extension '{:s}' ne peuvent pas être enregistrés. Une donnée requise '{:s}' n'a pas été saisie. Installation de '{name}' '{name}' n'est pas disponible pour votre système d'exploitation. L'installation de '{name}' est désactivée. '{name}' est installé. L'installation de '{name}' a échouée. Installation terminée Aucune commande système n'est nécessaire. '{name}' ne correspond à aucun package valide. 
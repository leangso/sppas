#!/usr/bin/env python
"""
:filename: sppas.__main__.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Entry point to launch the Graphical User Interface of SPPAS

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

In Python, '__main__' is the name of the scope in which top-level code
executes. Within SPPAS, it allows to launch the Graphical User Interface.

To launch the GUI, this main file allows the followings 3 solutions:

>>> py3 -m sppas
>>> py3 sppas
>>> py3 sppas/__main__.py

It is supposed that the 'py3' is a link to the python virtuel environment,
created during the setup, i.e. either .sppaspyenv/bin/python (Unix OS)
or .sppaspyenv/Scripts/python.exe (Windows).

In case of error, SPPAS creates a log file with the error message, and it
displays it in the web browser.

"""

# Import of python standard libraries then python version check.
import sys
import os
import webbrowser
import time
import traceback

if sys.version_info < (3, 7):
    print("[ WARNING ] Python should to be updated. "
          "The GUI of SPPAS requires version 3.7+.")

# package is not defined if the program is launched without the -m option
if __package__ is None or len(__package__) == 0:
    sppas_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.insert(0, sppas_dir)
    __package__ = "sppas"

try:
    from sppas.src.config import cfg
    from sppas.src.config import sppasLogFile
    from sppas.src.config import sppasEnableFeatureError
    from sppas.src.config import sppasPackageFeatureError
    from sppas.src.config import sppasPackageUpdateFeatureError
    from sppas.ui.wxapp import sppasApp
except Exception as e:
    # The API wasn't imported.
    # Something is wrong with the installation of python, wxpython or SPPAS.
    msg = traceback.format_exc()
    try:
        status = e.status
    except:
        status = 1
    print(msg)
    print("* * * * *  SPPAS exited with error number: {:04d}  * * * * * ".format(status))
    webbrowser.open(url="https://sppas.org/documentation_09_annexes.html#error-" + "{:04d}".format(status))
    time.sleep(10)
    sys.exit(1)

# The API was successfully imported
status = 1
msg = ""
try:
    # Create and run the wx application
    app = sppasApp()
    status = app.run()
except sppasEnableFeatureError as e:
    # wxpython feature is not enabled
    print(traceback.format_exc())
    status = e.status
    msg = str(e)
except sppasPackageFeatureError as e:
    # wxpython is enabled but wx can't be imported
    print(traceback.format_exc())
    status = e.status
    msg = str(e)
except sppasPackageUpdateFeatureError as e:
    # wxpython is enabled but the version is not the right one
    print(traceback.format_exc())
    status = e.status
    msg = str(e)
except Exception as e:
    # any other error...
    print(traceback.format_exc())
    msg = str(e)

if status != 0:
    # open the report to display its content
    report = sppasLogFile(pattern="run")
    with open(report.get_filename(), "w") as f:
        f.write(report.get_header())
        f.write(msg)
        f.write("\n")
        f.write("\n")
        f.write("SPPAS application exited with error status: {:d}.".format(status))
    webbrowser.open(url=report.get_filename())

sys.exit(status)

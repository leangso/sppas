"""
:filename: sppas.ui.htmlmaker.hresponse.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Base class to create an HTML response.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

from sppas.ui.htmlmaker.hleaf import HTMLComment
from sppas.ui.htmlmaker.hnode import HTMLNode
from sppas.ui.htmlmaker.htree import HTMLTree

from .hstatus import sppasHTTPDStatus

# ---------------------------------------------------------------------------


class BaseResponseRecipe(object):
    """Base class to create an HTML response file.

    """

    def __init__(self, name="Undefined", tree=None):
        """Create a new ResponseRecipe instance with a default response.

        """
        # Define members with default values
        self._name = name
        self._status = sppasHTTPDStatus()

        # Define workers: the HTML bakery
        if tree is not None and isinstance(tree, HTMLTree):
            self._htree = tree
        else:
            self._htree = HTMLTree(self._name.replace(" ", "_"))
        self.create()

    # -----------------------------------------------------------------------
    # Getters
    # -----------------------------------------------------------------------

    @staticmethod
    def page() -> str:
        """Return the HTML page name. To be overridden."""
        return "undefined.html"

    @property
    def name(self) -> str:
        return self._name

    @property
    def status(self) -> sppasHTTPDStatus:
        return self._status

    # -----------------------------------------------------------------------
    # Convenient methods to add HTML nodes in the body part of the tree
    # -----------------------------------------------------------------------

    def comment(self, content):
        """Add a comment to the body->main."""
        node = HTMLComment(self._htree.body_main.identifier, content)
        self._htree.body_main.append_child(node)
        return node

    def div(self, class_name):
        """Add a div to the body->main."""
        node = HTMLNode(self._htree.body_main.identifier, None, "div", attributes={"class": class_name})
        self._htree.body_main.append_child(node)
        return node

    def section(self, ident=None, class_name=None):
        """Add a section to the body->main.

        :param ident: (str) Identifier or None to automatically assign one
        :param class_name: (str) A css class name

        """
        att = dict()
        if ident is not None:
            att["id"] = str(ident)
        if class_name is not None:
            att["class"] = str(class_name)

        node = HTMLNode(self._htree.body_main.identifier, ident, "section", attributes=att)
        self._htree.body_main.append_child(node)
        return node

    # -----------------------------------------------------------------------
    # Workers
    # -----------------------------------------------------------------------

    def create(self) -> None:
        """Create the fixed page content in HTML. Intended to be overridden.

        This method is intended to be used to create the parts of the tree
        that won't be invalidated when baking.

        """
        pass

    # -----------------------------------------------------------------------

    def bake(self, events) -> str:
        """Return the HTML response after processing the events.

        Processing the events may change the response status.

        :param events: (dict) The requested events to be processed

        """
        # Process the given events with SPPAS API.
        dirty = False
        if len(events) > 0:
            dirty = self._process_events(events)

        # Re-create the page content only if something changed during
        # processing the events.
        if dirty is True:
            self._invalidate()
            self._bake()

        # Turn the page content into an HTML string.
        return self._htree.serialize()

    # -----------------------------------------------------------------------
    # Private: methods to be overridden by children to customize the recipe.
    # -----------------------------------------------------------------------

    def _process_events(self, events) -> bool:
        """Process the given events.

        The given event name must match a function of the event's manager.

        :param events (dict): key=event_name, value=event_value
        :return: None

        """
        self._status.code = 200
        return False

    # -----------------------------------------------------------------------

    def _invalidate(self):
        """Remove children nodes of the tree. Must be overridden."""
        raise NotImplementedError

    # -----------------------------------------------------------------------

    def _bake(self) -> None:
        """Fill in the HTML page generator. Must be overridden.

        This method is baking the "dynamic" content of the page, i.e. it
        should not change the content created by the method create().

        """
        raise NotImplementedError

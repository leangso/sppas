
import unittest

from ..hstatus import sppasHTTPDValueError
from ..hstatus import sppasHTTPDStatus

# ---------------------------------------------------------------------------


class TestHTTPDExceptions(unittest.TestCase):

    def test_status_value_errors(self):
        try:
            raise sppasHTTPDValueError("value")
        except ValueError as e:
            self.assertTrue(isinstance(e, sppasHTTPDValueError))
            self.assertTrue("0377" in str(e))
            self.assertTrue(377, e.status)


class TestHTTPDStatus(unittest.TestCase):

    def test_check(self):
        # Check success
        self.assertEqual(100, sppasHTTPDStatus.check(100))
        self.assertEqual(200, sppasHTTPDStatus.check("200"))

        # Check fail
        with self.assertRaises(sppasHTTPDValueError):
            sppasHTTPDStatus.check("AZERTY")
        with self.assertRaises(sppasHTTPDValueError):
            sppasHTTPDStatus.check(84)

    def test_init(self):
        s = sppasHTTPDStatus()
        self.assertEqual(str(s), "200")
        self.assertTrue(s == 200)
        self.assertEqual(200, s)

    def test_get_set(self):
        s = sppasHTTPDStatus()
        s.code = 404
        self.assertEqual(404, s)
        self.assertEqual(404, s.code)

        with self.assertRaises(sppasHTTPDValueError):
            s.code = "azerty"
        with self.assertRaises(sppasHTTPDValueError):
            s.code = 1974



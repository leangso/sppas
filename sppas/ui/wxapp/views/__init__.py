# -*- coding: UTF-8 -*-

from .about import About
from .about import AboutPlugin
from .metaedit import MetaDataEdit

from .feedback import Feedback
from .settings import Settings

__all__ = (
    "MetaDataEdit",
    'Feedback',
    'About',
    'AboutPlugin',
    'Settings'
)

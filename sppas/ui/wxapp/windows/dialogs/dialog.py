# -*- coding: UTF-8 -*-
"""
:filename: sppas.ui.wxapp.windows.dialogs.dialog.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  A custom dialog to look the same on all platforms.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import wx

from sppas.src.config import msg

from sppas.ui.wxapp.events.wins import sb
from sppas.ui.wxapp.windows.buttons import BitmapTextButton
from sppas.ui.wxapp.windows.buttons import BitmapButton
from sppas.ui.wxapp.windows.panels import sppasPanel
from sppas.ui.wxapp.windows.line import sppasStaticLine
from sppas.ui.wxapp.windows.label import sppasLabelHeader

# ----------------------------------------------------------------------------


MSG_ACTION_OK = msg("Okay", "ui")
MSG_ACTION_CANCEL = msg("Cancel", "ui")
MSG_ACTION_YES = msg("Yes", "ui")
MSG_ACTION_NO = msg("No", "ui")
MSG_ACTION_APPLY = msg("Apply", "ui")
MSG_ACTION_CLOSE = msg("Close", "ui")
MSG_ACTION_SAVE = msg("Save", "ui")

# ----------------------------------------------------------------------------


class sppasDialog(wx.Dialog):
    """Base class for dialog views in SPPAS.

    """

    def __init__(self, *args, **kw):
        """Create a dialog.

        Possible constructors:

            - sppasDialog()
            - sppasDialog(parent, id=ID_ANY, title="", pos=DefaultPosition,
                     size=DefaultSize, style=DEFAULT_DIALOG_STYLE,
                     name=DialogNameStr)

        A sppasDialog is made of 3 (optional) wx.Window() with name:

            - at top: "header"
            - at middle: "content"
            - at bottom: "actions"

        Keys can only be captured by the content panel.

        """
        super(sppasDialog, self).__init__(*args, **kw)
        self._init_infos()
        if "name" not in kw:
            self.SetName('dlg_{:d}'.format(self.GetId()))

        self.SetAutoLayout(True)

        # To fade-in and fade-out the opacity
        self.opacity_in = 0
        self.opacity_out = 255
        self.delta = None
        self.timer1 = None
        self.timer2 = None

    # -----------------------------------------------------------------------

    def _init_infos(self):
        """Initialize the main frame.

        Set the title, the icon and the properties of the frame.

        """
        # colors & font
        try:
            settings = wx.GetApp().settings
            self.SetBackgroundColour(settings.bg_color)
            self.SetForegroundColour(settings.fg_color)
            self.SetFont(settings.text_font)
        except:
            self.InheritAttributes()

        # Fix minimum frame size
        self.SetMinSize(wx.Size(sppasDialog.fix_size(320),
                                sppasDialog.fix_size(200)))

        # icon
        try:
            _icon = wx.Icon()
            bmp = wx.GetApp().get_icon("sppas_64", height=64)
            _icon.CopyFromBitmap(bmp)
            self.SetIcon(_icon)
        except AttributeError:
            pass

    # -----------------------------------------------------------------------
    # Fade-in at start-up and Fade-out at close
    # -----------------------------------------------------------------------

    def FadeIn(self, delta=None):
        """Fade-in opacity.

        :param delta: (int)

        """
        if delta is None:
            try:
                delta = wx.GetApp().settings.fade_in_delta
            except AttributeError:
                delta = 5
        self.delta = int(delta)
        self.SetTransparent(self.opacity_in)
        self.timer1 = wx.Timer(self, -1)
        self.timer1.Start(5)  # call the cycle in every 5 milliseconds
        self.Bind(wx.EVT_TIMER, self.__alpha_cycle_in, self.timer1)

    # -----------------------------------------------------------------------

    def DestroyFadeOut(self, delta=None):
        """Destroy with a fade-out opacity.

        :param delta: (int)

        """
        if delta is None:
            try:
                delta = wx.GetApp().settings.fade_out_delta
            except AttributeError:
                delta = -5
        self.delta = int(delta)
        self.timer2 = wx.Timer(self, -1)
        self.timer2.Start(5)  # call the cycle out every 5 milliseconds
        self.Bind(wx.EVT_TIMER, self.__alpha_cycle_out, self.timer2)

    # -----------------------------------------------------------------------
    # Override existing but un-useful methods
    # -----------------------------------------------------------------------

    def CreateButtonSizer(self, flags):
        """Overridden to disable."""
        pass

    def CreateSeparatedButtonSizer(self, flags):
        """Overridden to disable."""
        pass

    def CreateSeparatedSizer(self, sizer):
        """Overridden to disable."""
        pass

    def CreateStdDialogButtonSizer(self, flags):
        """Overridden to disable."""
        pass

    def CreateTextSizer(self, message):
        """Overridden to disable."""
        pass

    # -----------------------------------------------------------------------

    @property
    def content(self):
        return self.FindWindow("content")

    @property
    def actions(self):
        return self.FindWindow("actions")

    @property
    def header(self):
        return self.FindWindow("header")

    # -----------------------------------------------------------------------
    # Methods to add/set the header, content, actions
    # -----------------------------------------------------------------------

    def CreateEmptyHeader(self):
        """Create an empty panel for an header.

        """
        # Create the header panel and sizer
        panel = sppasPanel(self, name="header")
        panel.SetMinSize(wx.Size(-1, wx.GetApp().settings.header_height))
        sizer = wx.BoxSizer(wx.HORIZONTAL)

        # This header panel properties
        panel.SetSizer(sizer)
        self.SetHeader(panel)

    # -----------------------------------------------------------------------

    def CreateHeader(self, title, icon_name=None):
        """Create a panel including a title with an optional icon.

        :param title: (str) The message in the header
        :param icon_name: (str) Base name of the icon.

        """
        spacing = self.get_font_height()
        min_height = wx.GetApp().settings.header_height
        # Create the header panel and sizer
        panel = sppasPanel(self, name="header")
        panel.SetMinSize(wx.Size(-1, min_height))
        sizer = wx.BoxSizer(wx.HORIZONTAL)

        # Add the icon, at left, with its title
        if icon_name is not None:
            static_bmp = BitmapButton(panel, name=icon_name)
            static_bmp.SetBorderWidth(0)
            static_bmp.SetFocusWidth(0)
            static_bmp.SetMinSize(wx.Size(min_height - 2, min_height - 2))
            sizer.Add(static_bmp, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_LEFT | wx.LEFT, spacing)

        txt = sppasLabelHeader(panel, label=title)
        txt.SetMinSize(txt.DoGetBestSize())
        sizer.Add(txt, 1, wx.EXPAND)

        # This header panel properties
        panel.SetSizer(sizer)
        self.SetHeader(panel)

    # -----------------------------------------------------------------------

    def SetHeader(self, window):
        """Assign the header window to this dialog.

        :param window: (wx.Window) Any kind of wx.Window, wx.Panel, ...

        """
        window.SetName("header")
        window.SetBackgroundColour(wx.GetApp().settings.header_bg_color)
        window.SetForegroundColour(wx.GetApp().settings.header_fg_color)
        window.SetFont(wx.GetApp().settings.header_text_font)

    # -----------------------------------------------------------------------

    def SetContent(self, window):
        """Assign the content window to this dialog.

        :param window: (wx.Window) Any kind of wx.Window, wx.Panel, ...

        """
        window.SetName("content")
        window.SetBackgroundColour(wx.GetApp().settings.bg_color)
        window.SetForegroundColour(wx.GetApp().settings.fg_color)
        window.SetFont(wx.GetApp().settings.text_font)

    # -----------------------------------------------------------------------

    def CreateActions(self, left_flags, right_flags=()):
        """Create the actions panel.

        Flags is a list of the following flags:
            - wx.ID_OK,
            - wx.ID_CANCEL,
            - wx.ID_YES,
            - wx.ID_NO,
            - wx.ID_APPLY,
            - wx.ID_CLOSE,
            - wx.ID_SAVE.

        :param left_flags: (list) Buttons to put at left
        :param right_flags: (list) Buttons to put at right

        """
        # Create the action panel and sizer
        panel = sppasPanel(self, style=wx.NO_BORDER | wx.TAB_TRAVERSAL | wx.WANTS_CHARS, name="actions")
        panel.SetMinSize(wx.Size(-1, wx.GetApp().settings.action_height))

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        if len(left_flags) > 0:
            for i, flag in enumerate(left_flags):
                button = self.__create_button(panel, flag)
                sizer.Add(button, 2, wx.LEFT | wx.EXPAND, 0)
                if len(right_flags) > 0 or i+1 < len(left_flags):
                    sizer.Add(self.VertLine(panel), 0, wx.EXPAND, 0)

        if len(right_flags) > 0:
            if len(left_flags) > 0:
                sizer.AddStretchSpacer(1)

            for flag in right_flags:
                button = self.__create_button(panel, flag)
                sizer.Add(self.VertLine(panel), 0, wx.EXPAND, 0)
                sizer.Add(button, 2, wx.RIGHT | wx.EXPAND, 0)

        # This action panel properties
        panel.SetSizer(sizer)
        self.SetActions(panel)
        return panel

    # -----------------------------------------------------------------------

    def SetActions(self, window):
        """Assign the actions window to this dialog.

        :param window: (wx.Window) Any kind of wx.Window, wx.Panel, ...

        """
        window.SetName("actions")
        window.SetBackgroundColour(wx.GetApp().settings.action_bg_color)
        window.SetForegroundColour(wx.GetApp().settings.action_fg_color)
        window.SetFont(wx.GetApp().settings.action_text_font)

    # ------------------------------------------------------------------------

    def VertLine(self, parent, depth=1):
        """Return a vertical static line."""
        line = sppasStaticLine(parent, orient=wx.LI_VERTICAL)
        line.SetMinSize(wx.Size(depth, -1))
        line.SetSize(wx.Size(depth, -1))
        line.SetPenStyle(wx.PENSTYLE_SOLID)
        line.SetDepth(depth)
        return line

    # ------------------------------------------------------------------------

    def HorizLine(self, parent, depth=1):
        """Return an horizontal static line."""
        nid = wx.NewId()
        line = sppasStaticLine(parent, nid, orient=wx.LI_HORIZONTAL,
                               name="line"+str(nid))
        line.SetMinSize(wx.Size(-1, depth))
        line.SetSize(wx.Size(-1, depth))
        line.SetPenStyle(wx.PENSTYLE_SOLID)
        line.SetDepth(depth)
        return line

    # ---------------------------------------------------------------------------
    # Put the whole content of the dialog in a sizer
    # ---------------------------------------------------------------------------

    def LayoutComponents(self):
        """Create the sizer and layout the components of the dialog."""
        sizer = wx.BoxSizer(wx.VERTICAL)

        # Add header
        header = self.FindWindow("header")
        if header is not None:
            sizer.Add(header, 0, wx.EXPAND, 0)

        # Add content
        content = self.FindWindow("content")
        if content is not None:
            sizer.Add(content, 1, wx.EXPAND, 0)
        else:
            sizer.AddSpacer(1)

        # Add action buttons
        actions = self.FindWindow("actions")
        if actions is not None:
            # proportion is 0 to tell the sizer to never hide the buttons
            sizer.Add(actions, 0, wx.EXPAND, 0)

        # Since Layout doesn't happen until there is a size event, you will
        # sometimes have to force the issue by calling Layout yourself. For
        # example, if a frame is given its size when it is created, and then
        # you add child windows to it, and then a sizer, and finally Show it,
        # then it may not receive another size event (depending on platform)
        # in order to do the initial layout. Simply calling self.Layout from
        # the end of the frame's __init__ method will usually resolve this.
        self.SetSizer(sizer)
        self.Layout()

    # -----------------------------------------------------------------------

    def UpdateUI(self):
        """Assign settings to self and children, then refresh."""
        # colors & font
        try:
            settings = wx.GetApp().settings
            self.SetBackgroundColour(settings.bg_color)
            self.SetForegroundColour(settings.fg_color)
            self.SetFont(settings.text_font)

            for w in self.GetChildren():
                name = w.GetName()
                if name == "header":
                    w.SetBackgroundColour(settings.header_bg_color)
                    w.SetForegroundColour(settings.header_fg_color)
                    w.SetFont(settings.header_text_font)
                elif name == "actions":
                    w.SetBackgroundColour(settings.action_bg_color)
                    w.SetForegroundColour(settings.action_fg_color)
                    w.SetFont(settings.action_text_font)
                else:
                    w.SetBackgroundColour(settings.bg_color)
                    w.SetForegroundColour(settings.fg_color)
                    w.SetFont(settings.text_font)
                w.Layout()
                w.Refresh()
        except:
            pass

        # other incoming settings...

        self.Layout()
        self.Refresh()

    # ---------------------------------------------------------------------------
    # Private

    def __create_button(self, parent, flag):
        """Create a button from a flag and return it.

        :param parent: (wx.Window)
        :param flag: (int)

        """
        btns = {
            wx.ID_OK: (MSG_ACTION_OK, "ok"),
            wx.ID_CANCEL: (MSG_ACTION_CANCEL, "cancel"),
            wx.ID_YES: (MSG_ACTION_YES, "yes"),
            wx.ID_NO: (MSG_ACTION_NO, "no"),
            wx.ID_APPLY: (MSG_ACTION_APPLY, "apply"),
            wx.ID_CLOSE: (MSG_ACTION_CLOSE, "close"),
            wx.ID_SAVE: (MSG_ACTION_SAVE, "save"),
        }
        btn = BitmapTextButton(parent, label=btns[flag][0], name=btns[flag][1])
        btn.SetLabelPosition(wx.RIGHT)
        btn.SetBorderWidth(0)
        btn.SetFocusWidth(1)
        # btn.SetFocusColour(self.GetForegroundColour())
        btn.SetId(flag)

        if flag == wx.CANCEL:
            self.SetAffirmativeId(wx.ID_CANCEL)

        elif flag in (wx.CLOSE, wx.OK):
            btn.SetFocus()
            self.SetAffirmativeId(flag)

        elif flag == wx.YES:
            self.SetAffirmativeId(wx.ID_YES)
        btn.Bind(sb.EVT_WINDOW_SELECTED, self._on_btn_selected)
        btn.Bind(sb.EVT_WINDOW_FOCUSED, self._on_btn_focused)

        return btn

    # -----------------------------------------------------------------------

    def _on_btn_selected(self, event):
        obj = event.GetEventObject()

    # -----------------------------------------------------------------------

    def _on_btn_focused(self, event):
        win = event.GetEventObject()
        is_focused = event.GetFocused()
        if is_focused is True:
            win.SetFont(win.GetFont().MakeLarger())
        else:
            win.SetFont(win.GetFont().MakeSmaller())

    # -----------------------------------------------------------------------

    def __alpha_cycle_in(self, *args):
        """Fade-in opacity of the dialog."""
        if self.delta <= 0:
            self.delta = -self.delta

        self.opacity_in += self.delta
        if self.opacity_in <= 0:
            self.opacity_in = 0

        if self.opacity_in >= 255:
            self.opacity_in = 255
            self.timer1.Stop()

        self.SetTransparent(self.opacity_in)

    # -----------------------------------------------------------------------

    def __alpha_cycle_out(self, *args):
        """Fade-out opacity of the dialog."""
        if self.delta >= 0:
            self.delta = -self.delta
        if self.opacity_out >= 255:
            self.opacity_out = 255

        self.opacity_out += self.delta
        if self.opacity_out < 0:
            self.opacity_out = 0
        self.SetTransparent(self.opacity_out)

        if self.opacity_out == 0:
            self.timer2.Stop()
            self.DeletePendingEvents()
            wx.CallAfter(self.Destroy)

    # -----------------------------------------------------------------------

    @staticmethod
    def fix_size(value):
        """Return a proportional size value.

        :param value: (int)
        :returns: (int)

        """
        try:
            obj_size = int(float(value) * wx.GetApp().settings.size_coeff)
        except AttributeError:
            obj_size = int(value)
        return obj_size

    # -----------------------------------------------------------------------

    def get_font_height(self):
        try:
            font = wx.GetApp().settings.text_font
        except AttributeError:
            font = self.GetFont()
        return int(float(font.GetPixelSize()[1]))


# ----------------------------------------------------------------------------
# Panels to test
# ----------------------------------------------------------------------------


class TestPanel(wx.Panel):

    def __init__(self, parent):
        super(TestPanel, self).__init__(
            parent,
            name="sppasDialog")

        btn1 = wx.Button(self, label="Open dialog", pos=(10, 10), size=(128, 64))
        self.Bind(wx.EVT_BUTTON, self._on_open_search, btn1)

    # -----------------------------------------------------------------------

    def _on_open_search(self, event):
        d = sppasDialog(self, name="test_dialog")
        wx.LogMessage("Dialog name (expected is test_dialog): {:s}".format(d.GetName()))
        d.Show()


import wx.lib.newevent

TimelineViewEvent, EVT_TIMELINE_VIEW = wx.lib.newevent.NewEvent()
TimelineViewCommandEvent, EVT_TIMELINE_VIEW_COMMAND = wx.lib.newevent.NewCommandEvent()

from .editor import sppasEditorPanel

__all__ = (
    "sppasEditorPanel"
)

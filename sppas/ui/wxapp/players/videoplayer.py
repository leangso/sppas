# -*- coding : UTF-8 -*-
"""
:filename: sppas.src.videodata.videoplayer.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  A video player set the current image and display it with wx.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

IMPORTANT LIMITATION:
====================
The displayed image is reduced to a max width of 512 pixels in order to be
able to play any video on - hopefully, any computer. This max value will be
an optional feature in a future version.
=> modify the calls to __process_frame(frame, max_image_width=XXX) by fixing
the appropriate XXX value.

"""

import logging
import os
import datetime
import time
import wx
import cv2
import numpy

from sppas.src.config import paths
from sppas.src.videodata.video import sppasVideoReader
from sppas.src.imgdata import sppasImage

from .baseplayer import sppasBasePlayer
from .penum import PlayerState
from .penum import PlayerType

from ..page_editor.media.mediaevents import MediaEvents
from ..windows.frame import sppasImageFrame

# ---------------------------------------------------------------------------


class sppasVideoPlayer(sppasBasePlayer):
    """A video player based on a wx panel to display the images.

    Load, play and browse throw the video stream of a given file.

    """

    def __init__(self, owner, player=None):
        """Create an instance of sppasVideoPlayer.

        :param owner: (wx.Window) Owner of this class.
        :param player: (wx.Window) Frame, panel... to play the video

        """
        super(sppasVideoPlayer, self).__init__(owner)
        self._current_image = None

        # Delay in seconds to update the position value in the stream & to notify
        self._time_delay = 0.04   # corresponds to fps=25

        self._player = None
        if player is not None:
            try:
                owner = player.GetParent()
                player.SetBackgroundImage(os.path.join(paths.etc, "images", "mire.jpg"))
                player.Show()
                if player.IsShown():
                    player.Hide()
                player.Refresh()
                self._player = player
            except AttributeError:
                wx.LogError("The given video player is not of a compatible "
                            "type. A sppasDCWindows() was expected.")

        if self._player is None:
            try:
                cv2.namedWindow("test", cv2.WINDOW_NORMAL)
                cv2.destroyAllWindows()
                self._player = "CV2"
            except:
                # namedWindow is not implemented in this version of cv2
                # Fall-back with a wx window
                # The frame in which images of the video are sent
                self._player = sppasImageFrame(
                    parent=owner,   # if owner is destroyed, the frame will be too
                    title="Video",
                    style=wx.CAPTION | wx.RESIZE_BORDER | wx.MAXIMIZE_BOX | wx.MINIMIZE_BOX | wx.DIALOG_NO_PARENT)
                self._player.SetBackgroundColour(wx.WHITE)
                self._player.Show(True)

    # -----------------------------------------------------------------------

    def close_player(self):
        if isinstance(self._player, wx.Window):
            self._player.Close()
        else:
            cv2.destroyAllWindows()
        self._player = None

    # -----------------------------------------------------------------------

    def _load(self, filename):
        """Open the file that filename refers to and load a buffer of frames.

        :param filename: (str) Name of a video file
        :return: (bool) True if successfully opened and loaded.

        """
        self.reset()
        self._filename = filename
        self._ms = PlayerState().loading
        loaded = False

        try:
            self._media = sppasVideoReader()
            self._media.open(filename)
            self._ms = PlayerState().stopped
            self._mt = PlayerType().video
            loaded = True
        except Exception as e:
            logging.error("OpenCV did not opened file {:s}: {:s}".format(filename, str(e)))
            self._media = sppasVideoReader()
            self._ms = PlayerState().unknown
            self._mt = PlayerType().unknown

        # With the following, the app will crash if load() is launch into
        # a thread - tested under MacOS only:
        if loaded is True:
            if isinstance(self._player, wx.Window):
                self._player.Show()
            else:
                # see https://docs.opencv.org/4.5.0/d7/dfc/group__highgui.html
                cv2.namedWindow(self._filename, cv2.WINDOW_NORMAL)

            self._time_delay = 1. / self.get_framerate()
            logging.info("Video {:s} image duration: {:.3f}".format(filename, self._time_delay))

            if wx.Platform != "__WXMAC__" and isinstance(self._player, wx.Window):
                # CRASH only under MacOS with the error:
                # *** Terminating app due to uncaught exception 'NSInternalInconsistencyException',
                # reason: 'NSWindow drag regions should only be invalidated on the Main Thread!'
                # terminating with uncaught exception of type NSException

                # we fix an height of 512 and set a proportional width
                ratio = float(self._media.get_height()) / 512.
                width = float(self._media.get_width()) / ratio
                width = int(width)
                self._player.SetSize(wx.Size(width, 512))
                logging.debug("Player frame size is set to: {}, {}".format(width, 512))

            # display a blank image in the player
            self._current_image = sppasImage(0).blank_image(w=self._media.get_width(), h=self._media.get_height())
            self.refresh_player_frame()

        return loaded

    # -----------------------------------------------------------------------

    def pause(self):
        """Pause to play the video.

        :return: (bool) True if the action of pausing was performed

        """
        if self._ms == PlayerState().playing:
            # stop playing
            self._ms = PlayerState().paused
            # stop the thread to prevent error:
            # "Assertion fctx->async_lock failed at libavcodec/pthread_frame.c:155"
            self._th.join()
            # get the exact moment we stopped to play
            self._from_time = self.tell()
            self._start_datenow = None
            # ensure the current frame is displayed
            self.refresh_player_frame()
            return True

        return False

    # -----------------------------------------------------------------------

    def _stop(self):
        """Stop playing of the video.

        :return: (bool) True if the action of stopping was performed

        """
        self._current_image = sppasImage(0).blank_image(w=self._media.get_width(), h=self._media.get_height())
        self.refresh_player_frame()
        return True

    # -----------------------------------------------------------------------

    def _play_process(self):
        """Run the process of playing.

        It is expected that reading a frame, converting it to an image and
        displaying it in the video frame is faster than the duration of a
        frame (1. / fps). If not, some frames can be skipped.

        """
        # The timer is already started. It's urgent to have an image...
        if self._current_image is None:
            self._current_image = sppasImage(0).blank_image(w=self._media.get_width(), h=self._media.get_height())

        self._start_datenow = datetime.datetime.now()
        # logging.debug("  -> video player start time: {}".format(self._start_datenow))
        # setting mouse handler for the image and calling the cv2_ignore_click_event() function
        if isinstance(self._player, wx.Window) is False:
            cv2.setMouseCallback(self._filename, self.cv2_ignore_click_event)

        # time_delay = round(1. / self._media.get_framerate(), 3)
        fps = self._media.get_framerate()
        min_sleep = 1. / (fps * 4.)

        # the position to start and to stop playing
        start_offset = self._media.tell()
        end_offset = int(self._period[1] * float(self._media.get_framerate()))
        logging.debug("  -> video player {:s} - offsets range: {}, {}"
                      "".format(self._filename, start_offset, end_offset))

        # the time when we started to play and the number of frames we displayed.
        frm = 0

        while self._media.is_opened() and self._ms == PlayerState().playing:
            # read the next frame from the file
            frame = self._media.read_frame(process_image=False)
            if frame is None:
                # we reached the end of the file
                self.stop()
                break
            # an image was grabbed and retrieved successfully
            self._current_image = self.__process_frame(frame)
            # does this image is still in the defined period?
            frm += 1
            cur_offset = start_offset + frm
            if cur_offset > end_offset:
                # we reached the end of the period
                self.stop()
                break

            # if the state changed during the image was read
            if self._ms != PlayerState().playing:
                break

            # reposition in stream, or sleep
            cur_time_value = datetime.datetime.now()
            # since how many time the video is playing
            time_delta = cur_time_value - self._start_datenow
            delta = time_delta.total_seconds()
            # how many frames this delta is representing
            position = round(delta * float(self._media.get_framerate()))
            # if the position is after the end of the video file...
            # if position+start_offset > self._media.get_nframes():
            #     break

            if position > frm:
                # Reading is too slow, frame position is in late. Go forward...
                self._media.seek(start_offset + position)
                nf = position - frm   # nb of frames in late
                logging.warning("video skipped {:d} frames from {:d}. New position: {:d}".format(nf, start_offset + frm, start_offset + position))
                frm = position

            else:
                expected_time = self._start_datenow + datetime.timedelta(seconds=(float(frm) / fps))
                if expected_time > cur_time_value:
                    delta = expected_time - cur_time_value
                    delta_seconds = delta.seconds + (delta.microseconds / 1000000.)
                    if delta_seconds > min_sleep:
                        # Reading is too fast. Wait a short time.
                        # logging.debug("video sleep at frame {:d} for {} seconds. expected time: {}, cur time: {}".format(start_offset + frm, delta_seconds, expected_time, cur_time_value))
                        time.sleep(delta_seconds - 0.002)
                        # Remember that under Windows, the real sleeping time is
                        # a multiple of 0.015.

        # setting mouse handler for the image and calling the click_event() function
        if isinstance(self._player, wx.Window) is False:
            cv2.setMouseCallback(self._filename, self.cv2_click_event)

        # end_datenow = datetime.datetime.now()
        # logging.debug("  -> video player end time: {}".format(end_datenow))
        # logging.debug("  -> video player total read time: {}".format(end_datenow - self._start_datenow))
        self._start_datenow = None
        self._current_image = None

    # -----------------------------------------------------------------------

    def __process_frame(self, frm, max_image_width=512):
        # Reduce numpy array to unsigned 8 bits integer,
        # i.e. RGB values are ranging 0-255
        if isinstance(self._player, wx.Window) is True:
            # and convert BGR into RGB
            image = sppasImage(input_array=numpy.array(frm[:, :, [2, 1, 0]], dtype=numpy.uint8))
        else:
            image = sppasImage(input_array=numpy.array(frm, dtype=numpy.uint8))

        offset = self._media.tell()
        #image.put_text((10, image.height-10, 10, 10), wx.WHITE, 2, str(offset))
        if image.width > max_image_width:
            return image.iresize(width=max_image_width)
        return image

    # -----------------------------------------------------------------------

    def play_frame(self, direction=1):
        """Play a single frame from the current position.

        Direction indicates which "next/previous" frame to play.

        :param direction: positive value = next; negative value = previous

        """
        logging.debug(" => play frame in direction = {:d}".format(direction))
        frame_duration = 1. / float(self._media.get_framerate())
        logging.debug("    - a frame is during {:f}".format(frame_duration))
        if direction < 0:
            # display the previous frame.
            offset = self._media.tell()
            current_time_value = float(offset) / float(self._media.get_framerate())
            logging.debug("    - current time value {:f}".format(current_time_value))
            # ... the direction is a negative value.
            expect_seek_at = current_time_value + (float(direction) * frame_duration)
            expect_seek_at = expect_seek_at - frame_duration
            if expect_seek_at > 0.:
                self.seek(expect_seek_at)
                logging.debug("    - seek at time value {:f}".format(expect_seek_at))
            else:
                return False
        elif direction > 1:
            # display a frame after the next one.
            offset = self._media.tell()
            current_time_value = float(offset) / float(self._media.get_framerate())
            logging.debug("    current time value {:f}".format(current_time_value))
            expect_seek_at = current_time_value + (float(direction - 1) * frame_duration)
            if expect_seek_at < self._media.get_duration():
                self.seek(expect_seek_at)
                logging.debug("    seek at time value {:f}".format(expect_seek_at))
            else:
                return False

        # read the next frame from the file
        frame = self._media.read_frame(process_image=False)
        if frame is None:
            # we reached the end of the file
            self.stop()
            return False

        # an image was grabbed and retrieved successfully
        self._current_image = self.__process_frame(frame)
        if self._current_image is None:
            # we reach the end of the video
            self.stop()
            return False

        self.refresh_player_frame()
        return True

    # -----------------------------------------------------------------------

    def refresh_player_frame(self):
        """Refresh the video frame."""
        if self._current_image is not None:
            if isinstance(self._player, wx.Window):
                self._player.SetBackgroundImageArray(self._current_image)
                self._player.Refresh()
            else:
                cv2.imshow(self._filename, self._current_image)

    # -----------------------------------------------------------------------

    def cv2_ignore_click_event(self, event, x, y, flags, params):
        pass

    # -----------------------------------------------------------------------

    def cv2_click_event(self, event, x, y, flags, params):

        # checking for left mouse clicks
        if event == cv2.EVENT_LBUTTONDOWN:
            # displaying the coordinates
            logging.debug("Left down: ({:d}, {:d})".format(x, y))

        # checking for right mouse clicks
        if event == cv2.EVENT_LBUTTONUP:
            # displaying the coordinates
            logging.debug("  -> then left up: ({:d}, {:d})".format(x, y))

            # displaying the coordinates on the image window
            # font = cv2.FONT_HERSHEY_SIMPLEX
            # b = img[y, x, 0]
            # g = img[y, x, 1]
            # r = img[y, x, 2]
            # cv2.putText(img, str(b) + ',' +
            #             str(g) + ',' + str(r),
            #             (x, y), font, 1,
            #             (255, 255, 0), 2)
            # cv2.imshow('image', img)

    # -----------------------------------------------------------------------

    def _seek(self, time_pos=0.):
        """Seek the video stream at the given position in time.

        :param time_pos: (float) Time in seconds

        """
        was_playing = self.is_playing()
        if was_playing is True:
            self.pause()

        # how many frames this time position is representing since the beginning
        self._from_time = float(time_pos)
        position = self._from_time * self._media.get_framerate()
        if self._period is not None:
            if self._from_time > self._period[1]:
                self.stop()

        # seek at the expected position
        try:
            self._media.seek(int(position))
            # continue playing if the seek was requested when playing
            if was_playing is True:
                self.play()
        except:
            # It can happen if we attempted to seek after the audio length
            self.stop()
            return False

        return True

    # -----------------------------------------------------------------------

    def media_tell(self):
        return self._media.tell()

    # -----------------------------------------------------------------------

    def tell(self):
        """Return the current time position in the video stream (float).

        :return: (float) The time at the middle of the current frame

        """
        # Time value at the end of the last read frame
        offset = self._media.tell()
        if offset == 0:
            return 0.
        time_value = float(offset) / float(self._media.get_framerate())
        # Duration of a frame
        frame_duration = 1. / self._media.get_framerate()
        # Time at the middle of the last read frame
        return time_value - (frame_duration / 2.)

    # -----------------------------------------------------------------------
    # About the video
    # -----------------------------------------------------------------------

    def get_duration(self):
        """Return the duration of the loaded video (float)."""
        if self._media is None:
            return 0.
        return self._media.get_duration()

    # -----------------------------------------------------------------------

    def get_framerate(self):
        if self._media is not None:
            return self._media.get_framerate()
        return 0

    # -----------------------------------------------------------------------

    def get_width(self):
        """Return the width of the frames in the video."""
        if self._media is None:
            return 0
        return self._media.get_width()

    # -----------------------------------------------------------------------

    def get_height(self):
        """Return the height of the frames in the video."""
        if self._media is None:
            return 0
        return self._media.get_height()

    # -----------------------------------------------------------------------
    # Manage events
    # -----------------------------------------------------------------------

    def Notify(self):
        """Override. Notify the owner of the EVT_TIMER event.

        Manage the current position in the audio stream.

        """
        if self._player is None:
            self.stop()
            return

        # Nothing to do if we are not playing
        if self._ms == PlayerState().playing:
            if self._current_image is None:
                # we reach the end of the video
                self.stop()
            else:
                self.refresh_player_frame()

            # Send the wx.EVT_TIMER event
            wx.Timer.Notify(self)

        elif self._ms != PlayerState().paused:
            self.stop()

# ---------------------------------------------------------------------------


class TestPanel(wx.Panel):
    def __init__(self, parent):
        super(TestPanel, self).__init__(
            parent, -1, style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN, name="VideoPlayer")

        # The player!
        #TOO SLOW: can't display 60 fps ->
        #self.player = sppasImageFrame(self)
        #self.ap = sppasVideoPlayer(owner=self, player=self.player)
        self.ap = sppasVideoPlayer(owner=self)

        # Actions to perform with the player
        btn2 = wx.Button(self, -1, "Play", name="btn_play")
        btn2.Enable(False)
        self.Bind(wx.EVT_BUTTON, self._on_play_ap, btn2)
        btn3 = wx.Button(self, -1, "Pause")
        self.Bind(wx.EVT_BUTTON, self._on_pause_ap, btn3)
        btn4 = wx.Button(self, -1, "Stop")
        self.Bind(wx.EVT_BUTTON, self._on_stop_ap, btn4)
        btn5 = wx.Button(self, -1, "Previous Frame")
        self.Bind(wx.EVT_BUTTON, self._on_prev_frame_ap, btn5)
        btn6 = wx.Button(self, -1, "Next Frame")
        self.Bind(wx.EVT_BUTTON, self._on_next_frame_ap, btn6)

        sizer = wx.BoxSizer()
        sizer.Add(btn2, 0, wx.ALL, 4)
        sizer.Add(btn3, 0, wx.ALL, 4)
        sizer.Add(btn4, 0, wx.ALL, 4)
        sizer.Add(btn5, 0, wx.ALL, 4)
        sizer.Add(btn6, 0, wx.ALL, 4)

        # a slider to display the current position
        self.slider = wx.Slider(self, -1, 0, 0, 10, style=wx.SL_HORIZONTAL | wx.SL_LABELS)
        self.slider.SetMinSize(wx.Size(250, -1))
        self.Bind(wx.EVT_SLIDER, self._on_seek_slider, self.slider)

        # Organize items
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer.Add(sizer, 1, wx.EXPAND)
        main_sizer.Add(self.slider, 1, wx.EXPAND)
        self.SetSizer(main_sizer)

        # Events
        # Custom event to inform the media is loaded
        self.ap.Bind(MediaEvents.EVT_MEDIA_LOADED, self.__on_media_loaded)
        self.ap.Bind(MediaEvents.EVT_MEDIA_NOT_LOADED, self.__on_media_not_loaded)
        # Event received every 10ms (in theory) when the audio is playing
        self.Bind(wx.EVT_TIMER, self._on_timer)

        wx.CallAfter(self._do_load_file)

    # ----------------------------------------------------------------------

    def _do_load_file(self):
        self.ap.load(os.path.join(paths.samples, "faces", "video_sample.mp4"))
        #self.ap.load("/E/data/corpus/FRA/Multimodal/CLeLfPC/01_syll/syll_2_MZ_dd520f/syll_2_MZ_dd520f_front.mkv")

    # ----------------------------------------------------------------------

    def __on_media_loaded(self, event):
        wx.LogDebug("Video file loaded successfully")
        self.FindWindow("btn_play").Enable(True)
        duration = self.ap.get_duration()
        self.slider.SetRange(0, int(duration * 1000.))

        # self.ap.set_period(10., 12.)
        # self.ap.seek(10.)
        # self.slider.SetRange(10000, 12000)

    # ----------------------------------------------------------------------

    def __on_media_not_loaded(self, event):
        wx.LogError("Video file not loaded")
        self.FindWindow("btn_play").Enable(False)
        self.slider.SetRange(0, 0)

    # ----------------------------------------------------------------------

    def _on_play_ap(self, event):
        self.ap.play()

    # ----------------------------------------------------------------------

    def _on_pause_ap(self, event):
        self.ap.pause()

    # ----------------------------------------------------------------------

    def _on_stop_ap(self, event):
        self.ap.stop()
        self.slider.SetValue(0)

    # ----------------------------------------------------------------------

    def _on_next_frame_ap(self, event):
        self.ap.play_frame()
        time_pos = self.ap.tell()
        self.slider.SetValue(int(time_pos * 1000.))

    # ----------------------------------------------------------------------

    def _on_prev_frame_ap(self, event):
        self.ap.play_frame(direction=-1)
        time_pos = self.ap.tell()
        self.slider.SetValue(int(time_pos * 1000.))

    # ----------------------------------------------------------------------

    def _on_timer(self, event):
        time_pos = self.ap.tell()
        self.slider.SetValue(int(time_pos * 1000.))
        event.Skip()

    # ----------------------------------------------------------------------

    def _on_seek_slider(self, event):
        time_pos_ms = self.slider.GetValue()
        self.ap.seek(float(time_pos_ms) / 1000.)
        self.ap.play_frame(direction=1)

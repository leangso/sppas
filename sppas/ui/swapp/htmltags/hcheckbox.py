# -*- coding: UTF-8 -*-
"""
:filename: sppas.ui.swapp.htmltags.hcheckbox.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: A checkbox node for a custom input of type checkbox.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

Requirements:

- javascript function checkbox_switch(checkbox);
- css property "focusable"

"""

from sppas.ui.htmlmaker import EmptyNode
from sppas.ui.htmlmaker import HTMLNode

# ---------------------------------------------------------------------------


class sppasHTMLCheckboxNode(HTMLNode):
    """A customized checkbox.

    Requires the javascript function checkbox_switch() and the css "focusable".
    The function is invoked each time the input is checked. The function then
    POST a form with the input to the server.

    :Example:

    Python code:
        > checkbox = HTMLCheckboxNode(self.identifier, "check_01", "I agree")
        > checkbox.check(True)
        > self.append_child(checkbox)

    HTML serialized result:
        <label for="check_01_input" class="focusable">
            <input id="check_01_input" type="checkbox" name="check_01_input" value="I agree"
            onkeydown="if (event.key == 'Enter') {event.target.checked = !event.target.checked; checkbox_switch(this);};"
            onchange="checkbox_switch(this);" checked />
            <span>I agree</span>
        </label>

    Posted event when clicked:
        key="check_01_input_posted", value="I+agree"

    """

    def __init__(self, parent_id, identifier="und", text=""):
        """Create the Checkbox node.

        :param parent_id: (str) Identifier of the parent node
        :param identifier: (str) Identifier of the checkbox
        :param text: (str) Text of the checkbox. It's also the value of the input.

        """
        super(sppasHTMLCheckboxNode, self).__init__(parent_id, identifier, "label")
        checkable_id = self.identifier + "_input"
        self.add_attribute("for", checkable_id)
        self.add_attribute("class", "focusable")

        # Create the input checkbox
        input_attributes = dict()
        input_attributes['id'] = checkable_id
        input_attributes['type'] = "checkbox"
        input_attributes['name'] = checkable_id
        input_attributes['value'] = text  # this is what is really received after a POST event */
        input_attributes["onkeydown"] = "if (event.key == 'Enter') {event.target.checked = !event.target.checked; checkbox_switch(this);};"
        input_attributes["onchange"] = "checkbox_switch(this);"
        checkable = EmptyNode(self.identifier, checkable_id, "input", attributes=input_attributes)
        self.append_child(checkable)

        # Create the text value of the label
        span_node = HTMLNode(self.identifier, None, "span", value=text)
        self.append_child(span_node)

    # -----------------------------------------------------------------------

    @property
    def _checkable(self):
        """Return the node of the input checkbox."""
        return self.get_child(self.identifier + "_input")

    # -----------------------------------------------------------------------

    def check(self, value=True):
        """Check the box or un-check it.

        Add the checked information both in the label and in the checkbox.

        :param value: (bool) True to turn the input into checked state

        """
        c = self._checkable
        # Remove the existing state (if any)
        if c.is_attribute("checked") is True:
            c.remove_attribute("checked")

        # Set the given state (if any)
        if value is True:
            c.set_attribute("checked", None)

    # -----------------------------------------------------------------------

    def is_checked(self):
        """Return True if the box is currently checked."""
        return self._checkable.is_attribute("checked")


# -*- coding: UTF-8 -*-
"""
:filename: sppas.ui.swpapp.htmltags.hbutton.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: A button node with easy access to icons.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

from sppas.ui.htmlmaker import HTMLButtonNode
from sppas.ui.swapp.wapputils import sppasImagesAccess

# ---------------------------------------------------------------------------


class sppasHTMLButton(HTMLButtonNode):
    """Represent a button element.

    Overridden for an easier icon access and CSS properties added:
    "sp-button-text" and "sp-button-icon".

    """

    def __init__(self, parent, identifier, attributes=dict()):
        """Create an input node. Default type is 'text'.

        """
        super(sppasHTMLButton, self).__init__(parent, identifier, attributes=attributes)

    # -----------------------------------------------------------------------

    def set_text(self, ident, text, attributes=dict()):
        """Override. Set a text to the button.

        Class sp-button-text is added.

        :param ident: (str) Identifier for the span text.
        :param text: (str) Button text.

        """
        node = HTMLButtonNode.set_text(self, ident, text, attributes)
        node.add_attribute("class", "sp-button-text")
        return node

    # -----------------------------------------------------------------------

    def set_icon(self, icon_name, attributes=dict()):
        """Override. Set an icon to the button from its name in the app.

        Class sp-button-icon is added.

        :param icon_name: (str) Name of an icon in the app.
        :param attributes: (dict).

        """
        icon = sppasImagesAccess.get_image_filename(name=icon_name)
        node = HTMLButtonNode.set_icon(self, icon, attributes)
        node.add_attribute("class", "sp-button-icon")
        return node

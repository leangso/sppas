# -*- coding: UTF-8 -*-
"""
:filename: sppas.ui.swapp.main_settings.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: The main settings of the SPPAS Web-based Applications.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import os
import json
import logging

# ---------------------------------------------------------------------------


class sppasWebAppSettings(object):
    """Manage the web-based application global settings.

    """

    def __init__(self):
        """Create or load the dictionary of settings for the application."""
        # Ensure all members will be set
        self.reset()
        # Load the existing stored members.
        self.load()

    # -----------------------------------------------------------------------

    def __enter__(self):
        return self

    # -----------------------------------------------------------------------

    def __exit__(self, exc_type, exc_value, traceback):
        self.save()

    # -----------------------------------------------------------------------
    # Setters
    # -----------------------------------------------------------------------

    def set(self, key, value):
        """Set a new value to a key.

        :param key: (str) A key of this class dictionary
        :param value: (any)

        """
        setattr(self, key, value)

    # -----------------------------------------------------------------------

    def reset(self):
        """Fill in the dictionary with the default values."""
        self.__dict__ = dict(
            # The current theme of icons -- it must be a folder of statics/icons
            icons_theme="Refine",

            # The current color scheme: one of 'light' or 'dark'
            colors_scheme="light",

            # The lastly used httpd port
            hport=80,
        )

        # Fix the name of the file with the stored settings [can't be changed]
        wapp_dir = os.path.dirname(os.path.abspath(__file__))
        self.__filename = os.path.join(wapp_dir, ".webapp")

        # Fix the relative path to statics
        # (an absolute path in the HTML files does not work)
        # --- Hum... we'll cook a little bit here ---
        logging.debug(" ============= START DIRECTORY: {} ========== ".format(os.getcwd()))
        # the start directory must be the SPPAS package, or sppas folder.
        launch_dir_list = os.getcwd().split(os.sep)
        launch_dir = "/".join(launch_dir_list)
        if launch_dir.endswith("sppas/ui/swapp") is True:
            # launching the UI from here.
            rel = ""
        elif launch_dir.endswith("sppas/ui") is True:
            rel = "swapp/"
        elif launch_dir.endswith("sppas") is True:
            rel = "ui/swapp/"
        else:
            # launching the UI from the SPPAS package.
            rel = "sppas/ui/swapp/"
        # --- end cooking ---

        # Absolute paths to statics [can't be changed]
        self.__base_dir = os.path.dirname(os.path.abspath(__file__))
        self.__statics = rel + "statics/"
        self.__css = rel + "statics/css/"
        self.__fonts = rel + "statics/fonts/"
        self.__icons = rel + "statics/icons/"
        self.__images = rel + "statics/images/"
        self.__js = rel + "statics/js/"

    # -----------------------------------------------------------------------
    # Getters
    # -----------------------------------------------------------------------

    @staticmethod
    def default_icons_theme():
        """The full theme: the one with all icons used to fall back."""
        return "Refine"

    # -----------------------------------------------------------------------

    @staticmethod
    def default_colors_scheme():
        """The full theme: the one with all icons used to fall back."""
        return "light"

    # -----------------------------------------------------------------------
    # Get access directly to the defined relative paths.

    @property
    def base_dir(self):
        return self.__base_dir

    @property
    def statics(self):
        return self.__statics

    @property
    def css(self):
        return self.__css

    @property
    def fonts(self):
        return self.__fonts

    @property
    def icons(self):
        return self.__icons

    @property
    def images(self):
        return self.__images

    @property
    def js(self):
        return self.__js

    # -----------------------------------------------------------------------
    # Manage the file with settings: load from, save to.
    # -----------------------------------------------------------------------

    def load(self):
        """Load the dictionary of settings from a dump file.

        """
        if os.path.exists(self.__filename):
            try:
                with open(self.__filename, "r") as fd:
                    d = json.load(fd)
                    self.__parse(d)
                    logging.info("Web-based application settings loaded from {:s}"
                                 "".format(self.__filename))
            except Exception as e:
                logging.error("Web-based application settings not loaded: {:s}. "
                              "Reset to default.".format(str(e)))
                self.reset()

        else:
            logging.info("No web-based application settings file defined. "
                         "Set settings to default.")
            self.reset()

    # -----------------------------------------------------------------------

    def save(self):
        """Save the dictionary of settings in a file.

        """
        try:
            with open(self.__filename, 'w') as fd:
                json.dump(self.serialize(), fd, indent=4, separators=(',', ': '))
            logging.info("Settings saved successfully in file {:s}".format(self.__filename))
        except Exception as e:
            logging.error("The settings were not saved due to the following"
                          "error: {:s}.".format(str(e)))

    # -----------------------------------------------------------------------

    def serialize(self):
        """Convert this setting dictionary into a serializable data structure.

        :returns: (dict) a dictionary that can be serialized (without classes).

        """
        d = dict()
        for k in self.__dict__:
            # save only members of the dict in reset,
            # but do not save added members.
            if k.startswith("_" + self.__class__.__name__) is False:
                v = self.__dict__[k]
                d[k] = v

        return d

    # -----------------------------------------------------------------------

    def __parse(self, d):
        """Fill in the internal dictionary with data in the given dict.

        :param d: (dict) a dictionary that can be serialized (without classes).

        """
        self.reset()
        for k in d:
            v = d[k]
            setattr(self, k, v)

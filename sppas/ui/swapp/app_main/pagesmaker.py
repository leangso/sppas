"""
:filename: sppas.ui.swapp.app_setup.setupmaker.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: The web-based application of SPPAS.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

This application allows to use SPPAS with a web-based font-end:
annotations, analysis, file conversion, plugins.

"""

import logging

from sppas.src.config import sg
from sppas.ui.httpd.hstatus import sppasHTTPDStatus
from sppas.ui.htmlmaker import HTMLNode
from sppas.ui.swapp.respmaker.response import sppasWebResponseRecipe

# -----------------------------------------------------------------------


class MainResponseRecipe(sppasWebResponseRecipe):
    """The sppas.html HTTPD response baker.

    """

    def __init__(self):
        super(MainResponseRecipe, self).__init__(name=sg.__name__)

        # Fix the "id" for this page
        self._htree.add_html_attribute("id", "main")

        # Create the dynamic HTTPD response content
        self._status = sppasHTTPDStatus()
        self._bake()

    # -----------------------------------------------------------------------

    @staticmethod
    def page() -> str:
        """Override. Return the HTML page name."""
        return "sppas.html"

    # -----------------------------------------------------------------------

    def create(self) -> None:
        """Override. Create the fixed page content in HTML.

        The fixed content corresponds to the parts that can't be invalidated:
        head, body_header, body_footer.

        """
        # Define this page title
        self._htree.set_title(self._name)
        # Add custom page style

    # -----------------------------------------------------------------------

    def _process_events(self, events) -> bool:
        """Process the given events coming from the POST of any form.

        :param events (dict): key=event_name, value=event_value
        :return: (bool) True if the whole page must be re-created.

        """
        logging.debug(" >>>>> Page Setup -- Process events: {} <<<<<< ".format(events))
        self._status.code = 200
        return True

    # -----------------------------------------------------------------------

    def _bake(self):
        """Create the dynamic page content in HTML."""
        # Define this page content: a form with an header, fieldsets and actions.
        self.comment("Body content")

        tmp = HTMLNode(self._htree.body_main.identifier, None, "h1",
                        value="HTTP response {:d}".format(self._status.code))
        self._htree.body_main.append_child(tmp)

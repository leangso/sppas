
'use strict';


function colors_scheme_switch() {
    let body = document.querySelector("body");
    let theme = "dark";
    if (body.getAttribute("class") === "dark") {
        theme = "light";
    }

    /* Take the new color scheme into account. */
    body.setAttribute("class", theme);

    const form = document.createElement("form");
    form.method = "POST";
    form.style.display = "none";

    const el = document.createElement("input");
    el.name = "colors_scheme";
    el.id = "colors_scheme";
    el.value = theme
    el.type = "hidden"
    form.appendChild(el);

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function checkbox_switch(checkable) {
    const form = document.createElement("form");
    form.method = "POST";
    form.style.display = "none";

    /* force to send another input than the one that was checked.
    this is due to the fact that the checkbox is not sent into a
    form if it is not checked (it is sent when checked).
     */
    const el = document.createElement("input");
    el.name = checkable.name + "_posted";
    el.id = checkable.name + "_posted";
    el.value = checkable.value
    el.type = "hidden"
    form.appendChild(el);

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function notify_action(action_btn) {
    const form = document.createElement("form");
    form.method = "POST";
    form.style.display = "none";

    const el = document.createElement("input");
    el.name = action_btn.name + "_action";
    el.id = action_btn.name + "_action";
    el.value = action_btn.getAttribute("value")
    el.type = "hidden"
    form.appendChild(el);

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}


function notify(form_ident) {
    var form = document.getElementById(form_ident);
    form.submit();
}

function openForm(form_ident) {
    document.getElementById(form_ident).style.display = "block";
}

function closeForm(form_ident) {
    document.getElementById(form_ident).style.display = "none";
}

/* Links with accessibility */

function goToLink(event, url) {
  var type = event.type;

  if (type === 'click' || (type === 'keydown' && event.keyCode === 13)) {
    window.location.href = url;

    event.preventDefault();
    event.stopPropagation();
  }
}


function tabToLink(event, url) {
  var type = event.type;

  if (type === 'click' || (type === 'keydown' && event.keyCode === 13)) {
    window.open(url);

    event.preventDefault();
    event.stopPropagation();
  }
}

/*
:filename: sppas.ui.swapp.statics.main.css
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Style to be used for any SPPAS Web-based UI application.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------
*/

/* =======================================================================
                              FONTS DEFINITIONS
  See here: https://v-fonts.com/licenses/open-source for a list of open
  source variable fonts.
  No cursive font ia available.

  ======================================================================== */

@font-face {
    font-family: SansFont;
    src: url('../fonts/Commissioner.ttf');
}

@font-face {
    font-family: SerifFont;
  src: url('../fonts/SourceSerifPro.ttf');
}

@font-face {
  font-family: MonoFont;
  src: url('../fonts/FiraCode.ttf');
}

@font-face {
  font-family: PreFont;
  src: url('../fonts/ScienceGothic.ttf');
}

/* =======================================================================
                            VARIABLE DEFINITIONS
  ======================================================================== */

:root {
    --transition: 0.4s ease-in-out;
    --splash-height: 200px;

    /* Fonts */
    --font-family-sans: SansFont, 'Verdana', sans-serif;
    --font-family-serif: SerifFont, 'Times New Roman', serif;
    --font-family-mono: MonoFont, 'Courier New', monospace;
    --font-family-pre: PreFont, 'Courier New', monospace;
    --typography-spacing-vertical: 1.2rem;
    --spacing: 1rem;

    /* Variables that are overridden if "sp-contrast" is added in a class */
    --line-height: 1.1;
    --font-weight-thin: 100;
    --font-weight-normal: 350; /* CSS3 requires to choose 300 or 400. 350 is not supported */
    --font-weight-bold: 650;
    --font-weight-black: 800;
    --font-size: 16px;

    --border-width: 0px;
    --border-color: transparent;

    /* Focus */
    --focus-border-color: rgba(0, 191, 255, 0.8);  /* Deep sky blue color */
    --focus-border-width: 2px;

    /* Table */
    --table-border-width: 1px;
    --table-border-color: transparent;
}

.sp-contrast {
    --border-width: 1px;
    --border-color: var(--fg-color);
    --line-height: 1.2;
    --font-weight-thin: 200;
    --font-weight-normal: 400;
    --font-weight-bold: 700;
    --font-weight-black: 900;
    --font-size: 18px;

    /* Focus */
    --focus-border-color: rgb(0, 191, 255);
    --focus-border-width: 4px;

    /* Table */
    --table-border-width: 2px;
    --table-border-color: var(--fg-color);
}

/* Font properties for a local context usage */
.sp-sans {
  font-family: var(--font-family-sans);
}

.sp-serif {
  font-family: var(--font-family-serif);
}

.sp-mono {
  font-family: var(--font-family-mono);
}

.sp-pre {
  font-family: var(--font-family-pre);
}

.sp-weight-thin {
    font-weight: var(--font-weight-thin);
}

.sp-weight-normal {
    font-weight: var(--font-weight-normal);
}

.sp-weight-bold {
    font-weight: var(--font-weight-bold);
}

.sp-weight-black {
    font-weight: var(--font-weight-black);
}

/* =======================================================================
                            PAGES STYLING
  ======================================================================== */

* {
    outline: none;
    margin: 0;
    padding: 0;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
}

*, *:before, *:after {
    box-sizing: inherit;
}

*, ::after, ::before {
    box-sizing: border-box;
    background-repeat: no-repeat
}

::after, ::before {
    text-decoration: inherit;
    vertical-align: inherit
}

html {
    width: 100%;
    height: 100%;
    scroll-behavior: smooth;
    overflow-x: hidden;
}

body {
    position: relative;
    cursor: default;

    background-clip: content-box;
    transition: var(--border-color) 0.3s;
    transition-duration: 1.8s;

    -webkit-tap-highlight-color: transparent;
    -webkit-text-size-adjust: 100%;
    -moz-text-size-adjust: 100%;

    background-color: var(--bg-color);
    color: var(--fg-color);

    font-family: var(--font-family-sans);
    font-weight: var(--font-weight-normal);
    font-size: var(--font-size);
    font-style: normal;
    font-variant: normal;
    line-height: var(--line-height);
    text-rendering: optimizeLegibility;
    overflow-wrap: break-word;

    -moz-tab-size: 4;
    tab-size: 4
}

hr {
    box-sizing: content-box;
    height: 0;
    overflow: visible;
    border: 0;
    border-top: 1px solid rgba(128, 128, 128, 0.5);
    margin: 1rem 0
}

/* =======================================================================
                       ACCESSIBILITY SPECIFIC STYLING
  ======================================================================== */

/* This is an attempt to list of all focusable tags on which no border is
designed by default (button is not in this list for example).
Could be better by taking a look at: https://allyjs.io/data-tables/focusable.html
*/
a, area, input, select, textarea, iframe {
    border: var(--focus-border-width) solid transparent;
    border-radius: calc(2 * var(--focus-border-width));
}

*:focus {
    border: var(--focus-border-width) solid var(--focus-border-color);
    border-radius: calc(2 * var(--focus-border-width));
}

/* And this is a property to be assigned to a tag in case we want to force
 it to have focus style.
 For example: <label class="focusable"> <input [...] /> </label>
*/

.focusable {
    border: var(--focus-border-width) solid transparent;
    border-radius: calc(2 * var(--focus-border-width));
}

.focusable:focus, .focused {
    border: var(--focus-border-width) solid var(--focus-border-color);
    border-radius: calc(2 * var(--focus-border-width));
}

/* =======================================================================
                       HTML ELEMENTS STYLING
  ======================================================================== */

*:disabled {
  opacity: 0.4;
  cursor: not-allowed;
}

section {
    margin-left: 1.5rem;
    margin-right: 1rem;
    margin-bottom: calc(2 * var(--typography-spacing-vertical));
}

/* ---------------------------- Head titles ------------------------------ */

h1, h2, h3, h4, h5, h6 {
    margin-bottom: var(--typography-spacing-vertical);
    letter-spacing: 1px;  /* For those who can't see colors */
}

h1 {
    margin-top: var(--typography-spacing-vertical);
    font-size: 1.6rem;
    font-weight: var(--font-weight-bold);
    letter-spacing: 2px;
    color: var(--h1-color);
}

h2 {
    font-size: 1.5rem;
    color: var(--h2-color);
    font-weight: var(--font-weight-bold);
}

h3 {
    font-size: 1.4rem;
    color: var(--h3-color);
    font-weight: var(--font-weight-normal);
    font-style: italic;
}

h4 {
    font-size: 1.25rem;
    color: var(--h4-color);
    font-weight: var(--font-weight-normal);
    font-style: italic;
}

h5 {
    font-size: 1.125rem;
    color: var(--h4-color);
    font-weight: var(--font-weight-normal);
    font-style: italic;
}

h6 {
    font-size: 1rem;
    color: var(--h4-color);
    font-weight: var(--font-weight-normal);
    font-style: italic;
}

/* ---------------------- Text & inline text ----------------------------- */

p {
    margin-bottom: var(--typography-spacing-vertical);
    text-align: justify;
    text-justify: inter-word;
}

b {
    font-weight: var(--font-weight-bold);
}

strong {
    font-weight: var(--font-weight-black);
}

mark {
    background: #ff0;
    color: #000
}

small {
    font-size: 80%
}

sub, sup {
    font-size: 75%;
    line-height: 0;
    position: relative;
    vertical-align: baseline
}

sub {
    bottom: -0.25em
}

sup {
    top: -0.5em
}

code, kbd, pre, samp {
    font-family: var(--font-family-pre);
    font-size: 0.9rem
}

kbd {
    font-weight: bolder;
}

mark {
    color: var(--mark-color);
    text-decoration: none;
}

ins {
    color: var(--ins-color);
    text-decoration: none;
}

del {
    color: var(--del-color);
}

blockquote {
    background-color: var(--bg-color2);
    display: block;
    margin-top: var(--typography-spacing-vertical);
    margin-bottom: var(--typography-spacing-vertical);
    margin-left: 4rem;
    margin-right: 2rem;
    padding: var(--spacing);
    border-right: none;
    border-left: .25rem solid var(--blockquote-border-color);
    border-inline-start: 0.25rem solid var(--blockquote-border-color);
    border-inline-end: none
}

blockquote footer {
    margin-top: calc(var(--typography-spacing-vertical) * .5);
    color: var(--blockquote-footer-color)
}

a {
    color: var(--a-color);
    opacity: 80%;
    -webkit-transition-duration: 0.2s;
    -moz-transition-duration: 0.2s;
    -o-transition-duration: 0.2s;
    transition-duration: 0.2s
}

a:hover {
    /*color: var(--sppas-color-contrast);*/
    opacity: 100%;
}

/* --------------------------------- List(s) ----------------------------- */

ol, ul {
    line-height: calc(1.4 * var(--line-height));
    padding: 0;
    margin-bottom: var(--typography-spacing-vertical);
}

ul li {
    list-style: square;
    margin-left: 1rem;  /* hum... why this is required ??? */
}

ol li {
    list-style-type: none;
    counter-increment: item;
}

ol li:before {
    content: counter(item) ".";
    font-family: var(--font-family-mono);
}

/* -------------------------------- Table(s) ----------------------------- */

:where(table) {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    text-indent: 0;
    empty-cells: show;
    margin-bottom: var(--typography-spacing-vertical);
    border: var(--table-border-width) solid var(--table-border-color);
    caption-side: top;
}

td, th {
    padding: calc(var(--spacing) / 2) var(--spacing);
    border-bottom: var(--table-border-width) solid var(--table-border-color);
    border-left: var(--table-border-width) dashed var(--table-border-color);

    color: var(--fg-color);
    font-weight: var(--font-weight);
    font-size: var(--font-size);
    text-align: left;
}

table[role=grid] thead {
    background-color: var(--table-head-bg-color);
    font-weight: bold;
}

tfoot td, tfoot th {
    border-top: var(--table-border-width) solid var(--table-border-color);
    border-bottom: 0;
}

table[role=grid] tbody tr:nth-child(even) {
    background-color: var(--table-row-stripped-bg-color);
}

caption {
    padding-left: var(--table-border-width);
    text-align: left;
    padding-top: 0.2rem;
    padding-bottom: 0.2rem;
}

caption span {
    font-style: italic;
}

table caption {
    background-color: var(--table-border-color);
    color: var(--bg-color2);
}

/* ----------------- Button, Label and Input tags ----------------------- */

button, input {
    overflow: visible;
}

button, select {
    text-transform: none;
}

button, [type=button], [type=reset], [type=submit] {
    -webkit-appearance: button;
}

button {
    border: var(--focus-border-width) solid var(--fg-color2);
    border-radius: calc(2 * var(--focus-border-width));

    background-color: var(--bg-color2);
    color: var(--fg-color2);

    display: inline-block;
    cursor: pointer;
    transition: all 0.4s;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    font-weight: normal;
    font-style: normal;
    font-variant: normal;
    font-size: 90%;
    overflow: hidden;

    padding-left: 0.5rem;
    padding-right: 0.5rem;
    padding-top: 0.3rem;
    padding-bottom: 0.4rem;

    /* for the buttons embedded in a box */
    flex-grow: 1;      /* default is 0 */
    flex-shrink: 1;    /* default is 1 */
    flex-basis: auto;  /* default is auto */
}

button:hover:enabled {
    background-color: var(--buttons-bg-color);
    color: var(--fg-color2);
}

button:enabled img:hover {
    transform: scale(1.25);
}

.sp-contrast button {
    font-weight: bolder;
    padding-left: 0.6rem;
    padding-right: 0.6rem;
    padding-top: 0.4rem;
    padding-bottom: 0.5rem;
}

.sp-button-icon {
	margin-left: 0.3rem;
	margin-right: 0.3rem;
    max-width: 2rem;
    max-height: 2rem;
	vertical-align: middle;
}

.sp-button-text {
	font-family: inherit;
    text-align: center;
    text-decoration: none;
    vertical-align: middle;
    border-bottom: 1px dashed;
    border-color: transparent;
}

button:enabled .sp-button-text:hover {
    border-color: var(--fg-color2);
}

.button-icon-only {
    flex-grow: 0;
    background: transparent;
}

/* ----------------- progress */

progress {
    width: 100%;
    height: 2rem;
    background: var(--fg-color2);
    color: var(--bg-color2);
    display: block;
    position: relative;
    border: 1px solid var(--sppas-color2);
    border-radius: calc(2 * var(--focus-border-width));
}

.progress-infinite {
    animation-name: animprogress;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-timing-function: ease;
}

@keyframes animprogress {
    from { background-color: var(--fg-color2); }
    to { background-color: var(--bg-color2); }
}

/* =======================================================================
                        Custom classes
   ======================================================================= */

/* -------- A splash... of SPPAS.
*/

.sppas-splash {
    background-image: url('../images/splash.png');
    background-repeat: no-repeat;
    background-size: 100% var(--splash-height);
    /*height: var(--splash-height);    ... is replaced by: */
    aspect-ratio: 5.5;
    max-height: var(--splash-height);
}

/* --------- A simple panel, intended to be used as container.  */

.sp-panel {
    border: 0;
    padding: 0;
    margin: 0;
    position: relative;
}

.sp-panel:after, .sp-panel:before,
.sp-row:after, .sp-row:before, .sp-row-padding:after, .sp-row-padding:before,
.sp-cell-row:before, .sp-cell-row:after,
.sp-clear:after, .sp-clear:before,
.sp-bar:before, .sp-bar:after {
    content: "";
    display: table;
    clear: both
}

/* --------- A simple panel with a flexible layout  */

.flex-panel {
    margin: auto;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
}

/* Properties of the items in a flex panel -- if declared as it. */
.flex-item {
    margin-right: 1rem;
}

.flex-item:first-child {
    margin-left: 1rem;
}

/* -------- Complex panel for any dialog window, with an header panel,
  a content panel and an action panel.
*/

.sp-panel-header {
    text-align: center !important;
    background-color: var(--sppas-color);
    color: var(--bg-color);
    width: 100%;
    padding: 0;
    min-height: 50px;
    max-height: 80px;
    z-index: 1;
}

.sp-panel-header-icon {
    padding-bottom: 10px;
    margin-right: 20px;
    width: 10%;
    vertical-align: middle;
}

.sp-panel-header-text {
    font-family: inherit;
    font-size: 130%;
    font-weight: bold;
    font-style: normal;
    padding-top: 10px;
    padding-bottom: 10px;
    text-align: center;
    text-decoration: none;
    width: 90%;
    vertical-align: middle;
}

.sp-panel-content {
    margin: 10px;
    width: 100%;
    min-height: 100px;
}

.sp-panel-action {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-around;
    column-gap: 2px;
    margin-top: 10px;
    width: 100%;
    padding: 0;
    border-top: 1px solid var(--fg-color);
    background-color: var(--bg-color);
    min-height: 40px;
    max-height: 50px;
}

.sp-action-1-btn {
    width: 100%;
}

.sp-action-2-btn {
    width: 49%;
}

.sp-action-3-btn {
    width: 32%;
}

.sp-action-4-btn {
    width: 24%;
}

.sp-action-5-btn {
    width: 19%;
}


/* ----------- Styling the color scheme switcher button ----------- */
/* It acts like a toggle button: it's a checkbox input with a label */

.accessibility-switcher {
    position: relative;
    display: inline-block;
    margin-top: auto;
    margin-bottom: auto;
    border: var(--focus-border-width) solid transparent;
    flex-grow: 0;
    background: transparent;
}

.accessibility-switcher input {
    opacity: 0;
    width: 0;
    height: 0;
}

.colors-scheme-switch-img {
    display: inline-block;
    width: 2rem;
    height: 2rem;
    cursor: pointer;
    -webkit-transition: .4s;
    transition: .4s;
}

input.colors-scheme-switch-input:checked + .colors-scheme-switch-img {
    transform: rotate(180deg) scale(1);
}

input.colors-scheme-switch-input:hover + .colors-scheme-switch-img {
    transform: scale(1.5);
}

input.colors-scheme-switch-input:focus + .colors-scheme-switch-img {
    border: var(--focus-border-width) solid var(--focus-border-color);
    border-radius: 2px;
}

/* ----------- Styling others ----------- */

.sp-body-header {
    margin: auto;
    background: var(--bg-color);
    background: linear-gradient(to top, var(--bg-color), var(--sppas-color-contrast));
    min-height: 3rem;
}

.sp-body-main {
    width: 100%;
    background-color: inherit;
    color: inherit;
    font-family: inherit;
    font-size: inherit;
}

.sp-body-footer {
    margin: auto;
    margin-top: 1rem;
    background: var(--bg-color);
    background: linear-gradient(to bottom, var(--bg-color), var(--sppas-color-contrast));
    min-height: 3rem;
}

.copyright {
    text-align: center;
    font-size: 80%;
    color: var(--fg-color2-a);
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
}

.sp-grayscale {
    filter: grayscale(50%)
}

.hide {
    display: none;
}

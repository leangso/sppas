"""
:filename: sppas.ui.swapp.app_setup.fieldsetfeature.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: Create a "Feature" fieldset node of the setup app.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

from sppas.src.config import msg
from sppas.src.config import info
from sppas.ui.htmlmaker import HTMLNode
from sppas.ui.swapp.htmltags import sppasHTMLCheckboxNode

from .basefieldset import SetupBaseFieldset

# ---------------------------------------------------------------------------


MSG_FEAT = msg("Feature", "install")
MSG_DESCR = msg("Description", "install")
MSG_FEATURES = msg("Features", "install")
LEGEND_FEATURES = msg("Check the SPPAS optional features and install programs", "install")
INFO_FEATURES = info(504, "install")
INFO_READY = info(506, "install")

# ---------------------------------------------------------------------------


class SetupFeaturesFieldset(SetupBaseFieldset):
    """List of features fieldset of the setup page.

    """

    def __init__(self, parent, installer):
        super(SetupFeaturesFieldset, self).__init__(parent, "features_field", LEGEND_FEATURES)
        self._msg = MSG_FEATURES
        self._ckeckables = list()   # for convenience (HTMLNode does not have get_descendant(ccid))

        # Create the list of features. Put in a table.
        table = HTMLNode(self.identifier, None, "table")
        table.set_attribute("class", "features-table")
        table.set_attribute("role", "grid")
        self.append_child(table)

        # Create the features install message. Put it as table caption.
        txt = HTMLNode(table.identifier, None, "caption", value=INFO_FEATURES)
        txt.add_attribute("class", "features-caption")
        table.append_child(txt)

        tbody = HTMLNode(table.identifier, None, "tbody")
        table.append_child(tbody)
        if installer is not None:
            for fid in installer.features_ids("deps"):
                tr = HTMLNode(tbody.identifier, None, "tr")
                tbody.append_child(tr)
                td1 = HTMLNode(tr.identifier, None, "td")
                tr.append_child(td1)
                checkable = sppasHTMLCheckboxNode(td1.identifier, "feature_" + fid, fid)
                checkable.check(installer.enable(fid))
                td1.append_child(checkable)
                self._ckeckables.append(checkable)
                td2 = HTMLNode(tr.identifier, None, "td")
                tr.append_child(td2)
                brief = HTMLNode(td2.identifier, None, "p", attributes={"class": "p-brief"},
                                 value=installer.brief(fid))
                descr = HTMLNode(td2.identifier, None, "p", attributes={"class": "p-descr"},
                                 value=installer.description(fid))
                td2.append_child(brief)
                td2.append_child(descr)

        # Create the install message
        txt = HTMLNode(self.identifier, None, "p", value=INFO_READY)
        self.append_child(txt)

    # -----------------------------------------------------------------------

    def process_event(self, event_name, event_value):
        """Process a received event.

        :param event_name: (str) Identifier name of the posted data
        :param event_value: (str) value of the corresponding posted data
        :return: (int) status 200 if event handled or 205 if unknown

        """
        check_name = event_name.replace("_input_posted", "")
        for checkable in self._ckeckables:
            if checkable.identifier == check_name:
                checkable.check(not checkable.is_checked())
                return 200
        return 205

"""
:filename: sppas.ui.swapp.app_setup.setupmaker.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: The web-based application "Setup" of SPPAS.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

This application setup allows to install several external programs SPPAS is
requiring in order to enable some of its features.

"""

import os
import sys
import logging
import traceback
from threading import Thread

from sppas.src.config import sg
from sppas.src.config import lgs
from sppas.src.config import sppasLogFile
from sppas.src.preinstall import sppasInstallerDeps

from sppas.ui.httpd.hstatus import sppasHTTPDStatus
from sppas.ui.htmlmaker import HTMLNode
from sppas.ui.swapp.wexc import sppasHTMLIncompleteFieldset
from sppas.ui.swapp.wappsg import wapp_settings
from sppas.ui.swapp.respmaker.response import sppasWebResponseRecipe

from .fieldsets import SetupFieldsets
from .headfootnodes import SetupHeaderNode
from .headfootnodes import SetupActionsNode

# -----------------------------------------------------------------------


class SetupResponseRecipe(sppasWebResponseRecipe):
    """The setup.html HTTPD response baker.

    For a good UX when installing:
        1. Queue up the long-running requested installation task
        2. Respond immediately so user can get back to his/her busy life
        3. Handle the long-running task out of process
        4. Allow the user to check the status of the long-running task
        5. Notify the user when the task status is changed or is completed

    """

    def __init__(self):
        super(SetupResponseRecipe, self).__init__(name=sg.__name__ + " Setup")
        try:
            self.__installer = sppasInstallerDeps()
        except Exception as e:
            logging.error("No installation will be performed. The installer "
                          "wasn't created due to the following error: {}"
                          "".format(str(e)))
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logging.error(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            self.__installer = None
        self.__th = Thread(target=self.__install_features)
        self.__errors = ""

        # Fix logging
        log_report = sppasLogFile(pattern="install_ui")
        lgs.file_handler(log_report.get_filename(), with_stream=True)

        # Fix the "id" for this page
        self._htree.add_html_attribute("id", "setup")

        # The wizard pages are fieldset tags
        self.__fieldsets = SetupFieldsets(self.__installer)
        self.__current = self.__fieldsets[0]

        # Create the dynamic HTTPD response content
        self._status = sppasHTTPDStatus()
        self._bake()

    # -----------------------------------------------------------------------

    @staticmethod
    def page() -> str:
        """Override. Return the HTML page name."""
        return "setup.html"

    # -----------------------------------------------------------------------

    def create(self) -> None:
        """Override. Create the fixed page content in HTML.

        The fixed content corresponds to the parts that can't be invalidated:
        head, body_header, body_footer.

        """
        # Define this page title
        self._htree.set_title(self._name)
        # Add this page style
        self._htree.add_css_link(os.path.join(wapp_settings.css, "page_setup.css"))

    # -----------------------------------------------------------------------

    def _process_events(self, events) -> bool:
        """Process the given events coming from the POST of any form.

        :param events (dict): key=event_name, value=event_value
        :return: (bool) True if the whole page must be re-created.

        """
        self._status.code = 200

        if "update_install_action" in events.keys():
            if self.__th.is_alive() is True:
                self._status.code = 202
            else:
                self.__current.completed(self.__errors, self.__installer)
            return True

        # If the installation button was clicked, all the other "events" are ignored.
        if "install_btn_action" in events.keys():
            self.__process_install()
            self._status_code = 202
            # 202 Accepted
            # The request has been accepted for processing,
            # but the processing has not been completed.
            return True

        refresh = True
        for event_name in events:
            event_value = events[event_name]

            if event_name in ("cancel_btn_action", "exit_btn_action"):
                self._status.code = 410

            elif event_name == "prev_btn_action":
                # does the client re-ask the same page and is sending the data
                # of the previously posted request???
                if str(self.__fieldsets.get_index(self.__current)) == event_value:
                    self.__current = self.__fieldsets.prev_field(self.__current)
                else:
                    logging.debug("... hum, the client re-asked the same page twice!")

            elif event_name == "next_btn_action":
                # does the client re-ask the same page and is sending the data
                # of the previously posted request???
                if str(self.__fieldsets.get_index(self.__current)) == event_value:
                    try:
                        self.__current.validate()
                        self.__current = self.__fieldsets.next_field(self.__current)
                    except sppasHTMLIncompleteFieldset as e:
                        # validate failed.
                        # self.__current does not change!
                        # [ we should open a modal panel with the error ...
                        # we re-send the same page instead ]
                        logging.error(e)
                else:
                    logging.debug("... hum, the client re-asked the same page twice!")

            elif event_name == "colors_scheme":
                wapp_settings.set("colors_scheme", event_value)
                self._htree.body.set_attribute("class", event_value)
                logging.debug("Switched to colors scheme: {:s}".format(event_value))
                self._status.code = 205
                refresh = False

            elif event_name.startswith("feature_") and event_name.endswith("_posted") is True:
                # it is asked to enable/disable a feature
                do_enable = not self.__installer.enable(event_value)
                self.__installer.enable(event_value, do_enable)
                logging.debug(" - feature {} enable is: {}".format(event_value, do_enable))
                self._status.code = self.__current.process_event(event_name, event_value)

            elif event_name.endswith("_posted") is True:
                self._status.code = self.__current.process_event(event_name, event_value)

            else:
                logging.error("Unknown event name {:s}".format(event_name))
                self._status.code = 205
                refresh = False

        return refresh

    # -----------------------------------------------------------------------

    def _bake(self):
        """Create the dynamic page content in HTML."""
        # Define this page content: a form with an header, fieldsets and actions.
        self.comment("Body content")

        if self._status in (100, 200, 202):
            # create an header. it displays the current page, and have actions.
            # not displayed if installing
            if self.__th.is_alive() is False:
                setup_header = SetupHeaderNode(self._htree.body_main.identifier, self.__fieldsets, self.__current)
                self._htree.body_main.append_child(setup_header)

            # attach the current fieldset node to the body main of the tree
            self.__current.set_parent(self._htree.body_main.identifier)
            self._htree.body_main.append_child(self.__current)

            # create an actions navbar
            # not displayed if installing
            if self.__th.is_alive() is False:
                action_nav = SetupActionsNode(self._htree.body_main.identifier, self.__fieldsets, self.__current, self.__installer is not None)
                self._htree.body_main.append_child(action_nav)

        elif self._status == 410:
            # The 410 is "Gone" response sent when the requested content has been
            # permanently deleted from server, with no forwarding address. Clients
            # are expected to remove their caches and links to the resource.
            # The 444 is "No Response" but it's a non-official code. It is
            # used internally to instruct the server to return no information
            # to the client and close the connection immediately.
            status_node = HTMLNode(self._htree.body_main.identifier, None, "h1",
                                   value="HTTP response {:d}".format(self._status.code))
            self._htree.body_main.append_child(status_node)
            text = HTMLNode(self._htree.body_main.identifier, None, "p", value="Time to say Goodbye!")
            self._htree.body_main.append_child(text)

        else:
            # This should not happen.
            status = HTMLNode(self._htree.body_main.identifier, None, "h1",
                              value="HTTP response {:d}".format(self._status.code))
            self._htree.body_main.append_child(status)

    # -----------------------------------------------------------------------
    # Installation process is here and private!
    # -----------------------------------------------------------------------

    def __process_install(self):
        """Start the installation in a thread."""
        if self.__th.is_alive() is True:
            return

        # The current page is the list of features.
        # Switch to the installation page.
        self.__current = self.__fieldsets[self.__fieldsets.get_index_from_name("install_field")]

        # Create a progress system
        progress = self.__current.install_progress()
        self.__installer.set_progress(progress)
        # ... but the installer does not indicate a percentage
        # p = progress.get_node().get_child("percent")
        # p.add_attribute("class", "progress-infinite")

        # Start the install (not allowed to be stopped, only killed with the app!)
        self.__errors = ""
        self.__th = Thread(target=self.__install_features)
        self.__th.daemon = True  # If the main thread is killed, this thread will be killed as well.
        self.__th.start()

    # -----------------------------------------------------------------------

    def __install_features(self):
        self.__errors = self.__installer.install("deps")

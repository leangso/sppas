"""
:filename: sppas.ui.swapp.app_setup.fieldsetwelcome.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: Create a "Welcome" fieldset node of the setup app.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

from sppas.src.config import sg
from sppas.src.config import cfg
from sppas.src.config import info
from sppas.ui.htmlmaker import HTMLNode
from sppas.ui.swapp.wexc import sppasHTMLIncompleteFieldset
from sppas.ui.swapp.htmltags import sppasHTMLCheckboxNode

from .basefieldset import SetupBaseFieldset

# ---------------------------------------------------------------------------


MSG_WELCOME = info(551, "install")   # "Welcome & conditions"
LEGEND_WELCOME = info(552, "install")
MSG_WELCOME1 = info(554, "install")
MSG_WELCOME2 = info(555, "install")
MSG_WELCOME3 = info(556, "install")
MSG_ACCEPT_REF = info(558, "install")

# ---------------------------------------------------------------------------


class SetupHomeFieldset(SetupBaseFieldset):
    """Welcome fieldset of the setup page.

    """

    def __init__(self, parent):
        field_title = LEGEND_WELCOME + " -- " + str(sg.__version__)
        super(SetupHomeFieldset, self).__init__(parent, "home_field", field_title)
        self._msg = MSG_WELCOME

        # --- The application splash
        splash = HTMLNode(self.identifier, "splash", "div", attributes={"class": "sppas-splash"})
        self.append_child(splash)

        # --- Welcome messages
        txt = HTMLNode(self.identifier, None, "p",
                       attributes={"class": "welcome-panel"},
                       value=MSG_WELCOME1 + MSG_WELCOME2)
        self.append_child(txt)

        # -- Author and reference agreement
        auth = HTMLNode(self.identifier, None, "p", value=MSG_WELCOME3)
        self.append_child(auth)
        checkbox = sppasHTMLCheckboxNode(self.identifier, "check_ref", MSG_ACCEPT_REF)
        if cfg.log_level == 0:
            checkbox.check()
        self.append_child(checkbox)

    # -----------------------------------------------------------------------

    def validate(self):
        """Raise an exception if the input is not checked.

        """
        checkbox = self.get_child("check_ref")
        if checkbox.is_checked() is False:
            raise sppasHTMLIncompleteFieldset("Accept Reference")

    # -----------------------------------------------------------------------

    def process_event(self, event_name, event_value):
        """Process a received event.

        :param event_name: (str) Identifier name of the posted data
        :param event_value: (str) value of the corresponding posted data
        :return: (int) status 200 if event handled or 205 if unknown

        """
        if event_name == "check_ref_input_posted":
            checkbox = self.get_child("check_ref")
            checkbox.check(not checkbox.is_checked())
            return 200
        return 205

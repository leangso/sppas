"""
:filename: sppas.ui.swapp.app_setup.fieldsetinstall.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: Create an "Install" fieldset node of the setup app.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

from sppas.src.config import msg
from sppas.src.config import info
from sppas.ui.htmlmaker import HTMLNode
from sppas.ui.htmlmaker import HTMLHr
from sppas.ui.swapp.wprogress import ProgressHTML

from .basefieldset import SetupBaseFieldset

# ---------------------------------------------------------------------------


MSG_COMPLETED = msg("Installation", "install")
MSG_ENABLED = msg("enabled", "install")
MSG_DISABLED = msg("disabled", "install")
LEGEND_COMPLETED = info(573, "install")
INFO_INSTALL_FINISHED = info(560, "install")
INFO_SEE_LOGS = info(512, "install")

# ---------------------------------------------------------------------------


class SetupInstallFieldset(SetupBaseFieldset):
    """Completed fieldset of the setup page.

    """

    def __init__(self, parent):
        super(SetupInstallFieldset, self).__init__(parent, "install_field", LEGEND_COMPLETED)
        self._msg = MSG_COMPLETED

    # -----------------------------------------------------------------------

    def install_progress(self):
        """Create the node to show progress.

        :return: (sppasBaseProgress)

        """
        p = ProgressHTML(self.identifier)
        self.append_child(p.get_node())

        js_value = """

        function init() { 
            const form = document.createElement("form");
            form.method = "POST";
            form.id = "form_update";
            form.name = "form_update";
            form.style.display = "none";

            const el = document.createElement("input");
            el.name = "update_install_action";
            el.id = "update_install_action";
            el.value = "true";
            el.type = "hidden"
            form.appendChild(el);

            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        }

        window.onload = setTimeout(function () { init(); }, 1000);

        """
        js = HTMLNode(self.identifier, "js_timeout", "script", value=js_value)
        self.append_child(js)

        return p

    # -----------------------------------------------------------------------

    def completed(self, errors, installer):
        """Append the completed messages to the node.

        :param errors: (list of str) List of installation errors or warnings
        :param installer: (sppasInstallerDeps)

        """
        self.remove_child("js_timeout")
        self.remove_child("progress_install")

        txt = HTMLNode(self.identifier, None, "p", value=INFO_INSTALL_FINISHED)
        self.append_child(txt)

        txt = HTMLNode(self.identifier, None, "p", value=INFO_SEE_LOGS)
        self.append_child(txt)

        # Show the list of features, and their status
        ul = HTMLNode(self.identifier, None, "ul")
        self.append_child(ul)
        if installer is not None:
            for fid in installer.features_ids("deps"):
                text = MSG_ENABLED if installer.enable(fid) is True else MSG_DISABLED
                li = HTMLNode(ul.identifier, None, "li", value=fid + ": " + text)
                ul.append_child(li)

        if len(errors) > 0:
            self.append_child(HTMLHr(self.identifier))
            pre = HTMLNode(self.identifier, None, "code")
            self.append_child(pre)
            for line in errors:
                msg = self.format_to_html(line)
                txt = HTMLNode(pre.identifier, None, "p", value=msg)
                pre.append_child(txt)

    # -----------------------------------------------------------------------

    def format_to_html(self, msg):
        """Format a message with HTML tags.

        :param msg: (str)
        :return: (str)

        """
        msg = msg.replace("\t", "    ")
        msg = msg.replace("\\t", "    ")
        msg = msg.replace("\r", " ")
        msg = msg.replace("\n", " <br/>")
        msg = msg.replace("\\n", " <br/>")
        msg = msg.replace("\\'", "&apos;")
        msg = msg.replace('\\"', "&quot;")

        return msg

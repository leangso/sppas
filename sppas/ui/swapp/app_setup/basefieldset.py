"""
:filename: sppas.ui.swapp.app_setup.basefieldset.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: A base class to create a fieldset node of the setup app.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

from sppas.ui.htmlmaker import HTMLNode

# -----------------------------------------------------------------------


class SetupBaseFieldset(HTMLNode):
    """A base class for any fieldset of the setup page.

    """

    def __init__(self, parent, identifier, legend=None):
        super(SetupBaseFieldset, self).__init__(parent, identifier, "fieldset")
        self.set_attribute("class", "wizard-fieldset")
        if legend is not None:
            legend = HTMLNode(self.identifier, "field_legend", "legend", value="{:s}".format(legend))
            self.append_child(legend)
        # A short version of the legend
        self._msg = ""

    # -----------------------------------------------------------------------

    def get_msg(self):
        """Return the title message of the fieldset (str)."""
        return self._msg

    # -----------------------------------------------------------------------

    def validate(self):
        """Raise an exception if the fieldset is not fulfilled or return None.

        :raises: IncompleteFieldset

        """
        return None

    # -----------------------------------------------------------------------

    def process_event(self, event_name, event_value):
        """Process a received event.

        :return: (int) Status value (default: 205)

        """
        return 205

"""
:filename: sppas.ui.swapp.main_app.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: The main class for any of the SPPAS Web-based Applications.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import logging

from sppas.src.config import lgs
from sppas.src.config import cfg
from sppas.ui.httpd import sppasHTTPDHandler
from sppas.ui.httpd import sppasBaseHTTPDServer
from sppas.ui.swapp.wappsg import wapp_settings

# Stable app:
from .app_setup import SetupResponseRecipe
# Under-construction apps:
from .app_main.pagesmaker import MainResponseRecipe  # sppasPagesMaker
from .app_test.testsmaker import TestsResponseRecipe

# ---------------------------------------------------------------------------


class sppasWebApp(object):
    """Create and run any of the SPPAS web-based applications.

    """

    class wappServer(sppasBaseHTTPDServer):
        """A custom HTTPD server for SPPAS wapp.

        """

        def create_pages(self, app_type="app"):
            """Override. Add bakeries for dynamic HTML pages of this app.

            :param app_type: (str) One of "setup", "test", or "app" (default)

            """
            if app_type == "setup":
                logging.debug("HTTPD server initialization for setup...")
                # Create the setup page -- only one for now!
                bakery = SetupResponseRecipe()
                self._pages[bakery.page()] = bakery
                self._default = bakery.page()

            elif app_type == "test":
                logging.debug("HTTPD server initialization for test...")
                # Create the test page -- only one for now!
                bakery = TestsResponseRecipe()
                self._pages[bakery.page()] = bakery
                self._default = bakery.page()

            else:
                logging.debug("HTTPD application server initialization...")
                # Temporary page
                bakery = MainResponseRecipe()
                self._pages[bakery.page()] = bakery
                self._default = bakery.page()
                # Create all the available pages with the pages' app maker
                # for page_name in sppasPagesMaker.pages():
                #     self._pages[page_name] = sppasPagesMaker(page_name).create()
                # self._default = sppasPagesMaker().default()

    # -----------------------------------------------------------------------

    def __init__(self, arguments):
        """HTTP Server initialization.

        Create the application for the GUI of SPPAS based on http.

        """
        self.__location = "localhost"
        self.__port = self.__port_value()
        self.__server = None
        server_address = (self.__location, self.__port)

        if "log_level" in arguments:
            cfg.log_level = arguments["log_level"]
            lgs.set_log_level(cfg.log_level)
            logging.debug("Log level is set to {:d}".format(cfg.log_level))

        self.__server = sppasWebApp.wappServer(server_address, sppasHTTPDHandler)
        app_type = "app"
        if arguments.get("setup", False) is True:
            app_type = "setup"
        elif arguments.get("test", False) is True:
            app_type = "test"
        self.__server.create_pages(app_type)

    # -----------------------------------------------------------------------

    def __port_value(self):
        """Return a port value for this instance."""
        pport = wapp_settings.hport + 1
        if pport > 99:
            pport = 80
        wapp_settings.hport = pport
        return pport + (pport*100)

    # -----------------------------------------------------------------------

    def client_url(self):
        """Return the client URL of this server.

        """
        return "http://{:s}:{:d}/".format(self.__location, self.__port)

    # -----------------------------------------------------------------------

    def run(self):
        """Run the application with a main loop.

        :returns: (int) Exit status (0=normal)

        """
        try:
            # Start the main loop of the HTTP server
            self.__server.serve_forever()
            # Notice that the sppasHTTPDHandler can shut down the server --
            # allowed because it's a local application, not an internet service.
        except KeyboardInterrupt:
            # Stop the server
            self.__server.shutdown()

        # Save current configuration
        logging.debug("Save config files.")
        cfg.save()
        wapp_settings.save()

        # Return an exit status 0 = normal.
        return 0

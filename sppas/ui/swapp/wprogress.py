#!/usr/bin/env python
"""
:filename: sppas.ui.swapp.wprogress.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary: Basic progress indicator for Web-based SPPAS applications.

.. _This file is part of SPPAS: http://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import logging

from sppas.ui.progress import sppasBaseProgress
from sppas.ui.htmlmaker import HTMLNode

# -----------------------------------------------------------------------


class ProgressHTML(sppasBaseProgress):
    """A very simple progress indicator with a div HTMLNode.

    """

    def __init__(self, parent_identifier):
        super(ProgressHTML, self).__init__()
        self.__node = HTMLNode(parent_identifier, "progress_install", "div")
        p = HTMLNode(self.__node.identifier, "percent", "progress",
                     attributes={"id": "percent", "max": "100", "value": "0"})
        self.__node.append_child(p)

    # ------------------------------------------------------------------

    def __node_header(self):
        c = self.__node.get_child("header")
        if c is not None:
            c.set_value(self._header)
        else:
            msg_node = HTMLNode(self.__node.identifier, "header", "h3", value=self._header)
            self.__node.append_child(msg_node)

    def __node_text(self):
        c = self.__node.get_child("text")
        if c is not None:
            c.set_value(self._text)
        else:
            msg_node = HTMLNode(self.__node.identifier, "text", "p", value=self._text)
            self.__node.append_child(msg_node)

    def __node_percent(self):
        p = self.__node.get_child("percent")
        p.set_attribute("value", str(self._percent))

    # ------------------------------------------------------------------

    def update(self, percent=None, message=None):
        """Update the progress.

        :param message: (str) progress bar value (default: 0)
        :param percent: (float) progress bar text  (default: None)

        """
        if percent is not None:
            self._percent = percent
            self.__node_percent()

        if message is not None:
            logging.info('  => ' + message)
            self._text = str(message)
            self.__node_text()

    # ------------------------------------------------------------------

    def set_header(self, header):
        """Set a new progress header text.

        :param header: (str) new progress header text.

        """
        if len(header) > 0:
            self._header = str(header)
            self.__node_header()
        else:
            self._header = ""
        logging.info(self._header)

    # ------------------------------------------------------------------

    def close(self):
        c = self.__node.get_child("header")
        if c is not None:
            self.__node.remove_child(c)
        c = self.__node.get_child("text")
        if c is not None:
            self.__node.remove_child(c)

    # ------------------------------------------------------------------

    def get_node(self):
        """Return the HTMLNode of the progress."""
        return self.__node

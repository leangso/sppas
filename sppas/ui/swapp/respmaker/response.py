"""
:filename: sppas.ui.swapp.respmaker.response.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary: HTML response maker for swapp.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

from sppas.ui.httpd.hresponse import BaseResponseRecipe

from .pagetree import sppasWebTree

# ---------------------------------------------------------------------------


class sppasWebResponseRecipe(BaseResponseRecipe):
    """Create an HTML response file for swapp HTML pages.

    Intent to be used as base a class for any HTML page of swapp.

    """

    def __init__(self, name="swapp"):
        """Create a new swapp response recipe with a default response.

        """
        # Create an instance of a custom tree for this response.
        page_id = name.replace(" ", "_")
        if len(page_id) == 0:
            page_id = "swapp"
        swapp_tree = sppasWebTree(page_id)

        # OK. Now we can inherit.
        super(sppasWebResponseRecipe, self).__init__(name, tree=swapp_tree)

    # -----------------------------------------------------------------------

    def _invalidate(self):
        """Remove all children nodes of the body "main".

        Do remove neither the head, nor the body header nor the body footer.

        """
        node = self._htree.body_main
        for i in reversed(range(node.children_size())):
            node.pop_child(i)

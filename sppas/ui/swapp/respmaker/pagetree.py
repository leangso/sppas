"""
:filename: sppas.ui.swapp.respmaker.pagetree.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary: HTML Tree generator for swapp.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

Requires the CSS properties:
"header-splash", "sp-body-main", "sp-footer", "copyright"

"""

from sppas.ui.htmlmaker import HTMLNode
from sppas.ui.htmlmaker import HTMLTree

from ..wappsg import wapp_settings
from .pagehead import sppasWebHeadNode

# ---------------------------------------------------------------------------


class sppasWebTree(HTMLTree):
    """The root of an HTML tree for a SPPAS Web-based application.

    """

    def __init__(self, identifier):
        super(sppasWebTree, self).__init__(identifier)

        # -----------  HTML head
        self.head = sppasWebHeadNode(self.identifier)

        #  -----------  HTML body
        self.body.set_attribute("class", wapp_settings.colors_scheme)

        # HTML "body_header" node is a body content that won't be invalidated
        self.body_main.set_attribute("class", "sp-body-header")

        # HTML "body_main" node is a customizable body content
        self.body_main.set_attribute("class", "sp-body-main")

        # HTML "body_footer" node is a body content that won't be invalidated
        copyreg = HTMLNode(self.body_footer.identifier, "copyright", "p",
                           attributes={"class": "copyright", "role": "none"},
                           value="Copyright &copy; 2023 Brigitte Bigi")
        self.body_footer.append_child(copyreg)
        self.body_footer.set_attribute("class", "sp-body-footer")

    # -----------------------------------------------------------------------
    # Header setters
    # -----------------------------------------------------------------------

    def set_title(self, entry):
        self.head.title(entry)

    def add_meta(self, metadict):
        self.head.meta(metadict)

    def add_link(self, rel, href, link_type=None):
        self.head.link(rel, href, link_type)

    def add_css(self, filename):
        self.head.css_from_file(filename)

    def add_css_link(self, href):
        self.head.link("stylesheet", href, link_type="text/css")

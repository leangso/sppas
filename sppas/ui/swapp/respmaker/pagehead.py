"""
:filename: sppas.ui.swapp.respmaker.pagehead.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary: The class to serialize the HTML <head> content for swapp.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import os

from sppas.src.config import sg
from sppas.ui.htmlmaker import HTMLNode
from sppas.ui.htmlmaker import HTMLHeadNode
from sppas.ui.swapp.wappsg import wapp_settings

# ---------------------------------------------------------------------------


class sppasWebHeadNode(HTMLHeadNode):
    """Class to represent the head node of any page of a web-based app.

    Add title "SPPAS", add SPPAS favicon, and add links to the followings:
    colors_dark.css, colors_light.css, fonts.css, main.css, scriptutils.js

    """

    def __init__(self, parent):
        """Create the head node.

        """
        super(sppasWebHeadNode, self).__init__(parent)
        self.reset()

    # -----------------------------------------------------------------------

    def reset(self):
        """Reset the header to its default values."""
        # Delete the existing list of children
        self._children = list()

        # Add a default title
        title_node = HTMLNode(self.identifier, "title", "title", value=sg.__name__)
        self.append_child(title_node)

        # The default meta tags
        self.meta({"charset": "utf-8"})
        self.meta({"name": "viewport",
                   "content": "width=device-width, initial-scale=1.0"})

        # The default links
        self.link(rel="icon", href=wapp_settings.statics+"icons/sppas.ico")

        # Add all the color schemes, not only the current one
        self.link(rel="stylesheet", href=wapp_settings.css+"colors_dark.css", link_type="text/css")
        self.link(rel="stylesheet", href=wapp_settings.css+"colors_light.css", link_type="text/css")

        # Add the CSS style of any SPPAS web-based applications
        self.link(rel="stylesheet", href=wapp_settings.css+"main.css", link_type="text/css")

        # Add the javascript with utility functions
        self.script(src=wapp_settings.js+"scriptutils.js", script_type="text/javascript")

    # -----------------------------------------------------------------------

    def css_from_file(self, filename) -> None:
        """Add the file content located into the CSS default directory.

        :param filename: (str) Name of the CSS file.

        """
        css_filename = os.path.join(wapp_settings.css, filename)
        # Load the content of the CSS file and add it to the HTML header
        # in the style tag.
        to_add = True
        content = ""
        with open(css_filename, "r") as fp:
            for line in fp.readlines():
                if line.startswith("/*") is True:
                    to_add = False
                if to_add is True:
                    self.css(line)
                    content += line
                    content += "\n"
                if line.startswith("*/") is True:
                    to_add = True
        if len(content) > 0:
            self.css(content)

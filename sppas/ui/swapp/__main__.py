#!/usr/bin/env python
"""
:filename: sppas.ui.swapp.__main__.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary: This is the main swapp entry point. Can launch any SPPAS Web-app.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

In Python, '__main__' is the name of the scope in which top-level code
executes. Within SPPAS, it allows to launch the Graphical User Interface.

To launch the Web-based UI, this main file allows the followings 2 solutions:

>>> cd SPPAS-PACKAGE
>>> py3 sppas/ui/swapp
>>> py3 sppas/ui/swapp/__main__.py

Here, 'py3' is a link to the python virtuel environment. It was created
during the setup, i.e. either .sppaspyenv/bin/python (Unix OS) or
.sppaspyenv/Scripts/python.exe (Windows).

"""

import os
import sys
import logging
import webbrowser
import time
import traceback
from argparse import ArgumentParser

# package is not defined if the program is launched without the -m option
if __package__ is None or len(__package__) == 0:
    _dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.insert(0, _dir)
    __package__ = "swapp"

sppas_dir = os.path.dirname(os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__)))))
sys.path.append(sppas_dir)

try:
    from sppas.src.config import sg
    from sppas.src.config import cfg
    from sppas.src.config import sppasLogFile
    from .main_app import sppasWebApp
except Exception as e:
    # The API wasn't imported.
    # Something is wrong with the installation of python, or SPPAS.
    msg = traceback.format_exc()
    try:
        status = e.status
    except:
        status = 1
    print(msg)
    print("* * * * *  SPPAS exited with error number: {:04d}  * * * * * ".format(status))
    webbrowser.open_new_tab(url="https://sppas.org/documentation_09_annexes.html#error-" + "{:04d}".format(status))
    time.sleep(10)
    sys.exit(1)

# ---------------------------------------------------------------------------


if __name__ == '__main__':
    """A unique entry point for all the SPPAS web-based applications.

    """
    status = 1

    # create a parser for the command-line arguments
    parser = ArgumentParser(
        usage="{:s} [options]".format(os.path.basename(__file__)),
        description="... " + sg.__name__ + " " + sg.__title__)

    # add arguments here
    parser.add_argument("-l", "--log_level",
                        required=False,
                        type=int,
                        default=cfg.log_level,
                        help='Log level (default={:d}).'
                             ''.format(cfg.log_level))

    group_app_type = parser.add_mutually_exclusive_group()
    group_app_type.add_argument("--app", action='store_true', help="Launch the app (default).")
    group_app_type.add_argument("--setup", action='store_true', help="Launch setup instead of the app.")
    group_app_type.add_argument("--test", action='store_true', help="Launch test instead of the app.")

    if len(sys.argv) <= 1:
        sys.argv.append('-h')

    # then parse arguments of the command line and turn them into a dict
    args = vars(parser.parse_args())

    try:

        # Create the application and start it once. Can stop it with CTRL+C.
        logging.info("Please wait while starting HTTPD server...")
        app = sppasWebApp(args)
        logging.info("The server started successfully.")
        logging.info("Client url is {:s}".format(app.client_url()))

        # Open the web browser at the default location
        logging.warning("----------------------------------------------------------------------")
        logging.warning("              This application was tested with Firefox.               ")
        logging.warning("   Use of this app with another web browser is without any warranty.  ")
        logging.warning("     You can get it here: https://www.mozilla.org/en-US/firefox/      ")
        logging.warning("----------------------------------------------------------------------")
        webbrowser.open_new_tab(app.client_url())

        # Run the application server in a mainloop
        logging.info("Running application...")
        logging.info("Enter CTRL+C to quit the application.")
        status = app.run()
        logging.debug("Application ended with status: {:d}".format(status))

    except Exception as e:

        msg = str(e)
        report = sppasLogFile(pattern="setup")
        with open(report.get_filename(), "w") as f:
            f.write(report.get_header())
            f.write(msg)
            f.write("\n")
        webbrowser.open_new_tab(url=report.get_filename())

    sys.exit(status)

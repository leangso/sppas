"""
:filename: sppas.ui.htmlmaker.__init__.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Package to create an HTML tree and to serialize into a page.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

This is a minimalistic system to work with an HTML Tree and to serialize it
into an HTML page. Neither the integrity of the tree is verified nor the
compliance with HTML standard.

"""

from .hleaf import Doctype
from .hleaf import HTMLComment
from .hleaf import HTMLImage
from .hleaf import HTMLHr
from .hnodetags import HTMLInputText
from .hnodetags import HTMLRadioBox
from .hnodetags import HTMLButtonNode
from .hnode import EmptyNode
from .hnode import HTMLNode
from .hnode import HTMLHeadNode
from .htree import HTMLTree

__all__ = (
    "Doctype",
    "HTMLComment",
    "HTMLImage",
    "HTMLHr",
    "HTMLInputText",
    "HTMLRadioBox",
    "HTMLButtonNode",
    "EmptyNode",
    "HTMLNode",
    "HTMLHeadNode",
    "HTMLTree"
)

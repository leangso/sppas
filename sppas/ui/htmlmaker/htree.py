"""
:filename: sppas.ui.htmlmaker.htree.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary: The root class of the tree to generate HTML elements.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

from .hleaf import Doctype
from .hnode import BaseNode
from .hnode import HTMLHeadNode
from .hnode import HTMLNode

# ---------------------------------------------------------------------------


class HTMLTree(BaseNode):
    """The root of an HTML tree.

    The <html> tag represents the root of an HTML document.
    The <html> tag is the container for all other HTML elements (except for
    the <!DOCTYPE> tag).

    This class does not support yet the global attributes -- i.e. attributes
    that can be used with all HTML elements.
    See https://www.w3schools.com/TAgs/ref_standardattributes.asp

    """

    def __init__(self, identifier):
        """Create the tree root and some children nodes.

        When serialized, this class is producing the following:
        <!DOCTYPE html>
        <html>
          <head>  </head>
          <body>
            <header>  </header>
            <main>  </main>
            <footer>  </footer>
          </body>
        </html>

        Example of use:
        >>> # Create the tree
        >>> htree = HTMLTree("index")
        >>> htree.add_html_attribute("lang", "en")
        >>> # Fill in the <head> with a title, meta and link
        >>> htree.head.title("Purpose")
        >>> htree.head.meta({"charset": "utf-8"})
        >>> htree.head.link(rel="icon", href="/static/favicon.ico")
        >>> # Fill in the <body>:
        >>> # a <nav> tag in the header, a <h1> tag in the body and a <p> tag in the footer
        >>> htree.body.set_attribute("class", "colors_scheme_dark")
        >>> nav = HTMLNode(htree.body_header.identifier, "navmenu", "nav")
        >>> htree.body_header.append_child(nav)
        >>> node = HTMLNode(htree.body_main.identifier, None, "h1", value="this is a title")
        >>> htree.body_main.append_child(node)
        >>> node = HTMLNode(htree.body_footer.identifier, None, "p", value="&copy; me now")
        >>> htree.body_footer.append_child(node)

        :param identifier: (str) An identifier for this specific node.

        """
        super(HTMLTree, self).__init__(parent=None, identifier=identifier)

        # The HTML tree has 2 children: a doctype and an HTML element.
        self.__doctype = Doctype()
        self.__html = HTMLNode(identifier, None, "html")

        # The HTML node has 2 children: the <head> and the <body> (public)
        self.__html.append_child(HTMLHeadNode(self.__html.identifier))
        body = HTMLNode(self.__html.identifier, "body", "body")
        self.__html.append_child(body)
        # HTML body "header"
        body_header = HTMLNode(body.identifier, "body_header", "header")
        body.append_child(body_header)
        # HTML body "main"
        body_main = HTMLNode(body.identifier, "body_main", "main")
        body.append_child(body_main)
        # HTML body "footer"
        body_footer = HTMLNode(body.identifier, "body_footer", "footer")
        body.append_child(body_footer)

    # -----------------------------------------------------------------------
    # Public access to the html attributes and to the head and body children.
    # -----------------------------------------------------------------------

    def add_html_attribute(self, key, value) -> None:
        """Add or append a property to the HTML node.

        :param key: (str) Key property
        :param value: (str) Value of the attribute

        """
        self.__html.add_attribute(key, value)

    # -----------------------------------------------------------------------

    def get_head(self):
        return self.__html.get_child("head")

    def set_head(self, head_node):
        """Replace the current head node by the given one.

        :param head_node: (HTMLNode)

        """
        if isinstance(head_node, HTMLNode) is False:
            raise TypeError("HTMLNode expected.")
        if head_node.identifier != "head":
            raise Exception("Head node identifier must be 'head'. Got {:s} instead"
                            "".format(head_node.identifier))
        head_node.set_parent(self.__html.identifier)
        self.__html.remove_child("head")
        self.__html.insert_child(0, head_node)

    head = property(get_head, set_head)

    # -----------------------------------------------------------------------

    def get_body(self):
        return self.__html.get_child("body")

    body = property(get_body, None)

    # -----------------------------------------------------------------------

    def get_body_header(self):
        return self.get_body().get_child("body_header")

    def set_body_header(self, body_node):
        """Replace the current body->header node by the given one.

        :param body_node: (HTMLNode)

        """
        if isinstance(body_node, HTMLNode) is False:
            raise TypeError("HTMLNode expected.")
        if body_node.identifier != "body_main":
            raise Exception("Body->main node identifier must be 'body_main'. "
                            "Got {:s} instead".format(body_node.identifier))
        body_node.set_parent(self.get_body().identifier)
        self.get_body().remove_child("body_header")
        self.get_body().insert_child(0, body_node)

    body_header = property(get_body_header, set_body_header)

    # -----------------------------------------------------------------------

    def get_body_main(self):
        return self.get_body().get_child("body_main")

    def set_body_main(self, body_node):
        """Replace the current body->main node by the given one.

        :param body_node: (HTMLNode)

        """
        if isinstance(body_node, HTMLNode) is False:
            raise TypeError("HTMLNode expected.")
        if body_node.identifier != "body_main":
            raise Exception("Body->main node identifier must be 'body_main'. "
                            "Got {:s} instead".format(body_node.identifier))
        body_node.set_parent(self.get_body().identifier)
        self.get_body().remove_child("body_main")
        self.get_body().insert_child(1, body_node)

    body_main = property(get_body_main, set_body_main)

    # -----------------------------------------------------------------------

    def get_body_footer(self):
        return self.get_body().get_child("body_footer")

    def set_body_footer(self, body_node):
        """Replace the current body->footer node by the given one.

        :param body_node: (HTMLNode)

        """
        if isinstance(body_node, HTMLNode) is False:
            raise TypeError("HTMLNode expected.")
        if body_node.identifier != "body_footer":
            raise Exception("Body->footer node identifier must be 'body_footer'. "
                            "Got {:s} instead".format(body_node.identifier))
        body_node.set_parent(self.get_body().identifier)
        self.get_body().remove_child("body_footer")
        self.get_body().append_child(body_node)

    body_footer = property(get_body_footer, set_body_footer)

    # -----------------------------------------------------------------------

    def serialize(self) -> str:
        """Serialize the tree into HTML. """
        return self.__doctype.serialize() + self.__html.serialize(nbs=0)

    # -----------------------------------------------------------------------
    # Override base class.
    # -----------------------------------------------------------------------

    def set_parent(self, node_id) -> None:
        """Override. Do not set the parent identifier. """
        return None

    # -----------------------------------------------------------------------

    def is_leaf(self) -> bool:
        """Override. Return False. """
        return False

    # -----------------------------------------------------------------------
    # Overloads.
    # -----------------------------------------------------------------------

    def __contains__(self, identifier):
        raise NotImplementedError

    # -----------------------------------------------------------------------

    def __str__(self):
        return self.serialize()

"""
:filename: sppas.ui.htmlmaker.test_hmaker.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary: Tests for the package htmlmaker.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import unittest

from .hexc import NodeIdentifierError
from .hexc import NodeTagError
from .hexc import NodeChildTagError
from .hexc import NodeKeyError
from .hnode import BaseNode
from .hnode import EmptyNode
from .hnode import HTMLNode
from .hnode import HTMLHeadNode
from .htree import HTMLTree

# ---------------------------------------------------------------------------


class TestExceptions(unittest.TestCase):

    def test_node_errors(self):
        try:
            raise NodeIdentifierError("invalid identifier")
        except ValueError as e:
            self.assertTrue(isinstance(e, NodeIdentifierError))
            self.assertTrue("9410" in str(e))
            self.assertTrue(9410, e.status)

        try:
            raise NodeTagError("invalid tag")
        except ValueError as e:
            self.assertTrue(isinstance(e, NodeTagError))
            self.assertTrue("9420" in str(e))
            self.assertTrue(9420, e.status)

        try:
            raise NodeChildTagError("invalid child tag")
        except ValueError as e:
            self.assertTrue(isinstance(e, NodeChildTagError))
            self.assertTrue("9425" in str(e))
            self.assertTrue(9425, e.status)

        try:
            raise NodeKeyError("data name", "key")
        except KeyError as e:
            self.assertTrue(isinstance(e, NodeKeyError))
            self.assertTrue("9400" in str(e))
            self.assertTrue(9400, e.status)

# ---------------------------------------------------------------------------


class TestBaseNode(unittest.TestCase):

    def test_init_success(self):
        node = BaseNode()
        self.assertIsInstance(node, BaseNode)
        self.assertEqual(36, len(node.identifier))
        self.assertTrue(node.is_leaf())
        self.assertTrue(node.is_root())
        self.assertFalse(node.has_child("lol"))
        self.assertIsNone(node.get_parent())

        node = BaseNode(parent=None, identifier="toto")
        self.assertEqual("toto", node.identifier)

        node = BaseNode(parent="dad", identifier="toto")
        self.assertEqual("dad", node.get_parent())
        self.assertEqual("toto", node.identifier)

        node = BaseNode(parent="dad")
        self.assertEqual("dad", node.get_parent())
        self.assertEqual(36, len(node.identifier))

    # -----------------------------------------------------------------------

    def test_init_errors(self):
        with self.assertRaises(NodeIdentifierError):
            BaseNode(parent=None, identifier="")

        with self.assertRaises(NodeIdentifierError):
            BaseNode(parent=None, identifier="  ")

        with self.assertRaises(NodeIdentifierError):
            BaseNode(parent=None, identifier="my id")

        with self.assertRaises(NodeKeyError):
            BaseNode(parent="dad", identifier="dad")

# ---------------------------------------------------------------------------


class TestEmptyNode(unittest.TestCase):

    def test_init(self):
        node = EmptyNode(None, None, "br")
        self.assertEqual(36, len(node.identifier))
        self.assertTrue(node.is_leaf())
        self.assertTrue(node.is_root())
        self.assertFalse(node.has_child("lol"))
        self.assertIsNone(node.get_parent())
        self.assertEqual(0, node.nb_attributes())

        node = EmptyNode(None, None, "img", {"src": "path/file"})
        self.assertEqual(1, node.nb_attributes())

    # -----------------------------------------------------------------------

    def test_init_errors(self):
        with self.assertRaises(NodeTagError):
            EmptyNode(None, None, "invented")

        with self.assertRaises(TypeError):
            EmptyNode(None, None, "img", "src")

        with self.assertRaises(ValueError):
            EmptyNode(None, None, "img", {"key": "value"})

# ---------------------------------------------------------------------------


class TestNode(unittest.TestCase):

    def test_init(self):
        node = HTMLNode(parent=None, identifier="id01", tag="p")

    def test_init_error(self):
        with self.assertRaises(NodeTagError):
            HTMLNode(parent=None, identifier="id01", tag="tag")

    def test_child(self):
        # Append a child
        # Insert a child
        # Pop a child
        # Remove a child
        pass

    def test_attribute(self):
        node = HTMLNode(parent=None, identifier="id01", tag="p")
        self.assertFalse(node.is_attribute("class"))
        self.assertIsNone(node.get_attribute_value("class"))

        node.add_attribute("class", None)
        self.assertTrue(node.is_attribute("class"))
        self.assertIsNone(node.get_attribute_value("class"))

        node.set_attribute("class", "toto")
        self.assertTrue(node.is_attribute("class"))
        self.assertEqual(node.get_attribute_value("class"), "toto")

        node.add_attribute("class", "titi")
        self.assertEqual(node.get_attribute_value("class"), "toto titi")

        node.set_attribute("class", "tata")
        self.assertEqual(node.get_attribute_value("class"), "tata")

# ---------------------------------------------------------------------------


class TestHeadNode(unittest.TestCase):

    def test_init(self):
        node = HTMLHeadNode(parent=None)
        self.assertTrue("head", node.identifier)

    def test_error(self):
        node = HTMLHeadNode(parent=None)
        with self.assertRaises(NodeChildTagError):
            node.append_child(HTMLNode(parent=node.identifier, identifier=None, tag="p"))

    def test_title(self):
        pass

    def test_meta(self):
        pass

    def test_link(self):
        pass

    def test_script(self):
        pass

    def test_css(self):
        pass

# ---------------------------------------------------------------------------


class TestTree(unittest.TestCase):

    def test_init(self):
        tree = HTMLTree("home")
        self.assertTrue(tree.is_root())
        self.assertFalse(tree.is_leaf())
        self.assertTrue(tree.has_child("html"))
        self.assertIsInstance(tree.head, HTMLNode)
        self.assertIsInstance(tree.body, HTMLNode)
        self.assertIsInstance(tree.body_header, HTMLNode)
        self.assertIsInstance(tree.body_main, HTMLNode)
        self.assertIsInstance(tree.body_footer, HTMLNode)

        self.assertEqual(tree.body_header.identifier, "body_header")
        self.assertEqual(tree.body_main.identifier, "body_main")
        self.assertEqual(tree.body_footer.identifier, "body_footer")

    def test_setters(self):
        """Test to replace the header, the body, the footer by a custom one."""
        pass

    def test_serialize(self):
        value = """<!DOCTYPE html>

<html>
    <head>    </head>
    <body>
        <header>        </header>
        <main>        </main>
        <footer>        </footer>
    </body>
</html>
"""
        tree = HTMLTree("home")
        self.assertEqual(tree.serialize(), value)
        self.assertEqual(str(tree), value)


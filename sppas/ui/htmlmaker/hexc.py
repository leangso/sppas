"""
:filename: sppas.ui.htmlmaker.hexc.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary: Exception classes to be used in htmlmaker package.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023 Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

from sppas.src.config import error


# -----------------------------------------------------------------------


class NodeIdentifierError(ValueError):
    """:ERROR 9410:.

    Invalid HTML node identifier '{!s:s}'.

    """

    def __init__(self, value):
        self._status = 9410
        self.parameter = error(self._status) + \
                         (error(self._status, "globals")).format(value)

    def __str__(self):
        return repr(self.parameter)

    def get_status(self):
        return self._status

    status = property(get_status, None)

# -----------------------------------------------------------------------


class NodeTagError(ValueError):
    """:ERROR 9420:.

    Invalid HTML node tag '{!s:s}'.

    """

    def __init__(self, value):
        self._status = 9420
        self.parameter = error(self._status) + \
                         (error(self._status, "globals")).format(value)

    def __str__(self):
        return repr(self.parameter)

    def get_status(self):
        return self._status

    status = property(get_status, None)

# -----------------------------------------------------------------------


class NodeChildTagError(ValueError):
    """:ERROR 9425:.

    Invalid HTML child node tag '{!s:s}'.

    """

    def __init__(self, value):
        self._status = 9425
        self.parameter = error(self._status) + \
                         (error(self._status, "globals")).format(value)

    def __str__(self):
        return repr(self.parameter)

    def get_status(self):
        return self._status

    status = property(get_status, None)

# -----------------------------------------------------------------------


class NodeAttributeError(ValueError):
    """:ERROR 9430:.

    Invalid HTML node attribute '{!s:s}'.

    """

    def __init__(self, value):
        self._status = 9430
        self.parameter = error(self._status) + \
                         (error(self._status, "globals")).format(value)

    def __str__(self):
        return repr(self.parameter)

    def get_status(self):
        return self._status

    status = property(get_status, None)

# -----------------------------------------------------------------------


class NodeKeyError(KeyError):
    """:ERROR 9400:.

    Invalid node '{!s:s}' for data '{!s:s}'.

    """

    def __init__(self, data_name, value):
        self._status = 9400
        self.parameter = error(self._status) + \
                         (error(self._status, "globals")).format(value, data_name)

    def __str__(self):
        return repr(self.parameter)

    def get_status(self):
        return self._status

    status = property(get_status, None)

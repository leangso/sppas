## References

**Brigitte Bigi, Caterina Petrone** (2014).
*A generic tool for the automatic syllabification of Italian.*
In Proceedings of the First Italian Conference on Computational Linguistics CLiC-it 2014 and of the Fourth International Workshop EVALITA 2014, pp. 73-77, Pisa, Italy.

**Brigitte Bigi** (2014).
*The SPPAS participation to Evalita 2014.*
In Proceedings of the First Italian Conference on Computational Linguistics CLiC-it 2014 and the Fourth International Workshop EVALITA 2014, Pisa, Italy.

**Brigitte Bigi** (2012).
*The SPPAS participation to Evalita 2011.*
Lecture Notes in Artifical Intelligence, LNAI-7689, pp. 312-321. Rome, Italy.


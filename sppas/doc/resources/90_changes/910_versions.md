## Changes 

### Version 1 - June, 2020

- Linguistic resources for Text Normalization, Phonetization, Alignment and Syllabification of SPPAS for the following languages: cat, cmn, deu, eng, fra, fraquebec, hun, ita, jpn, kor, nan, pcm, pol, por, spa, vie, yue. 

- Data resources for face detection, face landmark and LPC automatic annotations of SPPAS.



### Version 2 - July 2020


- Updated data for face detection and face landmark.


### Version 3 - Sept 2020

- Updated linguistic resources of Polish language.
- Add of this documentation.


### Version 4 - Feb 2021

- A DNN model is added into Face Detection package
- The file fra.txt of LPC package is modified -- corrected by experts
- Lightness of LPC package hand pictures is adjusted


### Version 5 - Sept 2021

- New acoustic model of Italian: it's no longer a context-dependent model. It's a monophone model like for the other languages. The French HMMs of "a~" and "O~", and the Naija HMM of "e~" were added to the hmmdefs.
- The LPC file fra.txt is renamed cueConfig-fra.txt.
- The keys of the LPC vowels are coded with characters "b", "s", "m", "c", "t" instead of numbers. It's not compatible with versions 3.x of SPPAS.
- The keys of the LPC consonant are coded differently, we now use the same than the previously defined ones for English.


### Version 6 - Nov 2021

- New resources for Bengali language: vocabulary, pronunciation dictionary and acoustic model.




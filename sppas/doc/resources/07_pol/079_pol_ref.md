## References

**Brigitte Bigi, Katarzyna Klessa** (2015).
*Automatic Syllabification of Polish.*
In 7th Language and Technology Conference: Human Language Technologies 
as a Challenge for Computer Science and Linguistics, pp. 262-266, 
Poznań, Poland.

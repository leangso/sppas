## List of phonemes


#### Consonant Plosives

| SPPAS |  IPA  | Description           | Examples             |
|:-----:|:-----:|:---------------------:|:---------------------|
|   p   |   p   | voiceless bilabial    | passé, par, appris   |
|   b   |   b   | voiced bilabial       | beau, abris, baobab  |
|   t   |   t   | voiceless alveolar    | tout, thé, patte     |
|   d   |   d   | voiced alveolar       | doux, deux, addition |
|   k   |   k   | voiceless velar       | cabas, psycho, quatre, kelvin |
|   g   |   g   | voiced velar          | gain, guerre, second |


#### Consonant Fricatives

| SPPAS |  IPA  | Description            | Examples    |
|:-----:|:-----:|:----------------------:|:------------|
|   f   |   f   | voiceless labiodental  | fête, pharmacie |
|   s   |   s   | voiceless alveolar     | sa, hausse, ce, garçon, option, scie |
|   S   |   ʃ   | voiceless postalveolar | choux, schème, shampooing   |
|   v   |   v   | voiced labiodental     | vous, wagon, neuf heures    |
|   z   |   z   | voiced alveolar        | hasard, zéro, transit       |
|   Z   |   ʒ   | voiced postalveolar    | joue  geai                  |


#### Consonant Nasals

| SPPAS |  IPA  | Description            | Examples         |
|:-----:|:-----:|:----------------------:|:-----------------|
|   m   |   m   | bilabial               | mou, homme       |
|   n   |   n   | alveolar               | nous, bonne      |


#### Consonant Liquids

| SPPAS |  IPA  | Description            | Examples        |
|:-----:|:-----:|:----------------------:|:----------------|
|   l   |   l   | alveolar lateral       | lit, ville, fil |
|   R   |   ʁ   | voiced uvular          | roue, rhume, arrive |


#### Semivowels

| SPPAS |  IPA  | Description            | Examples      |
|:-----:|:-----:|:----------------------:|:--------------|
|   j   |   j   | palatal                | payer, fille, travail |
|   w   |   w   | voiced labiovelar      | oui, web, whisky | 
|   H   |   ɥ   | labial-palatal         | huit, Puy     |


#### Vowels for French (fra dictionnary)

| SPPAS |  IPA  | Description               | Examples           |
|:-----:|:-----:|:-------------------------:|:-------------------|
|   E   |   ɛ   | open-mid front unrounded  | crème, faite, peine, fête, maître, mètre |
|   A/  |   a   | open front unrounded      | patte, là          |
|   A/  |   ɑ   | open back unrounded       | pâte  glas         |
|   9   |   œ   | open-mid front rounded    | sœur, neuf, œuf    |
|   i   |   i   | close front unrounded     | si, île, régie, y  |
|   e   |   e   | close-mid front unrounded | clé, les, chez, aller, pied, journée |
|   O/  |   o   | close-mid back rounded    | sot, hôtel, haut   |
|   O/  |   ɔ   | open-mid back rounded     | sort, minimum      |
|   u   |   u   | close back rounded        | cou, clown, roue   |
|   y   |   y   | close front rounded       | tu, sûr, rue       |
|   2   |   ø   | close-mid front rounded   | ceux, jeûner, deux |
|   @   |   ə   | schwa                     | le, reposer, faisons |
|  a~   |  ã    | nasal | sans, champ, vent, temps, Jean, taon |
|  U~/  |  ɛ̃    | nasal | vin, pain, brin, printemps           |
|  U~/  |  œ̃    | nasal | un, parfum, brun                     |
|   O~  |  ɔ̃    | nasal | son, nom, bon |


#### Vowels for Quebec French

| SPPAS |  IPA  | Description               | Examples           |
|:-----:|:-----:|:-------------------------:|:-------------------|
|   E   |   ɛ   | open-mid front unrounded  | crème, faite, peine  |
|   a   |   a   | open front unrounded      | patte              |
|   A   |   ɑ   | open back unrounded       | pâte               |
|   2   |   ø   | close-mid front rounded   | deux               |
|   3   |   ɜ   | open-mid central unrounded | fête, maître, mètre |
|   9   |   œ   | open-mid front rounded    | sœur, neuf, œuf |
|   i   |   i   | close front unrounded     | régie |
|   I   |   ɪ   | near-close front unrounded | île |
|   e   |   e   | close-mid front unrounded | clé, les, chez, aller, pied, journée  |
|   o   |   o   | close-mid back rounded    | sot, haut, hôtel |
|   O   |   ɔ   | open-mid back rounded     | sort, minimum |
|   u   |   u   | close back rounded        | cou |
|   U   |   ʊ   | near-close back rounded   | clown |
|   y   |   y   | close front rounded       | tu, sûr, rue  |
|   Y   |   ʏ   | near-close front rounded  | thune, truc |
|   @   |   ə   | schwa                     |  |
|  A~   |   ɑ̃   | nasal                     | banc, sans, champ, vent |
|   E~  |   ɛ̃   | nasal                     | bassin, pain, brin |
|   O~  |  ɔ̃    | nasal                     | son, nom, bon    |
|  U~/  |   œ̃   | nasal                     | un, parfum, brun |



#### Fillers

| SPPAS | Description                     | 
|:-----:|:--------------------------------|
| laugh |  laughter                       |
| noise |  noises,  unintelligible speech |
| fp    |  filled pause ("euh")           |
| dummy |  un-transcribed speech          |



#### Other phonemes for French

The following list of phonemes are used only in the "fre" 
dictionnary, but not in the "fra" one.
There are all represented in the acoustic model, so any of
them can be used in any pronounciation dictionary.

| SPPAS |  IPA  | Description               | Examples           |
|:-----:|:-----:|:-------------------------:|:-------------------|
|   o   |   o   | close-mid back rounded    | sot, hôtel, haut   |
|   O   |   ɔ   | open-mid back rounded     | sort, minimum      |
|  e~   |  ɛ̃    | nasal                     | vin, pain, brin    |
|  9~   |  œ̃    | nasal                     | un, parfum, brun   |
|   N   |   ŋ   | voiced velar              | camping, bingo     |
|   J   |   ɲ   | palatal                   | gagne, pignon      |


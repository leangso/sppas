## Syllabification configuration file

The syllabification configuration file corresponds to the one described
in the paper (Bigi et al. 2010).
It was adapted to Quebec French by adding the missing vowels.
It is distributed under the terms of the 
*GNU General Public License*.




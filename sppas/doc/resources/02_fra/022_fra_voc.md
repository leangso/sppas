## Lexicons

All French lexicons are *(c) Laboratoire Parole et Langage, Aix-en-Provence, France*:

- `fra.vocab` contains a list of 345k different words;
- `fra.stp` contains a list of 65 stop-words;
- `fra.lem` is a list of words with their lemmas and occurrences;
- `fra_num.repl` allows to convert numbers to their written form;
- `fra.repl` allows to convert symbols and abbreviations into a text form.

All of them are distributed under the terms of the *GNU General Public License*.


## Acoustic Models

The French acoustic model was created by Brigitte Bigi from various corpora
mainly recorded at Laboratoire Parole et Langage. 
Special thanks are addressed to Roxane Bertrand, Béatrice Priego-Valverde, 
Sophie Herment, Amandine Michelas, Christine Meunier and Cristel Portes 
for kindly sharing their corpora. 
This model was evaluated in (Bigi & Meunier, 2018).

The model was updated in February 2022, by adding 31 minutes of manually 
time-aligned read speech of the CLeLfPC corpus, and by adding an HMM model
the phonemes: o, O, e~, 9~ and N. There's no evaluation of this new version.

The Quebec French acoustic model is based on the HMMs of the French model
and the missing vowels were picked up from the Deutch, the English and the
Polish models. This initial QF acoustic model created from the HMMs of 
other languages was then adapted to QF with a corpus Mélanie Lancien created 
for this purpose. This training subset is made of 273 seconds of speech - 
2390 phonemes, an extract of the PFC corpus. The model was evaluated in
(Lancien et al., 2020).

Both models are distributed under the terms of the 
*Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License*.


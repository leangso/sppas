## Dictionaries

There are 3 dictionnaries for French language:

1. fra.dict which is recommended for spontaneous speech;
2. fre.dict which is recommended for standard read speech;
3. fra_quebec.dict is for Quebec French.

The Quebec French pronounciation dictionary is 
*(c) Marie-Hélène Côté, Université de Lausanne*.
The other two dictionaries are *(c) Laboratoire Parole et 
Langage, Aix-en-Provence, France*. 
All of them are distributed under the terms of the 
*GNU General Public License*.

The 2 French pronunciation dictionaries were created by Brigitte Bigi by 
collecting and merging several free dictionaries loaded from the web. 

In the "fra" one, some pronunciations were added using the LIA_Phon tool. 
Many words were manually corrected and a large set of missing words and 
pronunciation variants were manually added. Moreover, the observed frequent
pronounciations in conversational corpora were added: it's mainly about
reductions. The following meta-phonemes are used:
A/ for both a and A; O/ for both o and O; U~/ for both e~ and 9~.
The following phoneme is not used: N, it is replaced by the sequence "n g".

In the "fre" one, only the "standard" French pronounciations are used. 
However, the only meta-phoneme is "A/". Moreover, "N" is used.

The Quebec French dictionary was created by Marie-Hélène Côté, Université de 
Lausanne. It was converted to the required format (.dict and SAMPA) by B. Bigi.
The version currently distributed is limited to 40k words. The full original
version (192k words) is available on-demand.




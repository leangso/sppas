## References

**Brigitte Bigi, Christine Meunier, Irina Nesterenko, Roxane Bertrand** (2010).
*Automatic detection of syllable boundaries in spontaneous speech.*
In Language Resource and Evaluation Conference (LREC), pp. 3285-3292, La Valetta, Malta.

**Brigitte Bigi, Christine Meunier** (2018). 
*Automatic speech segmentation of spontaneous speech.* 
Revista de Estudos da Linguagem. International Thematic Issue: 
Speech Segmentation. Volume 26, number 4, pages 1489-1530, e-ISSN 2237-2083.

**Mélanie Lancien, Marie-Hélène Côté, Brigitte Bigi** (2020).
*Developing Resources for Automated Speech Processing of Quebec French.*
In Language Resources and Evaluation Conference (LREC), pp. 5323–5328, Marseille, France.




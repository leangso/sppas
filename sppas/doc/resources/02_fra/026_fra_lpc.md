## Cued Speech

The resources for the automatic generation of Cued Speech keys are
under construction. They can only be used in order to help in their
development.
They are created by Brigitte Bigi in collaboration with Datha:
<http://www.datha.io>

The file is a set of rules in order to be used to convert a sequence
of phonemes into a sequence of keys.

They are distributed under the terms of the 
*GNU General Public License*.

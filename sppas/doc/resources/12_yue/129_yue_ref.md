## References

**Roxana Fung, Brigitte Bigi** (2015).
*Automatic word segmentation for spoken Cantonese.*
In Oriental COCOSDA and Conference on Asian Spoken 
Language Research and Evaluation (O-COCOSDA/CASLRE), pp. 196-201.


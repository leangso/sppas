## Available data 

### Description

- Repository: <https://www.ortolang.fr>
- Owner: Brigitte Bigi `contact@sppas.org`
- Permanent URL: <https://hdl.handle.net/11403/sppasresources>

There are 2 types of data in the Ortolang repository, separated into two folders.

The folder `lang` contains linguistic resources that are needed by 
language-dependent automatic annotations. For each language, it can 
include lexicons, pronunciation dictionaries, acoustic models and/or 
syllabification rules. For a given language, these resource files are 
compressed into a ZIP file. The language name is coded with the iso639-3 code 
- "fra" for French, "eng" for English, "cmn" for Mandarin Chinese, etc.
See <http://www-01.sil.org/iso639-3/> for the full list of all languages and codes.

The folder `annot` includes linguistic resources required by some automatic 
annotations like for example statistical models. The resources are available 
in compressed ZIP files, one for each supported annotation.

It should be noted that these resources were initially created to be 
used by SPPAS <http://sppas.org>.  They are open source which means they can 
be downloaded without limitation and used by any other annotation tool 
or anything else. All files can be edited, modified and most of the time 
re-distributed. Since SPPAS itself is a language-independent annotation program, 
you can make annotation of a language if given the appropriate language 
resources for the target language. The annotation accuracy largely depends 
on the resources. 


### Download and install

The "Download" button of the main page of the Ortolang website should **not**
be used. It will download the whole repository into a corrupted zip file that
can't be un-compressed. Instead, click on the "Browse" button and download the
individual ZIP file(s) you are interested in, from either the `lang` folder
or the `annot` folder or both.

To *install manually* the resources into SPPAS software tool, download and 
un-compress the ZIP file(s) you are interested in into the `resources` 
folder of the SPPAS package. 

To *install automatically* the resources into SPPAS software tool, it is not
needed to download them manually. Launch the graphical user interface with
the `sppas` command, then click on the "Annotate" button of the menu, then 
click "Add languages" or "Add annotations" button of the toolbar.
It'll open a dialog in which you'll check the languages or annotations 
you want. Then, click the "Install" button. You can also use the program
"sppas/bin/preinstall.py" in the command-line user interface.


## lang folder

### Overview

Automatic annotations proposed by SPPAS are implemented with
language-independent algorithms. The main and interesting consequence is
that adding a new language into SPPAS only consists in adding the required
linguistic resources related to the annotation:

- Text normalization requires lexicons;
- Phonetization requires a pronunciation dictionary;
- Alignment requires an acoustic model;
- Syllabification requires a configuration file with the rules.

In this document, all the table indicate the list of phonemes that are 
included in the resources required for phonetization, alignment and 
syllabification of a given language. The first column represents the symbols
used by SPPAS and the other columns are intended to help users to better
understand what it means.

The encoding of phonemes in SPPAS resources is based on a computer-readable
phonetic list of 7-bit printable ASCII characters: X-SAMPA. 
This Extended Speech Assessment Methods Phonetic Alphabet was developed in 
1995 by John C. Wells, professor of phonetics at the University of London.
X-SAMPA is a language-independent notation covering the entire 
International Phonetic Alphabet - IPA repertoire.

A plugin of SPPAS allows to convert time-aligned phonemes from X-SAMPA to IPA.

The acoustic models created by the author, Brigitte Bigi, were trained using
the HTK toolbox, version 3.4.1. 
"HTK has been developed by the Machine Intelligence Laboratory
(formerly know as the Speech Vision Robotics Group) at the
[Cambridge University Engineering Department](http://mi.eng.cam.ac.uk) (CUED)
and [Entropic Ltd](http://www.entropic.com).
Microsoft has now licensed HTK back to CUED and is providing support so 
that CUED can redistribute HTK and provide development support via the 
HTK3 web site." (source: <http://htk.eng.cam.ac.uk/>)
Notice that HTK is available for free download after registration and users 
must first agree to the license. It should be noted that the section 2.2 of 
the license terms mentions that HTK *"either in whole or in part can not be 
distributed or sub-licensed to any third party in any form."*

### Contribute

Each of the provided resources is imperfect... and could be improved. How?

1. Edit the file, apply your changes, e-mail your modified version;
2. Send new audio and transcribed data to re-train the acoustic model.

You can also create new resource files and share them! 
Altruism is resulting a quality of life... you can do it.

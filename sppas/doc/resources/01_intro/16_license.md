## Licenses

Most of the time, the distributed files are under the terms of the GNU General Public License.

However, some of the resources are under the terms of a more restritive license. Please, take care of it, particularly before re-distributing.



#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
    genresdoc.py
    ~~~~~~~~~~~~

    :author:       Brigitte Bigi
    :organization: Laboratoire Parole et Langage, Aix-en-Provence, France
    :contact:      contact@sppas.org
    :license:      GPL, v3
    :copyright:    Copyright (C) 2011-2018  Brigitte Bigi
    :summary:      Documentation generator for linguistic resources.

"""
import os
import sys
import glob
from subprocess import Popen, PIPE, STDOUT

PROGRAM = os.path.abspath(__file__)


# ---------------------------------------------------------------------------

MARKDOWN_EXT = ".md"
INDEX_EXT = ".idx"
VERSION = "3"

# ---------------------------------------------------------------------------


class sppasDocFiles(object):
    """Documentation explorer.
    
    :author:       Brigitte Bigi
    :organization: Laboratoire Parole et Langage, Aix-en-Provence, France
    :contact:      contact@sppas.org
    :license:      GPL, v3
    :copyright:    Copyright (C) 2011-2018  Brigitte Bigi

    Fix all files required to generate the documentation, and explore the
    documentation directory and folders.

    The file markdown.idx contains the list of folders of the documentation.
    It can be a 1 column file, or a two-columns file with ';' to separate
    the two columns.
    Each folder must also contain an index file with the list of markdown
    files.

    """
    # Documentation file explore
    BASE_INDEX = "markdown.idx"
    MARKDOWN_HEADER = "header.md"
    MARKDOWN_FOOTER = "footer.md"

    # Documentation generation
    LATEX_TEMPLATE = "sppasdoctemplate.tex"
    LATEX_FRONTPAGE = "frontpage.pdf"

    # -----------------------------------------------------------------------

    def __init__(self, doc_dir, doc_temp):
        """Create a sppasDocFiles instance.

        :param doc_dir: (str) Name of the directory of the markdown and index files
        :param doc_temp: (str) Name of the directory of the external template files.

        """
        self._doc_dir = doc_dir
        self._doc_temp = doc_temp

    # -----------------------------------------------------------------------

    @staticmethod
    def test_file(filename):
        """Test if a file exists.

        :param filename: (str) Name of the file to test.
        :raises: IOError

        """
        if os.path.exists(filename) is False:
            raise IOError("The file {:s} is missing of the documentation."
                          "".format(filename))

    # -----------------------------------------------------------------------
    # Getters
    # -----------------------------------------------------------------------

    def get_file(self, filename):
        """Return a file of the documentation. """
        new_filename = os.path.join(self._doc_temp, filename)
        sppasDocFiles.test_file(new_filename)
        return new_filename

    # -----------------------------------------------------------------------

    def get_base_index(self):
        """Return the file indicating the list of documentation folders. """
        filename = os.path.join(self._doc_dir, sppasDocFiles.BASE_INDEX)
        sppasDocFiles.test_file(filename)
        return filename

    # -----------------------------------------------------------------------

    def get_markdown_header(self):
        """Return the header markdown file. """
        return self.get_file(sppasDocFiles.MARKDOWN_HEADER)

    # -----------------------------------------------------------------------

    def get_markdown_footer(self):
        """Return the footer markdown file. """
        return self.get_file(sppasDocFiles.MARKDOWN_FOOTER)

    # -----------------------------------------------------------------------

    def get_latex_template(self):
        """Return the LaTeX template file. """
        return self.get_file(sppasDocFiles.LATEX_TEMPLATE)

    # -----------------------------------------------------------------------

    def get_latex_front_page(self):
        """Return the LaTeX front page file. """
        return self.get_file(sppasDocFiles.LATEX_FRONTPAGE)

    # -----------------------------------------------------------------------
    # Explore the documentation folders and files
    # -----------------------------------------------------------------------

    @staticmethod
    def get_index(dirname):
        """Return the filename of the index of a directory. """
        try:
            index_filename = os.path.join(dirname, sppasDocFiles.BASE_INDEX)
            sppasDocFiles.test_file(index_filename)
        except IOError:
            index_files = glob.glob(os.path.join(dirname, "*" + INDEX_EXT))
            if len(index_files) > 0:
                index_filename = index_files[0]
            else:
                raise IOError("No index found in {:s}.".format(dirname))

        return index_filename

    # -----------------------------------------------------------------------

    def get_idx_markdown_files(self, folder):
        """Return a list of markdown files.
        Search for an index file either in doc_dir/folder or in doc_dir.

        :param folder: (str) Folder to search for markdown files. If folder is
        None, search is performed directly in doc_dir.
        :returns: list of filenames

        """
        # Search the index file
        if folder is None:
            index_filename = self.get_index(self._doc_dir)
        else:
            index_filename = self.get_index(os.path.join(self._doc_dir, folder))

        # Get all files mentioned in the idx
        files = list()
        fp = open(index_filename, "r")
        for line in fp.readlines():
            line = line.strip()
            if ";" in line:
                tabline = line.split(';')
                filename = tabline[0]
            else:
                filename = line.strip()
            # Add its path to each file name
            if folder is not None:
                new_file = os.path.join(self._doc_dir, folder, filename)
            else:
                new_file = os.path.join(self._doc_dir, filename)

            if os.path.exists(new_file) is False:
                raise IOError("File {:s} is missing.".format(new_file))
            files.append(new_file)

        return files

    # -----------------------------------------------------------------------

    def get_doc_folders(self):
        """Return the list of folders of the documentation.

        This list is extracted from the BASE_INDEX file which should be included
        into the documentation directory.
        Instead, all 1st level folders will be explored.

        :returns: list of dirnames

        """
        folders = list()
        try:
            index_filename = self.get_base_index()
            fp = open(index_filename, "r")
            # Get all folders mentioned in the index file
            for line in fp.readlines():
                dirname = line.strip()
                # Add its path to each folder name
                new_folder = os.path.join(self._doc_dir, dirname)
                if os.path.exists(new_folder) is False:
                    raise IOError("Folder {:s} is indexed but is missing.".format(new_folder))
                folders.append(dirname)

        except IOError:
            # No base index. Explore folders
            for folder in os.listdir(self._doc_dir):
                try:
                    self.get_idx_markdown_files(folder)
                    folders.append(folder)
                except IOError:
                    pass
            folders = sorted(folders)

        return folders

    # -----------------------------------------------------------------------

    def get_all_md(self, header=True, footer=False):
        """Return the list of markdown files for a documentation directory.
        Explore 1st level folders (if any) OR the doc dir.

        :param header: (bool) Add an header markdown file into the list
        :param footer: (bool) Add a footer markdown file into the list

        :returns: list of filenames

        """
        files = list()

        # add the header file into the list
        if header is True:
            files.append(self.get_markdown_header())

        # get the list of all indexed folders in the documentation directory
        folders = self.get_doc_folders()

        # get all indexed files of each folder
        if len(folders) > 0:
            for folder in folders:
                new_files = self.get_idx_markdown_files(folder)
                files.extend(new_files)
        else:
            files.extend(self.get_idx_markdown_files(folder=None))

        # add the footer file into the list
        if footer is True:
            files.append(self.get_markdown_footer())

        return files


# ---------------------------------------------------------------------------
# Utility functions
# ---------------------------------------------------------------------------


def exec_external_command(command, result_filename):
    """Execute a command and check the output file.

    :param command: (str) The external command to be executed, with its parameters.
    :param result_filename: (str) Name of the expected output file of the command
    :raises: Exception

    """
    p = Popen(command, shell=True, stdout=PIPE, stderr=STDOUT)
    # p.wait()
    line = p.communicate()

    # Check output file
    if os.path.exists(result_filename) is False:
        raise Exception("Error: {}.".format(line[0]))

# ---------------------------------------------------------------------------
# Generate a documentation file from some markdown sources
# ---------------------------------------------------------------------------


def gen_pdf_file(doc, files, result_filename):
    """Execute pandoc to generate a documentation in PDF.

    :param doc: (sppasDocFiles) Documentation, used to get filenames.
    :param files: (list) List of filenames (markdown files)
    :param result_filename: (str) Name of the newly created PDF documentation file

    """
    if len(files) == 0:
        raise IOError("No markdown files to generate the documentation!")

    # The expected files to generate the PDF:
    latex_template = doc.get_latex_template()
    front_page = doc.get_latex_front_page()

    # Remove the existing file
    if os.path.exists(result_filename):
        os.remove(result_filename)

    # the command
    command = "pandoc"

    # the parameters
    command += " -N --template=" + latex_template
    command += ' --variable toc-depth=2'
    command += ' -V geometry:a4paper'
    command += ' -V geometry:"top=3cm, bottom=3cm, left=3cm, right=2.5cm"'
    command += ' --variable documentclass="report"'
    command += ' --variable classoption="twoside, openright"'
    command += ' --variable mainfont="FreeSerif"'
    command += ' --variable sansfont="FreeSans"'
    command += ' --variable monofont="FreeMono"'
    command += ' --variable fontsize=11pt'
    command += ' --variable version="' + VERSION + '"'
    command += ' --variable frontpage="' + front_page + '"'
    command += ' --resource-path=web'  # work only if pandoc > 2.0

    # the files and LaTeX options
    command += " "
    command += " ".join(files)
    #command += ' --latex-engine=xelatex'  # for old versions of pandoc
    command += ' --pdf-engine=xelatex'
    command += ' --toc'
    command += ' -o ' + result_filename

    print("Command to be executed: {:s}".format(command))

    # Execute the command
    # -------------------
    exec_external_command(command, result_filename)


# ---------------------------------------------------------------------------
# Main
# ---------------------------------------------------------------------------

if __name__ == "__main__":

    # fix directory for external files is ok.
    # doc_temp = "doc_temp"
    doc_temp = os.path.dirname(PROGRAM)
    if os.path.exists(doc_temp) is False:
        raise IOError("Directory {:s} of the documentation is missing. "
                      "Program halted.".format(doc_temp))

    # test if documentation directory for markdown files is ok.
    doc_dir = os.path.dirname(PROGRAM)
    print(doc_dir)
    if os.path.exists(doc_dir) is False:
        raise IOError("Directory {:s} of the documentation is missing. "
                      "Program halted.".format(doc_dir))
    
    doc = sppasDocFiles(doc_dir, doc_temp)

    # name of the resulting file
    result_filename = os.path.join(os.path.dirname(PROGRAM), "SPPAS-Resources.pdf")

    # A unique PDF documentation file is generated from all markdown files
    md_files = doc.get_all_md(header=False, footer=False)
    gen_pdf_file(doc, md_files, result_filename)


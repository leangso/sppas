## References

**Brigitte Bigi, Abiola S. Oyelere, Bernard Caron** (submitted).
*Resources for Automated Speech Segmentation of the African Language Naija (Nigerian Pidgin).*
LNAI, Springer.

**Brigitte Bigi, Bernard Caron, Abiola S. Oyelere** (2017).
*Developing Resources for Automated Speech Processing of the African Language Naija (Nigerian Pidgin).*
In 8th Language and Technology Conference: Human Language Technologies 
as a Challenge for Computer Science and Linguistics, pp. 441-445, Poznań, Poland.


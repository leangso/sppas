## About this documentation


This documentation is governed by 
[GNU Free Documentation License, version 1.3](https://www.gnu.org/licenses/fdl-1.3.en.html).
It will assume that you are using a relatively recent version 
of SPPAS. There's no reason not to download the latest version whenever 
released: it's easy and fast!

**Any and all constructive comments are welcome.**

## What is SPPAS?

### Overview

*SPPAS - the automatic annotation and analyses of speech* is a scientific 
computer software package.
SPPAS is daily developed with the aim to provide a robust and reliable
software. Available for free, with open source code, there is simply no other
package for linguists to simple use in both the automatic annotations of
speech, the analyses of any kind of annotated data and the conversion
of annotated file formats.
You can imagine the annotations or analyses you need, SPPAS does the rest!

**SPPAS ensures flexibility rather than a one-size-fits all approach in the 
implementation of the proposed features. There are no fixed or unique 
solutions, but a bunch of customizable features to make choices according 
to the user requirements.**

Annotating recordings is very labor-intensive and cost-ineffective since it
has to be performed manually by experienced researchers with many hours 
of work. As the primary functionality, SPPAS proposes a set of **automatic or
semi-automatic annotations of recordings**. 
In the present context, annotations are "defined as the practice of adding
interpretative, linguistic information to an electronic corpus of spoken and/or
written language data. 'Annotation' can also refer to the end-product of this
process" (Leech, 1997). 
SPPAS automatizes the annotation processes and allows users to save time.
In order to be used efficiently, SPPAS expects a rigorous methodology to 
collect data and to prepare them.

Linguistics annotation, especially when dealing with multiple domains, 
makes use of different tools within a given project. This implies a rigorous 
annotation framework to ensure compatibilities between annotations and time-saving. 
SPPAS annotation files are in a specific XML format with extension `xra`. 
**Annotations can be imported from and exported to a variety of other formats**
including Praat (TextGrid, PitchTier, IntensityTier), Elan (eaf), 
Transcriber (trs), Annotation Pro (antx), Phonedit (mrk), Sclite (ctm, stm), 
HTK (lab, mlf), subtitles formats (srt, sub), CSV files...

"[...] when multiple annotations are integrated into a single data set, 
inter-relationships between the annotations can be explored both qualitatively
(by using database queries that combine levels) and quantitatively 
(by running statistical analyses or machine learning algorithms)" 
(Chiarcos 2008). As a consequence, the annotations must be time-synchronized:
annotations need to be time-aligned in order to be useful for purposes such
as analyses. Some special features are offered in SPPAS for **managing 
annotated files and analyzing data**. Among others, it includes 
a tool to filter multi-levels annotations. Other 
included tools are to estimate descriptive statistics, to manage annotated files, 
to manage audio files, etc.
These data analysis tools of SPPAS are mainly proposed in the Graphical User 
Interface. However, advanced users can also access directly the Application 
Programming Interface, for example to estimate statistics or to manipulate
annotated data.


### User engagement

By using SPPAS, **you agree to cite a reference in your publications**.
The full list of references is available in Chapter 7.


### Need help

1. Many problems can be solved by updating the version of SPPAS.

2. When looking for more detail about some subject, one can search this
documentation. This documentation is available in-line - see the SPPAS 
website, it is also included in the package in PDF format.

3. There are: a F.A.Q., tutorials and slides in the SPPAS web site. 


### About the author 

Since January 2011, **Brigitte Bigi** is the main author of SPPAS. 
She has a tenured position of researcher at the French CNRS - 
[Centre National de la Recherche Scientifique](http://www.cnrs.fr/).
She's working since 2009 at Laboratoire Parole et Langage in Aix-en-Provence, 
France.

More about the author: 

- <https://sppas.org/bigi>
- <https://orcid.org/0000-0003-1834-6918>

Contact the author by e-mail:

- to improve the quality of the linguistic resources; 
- to add new linguistic resources; 
- to help in development; 
- to add new annotations or analysis methods;
- to propose to collaborate for a research purpose;
- to declare an issue;
- to send a constructive comment.

Do not contact the author if:

- you failed to install Python or wxPython;
- you didn't follow the tutorials;
- you didn't read both the documentation and the tutorials.

Possible e-mails are: 

- *contact@sppas.org* for general purposes;
- *develop@sppas.org* for developer questions or for a bug alert.


#### Contributors

Here is the list of other contributors in programming:

* April 2012-June 2012: Alexandre Ranson (1st website)
* April 2012-July 2012: Cazembé Henry (1st GUI)
* April 2012-June 2013: Bastien Herbaut (help system)
* March 2013-March 2014: Tatsuya Watanabe (annotated data support)
* April 2015-June 2015: Nicolas Chazeau (audio support)
* April 2015-June 2015: Jibril Saffi (annotated data support)
* April 2019-June 2019: Barthélémy Drabczuk (workspaces and num2letter)
* April 2020-June 2020: Laurent Vouriot (a SPEAKER annotation)
* April 2020-June 2020: Florian Hocquet (installer and video support)


### Licenses

SPPAS software, except documentation and resources, are distributed 
under the terms of the 
[GNU GENERAL PUBLIC LICENSE v3](https://www.gnu.org/licenses/gpl-3.0.en.html).

Linguistic resources of SPPAS are either distributed:

* under the terms of the "GNU GENERAL PUBLIC LICENSE, v3", or
* on the terms of the "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License".

See the documentation of the resources for details about individual license.

To summarize, SPPAS users are: 

- free to study the source code and the resources of the software they use, 
- free to share the software and resources with other people, 
- free to modify the software and resources.


### Supports

SPPAS is regularly supported by the Laboratoire Parole et Langage in many 
ways.

#### 2011-2012:

Partly supported by ANR OTIM project (Ref. Nr. ANR-08-BLAN-0239),
Tools for Multimodal Information Processing.
Read more at: <http://www.lpl-aix.fr/~otim/>


#### 2013-2015:

Partly supported by ORTOLANG (Ref. Nr. ANR-11-EQPX-0032) funded by the
« Investissements d'Avenir » French Government program managed by the
French National Research Agency (ANR).
Read more at: <http://www.ortolang.fr/>


#### 2014-2016:

SPPAS was also partly carried out thanks to the support of the
following projects or groups:

- CoFee - Conversational Feedback <http://cofee.hypotheses.org>
- Variamu - Variations in Action: a MUltilingual approach <http://variamu.hypotheses.org>
- Campus France, Procore PHC.


#### 2017-2020:

The introduction of Naija language is supported by the ANR
[NaijaSynCor](http://naijasyncor.huma-num.fr/).


#### 2019-2020:

The introduction of workspaces to manage files and the SPEAKER
annotation type were both supported by the Vapvisio ANR project 
(ANR-18-CE28-0011-01).


#### 2020:

Both the Proof of Concept of the Cued Speech automatic annotation 
and the package for the dependencies' installation was supported
by the LPL.



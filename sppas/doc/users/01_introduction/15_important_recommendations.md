## Main and important recommendations


### About files

There is a list of important things to keep in mind while annotating
with SPPAS. They are summarized as follows and detailed in the chapters 
of this documentation:

1. Speech audio files for automatic annotations:

    - only `wav` and `au` files are supported
    - only mono (= one channel) files are supported
    - frame rate is *preferably* 16000hz
    - bit rate is *preferably* 16 bits
    - good recording quality is expected. It is obviously required to
    never convert from a compressed file, as mp3 for example.

2. Annotated data files:

    - *UTF-8* encoding only

3. It is recommended to use only US-ASCII characters in file names, 
obviously it includes its path.


### About automatic annotations

The quality of the results for most of the automatic annotations is highly
influenced by **the quality of the data the annotation takes in input**.
This is a politically correct way to say: *Garbage in, garbage out!*

Annotations are based on the use of linguistic resources. Resources for
several languages are gently shared and freely available or downloadable. 
The quality of the automatic annotations is largely influenced by **the
quality of the linguistic resources**. 

### About linguistic resources 

**Users are of crucial importance for resource development**. 

Do not hesitate to help in checking resource files and sharing your corrections with the community.

The users of SPPAS are invited to contribute to improve them. 
They can release the improvements to the public, so that the whole community 
benefits. 

> Any help is welcome to improve existing resources or to create new ones. 

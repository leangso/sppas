## Features


### How to use SPPAS?

There are three main ways to use SPPAS:

1. The Graphical User Interface (GUI) is as user-friendly as possible. It
   requires wxPython to be installed, i.e. to check 'wxpython' feature  
   during the Setup step. 

    * double-click on `sppas.bat` file, under Windows;
    * double-click on `sppas.command` file, under MacOS or Linux.

3. The Command-line User Interface (CLI), with a set of programs, each one
   essentially independent of the others, that can be run on its own at the 
   level of the shell.

4. Advanced users can also access directly the Application Programming Interface - API.
   Scripting with Python and SPPAS provides the more powerful way.



### What SPPAS can do?

Features of SPPAS can be divided into 3 main categories: 

1. Annotate automatically or semi-automatically;
2. Analyze annotated data;
3. Convert from/to a large variety of file formats.

The SPPAS website contains the list of the features and how most of the
automatic annotations can be organized in an annotation workflow.
See <https://sppas.org/features.html>

## Getting and installing


### Websites

The website of SPPAS is located at the following URL: <https://sppas.org>

The releases and the source code are hosted by SourceForge at: <http://sppas.sf.net/>


### Download and install SPPAS

The main website contains the `Download` page to download recent versions
of the SPPAS software package and it describes the installation instructions.

#### Download and decompress the SPPAS package:

There is a unique version of SPPAS which does not depend on the operating system. 
SPPAS is ready to run, so it does not need elaborate installation. All you 
need to do is to copy the SPPAS package from the website to somewhere
on your computer. Choose *a location with preferably only US-ASCII 
characters in the name - it obviously includes the path*.
The package of SPPAS is compressed and zipped, so you will need to
*decompress and unpack* it once you've got it.

#### Install external programs

An installation guide is available and must be followed carefully.  
Notice that administrator rights can be required.

**To summarize it:**

- STEP 1: install the recommended version of **Python 3.x ONCE** (Windows/Linux)

- STEP 2:
  
  - either GUI: launch the `setup.bat` (Windows) or `setup.command` (Linux, MacOS) 
  to continue the installation and follows instructions;
  - or CLI: create a Python Virtual Environment with name **".sppaspyenv~"**, 
  then launch the program `sppas\bin\preinstall.py`. It allows installing 
  external programs to enable some features, and the linguistic resources. 

In case of difficulty arising from this installation, you're invited to consult 
the web first. It probably will provide the solution. If, however, the 
problems were to persist, ask a technician for help.


### The package

SPPAS is a *Research Software*, distributed in the context of the **"Open Science"**.
Then, unlike many other software tool, SPPAS is not distributed as an executable 
program. Instead, **everything is done so that users can
check / change operations**.

It is particularly suitable for automatic annotations: it allows anyone
to customize automatic annotations to its own needs and to implement its 
custom annotation solution.

The SPPAS package is a directory with content as files and folders:

- the `README.txt` file, which aims to be read!
- the files `setup.bat` and `setup.command` to install some external programs;
- the files `sppas.bat` and `sppas.command` to launch the Graphical User Interface;
- the `samples` directory contains data of various languages: they are proposed
  in order to test various features of SPPAS;
- the `demo` folder contains an audio file, its corresponding orthographic 
  transcription and the video file. It also contains a large amount of 
  annotations automatically created by SPPAS;
- the `plugins` directory with one plugin. Others are available on the website;
- the `workspaces` directory with an example of a workspace;
- the `sppas` directory contains the program itself, it's the SPPAS software itself;
- the `resources` directory contains data that are used by automatic annotations (lexicons, dictionaries, ...);
- the `documentation` directory contains:

    - the terms of the licenses
    - the printable documentation
    - the printable resources documentation
    - the printable XRA file format documentation
    - the printable version of the main reference published in "the Phonetician" journal
    - the orthographic transcription convention
    - the folder `scripting_solutions` is a set of python scripts corresponding 
      to the exercises proposed in the chapter "Scripting with Python and SPPAS"


### Update

SPPAS is constantly being improved and new packages are published 
frequently (about 10 versions a year). It is important to update 
regularly in order to get the latest features and corrections.

Updating SPPAS consists of downloading, decompressing and launching
the setup.

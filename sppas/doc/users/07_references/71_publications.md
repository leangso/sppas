## References

### How to cite SPPAS?

For a general citation of SPPAS, use the reference below.
A PDF version of this publication is available in the folder
`documentation` of the package.

> *Brigitte Bigi (2015).*
> **SPPAS - Multi-lingual Approaches to the Automatic Annotation of Speech**.
> In "the Phonetician" - International Society of Phonetic Sciences, 
> ISSN 0741-6164, Number 111-112 / 2015-I-II, pages 54-69. 


For a specific feature of SPPAS, like the automatic syllabification,
the forced-alignment, or for the request system, please refer to the
related specific publication. The full list of references and the 
corresponding PDF files are available on
[the author website](https://sppas.org/bigi), 
at the following URL:
<https://sppas.org/bigi/publications.html>


### SPPAS software description

> *Brigitte Bigi (2015).*
> **SPPAS - Multi-lingual Approaches to the Automatic Annotation of Speech**.
> In "the Phonetician" - International Society of Phonetic Sciences,
> ISSN 0741-6164, Number 111-112 / 2015-I-II, pages 54-69.

> *Brigitte Bigi, Daniel Hirst* (2013).
> **What's new in SPPAS 1.5?**,
> Tools ans Resources for the Analysis of Speech Prosody, Aix-en-Provence,
> France, pp. 62-65.

> *Brigitte Bigi* (2012).
> **SPPAS: a tool for the phonetic segmentations of Speech**,
> The eight international conference on Language Resources and Evaluation,
> Istanbul (Turkey), pages 1748-1755, ISBN 978-2-9517408-7-7.

> *Brigitte Bigi, Daniel Hirst* (2012)
> **SPeech Phonetization Alignment and Syllabification (SPPAS):
> a tool for the automatic analysis of speech prosody**,
> Speech Prosody, Tongji University Press, ISBN 978-7-5608-4869-3,
> pages 19-22, Shanghai (China).


### About linguistic resources


> *Brigitte Bigi, Abiola S. Oyelere, Bernard Caron* (2020).
> **Resources for Automated Speech Segmentation of the African Language Naija (Nigerian Pidgin).**
> Human Language Technologies Challenges for Computer Science and Linguistics,
> LNAI 12598, pp. 164-173, Springer, Heidelberg.

>*Mélanie Lancien, Marie-Hélène Côté, Brigitte Bigi* (2020).
>**Developing Resources for Automated Speech Processing of Quebec French.**
>In Proceedings of The 12th Language Resources and Evaluation Conference, pp. 5323–5328, Marseille, France.

>*Brigitte Bigi, Bernard Caron, Abiola S. Oyelere* (2017).
>**Developing Resources for Automated Speech Processing of the African Language Naija (Nigerian Pidgin).**
>In 8th Language and Technology Conference: Human Language Technologies as a 
>Challenge for Computer Science and Linguistics, pp. 441-445, Poznań, Poland.


### About Search for IPUs

>*Brigitte Bigi and Béatrice Priego-Valverde* (2021).
>**The automatic search for sounding segments of SPPAS: application to Cheese! corpus**
>Human Language Technologies Challenges for Computer Science and Linguistics,
>LNAI, Springer, Heidelberg.

>*Brigitte Bigi, Béatrice Priego-Valverde* (2019).
>**Search for Inter-Pausal Units: application to Cheese! corpus.**
>In 9th Language & Technology Conference: Human Language Technologies
>as a Challenge for Computer Science and Linguistics, pp. 289-293, Poznań, Poland.


### About Text Normalization


>Roxana Fung, Brigitte Bigi* (2015).
>**Automatic Word Segmentation for Spoken Cantonese**,
>The Oriental Chapter of COCOSDA (International Committee for the Co-ordination
>and Standardization of Speech Databases and Assessment Techniques).

>*Brigitte Bigi* (2014).
>**A Multilingual Text Normalization Approach**,
>Human Language Technologies Challenges for Computer Science and Linguistics LNAI 8387,
>Springer, Heidelberg. ISBN: 978-3-319-14120-6. Pages 515-526.

>*Brigitte Bigi* (2011).
>**A Multilingual Text Normalization Approach**,
>2nd Less-Resourced Languages workshop, 5th Language & Technology Conference, Poznan (Poland).


### About Phonetization


>*Brigitte Bigi* (2016).
>**A phonetization approach for the forced-alignment task in SPPAS**,
>Human Language Technologies Challenges for Computer Science and Linguistics, LNAI 9561, , pp. 515–526,
>Springer, Heidelberg. ISBN: 978-3-319-14120-6.

>*Brigitte Bigi* (2013).
>**A phonetization approach for the forced-alignment task**,
>3rd Less-Resourced Languages workshop, 6th Language & Technology Conference, Poznan (Poland).

>*Brigitte Bigi, Pauline Péri, Roxane Bertrand* (2012).
>**Orthographic Transcription: Which Enrichment is required for Phonetization?**,
>Language Resources and Evaluation Conference, Istanbul (Turkey),
>pages 1756-1763, ISBN 978-2-9517408-7-7.


### About Forced-Alignment

>*Brigitte Bigi, Christine Meunier* (2018).
>**Automatic speech segmentation of spontaneous speech**. 
>Revista de Estudos da Linguagem. International Thematic Issue: Speech Segmentation. 
>Editors: Tommaso Raso, Heliana Mello, Plinio Barbosa, 
>Volume 26, number 4, pages 1489-1530, e-ISSN 2237-2083.

>*Brigitte Bigi, Christine Meunier* (2018).
>**euh, rire et bruits en parole spontanée : application à l'alignement forcé**,
>Actes des 32èmes Journées d'études sur la Parole, 
>Aix-en-Provence, France.

>*Brigitte Bigi* (2014).
>**The SPPAS participation to Evalita 2014**,
>Proceedings of the First Italian Conference on Computational Linguistics
>CLiC-it 2014 and the Fourth International Workshop EVALITA 2014, Pisa (Italy).
>Editors R. Basili, A. Lenci, B. Magnini. ISBN 978-886741-472-7.
>Volume 2. Pages 127-130.

>*Brigitte Bigi* (2014).
>**Automatic Speech Segmentation of French: Corpus Adaptation**.
>2nd Asian Pacific Corpus Linguistics Conference, p. 32, Hong Kong.

>*Brigitte Bigi* (2012).
>**The SPPAS participation to Evalita 2011**,
>Working Notes of EVALITA 2011, Rome (Italy), ISSN: 2240-5186.


### About Syllabification

>*Brigitte Bigi, Katarzyna Klessa* (2015).
>**Automatic Syllabification of Polish**,
>7th Language and Technology Conference: Human Language Technologies as a Challenge for Computer Science and Linguistics, pp. 262–266, Poznan, Poland.

>*Brigitte Bigi, Caterina Petrone, Leonardo Lancia* (2014).
>**Automatic Syllabification of Italian: adaptation from French.**
>Laboratory Approaches to Romance Phonology VII, Aix-en-Provence (France).

>*Brigitte Bigi, Caterina Petrone* (2014).
>**A generic tool for the automatic syllabification of Italian**,
>Proceedings of the First Italian Conference on Computational Linguistics
>CLiC-it 2014 and the Fourth International Workshop EVALITA 2014, Pisa (Italy).
>Editors R. Basili, A. Lenci, B. Magnini. ISBN 978-886741-472-7.
>Volume 1. Pages 73-77.

>*Brigitte Bigi, Christine Meunier, Irina Nesterenko, Roxane Bertrand* (2010).
>**Automatic detection of syllable boundaries in spontaneous speech**,
>Language Resource and Evaluation Conference, pages 3285-3292, La Valetta, Malte.


### About Repetitions

>*Brigitte Bigi, Roxane Bertrand, Mathilde Guardiola* (2014).
>**Automatic detection of other-repetition occurrences: application to French conversational speech**,
>9th International conference on Language Resources and Evaluation (LREC), Reykjavik (Iceland), pp. 2648-2652. ISBN: 978-2-9517408-8-4.


### About analyses tools

>*Brigitte Bigi* (2019).
>**Filtering multi-levels annotated data.**
>In 9th Language & Technology Conference: Human Language Technologies as a 
>Challenge for Computer Science and Linguistics, pp. 13-14, Poznań, Poland.

>*Brigitte Bigi, Jorane Saubesty* (2015).
>**Searching and retrieving multi-levels annotated data**,
>Proceedings of Gesture and Speech in Interaction, Nantes (France).


### About the API

>*Brigitte Bigi* (2018).
>**Annotation representation and file conversion tool.**
>Contributi del Centro Linceo Interdisciplinare 'Beniamino Segre' 
>ISSN 0394-0705), 137, pp. 99-116.

>*Brigitte Bigi, Tatsuya Watanabe, Laurent Prévot* (2014).
>**Representing Multimodal Linguistics Annotated Data**,
>9th International conference on Language Resources and Evaluation (LREC), Reykjavik (Iceland), pages 3386-3392. ISBN: 978-2-9517408-8-4.


### Related references

#### Some results of analyses

>*Christine Meunier, Brigitte Bigi* (to be published).
>**Propriétés phonétiques et lexicales dans les variations temporelles des phonèmes en parole conversationnelle**,
>Studii de lingvistică, SL11: « Variation phonétique en parole continue »,
>coord.: Yaru Wu, Martine Adda-Decker, Lori Lamel, Ioana Vasilescu

>*Christine Meunier, Morgane Peirolo, Brigitte Bigi* (2020).
>**La mobilisation du tractus vocal est-elle variable selon les langues en parole spontanée ?**,
>In Journées d'Etudes sur la Parole, 33e édition, pp. 433-441, Nancy, France.

>*Béatrice Priego-Valverde, Brigitte Bigi, Salvatore Attardo, Lucy Pickering, Elisa Gironzetti* (2018).
>**Is smiling during humor so obvious? A cross-cultural comparison of smiling behavior in humorous sequences in American English and French interactions.**
>Intercultural Pragmatics. 10/2018; 15(4):563-591.

>*Brigitte Bigi, Roxane Bertrand* (2016).
>**Laughter in French Spontaneous Conversational Dialogs**,
>Tenth International Conference on Language Resources and Evaluation (LREC 2016), pp. 2168-2174, Portoroz, Slovenia.

>*Laurent Prevot, Jan Gorisch, Roxane Bertrand, Emilien Gorene and Brigitte Bigi* (2015).
>**A SIP of CoFee: A Sample of Interesting Productions of Conversational Feedback**,
>16th Annual Meeting of the Special Interest Group on Discourse and Dialogue,
>Prague (Czech Republic), pp. 149–153.

>*Brigitte Bigi, Katarzyna Klessa, Laurianne Georgeton, Christine Meunier* (2015).
>**A syllable-based analysis of speech temporal organization: a comparison between speaking styles in dysarthric and healthy populations.**
>Interspeech2015, Dresden (Germany), pages 2977-2981.

>*Marion Tellier, Gale Stam, Brigitte Bigi* (2013).
>**Gesturing While Pausing In Conversation: Self-oriented Or Partner-oriented?**,
>TIGER, Tillburg.

>*Laurent Prévot, Brigitte Bigi, Roxane Bertrand* (2013).
>**A quantitative view of feedback lexical markers in conversational French**,
>14th Annual Meeting of the Special Interest Group on Discourse and Dialogue,
>Metz, France.

>*Marion Tellier, Gale Stam, Brigitte Bigi* (2012).
>**Same speech, different gestures?**,
>5th International Society for Gesture Studies (ISGS), Lund, Sweden.


#### Some corpora


>*Birgit Rauchbauer, Youssef Hmamouche, Brigitte Bigi, Laurent Prévot, Magalie Ochs, Thierry Chaminade* (2020).
>**Multimodal Corpus of Bidirectional Conversation of Human-human and Human-robot Interaction during fMRI Scanning.**
>In Proceedings of The 12th Language Resources and Evaluation Conference, pp. 668–675, Marseille, France.

>*Béatrice Priego-Valverde, Brigitte Bigi, Mary Amoyal* (2020).
>**"Cheese!": a Corpus of Face-to-face French Interactions. A Case Study for Analyzing Smiling and Conversational Humor.**
>In Proceedings of The 12th Language Resources and Evaluation Conference, 
>pp. 467–475, Marseille, France.

>*Christine Meunier, Cécile Fougeron, Corinne Fredouille, Brigitte Bigi, Lise Crevier-Buchman, Elisabeth Delais-Roussarie, Laurianne Georgeton, Alain Ghio, Imed Laaridh, Thierry Legou, Claire Pillot-Loiseau, Gilles Pouchoulin* (2016).
>**The TYPALOC Corpus: A Collection of Various Dysarthric Speech Recordings in Read and Spontaneous Styles**,
>Tenth International Conference on Language Resources and Evaluation (LREC 2016), pp. 4658-4665, Portoroz, Slovenia.

>*Jan Gorish, Corine Astésano, Ellen Gurman Bard, Brigitte Bigi, Laurent Prévot* (2014).
>**Aix Map Task corpus: The French multimodal corpus of task-oriented dialogue**,
>9th International conference on Language Resources and Evaluation (LREC),
>Reykjavik (Iceland), pages 2648-2652. ISBN: 978-2-9517408-8-4.

>*Daniel Hirst, Brigitte Bigi, Hyongsil Cho, Hongwei Ding, Sophie Herment, Ting Wang* (2013).
>**Building OMProDat: an open multilingual prosodic database**,
>Proceedins of Tools ans Resources for the Analysis of Speech Prosody,
>Aix-en-Provence, France, Eds B. Bigi and D. Hirst, ISBN: 978-2-7466-6443-2, pp. 11-14.

>*Ellen Gurman Bard, Corine Astésano, Alice Turk, Mariapaola D'imperio, Noel Nguyen, Laurent Prévot, Brigitte Bigi* (2013).
>**Aix MapTask: A (rather) new French resource for prosodic and discourse studies**,
>Proceedins of Tools ans Resources for the Analysis of Speech Prosody,
>Aix-en-Provence, France, Eds B. Bigi and D. Hirst, ISBN: 978-2-7466-6443-2, pp. 15-19.

>*Sophie Herment, Anastasia Loukina, Anne Tortel, Daniel Hirst, Brigitte Bigi* (2012).
>**AixOx, a multi-layered learners corpus: automatic annotation**,
>Proceedings of international conference on corpus linguistics, Jaèn (Spain).


#### Other tools re-implemented into SPPAS

>*Maciej Karpinski, Katarzyna Klessa* (2018).
>** Methods, Tools and Techniques for Multimodal Analysis of Accommodation in Intercultural Communication**,
>CMST 24(1) 29–41 (2018), DOI:10.12921/cmst.2018.0000006


>*Dafydd Gibbon* (2013).
>**TGA: a web tool for Time Group Analysis**,
> Tools ans Resources for the Analysis of Speech Prosody, Aix-en-Provence, France, pp. 66-69.

>*D.-J. Hirst* (2011).
>**The analysis by synthesis of speech melody: from data to models**, 
>Journal of Speech Sciences, vol. 1(1), pages 55-83.

>*Daniel Hirst and Robert Espesser* (1993).
>**Automatic modelling of fundamental frequency using a quadratic spline function.**
>Travaux de l’Institut de Phonétique d’Aix. vol. 15, pages 71-85.

## Migrate to Python 3

**Python 2.7 reached the end of its life.**
This version expects Python to 3.x as Python 2.7 is no longer maintained.
No new bug reports, fixes, or changes will be made to SPPAS when used
with Python 2.

A new setup program allows to install external programs to enable some 
features of SPPAS. It includes 'wxpython' to enable the Graphical User 
Interface.


### SPPAS-3.0

(2020, 20th May)

#### Known bugs

- Does not support Julius 4.5+

#### GUI:

- The version based on wxPython 3 is deprecated. Instead, the one based
  on wxPython 4 is the default but some features are still not implemented.
- A large amount of debug of the GUI. Remaining bug: In the page 'Analyze', the view 
  "Multi-Player" does not properly update the scrollbar when needed.
- Page annotate: can display the content of any report of the list, can delete existing reports.

#### Development:

- new package "preinstall" and new script preinstall.py to install external programs
- Package "files" renamed to "wkps". Updated classes and methods for a better support
  of reading and writing wjson files; a version 2.0 is then introduced. Add of a "MISSING"
  state allowing to properly manage missing entries of a workspace, relative paths added, etc.
- config package, classes and methods fully verified/modified including a new sppasAppConfig() class
- Modified __main__ to enable to launch SPPAS GUI with the command: "python3 sppas"
- new bin "juliusdownload.py" to automatize the installation of julius under Windows.

#### Resources:

- Updated lexicon, dictionary and acoustic model for Naija Language.

#### Automatic annotations:

- Automatic alignment, when EOT, is rescueing a failed interval alignment with the "Tokens" tier instead of the default one.



### SPPAS-3.1

(2020, 10th July)

#### Annotations:

- new annotation FaceDetection to detect faces in an image
- new annotation FaceLandmark to find coordiates of 68 points on an image representing a face, and only one
- new annotation SpkLexRep to find what are the tokens or phases repreated from one file to another one of the same speaker
- linguistic resources are downloaded and installed with the setup or directly from Ortolang web site


#### Others:

- renamed .deps~ into .app~ 




### SPPAS-3.2

(2020, 4th September)

#### Known bugs

- Under Windows, when a ComboBox is clicked, the Log Window is focused.

#### Annotations

- better support of antx files
- new FaceTracking annotation to detect faces of a video, still under development, and not in the GUI.
- transfer the metadata of the input transcription to the output transcription. Reliable only for annotation formats that support metadata.
- face detection: sort detected faces by confidence score

#### Development

- bug correction and tests added
- debug of the features reading with UTF-8 encoding allowing the setup to work properly
- new videodata package to read and manipulate video files. 

#### GUI

- The Home page includes links to the SPPAS web site
- some wx.ComboBox replaced by our sppasComboBox to look the same on all platforms
- Keyboard shortcuts added to message dialogs:
	* ESC: Cancel
	* Return: OK
	* n: No
	* y: Yes
 - New keyboard shortcuts in the "Files" page of the main frame:
	* alt + a: add files
	* alt + e: export workspace
	* alt + i: import workspace
	* alt + s: pin&save workspace
	* alt + n: rename the workspace
	* alt + r: create a new reference
	* alt + f: check some files
	* alt + g: check all files
	* alt + l: link checked files to checked references

	
### SPPAS-3.3

(2020, 20th October)

#### Package

- ".logs" and ".trash" folders are no longer hidden. Now, they are named "logs" and "trash".

#### GUI

- The page Analyze is simplified: there's no longer a notebook to open files into tabs, 
and there's no longer different "views". Instead, 1/ to edit the text content of a file,
the page "Files" has a new button "Edit checked" in the top toolbar; and 2/ to view files 
in a time-line style, a new page "Editor" was created. The latter is still under development.

#### Annotations

- Support of versions 4.5+ of the Julius CSR Engine, used by the "Alignment automatic annotation.
- new FaceTrack annotation to detect faces into a video. Currently not fully tested.

#### Development

- new package "videodata" to support video files with opencv.
- preinstall: a different way to search for the python command. It should solve problems when the Python3 executable is "python" command instead of "python3".


### SPPAS-3.4

(2020, 4th December)

#### GUI

- web links to the reference publications are indicated for each automatic annotations, either in PDF or URL.
- the main app is no longer using the option "useBestVisual" - a user reported that the app crashes with anaconda/ubuntu if enabled.
- some debug and improvement of the page Analyze. Now, it looks better under Windows.
- the page Editor is continued. It allows to:

	1. open audio, video and transcription files (no limit on the number of files);
	2. play all audio and video files really synchronously: the max delay between media players is 10ms;
	3. a slider allows to select 5 different periods to play: either the whole duration, or the visible part, or the selected part, or the visible part until the selected part or the visible part after the selected part.
	4. scroll/zoom the files to change the visible part;
	5. display annotations both in a spreadsheet and in a timeline;
	6. select an annotation;
	7. edit the annotation content: a text entry allows to edit the labels of an annotation either in a serialized string, in XML or in JSON.
	8. add/delete/split annotations.
	9. save.

#### Development

- setup.bat was modified, expecting it can install wxpython even if python was not installed from the Windows store. not tested.
- analyze: added a new option in the filter system when using relations. It allows to crop annotations of X when they are ovelapping Y (overlaps, overlapped by and contains).


### SPPAS-3.5

(2021, 20th January)

#### GUI

- Page Annotate: the installation of new languages and new annotations is enabled.
- Page Analyze: 

	1. view of the waveform of audio files
	2. the files of the timeline can be sorted
	3. in the timeline, right-click on a transcription file to check-uncheck tiers

#### Annotations

- LexMetric: bug correction of the supported files. No remaining unjustified error message.
- FaceDetection: can detect faces on both an image or a video.
- FaceMark renamed FaceSights: can fix 68 sights of detected faces on both an image or a video.

#### Development

- two new scripts: 1/ to convert a folder of images into a video and 2/ to export a video into a folder of images



### SPPAS-3.6

(2021, 23th February)

#### Plugins

- cleanipus updated: solved import error and version updated to 1.1

#### Annotations

- INFO1220 migrated from report to logging
- <UNK> replaced by dummy for unknown phonetizations
- FaceDetection: more options, more models
- New annotation FaceIdentity to assign an identity on detected faces

#### Development

- bug correction in audioframes with an exception not properly raised
- anndata: sppasWEKA renamed to sppasTable and enhanced with export of TRA files -- Table Rich Annotations
- installer: added numpy to video feature with version > 1.20 due to the Windows 2004 update and its consequences...
- workspaces: when reading wjson, the references are properly assigned to files
- sppasLPC renamed to sppasCuedSpeech

#### CLI

- the script trstotable.py is replacing trstoweka.py
- other-repetition script can take a workspace as input 



### SPPAS-3.7

(2021, 14th April)

#### Annotations

- creating a merged file is enabled by default
- Overlaps: new INTERACTION annotation to create a tier with overlapping activities.
- Alignment: a better support of non us-ascii characters
- Stopwords: no remaining error about file extension.
- FaceDetection: the default is to load both a DNN and a HAAR model instead of two DNNs.
  Enlarged portrait size (if option is enabled).
- FaceIdent: can export a smoothed portrait video.
- CuedSpeech: can export the video with the position of the 5 vowels and 
  a number to represent the consonant during the audio key. The resulting tier
  with the key is made of two labels: the consonant number and the vowel number.
- OtherRepetitions: 3 more tiers about the OR source are created 
  (word strain, nb of labels, source type). Bug correction in the position of
  the first found echo.
- SelfRepetitions: 3 more tiers about the SR source are created 
  (word strain, nb of labels, source type)
- Linguistic resources and annotation resources are no longer downloaded from 
  ortolang but from gandi instead. Manual download is still on ortolang (when
  it works!)

#### Analysis

- filter system: New filter 'nlab' on the number of labels of annotations

#### Development

- bug correction in video coords reader
- videodata: the FPS is not converted to integer anymore, now it can be a float.
  Support of .mov files with H264 codec.
- updated sppasui.json to re-rank annotations of faces
- images: extended support with transparency and overlay added

#### GUI

- page files: bug correction when adding a folder, automatically adjust the 
  column width of the filename when adding new files
- page analyze: new filter added to the SingleFilter,
  modified files can be saved
- page editor: annotation list view and labels edit re-organized, a new rise 
  panel for the timeline view of file allows to see action buttons when 
  collapsed instead of when expanded. No remaining infinite loop (macos, linux)
  when displaying tiers. A proportional frame when reading a video.
- Problem with foreground color of lists solved (macos, linux)



### SPPAS-3.8

(2021, 26th May)

#### Annotations

- New annotation IVA - Interval Values Analysis. 
- Lexical Metric: Add the number of annotations and the number of labels in segments. Segments are fixed from a list of separators.
- Fill in IPUs: more options to fix shift start and shift end.
- CuedSpeech: bug correction in key generation at the end of an IPU with a consonant.
- CuedSpeech: return 2 different tiers with the key, either with a single combined label or with two different labels (one for C then one for V).
- Alignment: a better way to convert point to intervals in case of alignment without audio.
- Other Repetitions: optionally can add a tier with all echo candidates.
- Extended search for syllables tier name: it allows to apply TGA to any tier with 'Syll' in its name.

#### GUI

- buttons have new custom events: it allows to customize their look when mouse is entering, leaving, ...
- page editor: annotation boundaries can be moved. The selected bound is in green if several annotations share the same time value, or in red if the bound corresponds to only one time value. Both the midpoint and the radius can be modified. 
- page editor: can create an annotation in the timeline by dagging with the mouse.
- page editor: Some minor changes in the look of the page.
- page editor: bug correction with threads in video player under MACOS. Allows to play several videos at a time, like on the other systems.
- page editor: bug correction in the list of annotations when adding before.
- page files: bug correction when adding a non-valid file.
- page files: adding the parent folder '..' is forbidden.

#### Development

- preinstall: added linux+microsoft in the list of supported distrib for those who installed Ubuntu from the Windows Store
- a global sppasLogSetup() instance is declared in the config.
- the script slmtrain.py can take sentences from stdin and then write the probas on stdout.
- anndata: refactor of sppasRW into sppasTrsRW.
- anndata: added a trs_type in all reader/writer classes and in sppasTrsRW file properties. Allows to classify annotated files into: ANNOT, MEASURE or TABLE.
- annotations: by default, automatic annotations can only create files of the type ANNOT (except if overwrite the method).
- config: add a log error message if no write access to sppas directory.

#### Scripts

audioinfo: new options to get clipping values in details or no clipping values at all


### SPPAS-3.9

(2021, 6th July)

#### Annotations

- More logging messages for a better understanding of the automatic annotations procedures
- bugs corrections in RMS and SpkLexRep
- The "merge" can now be checked/unchecked in the page to annotate. If checked, it merges only checked files of each root.

#### GUI

- Under MacOS, the GUI application is not crashing anymore on exit - previously it was randomly either not crashing, or bus error or segmentation fault
- Make the GUI work correctly with the MacOS Dock
- Page File: Edit files button is moved to the vertical toolbar and two buttons are added to remove missing files and to add all files sharing checked roots
- Page Editor: An elaborated search dialog is available in the editor in order to find label patterns in annotations of one or several tiers


#### Development

- setup for Windows is simplified
- a new class autils.SppasFiles() allows a better control of the files to manage in the automatic annotations
- annotations: No longer required vs optional inputs. A more generic solution is used instead in all the annotations

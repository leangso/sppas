## Stable


**Python 2.7 is no longer supported.**


### SPPAS-4.0

(2021, 5th October)

** One of Python 3.6 or 3.7 or 3.8 is required.**


#### GUI

- sppas.bat: do not launch pythonw3 anymore but python3 in order to display error messages if any
- GUI for wx3+py2 is definitively removed
- A new "Colored" icons theme
- Translation enabled under MacOS
- Bug correction: the settings are not modified anymore when cancel is clicked
- Fade in and fade out of dialog windows can be fixed into the settings
- The splash delay can be fixed into the settings
- The Page Editor can play synchronously audio and video files
- Page Editor: Bug correction in displaying the waveform. It is now properly synchronized on the timeline.
- Page Editor: Bug correction in the annotation label editor which can now write special chars - very useful for an enriched transcription.
- Page Files: Remove missing files also remove remaining missing roots and paths.


#### Development

- config: simplified use of locale. We now use language but not country (en_US => en ; fr_FR => fr)
- gui: the application has a new custom splash with a gauge to indicate the loading progress
- gui: the install app is updated -- code cleaning, better macOS support, etc
- annotations: bug correction in Face Detection because in some videos an image of the buffer can be None
- annotations: bug correction of input pattern management in LPC, Syllabify, FaceIdent, Repet
- annotations: JSON config file modified for LPC and Syllabify
- tips moved to structs package (they are now unused)
- ui: only one gettext message definition of _ in init. No longer a "msg" definition in each class...
- ui.players: code re-organized, class diagram simplified, package moved into ui.phoenix
- videodata: a faster solution to seek at a pos when the pos is closely later
- preinstall: the installer uses sys.executable to get the appropriate python


#### Annotations

- CuedSpeech: use a different code to represent vowels and consonants. 
The new configuration file is not compatible with previous versions of SPPAS.



### SPPAS-4.1

(2021, 25th November)

#### Known problems
- 'audioplay' feature is not available for python 3.9 or 3.10 because simpleaudio
pypi package is not available. Another solution has to be found...
- opencv-contrib 4.5.4+ is not supported. The authors changed something in the
library. Some tests have to be performed to find what exactly changed and how 
SPPAS can deal with. opencv-contrib is required by the 'video' feature.

#### Package

- The XRA 1.5 scheme is described in a PDF documentation file (16 pages)
- The package includes the PDF of the  documentation of resources
- The package includes a PDF with the workflow of 19 auto. annotations

#### GUI

- page edit: the waveform is not drawn in an over-period anymore.
- page edit: new keyboard shortcuts in the timeline to:

  - zoom in (ctrl+i), 
  - zoom out (ctrl+o), 
  - scroll before (ctrl+left) 
  - scroll later (ctrl+right)
  - select the next annotation (Alt+RIGHT)
  - select the previous annotation (Alt+LEFT)

#### Annotations

- Search for IPUs: the min sil value is taking into account the shift values. 
In the new results, none of the silences will have a duration less than the given one.
- By default, the Face Detection, Face Landmark and Person Face Identity are creating 
an XRA file instead of a CSV one.
- Person Face Identity is creating a video and an XRA of the portrait of each identified person. 
It also has a better selection of identities, based on both coordinates overlap and image similarity.
- The "do merge" option is turned off by default when using either the GUI or the CLI
- An annotation development progress indicator is introduced. This is a percentage value 
defined into the annotation JSON files and displayed in the GUI if no reference is available.
- Person Face Identity: two different videos are created when 'tag' option is enabled, one 
without the tagged face and one with.

#### Resources

- Support of Bengali language.

#### Development

- an exception is raised at the __init__ stage if python is deprecated.
- anndata.annlocation: bug correction in filtering system with overlaps intervals
- anndata: Two new types are introduced for a sppasTag: it can be a "fuzzy point"
or a "rectangle"
- anndata: A sppasLabel has a new member 'key' (string).
- new XRA format. Version 1.5 can take into account the 2 new types of a sppasTag and the 'key' attribute of a label.
- bug correction in the workspaces: the application captures the ValueEror 
that is raised when using "relpath" on Windows OS if the path is on a different drive.
- bug correction in the workspaces when adding brothers in roots
- Less restrictions in the definition of a pattern filename (now it can have a single char). 
- "sppas" is not longer added in the python path by the __init__
- models package migrated into 'Align' to provide circular imports. The training of ACM models is updated.
- Bengali language added to the num2letter module
- fileutils.py migrated into utils package; makeunicode.py migrated into config package
- annotations: bug correction with exception EmptyOutput and an added message in TextNorm and Phon (name of the tier).
- structs: bug correction in lang list. It isn't ignoring the resource name anymore.
- videodata: bug correction in the videowriter.set_size() in order to write videos of a custom size.
- sppas.json renamed sppasmeta.json with a few meta added
- install: takes the version into account when installing a pypi package
- setup.bat: add a 5 seconds delay after wx is installed in order to let Windows OS realize that something changed...


### SPPAS-4.2

(2022, 28th January)

#### Known problems

- This version (and the previous ones) does not work with Python 3.10.x
- 'audioplay' feature is not available for python 3.9 or 3.10 because simpleaudio
pypi package is not available. Another solution has to be found... 
- **The use of Python 3.6, 3.7 or 3.8 is then recommended**.

#### Package

**Important** The setup is now creating a Python Virtual Environment in the
hidden folder .sppaspyenv~. Then, the setup has to be launch each time a new
SPPAS package is installed.
The sppas commands are launching the GUI with Python of this virtual environment.

#### Annotations

- Syllabification: a new option to output one more tier with syllable structures
- CuedSpeech: code cleaning and re-organization, two more tiers are created: 
HandShapes and HandPositions. They are predicting respectively the moments D1-D2
(change of shape) and M1-M2 (change of position). The prediction model is a set
of rules based on the work described in (Attina, 2005), page 117 and page 136.
It can export a video with badges moving and standing depending on this model,
or with pictures of a hand when the debug logging level is set (in the .app~ file).
- FaceDetection: can choose the model(s) to enable or disable in the options. 
MediaPipe Face Detection is added to one of the possible recognizers - it's 
very fast but results are worse than other models.
- FaceSights: MediaPipe Face Mesh is used to predict the 68 face sights 
(weight=6) interpolated with OpenCV LBF (weight=1). No longer use of the 
DAT model nor the empirical sights.
- PersonFaceIdentity: A new filter is added to remove un-relevant known identified
persons, when number of stored images is less than 1/3 of the expected ones.
Added options: mincoords, minrefcoords, mindistcoords, mindistimgs, nbfrimg.
And now, the person is identified with the automatic face recognizer first,
then with the coordinates instead of the contrary.

#### GUI
 - Page Edit: new buttons in the player control panel to play one frame of a video in both ways (next or previous frame)
 - Page Files: a better view of the filename in the tree, particularly under Windows, 
and the pattern is now in a specific column.
 
#### Development
 - the "video" feature is now also requiring MediaPipe - https://mediapipe.dev/#!
 - workspaces: extended max length of the key of an attribute. It was 12, it's 20.
 - features.ini and object detection: can support all versions of opencv-contrib. They changed the output of the detection data structure at version 4.5.4. 
 - imgdata: the objects detection can load all models then enable or disable any of them.
 - Sights and SightsWriter migrated into imgdata package. 
 - sppas.command is 'cd' to $SPPAS in order to make sppas.command working properly 
with the Finder on MacOS and is creating a 'symlinks' virtual environment on MacOS 
because it does not support the 'copies' option
 - setup.bat: search for python.exe first.


### SPPAS-4.3

(2022, 22th March)

#### Known problems

**This version has an "import" bug: 
it does not start at all on some computers**.

#### Resources 

- Updated French acoustic model, added French pronunciation dictionary for standard 
  French with name fre.dict, and updated documentation of French resources.

#### Annotations

- MediaPipe (https://mediapipe.dev/) can be used by Face Detection and is used by default
  in the Face Landmarks automatic detection. It is installed in order to enable the
  video feature.
- Face landmarks: smooth sights trajectory
- Search for IPUs: the default output pattern is -ipus
- RMS: can optionally add 2 tiers with min and max value of each interval
- CuedSpeech: a new tier is created with the coordinates of the vowels (if sights are given)
- If any, add properly the language in the metadata of the output transcription file, and 
add the linguistic resource filename in the metadata of the tier.

#### Development

- anndata: bug correction when reading and writing .lab files
- plugins: audioseg bug correction when searching for the annotated file and a new 
option is added.
- plugins: classphones bug correction in imports and new options.
- GUI-Edit: the player control can play frame-by-frame with a step value, not only 1.
- facesights & faceidenity: smooth points trajectory with another algorithm
- cuedspeech: what, when and where files separated in 3 packages

#### GUI

- updated icon: a more modern version with fewer colors
- updated splash image in order to add a representation of both a video and a hand
- plugins: solved a problem of page refresh under Windows when adding or deleting a plugin

#### Scripts

- formants.py creates a file with f1, f2, f3, f4 of all phonemes. It requires 
Praat and to install the pypi of ParselMouth.
- saplay.py plays an audio file or a part of it. It uses either pyaudio or 
simpleaudio - one of them must be installed with pypi.
- img_to_cartoon.py is able to cartoonize an image or a video.
- sppasseg.py allows to perform the full automatic segmentation process. It's a 
three automatic steps process:

  1. Prepare the data (if required):

      - Nothing is done if the ortho. transcription is already time-aligned into
        the IPUs;
      - Fill in IPUs if the ortho. transcription is already available but not
        time-aligned. Notice that it can't work if the recording protocol was
        not very very very strict: it works only if the number of sounding
        segments EXACTLY matches the given number of transcribed segments.
      - Search for IPUs and auto. transcribe with the STT DeepSpeech. It
        requires installing DeepSpeech in the currently used Python and to give
        the path of the models. Get it here (DeepSpeech + English model):
        https://github.com/mozilla/DeepSpeech/releases/
        or here for another language:
        https://discourse.mozilla.org/t/links-to-pretrained-models/62688

  2. Perform Speech Segmentation:

      - no option to configure the annotations. They are all fixed internally!
      - can estimate an intensity coefficient;
      - can predict silent pauses (the ones into the IPUs).

  3. Format the output (if asked):

      - can map the phoneme into another set (IPA, Praat, ...) if a mapping
        table is given. It's a file with two columns, both separated by a
        whitespace: 1st is SPPAS phoneme, 2nd is expected phoneme.
      - can create a data structure with all the results and write into a JSON
        file.


### SPPAS-4.4

(2022, 5th April)

#### Known problems

The use of Python 3.6 or 3.7 is possible but **Python 3.8 is recommended** 
because:
  - the feature "audioplay" can't be enabled with a more recent version,
  - recent versions of MediaPipe have better results and the most recent 
    one is available for python 3.8. 
  
#### Annotations

- Hand&Pose: a new automatic annotation of an image or a video. SPPAS is a 
wrapper for MediaPipe Hand detection and Mediapipe Pose detection. It also 
proposes a custom solution in order to detect right-left hands of a person.



### SPPAS-4.5

(2022, 15th April)

#### Known problems

For a full experience of SPPAS **using of Python 3.8 or 3.9 is recommended**. 
The use of Python 3.6 or 3.7 is possible.

#### Install

- Under Windows, pipwin is installed and used when pip failed to install
  a package. pipwin installs unofficial python package binaries for Windows 
  provided by Christoph Gohlke here: 
  <http://www.lfd.uci.edu/~gohlke/pythonlibs/>
- Both simpleaudio and PyAudio are installed - if available for the OS. 
  The "audioplay" feature is enabled if one of them is successfully 
  installed.

#### Package

- a demo is added with all its annotated files. Only the orthographic
  transcription was performed manually. SPPAS did the rest.

#### Resources

- updated French dictionary (fra.dict)

#### GUI

- Page Edit: select the nearest annotation when clicking in a hole of a tier
- Page Analyze: when a tier is copied/paste, its media and controlled vocab
  are also copied/paste to the destination file.

#### Annotations

- Alignment: add the aligner name in the metadata of the result, i.e. either 
  julius, hvite or basic.
- FaceSights: bug correction when writing coordinates in a CSV file.



### SPPAS-4.6

(2022, 22th August)

#### Known problems or bugs

The French resources distributed in the package are corrupted. They have
to be re-installed in order to perform 'Alignment' annotation.

For a full experience of SPPAS **using of Python 3.8 is recommended**
because some of the dependencies are no longer maintained and no new 
release is published. Other versions of Python are possible but without
any warranty.

#### Annotations

- CuedSpeech is continued. The Proof of Concept is turned into some
stable and tested packages but it is not yet a version 1.
- FaceIdentity: new option to shift the portrait/selfie at left or right
- FaceIdentity and FaceSights: debug of the smoothing. The points are
no longer shivering.
- HandPose: bug correction when writing hand sights for an image for XRA format
- Updated demo files with the results of the automatic annotations

#### Plugins

- The plugins folder migrated from the base directory to the sppas directory;
and each plugin is now a python package.
- audioseg: A version 3.0 is released, an option "video" was added in order 
to segment both the audio and the video into tracks. Track filenames start 
with an index instead of the interval number.

#### Resources

- updated French dictionary (fre.dict) with new entries added and
with "J" instead of "n j" in all words containing "gn"

#### Development:

- anndata.tier: bug correction in index() method in order to search for 
the index of a point in a point-tier.
- preinstall: a new key "pip_opt" in the features.ini file. The installer is now
trying pip with option '--pre' if the installation of the stable version failed'. 
The pip_opt allows to set options to pip. It is used by wxPython to fix the 
pre-build host.
- imgdata.coordinates: added a xy ratio in parameters of portrait() in order to 
force a ratio between width and height. Default is 14:16.
- anndata.label: can optionally add the same tag into a label, without adding their scores
- anndata.aio.praat: an appropriate error message in case of chronological textgrid file.


### SPPAS-4.7

(2022, 12th October)

#### Annotations

- CuedSpeech is continued. The transition duration of the shape is 
reduced (d2 is before) and the hand picture is not blur on the video when 
the position is changing.
- Text Normalization: a new option is used by default to estimate the number of 
tokens in each IPU but only if the token is speech, events are ignored (i.e. dummy, 
noise, laugh, pause and silence).

#### Development

- Python 3.10 compatibility.
- Bug correction with imports if video is not enabled.
- Bugs correction in workspaces: Can append a file with a whitespace before 
the pattern, like "toto - Copie.txt" and the relative path should work properly.
- Installation: bug correction with quotes when launching a command that solved
the installation problem - setup.bat does not need to be launch twice anymore, 
once is enough!
- Installation: do not stop an installation if a pip package fails to be updated.
- Script mlf2labs.py: a new script to split an MLF file into several lab files.
- The custom event's management changed in the Phoenix UI.

#### GUI

- There's no longer a LogWindow in the installation wizard.
- Bug correction in slider and progress dialog (mainly used by the Edit page).
- Revised toolbars.
- Page Files: 2 buttons moved from associate to files (remove missing, add bros) 
and the delete button migrated from files to associate.
- Page Editor: Search dialog debugged.

#### Resources

- Updated list of phones for Bengali language, updated dictionary and acoustic model


### SPPAS-4.8

(2022, 14th December)

#### Development

- Break the dependency to wx when importing annotations. The SPPAS API can 
  then be used without requiring wx to be installed.
- the script evalsegmentation.py is more generic (vowel and consonants lists), 
   boxplots are in case insensitive order, can deal with phonemes with a '\' 
   and do not remove R files.
- anndata.aio: bug correction when reading CSV. Now, use the Python CSV reader.
- Updated SWHIG

#### Annotations
 
- Alignment: bug correction when using HVite instead of Julius.
- The transcribed tier -- with orthographic transliteration, can't have a name with "ipu" anymore. 

#### Resources

- Added an acoustic model of Persian language for Alignment 
- A new acoustic model for English, with phonemes of British English added, and the full model re-trained.
- Updated French vocabulary and pronunciation dictionary

#### Installation

- Under Windows, do not stop an installation if pipwin fails to be installed or 
  updated.
- Under MacOS: Homebrew is removed of the automatic setup. Users will have to 
  install it themselves.


### SPPAS-4.9

(2023, 10th February)

Web site url has changed: <http://sppas.org/> is replacing 
http://www.sppas.org/. An automatic redirection is maintained.

#### Development

- new ui package: htmlmaker is a package to work with a tree of HTML nodes 
  and to serialize it into an HTML page.
- updated SWHIG, and files README, AUTHORS and LICENSE of "src" folder
- a standard codemeta.json is added at the root of the "src" folder. It replaces
  the sppas.json file that was previously localed in "etc" folder.
- anndata: bug correction when reading and writing a CSV (a problem with quotes
  was solved).
- all available features are saved into the .app~ file, with true or false value 
  to mention that they are enabled or not.
- preinstall: catch the exception "http.client.HTTPResponse" which can occur if 
  unstable internet connection when downloading an annotation (because too large 
  zip file).

#### Resources

- updated English resources, the resources' documentation indicates the list 
  of phonemes, including the ones of British English. 
- updated French resources

#### GUI

- a major bug correction: when the workspace is modified in a page, the other 
  ones are properly informed and taking into account the new version. 
- Page Edit: a major bug correction allows to play audio with "pyaudio" under
  windows, and not only once like previously!



### SPPAS-4.10

(2023, 10th March)

Web site url has changed: <https://sppas.org/> is replacing 
http://sppas.org/. An automatic redirection is maintained.

> Major change: The Setup is launching a SPPAS Web-based application, 
> instead of the one based on WxPython.

#### GUI

- The *Setup* program has been fully re-implemented with a Web-based User 
  Interface. As a consequence, it does not require wxPython to be installed.
  Notice that it must be launched within the SPPAS python virtual environment.
  **Firefox is highly recommended**, none of the other web-browsers was tested. 

#### Development

- new ui package 'swapp': for SPPAS Web-based APPlication. Already contains
  a "httpd" package to implement a server and an handler, and an "app_setup" 
  package for the SPPAS Setup, with main program, main settings, etc.
- ui.phoenix was renamed to ui.wxapp -- which means WX-based sppas application, 
  to be consistent with 'ui.swapp'.
- Migrated sppas/bin/preinstallgui.py into ui.wxapp package. It is intended to
  be removed in next version, like the WX-based setup app.
- The custom settings of user interfaces are stored in wxapp and swapp folders 
  instead of sppas base directory.
- preinstall: The installer system was entirely revised. An alternative list of
  pip packages can be defined, the progress indicates absolute percentage, and
  the installation error is not raised anymore if the package was installed but
  returned a warning/error message.

#### Annotations

- The diagnosis of the file is made on the exact filename without lowering 
  the extension like it was previously done, because the file extensions for
  the annotations are *case-sensitive*.

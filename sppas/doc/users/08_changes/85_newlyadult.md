## The stabilization phase


### SPPAS-1.8.0

(2016, 30th August)
 
#### GUI:

- Design fully revisited and tested under Linux Mint, Windows 10 and MacOS 10.9

#### Development:

- SLM package created: can estimate a statistical language model (without 
  smooth method) on a small corpus

#### Automatic annotations:

- Add a diagnosis of files
- Tokenize extended: applied also on alternative labels
- Phonetize extended: applied also on alternative labels
- Alignment code cleaning and partly re-implemented
- Add "Chunk alignment"
- Use of a .ini file to configure each annotation instead of a sppas.conf file


### SPPAS-1.8.1 

(2016, 28th November)

A few tutorials are available on the web site.

#### Automatic annotations:

- Align: an ActivityDuration tier can be optionally added.
- Support of 3-columns tab-delimited files with .txt extension. It allows the
  compatibility with Audacity Label track files.
- Acoustic models training validated.

#### Resources:

- Catalan: new pronunciation dictionary and new acoustic model.


### SPPAS-1.8.2 

(2017, 18th January)

#### Analysis:

- debug of DataFilter

#### Resources:

- French vocabulary and dictionary updated

#### Development:

- new plugins package with a new plugin manager
- GUI integration of this new plugins system
- some unittest appended and all existing ones updated
- annotationdata.io renamed annotationdata.aio
- docstrings of some packages converted from epytext to reST syntax

#### GUI: 

- DataStats, DataFilter and DataRoamer toolbars don't scroll anymore
- Themes management changed.
- Main font is managed by the Themes.


### SPPAS-1.8.3 

(2017, 10th March)

#### Development:

- Elan reader highly improved (faster reader).
- updated plugins


### SPPAS 1.8.4 

(2017, 10th April)

#### Development:

- Elan writer modified: create a time slot for each localization.


### SPPAS 1.8.5 

(2017, 20th April)

#### Development:

- Vizualizer renamed Visualizer
- AudioRoamer: bug with an icon corrected
- New Phonedit mrk format support.
- Updated AnnotationPro Antx reader/writer


### SPPAS 1.8.6 

(2017, 19th June)

#### Resources:

- Polish dictionary and acoustic model updated.


### SPPAS 1.9.0 

(2017, 28th July)

#### Programming:

- Relative imports used in the standard way for Python
- PEP 8 code style (except for wxgui/annotationdata)
- PEP 257 reST code documentation style (except for wxqui/annotationdata)
- Unittests:

      - existing tests verified, improved, extended
      - new tests added
      - tests migrated into the packages
      
- Compatibility for both Python 2.7 and Python > 3.2:

    - makeunicode.py contains functions and classes to deal with strings
    - utils, term, structs, resources, presenters, plugins, calculus packages:
      migration is done
      
- Exceptions are separately managed
- Introduction of a system for the internationalization of the messages. Done
  in English and French for the packages: audiodata, calculus, plugins, 
  resources, structs, term, utils
- Package re-organization:

    - new package "models", with acm and slm
    - utils 
    - resources
    - ...
    
- meta.py is replacing sp_glob.py
- new scripts: tieraligntophon.py, dictmerge.py
- new bin: pluginbuild.py
- Robustness to read malformed HTK-ASCII pronunciation dictionaries.
- Bug corrected in the management of pronunciation variants
- re-structured package TextNormalization
- re-structured package Repetitions
- new version of the plugins: updated and debugged.

#### Resources: 

- Add support of Naija language (pcm)
- English-French mapping table updated

#### Annotations:

- Tokenizer renamed into Text Normalization:

    - a lot of debug mainly for English language, and punctuation managements
    - new option: can output a customized tier.
    
- Repetitions:

    - some debug
  
#### Communication:

- Add a description document of the orthographic transcription convention
  in the package.
- Web page updated, new tutorials available
- Documentation updated
- Better information about the licenses


### SPPAS 1.9.1 

(2017, 1st September)

#### Programming:

- Bug correction in the ELAN reader.
- Bug correction with quotation marks of the Praat writer.

#### Resources: 

- Add an acoustic model for English, including laughter and noises.


### SPPAS 1.9.2 

(2017, 6th October)

#### Programming:

- Bug correction in the diagnosis of audio files.
- Package "anndata" continued, in the scope of replacing "annotationdata".
- Acoustic model training procedure debugged and improved, code cleaned, 
  scripts updated, etc.


### SPPAS 1.9.3 

(2017, 18th October)

#### Programming:

- Critical bug correction in Alignment: error when loading the tiedlist.
- Correction of TextNormalization of broken words: "-" is now not removed.

#### Resources: 

- Italian pronunciation dictionary updated.
- French vocabulary updated: list of compound words revised.

#### Known bugs:

- Spanish alignment does not work: corrupted acoustic model


### SPPAS 1.9.4 

(2018, 15th January)

#### Programming:

- Script to train acoustic models updated
- Script to evaluate alignments updated
- Bug corrections of the search tier system

#### Resources: 

- Spanish:
    
    - new pronunciation dictionary
    - new acoustic model


### SPPAS 1.9.5 

(2018, 26th April)

#### Programming:

- I/O development of the package "anndata".
- Reading and writing of annotated files is now based on the new 'anndata'
  package instead of the old 'annotationdata' package.
  This was a big issue and it allows a better support of annotated files
  (less bugs and the amount of lost information is drastically reduced).



### SPPAS 1.9.6

(2018, 25th May)

#### Resources: 

- New: support of German language for Text Normalization (except num2letter), Phonetization and Alignment.
- French: new acoustic model with an updated phoneset and a better accuracy.
- Laugh and noise are uniformly represented in all languages.
- English: context-independent model is the default, instead of context-dependent model, for Alignment.


#### Development

- debug of the Transcriber file reader. 
- debug in the management of the hierarchy in file readers/writers.
- Resources package: add compatibility with PLS (W3C xml) pronunciation dictionaries


#### Known bugs:
- Syllabification of French: the configuration file is not correct.


### SPPAS 1.9.7

(2018, 23th July)

#### Development

- new filter system in anndata
- IPUs segmentation: debug in fixing automatically the threshold for the search of silences (can't be negative!)
- Annotations: improved messages for the procedure outcome report
- Annotations: Text normalization is based on `anndata` API instead of `annotationdata`
    * the result is now a sequence of individual tokens 
    (each token is a sppasLabel()), instead of a single string separating tokens 
    with space.
- Annotations: Phonetization is based on `anndata` API instead of `annotationdata`
    * the result is now a sequence of sppasLabel() with alternative tags: 
    each tag corresponds to a pronunciation variant
- Annotations: Syllabification re-programmed and now based on `anndata` API
    * only sequences of phonemes are syllabified (i.e. no silence, no laugh nor noise in the result)
    * `O` class name changed into `P` class name
    * do not generate the tier "structures" tier anymore
    * generating the "classes" tier is optional
- Annotations: INTSINT is based on `anndata` API instead of `annotationdata`
- IPUScriber is based on `anndata` API instead of `annotationdata`

#### Resources:

- French syllabification rules updated to be compliant with the new phoneset (used since version 1.9.6)

#### Documentation

- updated chapter 6: scripting with Python and SPPAS. 
  It is now based on `anndata` API instead of `annotationdata`.


### SPPAS 1.9.8

(2018, 06th September)

e-mail contact is changed to: 

- *contact@sppas.org* for general questions of users
- *develop@sppas.org* for developer questions or for a bug alert

#### Development

- bug correction for GMT with a negative value, like GMT-4.
- most of the scripts are based on `anndata` API instead of `annotationdata`
- dependency to markdown removed
- new solution to work with global variables with the new package `config` and
the new classes like `sppasGlobalSettings`, `sppasPathSettings`, ...
- translation management moved in package `config`
- better way to work with imports
- increased pep8 and pep257 compatibility (should be continued)
- `anndata` is used in DataRoamer, Visualizer, DataFilter
- new script trsshift.py to shift the transcription of a delay in time

#### Documentation

The API documentation is based on Sphinx <http://www.sphinx-doc.org>.



### SPPAS 1.9.9

(2018, 23th October)

#### Development

- package for the 'aligment' is fully re-structured (re-implemented partially), 
new unittests are added, and compatibility with python 3, pep8 and pep257.
- new GUI based on python3 + phoenix: it prints an information message in 
case SPPAS is launched with these versions.
- package 'prensenters' updated: unittests, python 3, pep8 and pep257.


#### Plugins

- sampa2ipa included to the SPPAS package
- audiosegmenter included to the SPPAS package


#### Annotations

- IPUs Segmentation removed. It is replaced by:

	1. Search for IPUs: to find silences/tracks from an audio file
	2. Fill in IPUs: to find silences/tracks from an audio file and its transcription
	3. AudioSegmenter plugin: to segment audio files into several tracks

- annotation.py: bug correction with the default output extension
- bug correction when the phonetization was unknown


#### Resources

- add a file 'monophones' into each model, for the compatibility with HVite.
- errors of the acoustic models of "spa" and "nan" corrected.



### SPPAS 2.0

(2019, 4th January)

The main change of this release is that the package `annotationdata` has been
removed, so that all packages are using `anndata` instead. One of the most
important consequence of this change is that all packages, except the wxGUI, 
are compatible with both Python 2.7 and Python 3.4+.

#### Annotations

- Alignment: bug correction if the last unit is empty.
- Self-Repetitions and Other-Repetitions split into two different packages
- Un-used package 'Chunks' removed
- Better management of annotations: sppasBaseAnnot, sppasParam and sppasManager 
  revised.

#### UI

- All programs in `bin` folder updated. A 'manual mode' and an 'auto mode'
  are available in the annotations to fix input/output, and a --log option
  is proposed to save a report.
- Plugins: marsatagplugin added and debugged.



### SPPAS 2.1

(2019, 28th February)

#### Development

- Praat TextGrid: it's now allowed to use the symbol '=' in annotation labels.
- Use of 'json' files instead of 'ini' files to configure SPPAS, the annotations 
and the plugins.
- New package 'analysis' with the filter and statistic systems.

#### Annotations

- Search for IPUs: the program has been improved and evaluated.
- Alignment: priority is given to standard tokens (if EOT).
- Alignment: problem of whitespace in filenames solved.

#### UI

- Plugins: new plugin to remove un-translated IPUs and to re-index the IPUs
- extended internationalization of the messages (for English and French)



### SPPAS 2.2

(2019, 09th May)

sppas.bat and sppas.command, the two main ways to launch the GUI, were
fully re-written in order to search for 'pythonw' command first. It results
in the following advantages:
    - it increases the compatibility with MacOS systems; 
    - it allows to not display the dark frame of 'python' under Windows.

#### Development

- New package 'files' for a further use.
- Increased compatibility of file formats: can read alternative tags
  with {|} system, whitespace vs CR, etc.

#### Plugins

- Debug of the plugin to clean IPUs.

#### Annotations

- Search for IPUs: a special threshold value is set when the audio 
  recording has a very low median value (ie very low volume values)
- Normalization/Phonetization/Alignment/Syllabification can work without audio
- Search for IPUs and Fill in IPUs: problem with upper/lower extension solved 

#### GUI

- "Delete" button of the "File Explorer": files are no longer definitively 
  deleted. Instead, they are moved into an hidden folder with name '.trash'
  in the package of SPPAS.
- wxFrame to display log messages.
- Annotations in GUI: better compatibility to annotate written texts

#### Resources

- fra.dict: corrected 6 errors with a non-existing 'eu' sound


### SPPAS 2.3

(2019, 25th June)

#### Development

- JSON configuration files of sppas modified.
- package "files" is used: the annotations manager is using a workspace instead of a list of files.
- the GUI based on wx4 is managing properly the workspaces and the annotations: a page with "Files" is displayed and allows to "add/remove/delete" files in the list, "import from/export to/pin&save/rename" workspaces, to "create/edit/delete" references and to "check with filters/check all/associate/dissociate" files and references. 
- Workspaces are saved in "JSON" format in the "workspaces" folder.

#### Annotations

- New annotation "Activity". This annotation was previously available as an option of "Alignment".
- Text Normalization: the Num2Letter module has been fully re-programmed.
- Text Normalization: new option "occurrence & duration" to estimate the number of tokens of each IPU and the duration of this latter.
- Annotations are categorized as: STANDALONE, SPEAKER or INTERACTION. The GUI based on wx3 only shows the STANDALONE ones, but the GUI based on wx4 can deal with all of them.

#### Resources

- New folder "num" with dictionaries of numbers for cmn, eng, fra, ita, jpn, khm, pol, por, spa, vie.



### SPPAS 2.4

(2019, 26th June)

#### Development

- the GUI based on wx4 is managing the plugins
- bug correction in 'files' package

#### Annotations

- New annotation "Re-Occurrences", of type INTERACTION. Can only be used with the CLI or the GUI based on wx4.
- Either one or two new options in all the annotations: 
    * inputpattern: to choose the pattern of the input file (like "-token" for Phonetization)
    * outputpattern: to choose the pattern of the output file (like "-token" for Tokenization)
- CLI: the script annotation.py is fully re-implemented

#### Resources

- New pronunciation dictionary for Iranian Persian (pes), for Phonetzation
- New acoustic model for Iranian Persian (pes), for Alignment



### SPPAS 2.5

(2019, 30th July)


#### Development

- the GUI based on wx4 can convert files.

#### Annotations

- New annotation "RMS" to estimate the Root-mean square of an audio file in 
  given intervals



### SPPAS 2.6

(2019, 1st October)


#### Various

- New script 'dictsampa.py' to convert a pronunciation dictionary in IPA into SAMPA encoding
- New script 'clippingrate.py' to estimate the clipping rate at several factors in sub-parts of the audio file
- New plugin 'StatGroups' to estimate distributional statistics on sequences of numbers in annotations.


#### Development

- the GUI based on wx4 can convert files (debugged and extended)
- the GUI based on wx4 can display the content of a file (ListView) in the page "Analyze"
- GUI: "Save all" button debugged in DataFilter (remaining bug in DataRoamer)


### SPPAS 2.7

(2019, 2nd December)

#### Various

- Add the possibility to remove un-labelled annotations in tiers (in DataFilter)
- Plugin "StatGroups" updated: it can optionnally create a tier with the not-selected intervals

#### Resources

- Modified French lexicon: include words of Quebec French
- New Quebec French pronounciation dictionary
- New Quebec French acoustic model
- Modified French syllabification file: added vowels of Quebec French
- Tiedlist of Italien model re-introduced... Alignment of "ita" is once again completely functional

#### Development

- the GUI based on wx4: in "Files", the list of files uses collapsile panels instead of a table  


### SPPAS 2.8

(2020, 17th January)

Two new scripts (sppas-py3.bat and sppas-py3.command) added to run directly 
the new GUI based on py3+wx4.
 
#### Development

- the GUI based on wx4: some debug, improved compatibility with the 3 OS and 
  DataFilter and DataStats implemented in the page "Analyze"

#### Annotations

- New annotation: Stop Tags: estimate if tags of annotations are relevant
  or not. Relevance is estimated like for Other-Repetitions detections.
  

### SPPAS 2.9

(2020, 03th April)

This is the last version for which both Python 2.7 and Python 3.5+ are
supported. Running SPPAS with Python 2.7 will not be maintained past 2020, May
when SPPAS 3.0 will be released.

#### Development

- the GUI based in wx4 is extended and debugged: audio roamer and
  ipuscriber integrated in the page 'Analyze', a multi-player is 
  added - it can play several media at a time.

#### Resources

- updated dictionary and acoustic model of Polish language

#### Annotations

- New annotation LexMetric to estimate occurrences and rank

#### Known bugs:

In the page 'Analyze', the view "Multi-Player" does not properly
displays the annotation boundaries and the scrollbar is not updated
when needed.




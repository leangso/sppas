## INTSINT: Encoding of F0 anchor points

INTSINT assumes that pitch patterns can be adequately described using a 
limited set of tonal symbols, T,M,B,H,S,L,U,D (standing for : Top, Mid, Bottom, 
Higher, Same, Lower, Up-stepped, Down-stepped respectively) each one of which 
characterises a point on the fundamental frequency curve.

The rationale behind the INTSINT system is that the F0 values of pitch targets 
are programmed in one of two ways : either as absolute tones T, M, B which are 
assumed to refer to the speaker’s overall pitch range (within the current 
Intonation Unit), or as relative tones H, S, L, U, D assumed to refer only to 
the value of the preceding target point.

![INTSINT example](etc/images/INTSINT-tones.png)

The rationale behind the INTSINT system is that the F0 values of pitch targets 
are programmed in one of two ways : either as absolute tones T, M, B which are 
assumed to refer to the speaker’s overall pitch range (within the current 
Intonation Unit), or as relative tones H, S, L, U, D assumed to refer only to
the value of the preceding target point.

A distinction is made between non-iterative H, S, L and iterative U, D relative
tones since in a number of descriptions it appears that iterative raising or 
lowering uses a smaller F0 interval than non-iterative raising or lowering. 
It is further assumed that the tone S has no iterative equivalent since there 
would be no means of deciding where intermediate tones are located.

![](etc/screenshots/Momel-INTSINT.png)


>**D.-J. Hirst** (2011).
>*The analysis by synthesis of speech melody: from data to models*, 
>Journal of Speech Sciences, vol. 1(1), pages 55-83.


### Perform INTSINT with the GUI

It is an annotation of STANDALONE type.

Click on the INTSINT activation button and click on the "Configure..." 
blue text to fix options.


### Perform INTSINT with the CLI

`intsint.py` is the program to perform INTSINT annotation of a given file with
momel anchors.


#### Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
intsint.py [files] [options]

INTSINT: INternational Transcription System for INTonation codes the
intonation of an utterance by means of an alphabet of 8 discrete symbols.
Requires Momel targets.

optional arguments:
  -h, --help  show this help message and exit
  --quiet     Disable the verbosity
  --log file  File name for a Procedure Outcome Report (default: None)

Files (manual mode):
  -i file     Input file name with anchors.
  -o file     Output file name (default: stdout)

Files (auto mode):
  -I file     Input file name with anchors (append).
  -e .ext     Output file extension. One of: .xra .TextGrid .eaf .csv .mrk
			  .txt .stm .ctm .lab .mlf .sub .srt .antx .arff .xrff

Options:
  --inputpattern INPUTPATTERN
                        Input file pattern (momel anchors) (default: -momel)
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: -intsint)

This program is part of SPPAS version 2.4. Copyright (C) 2011-2019 Brigitte
Bigi. Contact the author at: contact@sppas.org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#### Examples of use

Apply INTSINT on a single file and print the result on the standard output:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\intsint.py -i .\samples\samples-eng\ENG_M15_ENG_T02-momel.xra --quiet
	1.301629 M
	1.534887 U
	1.639614 H
	1.969234 L
	2.155284 S
	2.354162 U
	2.595364 L
	2.749773 S
	2.933222 S
	3.356651 H
	3.502254 D
	3.707747 H
	4.000578 L
	4.141915 S
	4.383332 H
	4.702203 L
	4.987086 U
	5.283864 L
	5.538984 U
	5.707147 D
	5.906895 S
	6.705373 M
	7.052992 U
	7.218415 S
	7.670083 D
	7.841935 S
	8.124574 D
	8.455182 U
	8.746016 D
	9.575359 M
	9.996245 U
	10.265663 D
	10.576394 D
	11.730570 M
	12.083323 U
	12.411790 D
	12.707442 S
	12.963805 U
	13.443483 L
	13.921939 S
	14.377324 B
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Apply INTSINT in auto mode:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\intsint.py -I .\samples\samples-eng\ENG_M15_ENG_T02.wav
python .\sppas\bin\intsint.py -I .\samples\samples-eng\ENG_M15_ENG_T02.PitchTier
python .\sppas\bin\intsint.py -I .\samples\samples-eng\ENG_M15_ENG_T02-momel.xra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

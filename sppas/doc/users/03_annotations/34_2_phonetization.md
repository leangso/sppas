## Phonetization

### Overview

Phonetization, also called grapheme-phoneme conversion, is the process of
representing sounds with phonetic signs. However, converting from written
text into actual sounds, for any language, cause several problems that have
their origins in the relative lack of correspondence between the spelling
of the lexical items and their sound contents.
As a consequence, SPPAS implements a dictionary based-solution which consists 
in storing a maximum of phonological knowledge in a lexicon. This approach
is then language-independent. SPPAS phonetization process is the equivalent
of a sequence of dictionary look-ups.

Most of the other systems assume that all words of the speech transcription
are mentioned in the pronunciation dictionary. On the contrary, SPPAS
includes a language-independent algorithm which is able to phonetize unknown
words of any language as long as a (minimum) dictionary is available (Bigi 2013).
The Procedure Outcome Report reports on such cases with a WARNING message.

### Adapt Phonetization

Since Phonetization is only based on the use of a pronunciation dictionary,
the quality of the result only depends on this resource.
If a pronunciation is not as expected, it is up to the user to change it in
the dictionary: Dictionaries are located in the folder "dict" of
the "resources" directory. They are all with
[UTF-8 encoding](https://en.wikipedia.org/wiki/UTF-8) 
and ["LF" for newline](https://en.wikipedia.org/wiki/Newline).
The format of the dictionaries is HTK-like. As example, below is a piece
of the `eng.dict` file:

        THE             [THE]           D @
        THE(2)          [THE]           D V
        THE(3)          [THE]           D i:
        THEA            [THEA]          T i: @
        THEALL          [THEALL]        T i: l
        THEANO          [THEANO]        T i: n @U
        THEATER         [THEATER]       T i: @ 4 3:r
        THEATER'S       [THEATER'S]     T i: @ 4 3:r z

The first column indicates the word, followed by the variant number (except for
the first one). The second column indicates the word between brackets. The last
columns are the succession of phones, separated by a whitespace. SPPAS is 
relatively compliant with the format and accept empty brackets or missing brackets.

The phoneset of the languages are mainly based on
[X-SAMPA](https://en.wikipedia.org/wiki/X-SAMPA) international standard. 
See the chapter "Resources" of this documentation to know the list of accepted 
phones for a given language. This list can't be extended nor modified by users.
However, new phones can be added: Send an e-mail to the author to collaborate 
in that way.

Actually, some words can correspond to several entries in the dictionary
with various pronunciations. These pronunciation variants are stored in the
phonetization result. By convention, whitespace separate words, minus characters
separate phones and pipe character separate phonetic variants of a word.
For example, the transcription utterance:

* Transcription: `The flight was 12 hours long.`
* Text Normalization: `the flight was twelve hours long`
* Phonetization: `D-@|D-V|D-i: f-l-aI-t w-A-z|w-V-z|w-@-z|w-O:-z t-w-E-l-v aU-3:r-z|aU-r-z l-O:-N`

### Support of a new language

The support of a new language in Phonetization only consists in:
1. creating the pronunciation dictionary. 
The following constraints on the file must be respected: 
    - its format (HTK-like),
    - its encoding (UTF-8), 
    - its newlines (LF), 
    - its phone set (X-SAMPA),
    - its file name (iso639-3 of the language and ".dict" extension).
2. adding the dictionary in the "dict" folder of the "resources" directory.


### Perform Phonetization with the GUI

It is an annotation of STANDALONE type.

The Phonetization process takes as input a file that strictly match the audio
file name except for the extension and that "-token" is appended. For example,
if the audio file name is "oriana1.wav", the expected input file name is
"oriana1-token.xra" if .xra is the default extension for annotations.
This file must include a **normalized** orthographic transcription.
The name of such tier must contains one of the following strings:

1. "tok"
2. "trans"

The first tier that matches one of these requirements is used
(this match is case-insensitive).

Phonetization produces a file with "-phon" appended to its name,
i.e. "oriana1-phon.xra" for the previous example.
This file contains only one tier with the resulting phonetization and with
name "Phones".

To perform the annotation, click on the Phonetization
activation button, select the language and click on the "Configure..."
blue text to fix options.


### Perform Phonetization with the CLI

`phonetize.py` is the program to perform Phonetization on a given file,
i.e. the grapheme-conversion of a file or a raw text.

#### Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
phonetize.py [files] [options]

Phonetization: Grapheme to phoneme conversion represents sounds with phonetic
signs. Requires a Text Normalization.

optional arguments:
  -h, --help            show this help message and exit
  --quiet               Disable the verbosity
  --log file            File name for a Procedure Outcome Report (default: None)

Files (manual mode):
  -i file               Input tokenization file name.
  -o file               Annotated file with phonetization.

Files (auto mode):
  -I file               Input transcription file name (append).
  -l lang               Language code (iso8859-3). One of: cat cmn deu eng fra
						ita jpn kor nan pcm pol por spa yue yue_chars.
  -e .ext               Output file extension. One of: .xra .TextGrid .eaf
						.csv .mrk .txt .stm .ctm .lab .mlf .sub .srt .antx
						.arff .xrff

Resources:
  -r dict               Pronunciation dictionary (HTK-ASCII format).
  -m map_file           Pronunciation mapping table. It is used to generate
						new pronunciations by mapping phonemes of the
						dictionary.

Options:
  --inputpattern INPUTPATTERN
                        Input file pattern (tokenization) (default: -token)
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: -phon)
  --unk UNK             Try to phonetize unknown words (default: True)
  --usestdtokens USESTDTOKENS
						Phonetize from standard spelling (default: False)

This program is part of SPPAS version 2.4. Copyright (C) 2011-2019 Brigitte
Bigi. Contact the author at: contact@sppas.org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#### Examples of use

Obviously, before launching the following commands, you already prepared
the required file (the result of text normalization segmented into IPUs). 


Example of the phonetization of a single input file in manual mode:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\phonetize.py -r .\resources\dict\eng.dict 
  -i .\samples\samples-eng\oriana1-token.xra --quiet
	Phones
	0.000000, 1.675000, sil
	1.675000, 4.570000, {D-@|D-i:|D-V} f-l-aI-t {w-@-z|w-V-z|w-O:-z|w-A-z} t-w-E-l-v 
	{aU-3:r-z|aU-r\-z} l-O:-N {{-n-d|@-n-d} w-i: {r\-I-l-i:|r\-i:-l-i:} g-A-t b-O:-r\-d
	4.570000, 6.390000, sil
	6.390000, 9.870000, D-eI @U-n-l-i: p-l-eI-d t-u m-u-v-i:-z sil {h-w-I-tS|w-I-tS} 
	w-i: h-{-d b-@U-T {O:-l-r\-E-4-i:|O:-r\-E-4-i:} s-i:-n
	9.870000, 11.430000, sil
	11.430000, 14.730000, aI n-E-v-3:r {g-I-t|g-E-t} {t-@|t-i|t-u} s-l-i:-p 
	{O:-n|A-n} {D-@|D-i:|D-V} E-r\-p-l-eI-n {b-i-k-O:-z|b-i-k-V-z} {i-t-s|I-t-s} 
	s-@U @-n-k-V-m-f-3:r-4-@-b-@-l
	14.730000, 17.792000, sil
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The same file in automatic mode can be annotated with one of the following commands:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\phonetize.py -l eng -I .\samples\samples-eng\oriana1-token.xra
python .\sppas\bin\phonetize.py -l eng -I .\samples\samples-eng\oriana1.xra
python .\sppas\bin\phonetize.py -l eng -I .\samples\samples-eng\oriana1.txt
python .\sppas\bin\phonetize.py -l eng -I .\samples\samples-eng\oriana1.wav
python .\sppas\bin\phonetize.py -l eng -I .\samples\samples-eng\oriana1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


This program can also phonetize data from the standard input.
Example of use, using stdin/stdout under Windows:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Write-Output "The flight was 12 HOURS {toto} long." | 
python .\sppas\bin\normalize.py -r .\resources\vocab\eng.vocab --quiet | 
python .\sppas\bin\phonetize.py -r .\resources\dict\eng.dict --quiet
	D-@|D-V|D-i:
	f-l-aI-t
	w-A-z|w-V-z|w-@-z|w-O:-z
	t-w-E-l-v
	aU-3:r-z|aU-r\-z
	l-O:-N
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


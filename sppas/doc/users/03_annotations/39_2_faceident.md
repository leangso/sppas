## Face Identity

This is a new and original automatic annotation but it's still in progress. It
has to be evaluated.

### Overview

Face Identity automatic annotation assigns a person identity to detected faces 
of a video. It takes as input a video and a CSV file with coordinates of the 
detected faces. It produces a CSV file with coordinates of the identified faces.
Assigned persons names are "id-00x". Obviously, the CSV file can be editer and
such names can be changed *a posteriori*.

This annotation requires to enable the "video" feature in the setup, so it will
install the external python libraries 'numpy' and 'opencv-contrib'.

No external resources are needed.


### Perform annotation with the GUI

It is a STANDALONE annotation.

The Face Identity process takes as input a video file.
To perform the annotation, click on the Face Identity activation button
and click on the "Configure..." blue text to fix options.


### Perform with the CLI

`faceidentity.py` is the program to perform Face Identity annotation
of a given video file, if the corresponding CSV file with detected faces
is existing.

#### Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: faceidentity.py [files] [options]

optional arguments:
  -h, --help            show this help message and exit
  --quiet               Disable the verbosity
  --log file            File name for a Procedure Outcome Report (default: None)

Files:
  -i file               Input video.
  -c file               Input CSV file with face coordinates and sights.
  -o file               Output base name.
  -I file               Input file name (append).
  -e .ext               Output file extension. One of: .mp4 .avi .mkv

Options:
  --inputpattern INPUTPATTERN
  --inputoptpattern INPUTOPTPATTERN (default: -face)
  --outputpattern OUTPUTPATTERN (default: -ident)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Lexical Metric

### Overview

The Lexical Metric is producing information about the number of 
occurrences and the rank of each eaccurrences of annotation labels. 

By default, the lexical metrics are estimated on a tier with name "TokensAlign" of 
a file with pattern "-palign". If a list of separators is given, segments are created
to estimate a number of occurrences. 
Default separators are "# + @ * dummy" in order to ignore silences, laughter
items, noises and untranscribed speech.

Both the tiername and the pattern can be modified by configuring the annotation.
The annotation provides an annotated file with the "-lexm" pattern. This file 
includes the tiers:

- LM-OccAnnInSegments: defined intervals with number of occurrences of annotations;
- LM-OccLabInSegments: defined intervals with number of occurrences of labels;
- LM-Occ: the number of occurrences of each label the annotation is representing;
- LM-Rank: the rank of each label the annotation is representing.


### Perform Lexical Metric with the GUI

It is an annotation of STANDALONE type.

To perform the annotation, click on the Lexical Metric activation button and 
click on the "Configure…" blue text to fix options.


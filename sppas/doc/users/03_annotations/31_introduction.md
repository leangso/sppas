## Introduction

### THE recommended corpus construction workflow

The kind of process to implement in the perspective of obtaining rich and 
broad-coverage multimodal/multi-levels annotations of a corpus is illustrated 
in next Figure. It describes the steps of the annotation workflow.
This Figure must be read from top to bottom and from left to right, starting by 
the recordings and ending to the analysis of annotated files.
Yellow boxes represent manual annotations, blue boxes represent automatic ones.
Obviously, this Figure is simplified and there are other ways to construct a 
corpus but this one is the best solution in order to get effective automatic 
annotations with SPPAS.

![Annotation methodology](etc/figures/methodo.png)

After recording an audio file (see recordings recommendations), the first
annotation to perform is to search for the Inter-Pausal Units - IPUs, which
are sounding segments surrounded by silent pauses of more than X ms, and 
time-aligned on the speech signal.

An orthographic transcription (OT) has to be performed **manually inside the 
IPUs**; an Enriched OT is a better idea -- see SPPAS transcription convention.
Then text normalization automatic annotation will normalize the orthographic
transcription or each IPU. The phonetization process will convert the 
normalized text in a grammar of pronunciations using X-SAMPA standard. 
Alignment will perform segmentation at phonemes and tokens levels, etc.

At the end of each automatic annotation process, SPPAS produces a 
*Procedure Outcome Report*. It contains important information about the 
annotations: it includes all parameters and eventually warnings and
errors that occurred during the annotation process. This window opens in the 
scope to be read by users (!) and should be saved with the annotated corpus. 



### SPPAS annotation workflow

Annotations of SPPAS are categorized as: 

- STANDALONE: they require the files of a single speaker, 
- SPEAKER: they require the files of a single speaker at two different 
  moments in time, for example "before sleeping versus after sleeping",
  "the morning versus the afternoon", "10 years ago versus nowadays"...
- INTERACTION: they require the files of two different speakers who are
  supposed to interact in the same conversation (i.e. the files are of 
  same duration).

All the 23 automatic annotations of type STANDALONE are illustrated in 
the Figure at top of the page. This Figure can also be downloaded at 
<https://sppas.org/etc/figures/workflow_standalone.pdf>; and it is 
also included into the documentation folder of the SPPAS package. 
It shows the detailed process in order to perform annotations in a
suitable way.

![Workflow of the STANDALONE annotations](etc/figures/workflow_standalone.png)

The best way to read this Figure is to search for the annotation result
you want and to follow the arrows that made it possible. They are 
representing all the annotations you'll have to ask SPPAS to perform 
and that you can manually check or customize to make it "YOUR SPPAS Solution".
Two examples of SPPAS solutions are available on the website:
<https://sppas.org/workdemo.html>.

    This chapter describes each annotation box.


### Recordings

SPPAS performs automatic annotations: It does not make sense to hope 
for miracles but you can expect good enough results that will allow you 
to save your precious time!
And *it begins by taking care of the recordings*...

#### Audio files 

>Only `wav` and `au` audio file formats are supported by Python, so does SPPAS;

>Only mono audio files are supported by automatic annotations of SPPAS.

SPPAS verifies if the audio file is 16 bits sample rate and 16000 Hz frame
rate. Otherwise it automatically creates a new converted audio file. For very 
long files, this process may take time. If Python can't read the audio file, 
an error message is displayed: you'll have to convert it with audacity, praat...
A relatively good recording quality is expected (see next Figure). 

For example, both "Search for IPUs" and "Fill in IPUs" require a better quality 
compared to what is expected by "Alignment", and for that latter, it depends 
on the language. The quality of the result of automatic annotations highly
depends on the quality of the audio file. 

![Example of expected recorded speech](etc/screenshots/signal.png)

Providing a guideline or recommendation of good practices is impossible, 
because it depends on too many factors. However, the followings are obvious:

- Never, never, never records a lossy audio file. It means that extracting the audio file from a video is feasible only if the embedded audio is either lossless or not compressed: see this page <https://en.wikipedia.org/wiki/Comparison_of_video_container_formats>.

- The better microphone, the better audio file! Using an headworn microphone is much
more better than a clip-on one. At LPL, we get very good results with the AKG C520.

- The recorded volume must be high enough. Ideally, it should be in range [-0.5 ; 0.5]. If all the amplitude values are in range [-0.1 ; 0.1], the difference between speech and silence is very slight and it makes the search for silences very difficult.

- The audio file should not be 32 floating bits. For speech, 32 bits are totally unusefull and - worse, sometimes Python can't read it.

- as you probably don't plan to burn your audio file on a CD-ROM, 44100Hz framerate does not make sense. 48000hz is a more reasonable choice, particularly because it doesn't need of elabotated interpolation methods when it's converted to 16000hz for automatic annotations.


#### Video files 

SPPAS is proposing a few automatic annotations of a video if the Python library
"opencv" is installed. All of them are annotating the face of recorded people. 




### File formats and tier names

When annotating with the GUI, the file names of each annotation is fixed and
can't be customized. A filename is made of a root, followed by a pattern
then an extension. For example "oriana1-palign.TextGrid" is made of
the root "oriana1", the pattern "-palign" and the extension ".TextGrid".
Each annotation allows to fix manually the pattern and to choose the extension
among the list of the supported ones. Notice that the pattern must start
with the "-" (minus) character. It means that the character "-" must only
be used to separate the root to the pattern:

> The character '-' can't be used in the root of a filename.

The name of the tiers the annotations are expecting for their input are 
fixed and can't be changed; so does the produced tier names.

> File extensions are case-sensitive, use TextGrid (Praat) instead of textgrid. 


### Resources required to annotate

The resources that are required to perform annotations are of two different
types: 

1. models, like a face model that is required to perform face detection,
2. language resources, like a pronunciation dictionary.  

   Not all the resources are distributed in the SPPAS package, but they can
   be installed at any time with the setup, see installation instructions.

All the automatic annotations proposed by SPPAS are designed with 
language-independent algorithms, but some annotations are requiring 
language-knowledges. This linguistic knowledge is represented in external 
files so they can be added, edited or removed easily. 

Adding a new language for a given annotation only consists in adding the 
linguistic resources the annotation needs, like lexicons, dictionaries, 
models, set of rules, etc. For exemple, see:

> Mélanie Lancien, Marie-Hélène Côté, Brigitte Bigi (2020).
Developing Resources for Automated Speech Processing of Quebec French.
In Proceedings of The 12th Language Resources and Evaluation Conference, pp. 5323–5328, Marseille, France.

> Brigitte Bigi, Bernard Caron, Abiola S. Oyelere (2017).
Developing Resources for Automated Speech Processing of the African Language Naija (Nigerian Pidgin).
In 8th Language and Technology Conference: Human Language Technologies as a Challenge for Computer Science and Linguistics, pp. 441-445, Poznań, Poland.




### Download and install linguistic resources

Since June 2020, the linguistic resources and models for some annotations 
are no longer distributed into the package of SPPAS. Instead, they are hosted
by Ortolang repository with a public access. 

They can be installed automatically into SPPAS by the `preinstall.py` 
program (CLI) or in the GUI by clicking "Add languages" or "Add annotations"
in the toolbar of the "Annotate" page.

They can also be installed manually by downloading them at: 
<https://hdl.handle.net/11403/sppasresources> and unpacking the zip
file into the `resources` folder of SPPAS package.

A full description of such resources and how to install them is available
in the repository: download and read the file Documentation.pdf. It contains
details about the list of phonemes, authors, licenses, etc.


### New language support

Some of the annotations are requiring external linguistic resources in order 
to work efficiently on a given language: text normalization requires a lexicon,
phonetization requires a pronunciation dictionary, etc. It is either possible
to install and use the existing resources or to create and use custom ones.

When executing SPPAS, the list of available languages of each annotation
is dynamically created by exploring the `resources` directory content.
This means that:

* the resources you added or modified are automatically taken into account 
  (ie. there's no need to modify the program itself);
* SPPAS needs to be re-started if new resources are added when it was 
  already being running.


### Annotate with the GUI

Performing automatic annotations with SPPAS Graphical User Interface is
a step-by-step process.

It starts by checking the list of paths and/or roots and/or files in 
the currently active workspace of the "Files" page. Then, in the 
"Annotate" page:

1. Select the output file format, i.e. the file format of the files SPPAS will create;

2. Select a language in the list;

3. Enable each annotation to perform by clicking on the button in red, among STANDALONE, SPEAKER and INTERACTION annotation types.
Each button will be turned green if some annotations are selected.

    3.1 Configure each annotation by clicking on the "Configure..." link text in blue;
    
    3.2 The language of any annotation can be changed.

4. Click on the *Perform annotations* button, and wait. A progress bar should indicates the annotation steps and files. Some annotations are very very fast but some others are not. For example, Face Detection is 2.5 x real times, i.e. annotating a video of 1 minute will take 2 minutes 30 secs.

5. It is important to read the Procedure Outcome report. It allows to check that 
everything happened normally during the automatic annotations. This report
is saved in the "logs" folder of the SPPAS package.


### Annotate with the CLI

To perform automatic annotations with the Command-line User Interface, 
there is a main program `annotation.py`. This program allows to annotate
in an easy-and-fast way but none of the annotations can be configured: 
their default parameters are used.
This program performs automatic annotations on a given file or on all 
files of a directory. It strictly corresponds to the button 
`Perform annotations` of the GUI except that annotations are pre-configured: 
no specific option can be specified.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: python .\sppas\bin\annotation.py -I file|folder [options]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
optional arguments:
  -h, --help       show this help message and exit
  --log file       File name for a Procedure Outcome Report (default: None)
  --momel          Activate Momel
  --intsint        Activate INTSINT
  --fillipus       Activate Fill in IPUs
  --searchipus     Activate Search for IPUs
  --textnorm       Activate Text Normalization
  --phonetize      Activate Phonetization
  --alignment      Activate Alignment
  --syllabify      Activate Syllabification
  --tga            Activate Time Group Analysis
  --activity       Activate Activity
  --rms            Activate RMS
  --selfrepet      Activate Self-Repetitions
  --stopwords      Activate Stop Tags
  --lexmetric      Activate LexMetric
  --otherrepet     Activate Other-Repetitions
  --reoccurrences  Activate Re-Occurrences
  --merge          Create a merged file with all the annotations

Files:
  -I file|folder   Input transcription file name (append).
  -l lang          Language code (iso8859-3). One of: por eng ita kor deu nan
                   vie und hun spa cat pol yue fra pcm yue_chars cmn jpn.
  -e .ext          Output file extension. One of: .xra .TextGrid .eaf .csv
                   .mrk .txt .stm .ctm .lab .mlf .sub .srt .antx .arff .xrff
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Examples of use:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
./sppas/bin/annotation.py -I .\samples\samples-eng
                          -l eng
                          -e .TextGrid
                          --fillipus --textnorm --phonetize --alignment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A progress bar is displayed for each annotation if the Terminal is supporting
it (`bash` for example). Instead, the progress is indicated line-by-line 
(Windows PowerShell for example). 

![CLI: annotation.py output example](etc/screenshots/CLI-example.png)

Each annotation has also its own program and all options can be fixed.
They are all located in the `sppas/bin` folder.


### The procedure outcome report

It is very important to read conscientiously this report: it describes 
exactly what happened during the automatic annotation process.
It is recommended to store a copy of the report within the corpus because
it contains information that are interesting to know for anyone using
the annotations.

By default, all reports are saved in the "logs" folder of the SPPAS package.

The text first indicates the version of SPPAS that was used. This information is 
very important. Annotations in SPPAS and their related resources are regularly 
improved and then, the result of the automatic process can change from one 
version to the other one.

Example:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SPPAS version 3.5
Copyright (C) 2011-2021 Brigitte Bigi
Web site: https://sppas.org/
Contact: Brigitte Bigi (contact@sppas.org)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Secondly, the text shows information related to the given input: 

1. the selected language of each annotation - only if the annotation is 
language-dependent. For some language-dependent annotations, SPPAS can 
still perform the annotation even if the resources for a given language are 
not available: in that case, select "und", which is the iso639-3 code for
"undetermined".
2. the selected files and folder to be annotated.
3. the list of annotations, and if each annotation was enabled. 
In that case, enabled means that the checkbox of the annotation was checked 
by the user and that the resources are available for the given language. On the 
contrary, disabled means that either the checkbox was not checked 
or the required resources are not available.
4. the file format of the resulting files.

Example:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Date: 2020-04-21T11:14:01+02:00
Input languages: 
  - Momel: ---
  - INTSINT: ---
  - Fill in IPUs: ---
  - Search for IPUs: ---
  - Text Normalization: eng
  - Phonetization: eng
  - Alignment: eng
  - Syllabification:
  - Time Group Analysis: ---
  - Activity: ---
  - RMS: ---
  - Self-Repetitions: 
  - Stop Tags: 
  - LexMetric: ---
  - Other-Repetitions: 
  - Re-Occurrences: ---

Selected files and folders: 
  - oriana1.wav

Selected annotations: 
  - Momel: enabled
  - INTSINT: enabled
  - Fill in IPUs: enabled
  - Search for IPUs: disabled
  - Text Normalization: enabled
  - Phonetization: enabled
  - Alignment: enabled
  - Syllabification: disabled
  - Time Group Analysis: disabled
  - Activity: disabled
  - RMS: disabled
  - Self-Repetitions: disabled
  - Stop Tags: disabled
  - LexMetric: disabled
  - Other-Repetitions: disabled
  - Re-Occurrences: disabled

File extension: .xra

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Thirdly, each automatic annotation is described in details, for each
annotated file. At a first stage, the list of options and their value
is summarized. 
Example:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                        Text Normalization

The vocabulary contains 121250 tokens.
The replacement dictionary contains 8 items.
Options: 
 ... inputpattern: 
 ... outputpattern: -token
 ... faked: True
 ... std: False
 ... custom: False
 ... occ_dur: True

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Then, a diagnosis of the given file is printed. It can be:
1. "Valid": the file is relevant
2. "Admit": the file is not like expected but SPPAS will convert it and work on the converted file.
3. "Invalid": SPPAS can't work with that file. The annotation is then disabled.
In case 2 and 3, a message indicates the origin of the problem.

Then, if any, the annotation procedure prints messages.
Four levels of information must draw your attention:

1. "[   OK    ]" means that everything happened normally. The annotation was 
performed successfully.
2. "[ IGNORE  ]" means that SPPAS ignored the file and didn't do anything.
3. "[ WARNING ]" means that something happened abnormally, but SPPAS found
a solution, and the annotation was performed anyway.
4. "[  ERROR  ]" means that something happened abnormally and SPPAS failed to
found a solution. The annotation was either not performed, or performed with
a wrong result.

Example of "Warning" message:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 ...  ... Export AP_track_0711.TextGrid
 ...  ... into AP_track_0711.xra
 ...  ... [ IGNORE  ] because a previous segmentation is existing.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Example of "Warning" message:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 ...  ... [ WARNING  ] chort- is missing of the dictionary and was 
                       automatically phonetized as S-O/-R-t
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

At the end of the report, the "Result statistics" section mentions the 
number of files that were annotated for each annotation, or -1 if the 
annotation was disabled.
 

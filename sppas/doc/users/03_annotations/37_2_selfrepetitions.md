## Self-Repetitions

### Overview

This automatic detection focus on word self-repetitions which can be exact
repetitions (named strict echos) or repetitions with variations (named 
non-strict echos). The system is based only on lexical criteria.
The algorithm is focusing on the detection of the source.

This system can use a list of stop-words of a given language. This is a list
of very frequent words like adjectives, pronouns, etc.
Obviously, the result of the automatic detection is significantly better 
if such list of stop-words is available. 

Optionnally, SPPAS can add new stop-words in the list: they are deduced from
the given data. These new entries in the stop-list are then different for 
each file (Bigi et al. 2014).

The annotation provides one annotated file with 2 to 4 tiers: 

1. TokenStrain: if a replacement file was available, it's the entry used by the system
2. StopWord: if a stop-list was used, it indicates if the token is a stop-word (True or False)
3. SR-Sources: tags of the annotations are prefixed by "S" followed an index
4. SR-Repetitions: tags of the annotations are prefixed by "R" followed an index


### Adapt to a new language

The list of stop-words of a given language must be located in the "vocab"
folder of the "resources" directory with ".stp" extension. This file is with
[UTF-8 encoding](https://en.wikipedia.org/wiki/UTF-8) 
and ["LF" for newline](https://en.wikipedia.org/wiki/Newline).


### Perform Self-Repetitions with the GUI

It is an annotation of STANDALONE type.

The automatic annotation takes as input a file with (at least) one
tier containing the time-aligned tokens.
The annotation provides one annotated file with 2 tiers: Sources and Repetitions.

Click on the Self-Repetitions activation button, select the language and 
click on the "Configure..." blue text to fix options.


### Perform SelfRepetitions with the CLI

`selfrepetition.py` is the program to perform automatic detection of
self-repetitions.

#### Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
selfrepetition.py [files] [options]

Self-repetitions: Self-repetitions searches for sources and echos of a
speaker. Requires time-aligned tokens.

optional arguments:
  -h, --help            show this help message and exit
  --quiet               Disable the verbosity
  --log file            File name for a Procedure Outcome Report (default: None)

Files (manual mode):
  -i file               Input time-aligned tokens file name.
  -o file               Output file name with syllables.

Files (auto mode):
  -I file               Input transcription file name (append).
  -e .ext               Output file extension. One of: .xra .TextGrid .eaf
						.csv .mrk .txt .stm .ctm .lab .mlf .sub .srt .antx
						.arff .xrff

Resources:
  -r file               List of stop-words
  
Options:
  --inputpattern INPUTPATTERN
                        Input file pattern (time-aligned words or lemmas)
                        (default: -palign)
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: -srepet)
  --span SPAN           Span window length in number of IPUs (default: 3)
  --stopwords STOPWORDS
						Add stop-words estimated from the given data (default:
						True)
  --alpha ALPHA         Coefficient to add data-specific stop-words (default:
						0.5)

This program is part of SPPAS version 3.0. Copyright (C) 2011-2020 Brigitte
Bigi. Contact the author at: contact@sppas.org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#### Examples of use

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\selfrepetition.py -i .\samples\samples-fra\F_F_B003-P8-palign.xra 
   -r .\resources\vocab\fra.stp
python .\sppas\bin\selfrepetition.py -I .\samples\samples-fra\F_F_B003-P8.wav -l fra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


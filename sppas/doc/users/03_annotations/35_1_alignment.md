## Alignment

### Overview

Alignment, also called phonetic segmentation, is the process of aligning
speech with its corresponding transcription at the phone level.
The alignment problem consists in a time-matching between a given speech
unit along with a phonetic representation of the unit.

**SPPAS Alignment does not perform the segmentation itself. It is a wrapper
either for the `Julius` Speech Recognition Engine (SRE) or for the `HVite` command
of HTK-Toolkit**. In addition, SPPAS can perform a "basic" alignment, 
assigning the same duration to each sound.

Speech Alignment requires an Acoustic Model in order to align speech.
An acoustic model is a file that contains statistical representations of each
of the distinct sounds of one language. Each sound is represented by one
of these statistical representations.
The quality of the alignment result only depends on both this resource and 
on the aligner. From our past experiences, we got better results with Julius.
See the chapter 4 "Resources for Automatic Annotations" to get the list of 
sounds of each language.

Notice that SPPAS allows to time-align automatically laugh, noises, or 
filled pauses (depending on the language): No other system is able to 
achieves this task!

![SPPAS alignment output example](etc/screenshots/alignment.png)


### Adapt Alignment

The better Acoustic Model, the better alignment results. 
Any user can append or replace the acoustic models included in the "models" 
folder of the "resources" directory. Be aware that SPPAS only supports 
HTK-ASCII acoustic models, trained from 16 bits, 16000 Hz wave files.

The existing models can be improved if they are re-trained with more data.
To get a better alignment result, any new data is then welcome: send an 
e-mail to the author to share your recordings and transcripts.


### Support of a new language

The support of a new language in Alignment only consists in adding
a new acoustic model of the appropriate format, in the appropriate
directory, with the appropriate phone set.

The articulatory representations of phonemes are so similar across
languages that phonemes can be considered as units which are independent 
from the underlying language (Schultz et al. 2001). In SPPAS package, 
9 acoustic models of the same type - i.e. same HMMs definition and 
acoustic parameters, are already available so that the phoneme prototypes 
can be extracted and reused to create an initial model for a new language.

Any new model can also be trained by the author, as soon as enough data
is available. It is difficult to estimate exactly the amount
of data a given language requires. 
That is said, we can approximate the minimum as follow:

- 3 minutes altogether of various speakers, manually time-aligned at the phoneme level.
- 10 minutes altogether of various speakers, time-aligned at the ipus level with the enriched orthographic transcription.
- more data is good data.


### Perform Alignment with the GUI

It is an annotation of STANDALONE type.

The Alignment process takes as input one or two files that strictly match the
audio file name except for the extension and that "-phon" is appended for the
first one and "-token" for the optional second one. For example,
if the audio file name is "oriana1.wav", the expected input file name is
"oriana1-phon.xra" with phonetization and optionally  "oriana1-token.xra"
with text normalization, if .xra is the default extension for annotations.

The speech segmentation process provides one file with name "-palign" appended
to its name, i.e. "oriana1-palign.xra" for the previous example.
This file includes one or two tiers:

* "PhonAlign" is the segmentation at the phone level;
* "TokensAlign" is the segmentation at the word level (if a file with tokenization was found).

The following options are available to configure Alignment:

* choose the speech segmentation system. It can be either: julius, hvite or basic
* perform basic alignment if the aligner failed, instead such intervals are empty.
* remove working directory will keep only alignment result: it will remove working files. Working directory includes one wav file per unit and a set of text files per unit.
* create the PhnTokAlign will append anoter tier with intervals of the phonetization of each word.

To perform the annotation, click on the Alignment activation button, select
the language and click on the "Configure..." blue text to fix options.


### Perform Alignment with the CLI

`alignment.py` is the program to perform automatic speech segmentation of a
given phonetized file.


#### Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
alignment.py [files] [options]

Alignment: Time-alignment of speech audio with its corresponding transcription
at the phone and token levels. Requires a Phonetization.

optional arguments:
  -h, --help            show this help message and exit
  --quiet               Disable the verbosity
  --log file            File name for a Procedure Outcome Report (default: None)

Files (manual mode):
  -i file               Input wav file name.
  -p file               Input file name with the phonetization.
  -t file               Input file name with the tokenization.
  -o file               Output file name with estimated alignments.
  
Files (auto mode):
  -I file               Input transcription file name (append).
  -l lang               Language code (iso8859-3). One of: cat cmn deu eng
						eng-cd fra ita jpn kor nan pcm pol por spa yue.
  -e .ext               Output file extension. One of: .xra .TextGrid .eaf
						.csv .mrk .txt .stm .ctm .lab .mlf .sub .srt .antx
						.arff .xrff

Resources:
  -r model              Directory of the acoustic model of the language of the
						text
  -R model              Directory of the acoustic model of the mother language
						of the speaker (under development)

Options:
  --inputpattern INPUTPATTERN
                        Input file pattern (phonetization) (default: -phon)
  --inputoptpattern INPUTOPTPATTERN
                        Optional input file pattern (tokenization) (default:
                        -token)
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: -palign)
  --aligner ALIGNER     Speech automatic aligner system (julius, hvite,
						basic): (default: julius)
  --basic BASIC         Perform basic alignment if the aligner fails (default:
						False)
  --clean CLEAN         Remove working directory (default: True)
  --activity ACTIVITY   Create the Activity tier (default: True)
  --activityduration ACTIVITYDURATION
						Create the ActivityDuration tier (default: False)

This program is part of SPPAS version 2.4. Copyright (C) 2011-2019 Brigitte
Bigi. Contact the author at: contact@sppas.org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#### Example of use

Obviously, before launching the following command, you already prepared
the required file (the result of phonetization) and the optional file
(the result of text normalization segmented into IPUs). 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\alignment.py -I .\samples\samples-eng\oriana1.wav -l eng
	2018-12-19 18:33:38,842 [INFO] Logging set up level=15
	2018-12-19 18:33:38,844 [INFO] Options
	2018-12-19 18:33:38,844 [INFO]  ... activityduration: False
	2018-12-19 18:33:38,845 [INFO]  ... activity: True
	2018-12-19 18:33:38,845 [INFO]  ... aligner: julius
	2018-12-19 18:33:38,845 [INFO]  ... clean: True
	2018-12-19 18:33:38,845 [INFO]  ... basic: False
	2018-12-19 18:33:38,845 [INFO] File oriana1.wav: Valid.
	2018-12-19 18:33:38,845 [INFO] File oriana1-phon.xra: Valid.
	2018-12-19 18:33:38,846 [INFO] File oriana1-token.xra: Valid.
	2018-12-19 18:33:38,846 [WARNING]  ... ... A file with name E:\bigi\Projets\sppas\samples\samples-eng\oriana1-palign.xra is already existing. It will be overridden.
	2018-12-19 18:33:38,855 [INFO]  ... Découpage en intervalles.
	2018-12-19 18:33:38,901 [INFO]  ... Intervalle numéro 1.
	2018-12-19 18:33:38,904 [INFO]  ... Intervalle numéro 2.
	2018-12-19 18:33:38,908 [INFO]  ... Intervalle numéro 3.
	2018-12-19 18:33:38,913 [INFO]  ... Intervalle numéro 4.
	2018-12-19 18:33:38,917 [INFO]  ... Intervalle numéro 5.
	2018-12-19 18:33:38,921 [INFO]  ... Intervalle numéro 6.
	2018-12-19 18:33:38,926 [INFO]  ... Intervalle numéro 7.
	2018-12-19 18:33:38,928 [INFO]  ... Fusion des alignements des intervalles.
	2018-12-19 18:33:38,969 [INFO]  ... Création de la tier des activités.
	2018-12-19 18:33:38,993 [INFO]  ... E:\bigi\Projets\sppas\samples\samples-eng\oriana1-palign.xra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



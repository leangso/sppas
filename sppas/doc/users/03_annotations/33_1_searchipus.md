## Search for Inter-Pausal Units (IPUs)

### Overview

The Search for IPUs is a semi-automatic annotation process. 
This segmentation provides an annotated file with one tier named "IPUs".
The silence intervals are labelled with the "#" symbol, and IPUs intervals
are labelled with "ipu_" followed by the IPU number. 
This annotation is semi-automatic: **it should be verified manually**.

Notice that the better recording quality, the better IPUs segmentation.

### The parameters

The following parameters must be properly fixed:

* Minimum volume value (in seconds):
If this value is set to zero, the minimum volume is automatically adjusted
for each sound file. Try with it first, then if the automatic value is not
correct, set it manually. The Procedure Outcome Report indicates the value
the system choose. The AudioRoamer component can also be of great help: it
indicates min, max and mean volume values of the sound.

* Minimum silence duration (in seconds):
By default, this is fixed to 0.2 sec. This duration mostly depends on the
language. It is commonly fixed to at least 0.2 sec for French and at least 
0.25 seconds for English language.

* Minimum speech duration (in seconds):
By default, this value is fixed to 0.3 sec. A relevant value depends on the 
speech style: for isolated sentences, probably 0.5 sec should be better,
but it should be about 0.1 sec for spontaneous speech.

* IPUs boundary shift (in seconds) for start or end: a duration which is 
systematically added to IPUs boundaries, to enlarge the IPUs interval,
and as a consequence, the neighboring silences are reduced.

The procedure outcome report indicates the values (volume, minimum durations)
that were used by the system for each sound file. 

### Perform "Search for IPUs" with the GUI

It is an annotation of STANDALONE type.

Click on the "Search IPUs" activation button and on the "Configure..." blue
text to fix options.

![Example of result](etc/screenshots/ipu-seg-result1.png)

Notice that the speech segments can be transcribed using SPPAS, in the
"Analyze" page.

![Orthographic transcription based on IPUs](etc/screenshots/IPUscribe-2.png)


### Perform "Search for IPUs" with the CLI

`searchipus.py` is the program to perform this semi-automatic annotation, i.e.
silence/IPUs segmentation, either on a single file (-i and optionnally -o) or 
on a set of files (by using -I and optionnally -e).


#### Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
searchipus.py [files] [options]

Search for IPUs: Search for Inter-Pausal Units in an audio file.

optional arguments:
  -h, --help            show this help message and exit
  --quiet               Disable the verbosity
  --log file            File name for a Procedure Outcome Report (default: None)

Files (manual mode):
  -i file               Input wav file name.
  -o file               Annotated file with silences/units segmentation
						(default: None)

Files (auto mode):
  -I file               Input wav file name (append).
  -e .ext               Output file extension. One of: .xra .TextGrid .eaf
						.csv .mrk .txt .stm .ctm .lab .mlf .sub .srt .antx
						.arff .xrff

Options:
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: )
  --win_length WIN_LENGTH
						Window size to estimate rms (in seconds) (default:
						0.020)
  --threshold THRESHOLD
						Threshold of the volume value (rms) for the detection
						of silences, 0=automatic (default: 0)
  --min_ipu MIN_IPU     Minimum duration of an IPU (in seconds) (default:
						0.300)
  --min_sil MIN_SIL     Minimum duration of a silence (in seconds) (default:
						0.200)
  --shift_start SHIFT_START
						Systematically move at left the boundary of the
						beginning of an IPU (in seconds) (default: 0.01)
  --shift_end SHIFT_END
						Systematically move at right the boundary of the end
						of an IPU (in seconds) (default: 0.02)

This program is part of SPPAS version 2.4. Copyright (C) 2011-2019 Brigitte
Bigi. Contact the author at: contact@sppas.org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#### Examples of use

A single input file and output on stdout:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\searchipus.py -i .\samples\samples-eng\oriana1.wav
 	2018-12-19 10:49:32,782 [INFO] Logging set up level=15
	2018-12-19 10:49:32,790 [INFO]  ... Information:
	2018-12-19 10:49:32,792 [INFO]  ... ... Number of IPUs found:       3
	2018-12-19 10:49:32,792 [INFO]  ... ... Threshold volume value:     0
	2018-12-19 10:49:32,792 [INFO]  ... ... Threshold silence duration: 0.200
	2018-12-19 10:49:32,792 [INFO]  ... ... Threshold speech duration:  0.300
	0.000000 1.675000 #
	1.675000 4.580000 ipu_1
	4.580000 6.390000 #
	6.390000 9.880000 ipu_2
	9.880000 11.430000 #
	11.430000 14.740000 ipu_3
	14.740000 17.792000 #
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Idem without logs:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\searchipus.py -i .\samples\samples-eng\oriana1.wav --quiet
	0.000000 1.675000 #
	1.675000 4.580000 ipu_1
	4.580000 6.390000 #
	6.390000 9.880000 ipu_2
	9.880000 11.430000 #
	11.430000 14.740000 ipu_3
	14.740000 17.792000 #
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Several input files, output in Praat-TextGrid file format:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\searchipus.py -I .\samples\samples-eng\oriana1.wav \
 -I .\samples\samples-eng\oriana3.wave -e .TextGrid
	2018-12-19 10:48:16,520 [INFO] Logging set up level=15
	2018-12-19 10:48:16,522 [INFO] File oriana1.wav: Valid.
	2018-12-19 10:48:16,532 [INFO]  ... Information:
	2018-12-19 10:48:16,532 [INFO]  ... ... Number of IPUs found:       3
	2018-12-19 10:48:16,532 [INFO]  ... ... Threshold volume value:     0
	2018-12-19 10:48:16,532 [INFO]  ... ... Threshold silence duration: 0.200
	2018-12-19 10:48:16,533 [INFO]  ... ... Threshold speech duration:  0.300
	2018-12-19 10:48:16,538 [INFO]  ... E:\bigi\Projets\sppas\samples\samples-eng\oriana1.TextGrid
	2018-12-19 10:48:16,538 [INFO] File oriana3.wave: Invalid. 
	2018-12-19 10:48:16,539 [ERROR]  ... ... An audio file with only one channel is expected. Got 2 channels.
	2018-12-19 10:48:16,540 [INFO]  ... No file was created.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


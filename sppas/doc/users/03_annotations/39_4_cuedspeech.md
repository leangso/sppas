## Cued speech - LfPC

**This automatic annotation is currently under development.**

It's an in-progress project and currently only a Proof of Concept
is distributed.
**It mustn't be used neither for any final application nor evaluation**. 

    Cued Speech annotation can only be used in order to test it and 
    to contribute in the project.

### Overview

Speech reading or lip-reading requires watching the lips of a speaker 
and is used for the understanding of the spoken sounds. However, various 
sounds have the same lips movement which implies a lot of ambiguity.
In 1966, R. Orin Cornett invented the Cued Speech, a visual system of 
communication. It adds information about the pronounced sounds that 
are not visible on the lips.

Thanks to this code, speech reading is encouraged since the Cued Speech
(CS) keys match all the spoken phonemes but phonemes with the same 
movement have different keys. Actually, from both the hand position on 
the face (representing vowels) and hand shapes, known as cues (representing 
consonants), CV syllables can be represented.
So, a single CV syllable will be generated or decoded through both the 
lips position and the key of the hand.

LfPC is the French acronym for "Langue française Parlée Complétée". 

The conversion of phonemes into keys of CS is performed using
a rule-based system. This RBS phoneme-to-key segmentation system
is based on the only principle that a key is always of the form CV.

This annotation requires both to enable the "video" feature in the setup 
to install the external libraries 'numpy' and 'opencv-contrib' and to 
check "cuedspeech" in the list of annotations.

### Perform annotation with the GUI

It is a STANDALONE annotation.

The annotation process takes as input a "-palign" file and optionally a 
video. To perform the annotation, click on its activation button
and click on the "Configure..." blue text to fix options.

### Perform with the CLI

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: cuedspeech.py [files] [options]

optional arguments:
  -h, --help            show this help message and exit
  --quiet               Disable the verbosity
  --log file            File name for a Procedure Outcome Report 
                        (default: None)

Files:
  -i file               Input time-aligned phonemes file name.
  -v file               Input video file name.
  -o file               Output file name with Cued Speech key codes.
  -r rules              File with Cued Speech keys description

  -I file               Input file name (append).
  -l lang               Language code (iso8859-3). One of: fra.
  -e .ext               Output file extension. One of: .xra .TextGrid 
                        .eaf .ant .antx .mrk .lab .srt .sub .vtt .ctm
                        .stm .csv .txt

Options:
  --inputpattern1 INPUTPATTERN1
                        Pattern of the file with time-aligned phonemes 
                        (default: -palign)
  --inputpattern2 INPUTPATTERN2
                        Pattern of the video file (default: )
  --inputpattern3 INPUTPATTERN3
                        Pattern of the file with sights of the face of 
                        the person (default: -sights)
  --outputpattern OUTPUTPATTERN
                        Pattern of the output files (default: -cuedsp)
  --createvideo CREATEVIDEO
                        Tag the video with the code of the key 
                        (needs video+csv) (default: False)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

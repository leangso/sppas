## Re-Occurrences

This annotation is searching for re-occurrences of an annotation of a speaker in the next N annotations of the interlocutor. It is originally used for gestures in (M. Karpinski et al. 2018).

> Maciej Karpinski, Katarzyna Klessa
> Methods, Tools and Techniques for Multimodal Analysis of Accommodation in Intercultural Communication
> CMST 24(1) 29–41 (2018), DOI:10.12921/cmst.2018.0000006


### Perform Re-Occurrences with the GUI


The automatic annotation takes as input any annotated file with (at least) one
tier, and another file+tier of the interlocutor.
The annotation provides one annotated file with 2 tiers: Sources and Repetitions.

Click on the Re-Occurrences activation button, and 
click on the "Configure..." blue text to fix options.


### Perform Re-Occurrences with the CLI


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: reoccurrences.py [files] [options]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Files:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  -i file               Input file name with time-aligned annotations of 
                        the main speaker.
  -s file               Input file name with time-aligned annotations of 
                        the interlocutor
  -o file               Output file name with re-occurrences.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Options:


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  --inputpattern INPUTPATTERN
                        Input file pattern (default: )
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: -reocc)
  --tiername TIERNAME   Tier to search for re-occurrences (default: )
  --span SPAN           Span window length in number of annotations (default:
                        10)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This program is part of SPPAS version 2.4. Copyright (C) 2011-2019 Brigitte
Bigi. Contact the author at: contact@sppas.org


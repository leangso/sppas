## Face Detection

This is a state-of-the-art implementation of face detection performed
with freely available models and tools. We introduced an original method
in order to use any or all of them and to combine their results. 
This combined result was not evaluated.

### Overview

FaceDetection annotation allows to search for coordinates of faces in an image
or in all images of a video. It requires both to enable the "video" feature in 
the setup to install the external libraries 'numpy', 'opencv-contrib' and 
'mediapipe'. It also requires to check "facedetect" in the list of annotations 
to be installed at the time of the setup.

On the basis of the detection methods implemented in the opencv library, 
SPPAS is able to use several systems and to combine their results.
These systems are based on 2 different methods:

1. an Artificial Neural Network (DNN);
2. an Haar Cascade Classifier (HCC).

The linguistic resources of this annotation include two DNN models and two 
models for HCC (a frontal-face model and a profile-face one).
SPPAS can also launch the MediaPipe face detection system which is much more
faster than the previous ones but results sounds worse.

By default, SPPAS launches two of these detectors: 1 DNN and 1 HCC, and 
it combines their results. This annotation is about 2.5x real time.
Even if it can increase the quality of the final result, other models are not 
used by default because the detection is very slow: 15x real time to use all 
of the 5 models. 
The options allow to choose the models to be used.


### Result of Face Detection

There are several output files that can be created:

- a copy of the image/video with all the detected faces sourrounded by a 
  square indicating a confidence score;
- as many cropped image files as the number of detected faces;
- an XRA or a CSV file with coordinates and confidence score of each detected face.

There's also the possibility to consider the selfie (portrait size) 
instead of the face.


### Perform Face Detection with the GUI

It is a STANDALONE annotation.

The Face Detection process takes as input an image file and/or a video.
To perform the annotation, click on the FaceDetection activation button
and click on the "Configure..." blue text to fix options.


### Perform Face Detection with the CLI

`facedetection.py` is the program to perform Face Detection annotation
of a given media file.

#### Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: facedetection.py [files] [options]

optional arguments:
  -h, --help            show this help message and exit
  --quiet               Disable the verbosity
  --log file            File name for a Procedure Outcome Report (default: None)

Files:
  -i file               Input image.
  -o file               Output base name.
  -I file               Input file name (append).
  -r model              Model base name (.caffemodel or .xml models as wishes)
  -e .ext               Output file extension (image or video)

Options:
  --inputpattern INPUTPATTERN
                        Input file pattern (default: )
  --nbest NBEST         Number of faces to select among those
                        detected (0=auto) (default: 0)
  --score SCORE         Minimum confidence score to select detected
                        faces (default: 0.2)
  --portrait PORTRAIT   Consider the portrait instead of the face in
                        outputs (default: False)
  --csv CSV             Save coordinates of detected faces in a CSV
                        file instead of XRA (default: False)
  --folder FOLDER       Save result in a folder with image files -- 
                        if video input only (default: False)
  --tag TAG             Surround the detected faces in the output
                        image (default: True)
  --crop CROP           Save detected faces in cropped images
                        (default: False)
  --width WIDTH         Resize all the cropped images to a fixed
                        width (0=no) (default: 0)
  --height HEIGHT       Resize all the cropped images to a fixed
                        height (0=no) (default: 0)
  --model:opencv_face_detector_uint8.pb MODEL
                        Enable the opencv's ANN TensorFlow model. 
                        (default: True)
  --model:haarcascade_frontalface_alt.xml MODEL
                        Enable the opencv's HaarCascade Frontal face model. 
                        (default: True)
  --model:res10_300x300_ssd_iter_140000_fp16.caffemodel MODEL
                        Enable the opencv's ANN Caffe model. 
                        (default: False)
  --model:haarcascade_profileface.xml MODEL
                        Enable the opencv's HaarCascade Profile face model. 
                        (default: False)
  --model:mediapipe MODEL:MEDIAPIPE
                        Enable the MediaPipe Face Detection system. 
                        (default: False)

This program is part of SPPAS version 4.2. Copyright (C) 2011-2021
Brigitte Bigi. Contact the author at: contact@sppas.org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#### Examples of use

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python3 ./sppas/bin/facedetection.py -I ./samples/faces/BrigitteBigi_Aix2020.png --tag=True --crop=True --csv=True --portrait=True
[INFO] Logging redirected to StreamHandler (level=0).
[INFO] SPPAS version 3.5
[INFO] Copyright (C) 2011-2021 Brigitte Bigi
[INFO] Web site: https://sppas.org/
[INFO] Contact: Brigitte Bigi (contact@sppas.org)
[INFO]  * * * Annotation step 0 * * *
[INFO] Number of files to process: 1
[INFO] Options:
[INFO]  ... inputpattern:
[INFO]  ... outputpattern: -face
[INFO]  ... nbest: 0
[INFO]  ... score: 0.2
[INFO]  ... portrait: True
[INFO]  ... csv: True
[INFO]  ... tag: True
[INFO]  ... crop: True
[INFO]  ... width: 0
[INFO]  ... height: 0
[INFO] File BrigitteBigi_Aix2020.png: Valid.
[INFO]  ...  ... 3 faces found.
[INFO]  ... ./samples/faces/BrigitteBigi_Aix2020-face.jpg
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It creates the following 5 files in the samples/faces folder:

- BrigitteBigi_Aix2020-face.jpg with the 3 detected faces sourrounded by a square and indicating the detection score
- BrigitteBigi_Aix2020_1-face.jpg: image of the face with the highest score
- BrigitteBigi_Aix2020_2-face.jpg: image of the face with the medium score
- BrigitteBigi_Aix2020_3-face.jpg: image of the face with the worse score
- BrigitteBigi_Aix2020-face.csv contains 3 lines with coordinates in columns 3-7 with (x,y) and (w,h) then the confidence score in column 8 ranging [0., 1.]

Notice that the image contains 3 faces and their positions are properly found.


## Momel (modelling melody)

Momel is an algorithm for the automatic modeling of fundamental frequency (F0)
curves using a technique called asymetric modal quadratic regression.

This technique makes it possible by an appropriate choice of parameters to 
factor an F0 curve into two components:

* a macro-prosodic component represented by a a quadratic spline function defined by a sequence of target points < ms, hz >.
* a micro-prosodic component represented by the ratio of each point on the F0 curve to the corresponding point on the quadratic spline function.

For details, see the following reference:

>**Daniel Hirst and Robert Espesser** (1993).
>*Automatic modelling of fundamental frequency using a quadratic spline function.*
>Travaux de l’Institut de Phonétique d’Aix. vol. 15, pages 71-85.

The SPPAS implementation of Momel requires a file with the F0 values
**sampled at 10 ms**. Two file formats are supported:

- ".PitchTier", from Praat.
- ".hz", from any tool. It is a file with one F0 value per line.


The following options can be fixed:

* Window length used in the "cible" method 
* F0 threshold: Maximum F0 value
* F0 ceiling: Minimum F0 value
* Maximum error: Acceptable ratio between two F0 values
* Window length used in the "reduc" method
* Minimal distance
* Minimal frequency ratio
* Eliminate glitch option: Filter f0 values before 'cible'


### Perform Momel with the GUI

It is an annotation of STANDALONE type.

Click on the Momel activation button then click on the "Configure..." 
blue text to fix options. 


### Perform Momel with the CLI

`momel.py` is the program to perform Momel annotation of a given file with
F0 values sampled at 10ms.


#### Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
momel.py [files] [options]

Momel: Proposed by D. Hirst and R. Espesser, Momel - Modelling of fundamental
frequency (F0) curves is using a technique called assymetric modal quaratic
regression. Requires pitch values.

optional arguments:
  -h, --help       show this help message and exit
  --quiet          Disable the verbosity
  --log file       File name for a Procedure Outcome Report (default: None)

Files (manual mode):
  -i file          Input file name (extension: .hz or .PitchTier)
  -o file          Output file name (default: stdout)

Files (auto mode):
  -I file          Input file name with pitch (append).
  -e .ext          Output file extension. One of: .xra .TextGrid .eaf .csv
				   .mrk .txt .stm .ctm .lab .mlf .sub .srt .antx .arff .xrff

Options:
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: -momel)
  --win1 WIN1      Target window length (default: 30)
  --lo LO          F0 threshold (default: 50)
  --hi HI          F0 ceiling (default: 600)
  --maxerr MAXERR  Maximum error (default: 1.04)
  --win2 WIN2      Reduce window length (default: 20)
  --mind MIND      Minimal distance (default: 5)
  --minr MINR      Minimal frequency ratio (default: 0.05)

This program is part of SPPAS version 2.4. Copyright (C) 2011-2019 Brigitte
Bigi. Contact the author at: contact@sppas.rg
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### Examples of use

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\momel.py -i .\samples\samples-eng\ENG_M15_ENG_T02.PitchTier
	2018-12-19 15:44:00,437 [INFO] Logging set up level=15
	2018-12-19 15:44:00,674 [INFO]  ... ... 41 anchors found.
	1.301629 109.285503
	1.534887 126.157058
	1.639614 143.657446
	1.969234 102.911464
	2.155284 98.550759
	2.354162 108.250869
	2.595364 87.005994
	2.749773 83.577924
	2.933222 90.218382
	3.356651 119.709142
	3.502254 104.104568
	3.707747 132.055286
	4.000578 96.262109
	4.141915 93.741407
	4.383332 123.996736
	4.702203 89.152708
	4.987086 101.561180
	5.283864 87.499710
	5.538984 92.399690
	5.707147 95.411586
	5.906895 87.081095
	6.705373 121.396919
	7.052992 130.821479
	7.218415 120.917642
	7.670083 101.867028
	7.841935 109.094053
	8.124574 90.763267
	8.455182 114.261067
	8.746016 93.704705
	9.575359 101.108444
	9.996245 122.488120
	10.265663 105.244429
	10.576394 94.875460
	11.730570 99.698799
	12.083323 124.002313
	12.411790 108.563104
	12.707442 101.928297
	12.963805 113.980850
	13.443483 90.782781
	13.921939 90.824376
	14.377324 60.126506
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Apply Momel on all files of a given folder:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\momel.py -I .\samples\samples-eng
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


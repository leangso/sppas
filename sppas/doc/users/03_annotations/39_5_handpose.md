## Hand&Pose

SPPAS is a wrapper for MediaPipe Hand detection and Mediapipe Pose detection.
It also proposes a custom solution in order to detect right-left hands of a 
person.

### Overview

MediaPipe Hands is a high-fidelity hand and finger tracking solution.
It employs machine learning (ML) to infer 21 3D landmarks of a hand
from just a single frame.
MediaPipe Hands utilizes an ML pipeline consisting of multiple models
working together: A palm detection model that operates on the full image
and returns an oriented hand bounding box. A hand landmark model that
operates on the cropped image region defined by the palm detector and
returns high-fidelity 3D hand key points.
For details about hand detection, see:
<https://google.github.io/mediapipe/solutions/hands.html>

MediaPipe Pose is a ML solution for high-fidelity body pose tracking, 
inferring 33 3D landmarks. For details about pose detection, see:
<https://google.github.io/mediapipe/solutions/pose.html>

Important:

- SPPAS is only exporting (x,y)
- SPPAS can export the results in either XRA or CSV formats.


Three detection "mode" are available:

1. hands only: when enabling hands and disabling pose, SPPAS is a simple
wrapper for MediaPipe Hand detection. The result of the system is a list
of 21 sights for each detected hand.
2. pose only: when disabling hand detection and enabling pose detection,
SPPAS is a simple wrapper for MediaPipe Pose detection. Notice that if
several persons are on the image, it will detect only one human body. The
result of the system is a list of the 33 sights of the human body.
3. hand&pose: when enabling both hand and pose, SPPAS is combining
both pose detections and hand detections. It results in two lists of
sights: the first list for the right hand and the second list of the left
hand. Each list is made either of 21 sights if the hand was properly 
detected by the hand prediction system, or made of the 4 sights of the 
pose detection system.

Here is the match between the indexes of the 8 sights of the pose 
detection and the indexes of the sights of the hand detection:

- right hand: pose 16 is hand 0, pose 18 is hand 8, 
pose 20 is hand 20 and pose 22 is hand 4.
- left hand: pose 15 is hand 0, pose 17 is hand 8, 
pose 19 is hand 20 and pose 21 is hand 4.


### Perform annotation with the GUI

It is a STANDALONE annotation.

The annotation process takes as input an image or a video file. To perform 
the annotation, click on its activation button and click on the "Configure..." 
blue text to fix options.

### Perform with the CLI

The CLI does not work on an image but video recordings only. To perform this
annotation on an image, use the script "annotation.py" instead.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: handpose.py [files] [options]

optional arguments:
  -h, --help            show this help message and exit
  --quiet               Disable the verbosity
  --log file            File name for a Procedure Outcome Report 
                        (default: None)

Files:
  -i file               Input video.
  -o file               Output base name.
  -I file               Input file name (append).
  -e .ext               Output file extension. One of: .mp4 .avi .mkv .mov

Options:
  --inputpattern1 INPUTPATTERN1
                        Input pattern of the video file. (default: )
  --outputpattern OUTPUTPATTERN
                        Output file pattern with the sights. (default: -hands)
  --hand HAND           Enable hands detection. (default: True)
  --pose POSE           Enable pose detection -- for only one human body. (default: True)
  --csv CSV             Save points in a CSV file instead of XRA (default: False)
  --tag TAG             Draw points on the video (default: True)
  --folder FOLDER       Save result in a folder with image files -- if video input only (default: False)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

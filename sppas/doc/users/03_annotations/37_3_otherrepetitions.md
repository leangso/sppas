## Other-Repetitions

### Overview

This automatic detection focus on other-repetitions, which can be either exact
repetitions (named strict echos) or repetitions with variations (named 
non-strict echos). The system is based only on lexical criteria (Bigi et al. 
2014). Notice that the algorithm is focusing on the detection of the source.

This system can use a list of stop-words of a given language. This is a list
of very frequent words like adjectives, pronouns, etc.
Obviously, the result of the automatic detection is significantly better 
if such list of stop-words is available. 

Optionnaly, SPPAS can add new stop-words in the list: they are deduced from
the given data. These new entries in the stop-list are then different for 
each file (see Bigi et al. 2014).

The detection of the ORs is performed in a span window of N IPUs; by default,
N is fixed to 5. It means that if a repetition is after these N IPUs, it won't
be detected. Technically, it also means that SPPAS needs to identify the 
boundaries of the IPUs from the time-aligned tokens: the tier must indicate
the silences with the '#' symbol. 

A file with the following tiers will be created:

- OR-Source: intervals with the number of the sources
- OR-SrcStrain: intervals with the tokens of the sources
- OR-SrcLen: intervals with the number of tokens in the source
- OR-SrcType: intervals with the type of the echos of the sources
- OR-Echo: intervals with the number of the echos


### Adapt to a language and support of a new one

This system can use a list of stop-words of a given language. This is a list
of very frequent words like adjectives, pronouns, etc.
Obviously, the result of the automatic detection is significantly better 
if such list of stop-words is available. It must be located in the "vocab" 
folder of the "resources" directory with ".stp" extension. This file is with
[UTF-8 encoding](https://en.wikipedia.org/wiki/UTF-8) 
and ["LF" for newline](https://en.wikipedia.org/wiki/Newline).


### Perform Other-Repetitions with the GUI

It is an annotation of INTERACTION type.

The automatic annotation takes as input a file with (at least) one
tier containing the time-aligned tokens of the main speaker, and another 
file/tier with tokens of the interlocutor.
The annotation provides one annotated file with 2 tiers: Sources and Repetitions.

Click on the Other-Repetitions activation button, select the language and 
click on the "Configure..." blue text to fix options.


### Perform Other-Repetitions with the CLI


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: otherrepetition.py -r stopwords [files] [options]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Files:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  -i file               Input file name with time-aligned tokens of the main
                        speaker.
  -s file               Input file name with time-aligned tokens of the
                        echoing speaker
  -o file               Output file name with ORs.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Options:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  --inputpattern INPUTPATTERN
                        Input file pattern (time-aligned words or lemmas)
                        (default: -palign)
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: -orepet)
  --span SPAN           Span window length in number of IPUs (default: 3)
  --stopwords STOPWORDS Add stop-words estimated from the given data 
                        (default: True)
  --alpha ALPHA         Coefficient to add data-specific stop-words 
                        (default: 0.5)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

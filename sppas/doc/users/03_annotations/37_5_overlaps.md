## Overlapping activity

This annotation is searching for the overlapping activity in annotations
of a speaker and the interlocutor. An overlapping activity is defined by 
the fact that the 2 speakers are doing the same thing, i.e. the annotation
labels are both the same at the same time.

When applied on the Activity tiers, it allows to know when the speakers
are laughting together, speaking together, or when they are not talking
together.


### Perform the annotation with the GUI

The automatic annotation takes as input any annotated file with (at least) one
tier, and another file+tier of the interlocutor. By default, the pattern of the
input file is "-activity" and the input tier is "Activity". 
It allows to fix a list of tags to ignore, i.e. this joint activity is not 
interesting. By default, it is considered that "dummy", "noise" and "pause" have
to be ignored.
The annotation provides one annotated file with 1 tier: Overlapped.

Click on the Overlaps activation button, and 
click on the "Configure..." blue text to fix options.


### Perform the annotation with the CLI


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: overlaps.py [files] [options]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Files:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  -i file               Input file name with activity - spk1 
  -s file               Input file name with activity - spk2
  -o file               Output file name with overlaps.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Options:


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  --inputpattern INPUTPATTERN
                        Input file pattern (default: -activity)
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: -overlaps)
  --tiername TIERNAME   Tier to search for overlaps (default: Activity)
  --ignore IGNORE       Tags to ignore, separated by commas 
                        (default: dummy,pause,noise)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This program is part of SPPAS version 3.7. Copyright (C) 2011-2021 Brigitte
Bigi. Contact the author at: contact@sppas.org


## Activity

### Overview

Activity tier represents speech activities, i.e. speech, silences, 
laughter, noises... It is based on the analysis of the time-aligned tokens.


### Perform Activity with the GUI

It is an annotation of STANDALONE type.

The Activity process takes as input a file that strictly match the
audio file name except for the extension and that "-palign" is appended.
For example, if the audio file name is "oriana1.wav", the expected input file
name is "oriana1-palign.xra" if .xra is the default extension for annotations.
This file must include time-aligned phonemes in a tier with name "PhonAlign".

The annotation provides an annotated file with "-activity" appended to its name,
i.e. "oriana1-activity.xra" for the previous example.
This file is including 1 or 2 tiers: Activity, ActivityDuration.

To perform the annotation, click on the Activity activation button and 
click on the "Configure..." blue text to fix options.


### Perform Alignment with the CLI

No CLI is available for this annotation.

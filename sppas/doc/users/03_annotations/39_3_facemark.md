## Face Landmarks

This is a state-of-the-art implementation of face landmark performed
with freely available models and tools. We introduced a solution to
combine the results when several methods are used. 
The combined result was not evaluated.

### Overview

SPPAS is using both the MediaPipe Face Mesh and the OpenCV's facial 
landmark API called Facemark. 
It includes three different implementations of landmark detection based 
on three different papers:

- FacemarkKazemi: This implementation is based on a paper titled
"One Millisecond Face Alignment with an Ensemble of Regression Trees"
 by V.Kazemi and J. Sullivan published in CVPR 2014.
- FacemarkAAM: This implementation uses an Active Appearance Model
and is based on an the paper titled "Optimization problems for fast
AAM fitting in-the-wild" by G. Tzimiropoulos and M. Pantic, published
in ICCV 2013.
- FacemarkLBF: This implementation is based a paper titled "Face
alignment at 3000 fps via regressing local binary features" by
S. Ren published in CVPR 2014.

The fundamental concept is that any person will have 68 particular points
on the face (called sights). SPPAS is able to launch several of them and
to combine their results in a single and hopefully better one.
Actually, SPPAS is launching MediaPipe Face Mesh and extracting the 
68 sights among the 468 that are detected ; then this result is combined
(weight=6) with the 68 sights of the LBF detection method (weight=1).

This annotation requires both to enable the "video" feature in the 
setup in order to install the external libraries 'numpy' and 
'opencv-contrib', and to check "facemark" in the list of annotations to 
be installed. Two different models will be downloaded and used: 
a Kazemi one and a LBF one.

### Perform annotation with the GUI

It is a STANDALONE annotation.

The Face Sights process takes as input an image file and/or a video.
To perform the annotation, click on the Face Sights activation button
and click on the "Configure..." blue text to fix options.

### Perform with the CLI

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
usage: facesights.py [files] [options]

optional arguments:
  -h, --help            show this help message and exit
  --quiet               Disable the verbosity
  --log file            File name for a Procedure Outcome Report

Files:
  -i file               Input image.
  -o file               Output base name.
  -I file               Input file name (append).
  -r model              Landmark model name (Kazemi, LBF or AAM)
  -R model              FaceDetection model name
  -e .ext               Output file extension. 

Options:
  --inputpattern INPUTPATTERN
  --inputoptpattern INPUTOPTPATTERN (default: -face)
  --outputpattern OUTPUTPATTERN (default: -sights)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Fill in Inter-Pausal Units (IPUs)

### Overview

This automatic annotation consists in aligning macro-units of a
document with the corresponding sound. This segmentation provides 
an annotated file with one tier named "Transcription".

IPUs are blocks of speech bounded by silent pauses of more than X ms.
This annotation searches for a silences/IPUs segmentation of
a recorded file (see previous section) and fill in the IPUs with the 
transcription given in a `txt` file.

### How does it work

SPPAS identifies silent pauses in the signal and attempts to align them with
the units proposed in the transcription file, under the assumption
that each such unit is separated by a silent pause. It is based on the
search of silences described in the previous section, but in this 
case, the number of units to find is known. The system adjusts automatically
the volume threshold and the minimum durations of silences/IPUs to
get the right number of units. The content of the units has no regard, 
because SPPAS does not interpret them: it can be the orthographic 
transcription, a translation, numbers, ...
This algorithm is language-independent: it can work on any language.

In the transcription file, **silent pauses must be indicated** using both
solutions, which can be combined:

* with the symbol '#';
* with newlines.

A recorded speech file must strictly correspond to a `txt` file of the 
transcription. The annotation provides an annotated file with one tier
named "Transcription". The silence intervals are labelled with the "#" symbol,
as IPUs are labelled with "ipu_" followed by the IPU number then the 
corresponding transcription.

The same parameters as those indicated in the previous section must be fixed.

> Remark:
> This annotation was tested on read speech no longer than a few sentences
> (about 1 minute speech) and on recordings of very good quality.

![Fill in IPUs](etc/screenshots/ipu-seg-result2.png)


### Perform "Fill in IPUs" with the GUI

It is an annotation of STANDALONE type.

Click on the "Fill in IPUs" activation button and on the "Configure..." blue
text to fix options.


### Perform "Fill in IPUs" with the CLI

`fillipus.py` is the program to perform this IPUs segmentation, i.e.
silence/ipus segmentation, either on a single file (-i and optionally -o) or 
on a set of files (by using -I and optionally -e).


#### Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
fillipus.py [files] [options]

Fill in IPUs: Search for Inter-Pausal Units and fill in with a transcription.
Requires an audio file and a .txt file with the transcription.

optional arguments:
  -h, --help         show this help message and exit
  --quiet            Disable the verbosity
  --log file         File name for a Procedure Outcome Report (default: None)

Files (manual mode):
  -i file            Input wav file name.
  -t file            Input transcription file name.
  -o file            Annotated file with filled IPUs

Files (auto mode):
  -I file            Input wav file name (append).
  -e .ext            Output file extension. One of: .xra .TextGrid .eaf .csv
                     .mrk .txt .stm .ctm .lab .mlf .sub .srt .antx .arff .xrff

Options:
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: )
  --min_ipu MIN_IPU  Initial minimum duration of an IPU (in seconds) (default:
					 0.300)
  --min_sil MIN_SIL  Initial minimum duration of a silence (in seconds)
					 (default: 0.200)

This program is part of SPPAS version 3.0. Copyright (C) 2011-2020 Brigitte
Bigi. Contact the author at: contact@sppas.org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#### Examples of use


A single input file with an input in manual mode:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\fillipus.py -i .\samples\samples-eng\oriana1.wav -t .\samples\samples-eng\oriana1.txt
	2018-12-19 11:03:15,614 [INFO] Logging set up level=15
	2018-12-19 11:03:15,628 [INFO]  ... Information:
	2018-12-19 11:03:15,628 [INFO]  ... ... Threshold volume value:     122
	2018-12-19 11:03:15,630 [INFO]  ... ... Threshold silence duration: 0.200
	2018-12-19 11:03:15,630 [INFO]  ... ... Threshold speech duration:  0.300
	0.000000 1.675000 #
	1.675000 4.570000 the flight was 12 hours long and we really got bored
	4.570000 6.390000 #
	6.390000 9.870000 they only played two movies + which we had both already seen
	9.870000 11.430000 #
	11.430000 14.730000 I never get to sleep on the airplane because it's so uncomfortable
	14.730000 17.792000 #
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A single input file in automatic mode:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\fillipus.py -I .\samples\samples-eng\oriana1
python .\sppas\bin\fillipus.py -I .\samples\samples-eng\oriana1.wav
python .\sppas\bin\fillipus.py -I .\samples\samples-eng\oriana1.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




## Text normalization

### Overview

In principle, any system that deals with unrestricted text need the text to
be normalized. Texts contain a variety of "non-standard" token types such as
digit sequences, words, acronyms and letter sequences in all capitals, mixed
case words, abbreviations, roman numerals, URL's and e-mail addresses...
Normalizing or rewriting such texts using ordinary words is then an important
issue. The main steps of the text normalization implemented in SPPAS
(Bigi 2011) are:

* Replace symbols by their written form, thanks to a "replacement" dictionary,
  located into the folder "repl" in the "resources" directory.
* Word segmentation based on the content of a lexicon.
* Convert numbers to their written form.
* Remove punctuation.
* Lower the text.


### Adapt Text normalization

Word segmentation of SPPAS is mainly based on the use of a lexicon.
If a segmentation is not as expected, it is up to the user to modify
the lexicon: Lexicons of all supported languages are all located in the folder
"vocab" of the "resources" directory. They are in the form of "one
word at a line" with [UTF-8 encoding](https://en.wikipedia.org/wiki/UTF-8) 
and ["LF" for newline](https://en.wikipedia.org/wiki/Newline).


### Support of a new language

Adding a new language in Text Normalization consists in the following steps:

1. Create a lexicon. Fix properly its encoding (utf-8), its newlines (LF), 
and fix the name and extension of the file as follow: 
    - language name with iso639-3 standard
    - extension ".vocab"
2. Put this lexicon in the `resources/vocab` folder
3. Create a replacement dictionary for that language (take a look on the ones of the other language!)
4. Optionally, the language can be added into the num2letter.py program 

That's it for most of the languages! If the language requires more steps,
simply write to the author to collaborate, find some funding, etc. like it
was already done for Cantonese (Bigi & Fung 2015) for example.


### Perform Text Normalization with the GUI

It is an annotation of STANDALONE type.

The SPPAS Text normalization system takes as input a file (or a list of files) 
for which the name strictly match the name of the audio file except the extension.
For example, if a file with name "oriana1.wav" is given, SPPAS will search for a
file with name "oriana1.xra" at a first stage if ".xra" is set as the default
extension, then it will search for other supported extensions until a file is
found.

This file must include a tier with an orthographic transcription.
At a first stage, SPPAS tries to find a tier with `transcription` as name.
If such a tier does not exist, the first tier that is matching
one of the following strings is used (case-insensitive search):

1. `trans`
2. `trs`
3. `toe`
4. `ortho`

Text normalization produces a file with "-token" appended to its name,
i.e. "oriana1-token.xra" for the previous example.
By default, this file is including only one tier with the resulting 
normalization and with name "Tokens". To get other versions of the 
normalized transcription, click on the "Configure" text then check 
the expected tiers. 

Read the "Introduction" of this chapter for a better understanding of the
difference between "standard" and "faked" results.

To perform the text normalization process, click on the Text Normalization
activation button, select the language and click on the "Configure..." blue 
text to fix options.


### Perform Text Normalization with the CLI

`normalize.py` is the program to perform Text Normalization, i.e. the 
text normalization of a given file or a raw text.

#### Usage

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
normalize.py [files] [options]

Text Normalization: Text normalization segments the orthographic transcription
into tokens and remove punctuation, convert numbers, etc. Requires an
orthographic transcription into IPUs.

optional arguments:
  -h, --help       show this help message and exit
  --quiet          Disable the verbosity
  --log file       File name for a Procedure Outcome Report (default: None)

Files (manual mode):
  -i file          Input transcription file name.
  -o file          Annotated file with normalized tokens.

Files (auto mode):
  -I file          Input transcription file name (append).
  -l lang          Language code (iso8859-3). One of: cat cmn deu eng fra hun
                   ita jpn kor nan pcm pol por spa vie yue yue_chars.
  -e .ext          Output file extension. One of: .xra .TextGrid .eaf .csv
                   .mrk .txt .stm .ctm .lab .mlf .sub .srt .antx .arff .xrff

Resources:
  -r vocab         Vocabulary file name

Options:
  --inputpattern INPUTPATTERN
                        Input file pattern (orthographic transcription)
                        (default: )
  --outputpattern OUTPUTPATTERN
                        Output file pattern (default: -token)
  --faked FAKED    Create a tier with the faked tokens (required for
				   phonetization) (default: True)
  --std STD        Create a tier with the standard tokens (useful if EOT)
				   (default: False)
  --custom CUSTOM  Create a customized tier (default: False)
  --occ_dur OCC_DUR     Create tiers with number of tokens and duration of
                        each IPU (default: True)

This program is part of SPPAS version 2.4. Copyright (C) 2011-2019 Brigitte
Bigi. Contact the author at: contact@sppas.org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#### Examples of use


A single input file with a raw transcription input in manual mode:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\normalize.py -r .\resources\vocab\eng.vocab -i .\samples\samples-eng\oriana1.txt
	2018-12-19 11:48:34,151 [INFO] Logging set up level=15
	2018-12-19 11:48:34,473 [INFO]  ... ... Intervalle numéro 1.
	2018-12-19 11:48:34,477 [INFO]  ... ... Intervalle numéro 2.
	2018-12-19 11:48:34,480 [INFO]  ... ... Intervalle numéro 3.
	Tokens
	1, the flight was twelve hours long and we really got bored
	2, they only played two movies + which we had both already seen
	3, i never get to sleep on the airplane because it's so uncomfortable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


A single input file with a transcription time-aligned into the IPUS, 
in manual mode and no logs:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\normalize.py -r .\resources\vocab\eng.vocab 
-i .\samples\samples-eng\oriana1.xra --quiet
	Tokens
	0.000000, 1.675000 #
	1.675000, 4.570000 the flight was twelve hours long and we really got bored
	4.570000, 6.390000 #
	6.390000, 9.870000 they only played two movies + which we had both already seen
	9.870000, 11.430000 #
	11.430000, 14.730000 i never get to sleep on the airplane because it's so uncomfortable
	14.730000, 17.792000 #
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The same file in automatic mode can be annotated with one of the following commands:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\normalize.py -I .\samples\samples-eng\oriana1.xra -l eng
python .\sppas\bin\normalize.py -I .\samples\samples-eng\oriana1.txt -l eng
python .\sppas\bin\normalize.py -I .\samples\samples-eng\oriana1.wav -l eng
python .\sppas\bin\normalize.py -I .\samples\samples-eng\oriana1 -l eng
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


This program can also normalize data from the standard input.
Example of use, using stdin/stdout under Windows:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Write-Output "The flight was 12 HOURS {toto} long." |
python .\sppas\bin\normalize.py -r .\resources\vocab\eng.vocab --quiet
	the
	flight
	was
	twelve
	hours
	long
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


In that case, the comment mentioned with the braces is removed
and the number is converted to its written form. The character "_" is
used for compound words (it replaces the whitespace).

## Introduction

The analyses of annotated files includes the descriptive statistics, the
filtering of the annotated data to get only the annotations you are
interested in, see/edit the information about files, etc.

Like the other features of SPPAS, analyzing data can be performed in
three different ways:

- the Application Programming Interface, in Python language;
- the Command-Line User Interface;
- the Graphical User Interface.

Among the features implemented in the API, a big majority are included in 
the GUI but just a few can be performed with the CLI.
This chapter then describes only the page "Analyze" of the GUI.



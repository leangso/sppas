
## The Command-Line user Interface - CLI

### Overview 

A command-line user interface (CLI) is a means of interacting with a 
computer program where the user issues 
commands to the program in the form of successive lines of text (command lines).
Command-line interfaces provide a more concise and powerful means to 
control the program than the GUI.

Operating system command line interfaces are called a command-line interpreter, 
command processor or shell. It displays a prompt, accept a "command line" 
typed by the user terminated by the Enter key, then execute the specified 
command and provide textual display of results or error messages.
When a shell is active a program is typically invoked by typing its name 
followed by command-line arguments (if any).

Such **CLI programs are located in the `bin` folder** of the `sppas` 
directory of the package.
All these programs are written with the programming language Python and
are compatible with version 3.4+.

**Installing Python is then the only required other program in order to work with SPPAS CLI** 
except the `alignment.py` program which requires either Julius CSR or HVite (see installation instructions).


### Usage

It is usual for a program to be able to display a brief summary of its
parameters. Each program included in SPPAS provides its usage by using the
option `--help`, as for example:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
prompt> python .\sppas\bin\trsconvert.py --help
usage: trsconvert.py [files] [options]

... a program to export annotated files.

optional arguments:
  -h, --help   show this help message and exit
  --quiet      Disable the verbosity
  --debug      Highest level of verbosity

Files:
  -i file      Input annotated file name.
  -o file      Output annotated file name.

Options:
  -n value     Number of a tier (use as many -n options as wanted). Positive
               or negative value: 1=first tier, -1=last tier.
  -t tiername  Name of a tier (use as many -t options as wanted).

This program is part of SPPAS version 2.0. Copyright (C) 2011-2019 Brigitte
Bigi. Contact the author at: contact@sppas.org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


### Arguments for input/output

In most of the programs, there is an option '-i' for the input file.
There's no specific constraint on this file name. For example, the 
following program will execute the Momel automatic annotation:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\momel.py -i .\samples\samples-eng\ENG_M15_ENG_T02.PitchTier
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With this option '-i', a name of an output file can be given with the 
option '-o'; if not, the main part of the result is printed on the standard 
output.

In several programs, an option '-I' can also be available to execute the 
program, and several files can be processed with this option. Moreover, there 
is some flexibility in file names with this option. SPPAS will search for the 
appropriate file from the given file name. For example, the next commands 
will proccess the same:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python .\sppas\bin\intsint.py -I .\samples\samples-eng\ENG_M15_ENG_T02.wav
python .\sppas\bin\intsint.py -I .\samples\samples-eng\ENG_M15_ENG_T02.PitchTier
python .\sppas\bin\intsint.py -I .\samples\samples-eng\ENG_M15_ENG_T02-momel.xra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With the option '-I', the name of the output file is fixed and can't be changed.
For example, the previous example will create a file with name 
`.\samples\samples-eng\ENG_M15_ENG_T02-intsint.xra`. An option '-e' allows
to choose the extension of the file, *.xra* is the default one.

The options to manage input/output files can be summarized as follow:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Files (manual mode):
  -i file               An input file.
  -o file               Output file name (optionnal).

Files (auto mode):
  -I file               Input file (append).
  -e .ext               Output file extension. One of: .xra .TextGrid .eaf
                        .csv .mrk .txt .stm .ctm .lab .mlf .sub .srt .antx
                        .arff .xrff
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


### preinstall.py

This program allows to:

1. install external programs to enable some of the features of SPPAS -- including the GUI;
2. install linguistic resources for the supported languages;
3. install resources required to enable some of the annotations.


## The graphical user interface

### Important

Once the installation is completed - including wxpython, the GUI feature 
is enabled. Do not launch the GUI of SPPAS if the installation is not finished, 
i.e. launch the setup first: see the tutorial series 2 and to follow instructions.

### Launch the GUI

#### Under Windows

Once the SPPAS package is opened in the File Explorer,
double-click on the `sppas.bat` file.

In recent versions of Windows (e.g. 10), the first time you try 
to run SPPAS you may get a window with title "Windows protected your PC"
and the following message: "Windows SmartScreen prevented an unrecognized 
app from starting. Running this app might put your PC at risk. More info".
Click `More info` message and then `Run anyway` button. 
The file will now run SPPAS, and you will now no longer get a Windows 
protected your PC prompt when you run this specific file next time.
This warning message comes from the fact that SPPAS is a free software
and we did not paid to Microsoft commercial fees which would remove the 
"Unknown Publisher" warnings. 

#### Under MacOS

Once the SPPAS package is opened in the Finder, double-click 
on the `sppas.command` file. 

The first time you try to run SPPAS you may get a message:
"sppas.command can't be opened because it is from an unidentified developer.".
This warning message comes from the fact that SPPAS is a free software
and we did not paid to Apple commercial fees. The solution is to run SPPAS 
with a right click (alt-click) on sppas.command file. This time you will get
a message: "sppas.command is from an unidentified developer. Are you sure 
you want to open it?" Then click on `Open`. 
It will also now work each time you run it.

#### Under Linux

Once the SPPAS package is opened in the File Explorer, double-click on 
the `sppas.command` file. Either, open a terminal and run it. Last 
solution is to directly execute `sppas/bin/sppasgui.py`.


### The GUI of SPPAS

Both the main windows and a "Log window" will open. 
The main frame is made of a menubar at top, a main content in the middle
and buttons for actions at bottom.

Most of the existing/incoming tutorials are explaining how to access 
features of SPPAS with this GUI. This chapter only summarizes some of them.


### The main frame

The top menu allows to open various pages like "Files", "Annotate" or 
"Analyze". There are 2 ways to browse through the items of the menu:
with the mouse or with the keyboard.
To open a page with the mouse, just click on the item button in the menubar.
To open a page with the keyboard, under Windows, press CTRL and under MacOS,
press COMMAND. Click left-right arrows to navigate to the previous/next page.
Use up-arrow to go to the 1st page and Down-arrow to the last page. 
Use "CTRL+F"/"COMMAND+F" to go to the "Files" page.

To change colors and fonts, click the `Settings` icon at bottom.
These settings are saved in a file which is used each time SPPAS is executed.

The `About` action button allows displaying information about SPPAS: 
author, license, a link to the web site, etc.


#### Home page

The home page shows a welcome message and gives a quick acess to a set of 
external links. Clicking on the buttons will open the default web-browser of 
the OS to this link.

The links are pointing to:

- the SPPAS home web site: <https://sppas.org>
- the in-line version of this documentation
- the in-line tutorials
- the frequently asked questions
- the home page of the author, including the whole list of publications 
related to SPPAS: <https://sppas.org/bigi/>


#### Files page

The page "Files" is dealing with the workspaces (see the previous section 
about workspaces).

##### Workspaces:

A column at left with green buttons contains a list of buttons to perform
actions related to the workspaces then the list of existing ones.

The actions to perform are:

1. "Import from": select a workspace file (.wjson) somewhere on the computer and 
add it into the SPPAS workspaces directory.
2. "Export to": save the currently enabled workspace somewhere in the computer
3. "Pin & Save": The currently selected workspace is saved. If the currently 
selected workspace is "Blank", clicking this action will ask for a workspace 
name and create it ("Pin"), then select it. 
4. "Rename": change the name of the currently selected workspace. Can't be applied
on the "Blank" workspace.

The list of workspaces allows to select a workspace among the existing ones
or to work with the "Blank" one (default). 

##### Files:

The files area with a pinky color displays the tree of files of the currently
selected workspace, and a toolbar with action buttons.

The actions to perform are:

1. "Add": add new files into the workspace
2. "Remove checked": remove the checked files of the workspace
3. Remove missing files of the workspace. The tree of paths/roots/files
is explored and if a filename does not correspond to an existing file on the 
disk, it is removed of the workspace tree
4. All files of the disk sharing the checked roots will be added to the workspace tree

##### References:

The "References" area with a blue color displays the tree of references of the 
currently selected workspace, and a toolbar with action buttons.

The actions to perform are:

1. "Create": Open a dialog to ask for a name and the reference type among: 
STANDALONE, INTERACTION, SPEAKER. It then creates the corresponding reference
and add it to the workspace
2. "Edit": Open a dialog to delete, add or modify the attributes of the 
checked references. 
3. "Delete": Definitively delete the checked references. There's no way to recover them if needed.

##### Column of action buttons:

Between the files and references, there's a column with action buttons.
The actions to perform are related to files:

1. Open the checked files with a text editor (under development)
2. "Delete checked": remove the checked files of the workspace *and* delete 
them of the disk. Actually, there are moved and renamed into the trash of SPPAS. 
To recover a mistaken deleted file, use your OS explorer.
3. Check/uncheck all files
4. Check only files matching some patterns.

or to link files and references:

5. Associate the checked roots to the checked references
6. Dissociate the checked roots of the checked references


#### Annotate page

This page allows annotating checked files of the currently enabled workspace.

The automatic annotation is a 5-steps process:

1. Choose the output file format. All the automatic annotations are creating
files. Choosing the extension means to choose the format of the resulting file.
Some formats are more "powerful" than others, some are more "sharable"... 
2. Fix the language. Some annotations are language-independent, so they don't 
need a specific language resource, like "Search for IPUs" or "Face 
detection". But some annotations are requiring linguistic resources like a 
lexicon (Text Normalization), a dictionary (Phonetization), etc. This step
allows to choose the language among the list of installed ones.
3. Select and configure annotations. Click any of the STANDALONE, SPEAKER or
INTERACTION button. It will change the page content in order to select the
annotations SPPAS will perform and to configure them. Click the top arrow to
come back to the main page.
4. Perform the annotations. Click the "Let's go" button to launch the automatic
annotation process, and wait! 
5. A procedure outcome report is displayed and saved in the logs folder of 
the SPPAS package.

The previously saved reports are listed in the column at right of the page.
Click the button "Show report" to display the content of the checked one.



#### Analyze page


This page is a merge of the "DataFilter", the "DataStats" and the "DataRoamer"
of the previous GUI version... with new features added.

It allows to display a summary of each file: 
either audio or annotated files can be opened and analyzed. 
The following analyses can be performed:

- manage tiers: duplicate, remove, copy, move, ...
- estimate distribution statistics on tiers;
- create new tiers with the filter systems, either the "simple" or the "relation one;
- check if an audio file is compatible with the automatic annotations.


#### Editor page

This page is a merge the "IPUScriber" and the "Visualizer" of the previous GUI 
version... with new features added.

It allows to annotate manually and to view files in a timeline.
The features "video" is required to play a video file and the feature 
"audioplay" is required to play an audio file.

This editor is very useful to play several media at a time synchronously.


#### Convert page

The convert page allows to export the checked annotated files of the workspace
in any of the supported file format.

The displayed table indicates all the format capabilities, including:

- Metadata: the file format supports metadata, i.e. it can save extra-information, not only annotations. If not, the metadata won't be exported;
- Multi tiers: the file format accepts to save more than one tier;
- No tier: the file format can save a file without tiers;
- Point: the file format can save tiers of type 'Point';
- Interval: the file format can save tiers of type 'Interval'. 
- Gaps: the file format supports gaps between annotations of a tier. If not, when exporting the file, SPPAS will fill in the gaps by annotations with an empty label;
- Overlaps: the file format supports overlaps between annotations of a tier. If not, when exporting the file, SPPAS will split annotations to create a sequence of non-overlapping ones.
- Hierarchy: the format can deal with a hierarchy of tiers. If not, the hierarchy won't be saved.
- Ctrl vocab: the format can save a controlled vocabulary of tiers. If not, the controlled vocabularies won't be saved.
- Media: the format can save a reference to a media of tiers. If not, the media information won't be saved.
- Vagueness: the format supports a vagueness value of the localization. If not, the vagueness value won't be saved.
- Alt. loc: the format supports alternative annotation localizations. If not, the alternative localizations won't be saved.
- Alt. tag: the format supports alternative annotation tags. If not, when exporting the annotations, SPPAS will use a "{ | }" system to parse the alternative tags and = for scores.


#### Plugins page

Installing plugins is a very useful solution to extend the features
of SPPAS. Several plugins are available for download in the main site
of SPPAS. The plugins of SPPAS are installed in a folder with name "plugins"
in the main directory of the SPPAS package.

To install a new plugin, simply follow this workflow:

1. Create or download the plugin package - e.g. a zip file.
2. Execute SPPAS.
3. Click on 'Plugin' then click on the 'Install' button of the toolbar.
4. Browse to indicate the plugin package.
5. See the new plugin at bottom of the plugins list.

To delete a plugin, click on the "Delete" button of the toolbar.
Choose the plugin in the given list then click on the "OK" button.
Notice that the plugin is definitively deleted of the disk.

To execute a plug-in, check the file(s) of your current workspace in the 
File page, then, click on the icon of the plug-in and follow instructions 
of the plugged program.

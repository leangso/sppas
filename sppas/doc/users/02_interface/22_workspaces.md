## Workspaces

### Overview

A workspace is a manager of a set of files and of manually defined references.
Several workspaces can be created but only one at a time is enabled
when using SPPAS. 
All the features of SPPAS are using such currently enabled workspace.

### Create and save a workspace

A workspace is described in a configuration file into the folder "workspaces" 
of the SPPAS package. This file is made of the name of the workspace followed
by the extension ".json". It includes all properties of a workspace that are 
described here.

This configuration file can be copied into any other directory and re-imported
into SPPAS. 


### Files

A workspace is managing a set of files. Each time a file is added into the 
currently selected workspace, its "root" and its "path" are extracted and 
stored into a tree-like architecture. 

The data are structured as follow:

- a workspace contains a list of paths,
- each path contains the list of roots sharing this path,
- each root contains the list of filenames sharing this root.

For example, when adding the file "/sppas/samples/samples-eng/oriana1.wav" 
into the workspace, the following next actions will be performed:

- the path "/sppas/samples/samples-eng/" is added, 
- the root "/sppas/samples/samples-eng/oriana1" is added into the path,
- the filename "/sppas/samples/samples-eng/oriana1.wav" is added into the root. 
Several properties of the file are also stored, including its size or the date
of modification.

When SPPAS is creating a new file - like when annotating automatically, the
filename is automatically added into the workspace. For example, when the
automatic annotation "Fill in IPUs" has finished, the newly created file 
"/sppas/samples/samples-eng/oriana1.xra" is added into the workspace:

- the path "/sppas/samples/samples-eng/" is already existing so it's not added,
- the root "/sppas/samples/samples-eng/oriana1" is not added into the path because it's also already existing,
- the filename "/sppas/samples/samples-eng/oriana1.xra" is added into the root.

All files sharing the same root are then stored in the same place in the tree
because there're sharing properties: they are all related to the same recording.
The only difference between files sharing the same root are:

1. the extension (.wav, .mp4, .mkv, .xra, .TextGrid, ...)
2. the pattern (-ipus, -token, -phon, -palign, -lexm, -orepet, ...)

Notice that a pattern:
 
 - must start by the character "-";
 - must contain at least 1 character.
 
Thanks to this new management of files, the patterns used by the automatic 
annotations are not fixed anymore. Both the input and output patterns of the 
annotations can be modified at the time of configuring the annotations.

The second advantage of such management of files into a workspace is to add 
all files of a corpus only once into a workspace and to save it. It is then 
ready-to-use each time SPPAS is started.

To summarize, the file "/sppas/samples/samples-eng/oriana1-token.TextGrid" will 
be represented as follow:

1. "/sppas/samples/samples-eng/" is the *path*
2. "/sppas/samples/samples-eng/oriana1" is the *root*
3. "/sppas/samples/samples-eng/oriana1-token.TextGrid" is the *filename*, in which:
        - "-token" is the *pattern*,
        - ".TextGrid" is the *extension*.
 
The workspace is also managing the state of each of these items: checked or 
not for filenames, checked/at least one checked/not checked for roots or paths.
 
 
### References

The "references" are information that can be associated to files.

There are 3 different types of references: STANDALONE, SPEAKER and INTERACTION.
Each of them corresponds to an annotation type.
The annotations of both SPEAKER and INTERACTION type require to associate
roots with these references.

Each reference can contain a list of attributes with an identifier key to define 
them, a value and its type, and a description. Here is an example of a reference 
"Oriana" of type SPEAKER with its list of attributes in the form "key: values (type)":

- firstname: Oriana (str)
- gender: female (str)
- recorded_in: 2012 (int)
- L1: eng (str)
- L2: fra (str)

It is recommended to use only us-ascii characters and no whitespace for the key.


### Associate files and references

Roots and references should be associated. In the previous example, it could
allow to declare the list of files related to a given speaker. In that example,
the reference "Oriana" should be associated to the roots: 
"/sppas/samples/samples-eng/oriana1"
and "/sppas/samples/samples-eng/oriana2".

In the specific case of interactions, a reference of the interaction has to
be created and associated to the roots of the filenames.

The definition of such references, and their link to the
corresponding roots allow to perform an elaborated way to check files of a 
workspace.
It's very easy for example to check all files with file extension .wav of 
"female" gender more than 30 years old.


# Corpus creation methodology

------------------------------------------------------------------------

## The context

* SPPAS was created in the scope of annotating automatically and exploring large multi-modal corpora
* A workflow was defined while creating CID - Corpus of Interactional Data
* It was then applied to create new corpora

![Screenshots of 4 corpora (left to right): CID, GrenelleII, Aix MapTask, DVD](./etc/screenshots/corpora.png)

------------------------------------------------------------------------

## The corpus creation workflow

![Corpus creation and annotation: methodology](./etc/figures/methodo.png)

------------------------------------------------------------------------

## Step 1: Recording speech

* One channel per speaker
* Anechoic room, or an environment with no/low noise
* Audio: for automatic annotation tools:
    - Any un-compressed file format, commonly WAVE
    - 16000Hz / 16bits is enough
* Audio: for manual annotation tools:
    - Any un-compressed file format
    - 48000Hz / 16bits is of high quality

------------------------------------------------------------------------

## Step 2: Search for Inter-Pausal Units

* Parameters to define manually:
    * fix the minimum silence duration
    * fix the minimum speech duration
* As results: 
    * speech and silences are time-aligned automatically
    * a manual verification is highly recommended

------------------------------------------------------------------------

## Step 3: Orthographic Transcription

* Any transcription must follow a convention
* In speech (particularly in spontaneous speech), many phonetic variations occur:
    - some of these phonologically known variants are predictable
    - but many others are still unpredictable (especially invented words, 
    regional words or words borrowed from another language)
* The orthographic transcription must be enriched: 
    - it must be a representation of what is “perceived” in the signal. 

------------------------------------------------------------------------

## Step 4: Speech segmentation

* In SPPAS, this problem is divided into 3 different tasks:
    1. Text normalization
    2. Phonetization 
    3. Alignment 

* Allows a fully automatic annotation, or a semi-automatic one
* See the tutorial 'Phonemes and words segmentation' for details

------------------------------------------------------------------------

## Other steps: 

* On the basis of the time-aligned phonemes/tokens, other automatic annotations:
    - Syllables
    - Repetitions
    - TGA
    - ...
* Analysis:
    - Filter data
    - Statistics
    - ...

--------------

### Interested in knowing more?

* You can see a full presentation 
[here](https://sppas.org/bigi/Doc/2015-SPPAS-Tutorial-HongKong/SPPAS-tutorial_04_methodo.html)
if you are interested in knowing more. It was created in 2015 and wasn't updated since!
However, it is general enough and can be of great help:


--------------

### You reached the end of this tutorial!

> [Click here to go back to SPPAS tutorials web page](./tutorial.html)

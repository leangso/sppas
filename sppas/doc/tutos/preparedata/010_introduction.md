# Introduction

-------------------------------

## About the author

* SPPAS is a scientific computer software package written and maintained
by Brigitte Bigi

* CNRS researcher at the Laboratoire Parole et Langage, 
in Aix-en-Provence, France 

![Brigitte Bigi - Author of SPPAS](./etc/images/me.jpg)

* Research topics are related to:
    
    1. Multimodal corpora: collect, annotate, analyze
    2. Multilinguality

-------------------------------

## Corpus and annotation

* Corpus linguistics is the study of language as expressed in samples (corpora) of "real world".
* Corpus annotation is a path to greater linguistic understanding.

> Corpus annotation “can be defined as the practice of adding interpretative,
   linguistic information to an electronic corpus of spoken and/or written language data.
   'Annotation' can also refer to the end-product of this process” (Leech, 1997).

-------------------------------

## Annotations

* Annotation is not an end in itself - it is a basis for further analyses
* Annotations should be time-synchronized. Temporal information makes it 
possible to describe simultaneous behaviours: 
    - of different levels in an utterance (e.g. prosody and locution)
    - of different modalities (e.g. speech and gesture)
    - of different speakers or extralinguistic events

![Example of multi-level annotations](./etc/screenshots/AB-merge.png)

-------------------------------

## Annotation software

* Manual annotation
    - Audacity, Praat, Elan, Anvil, Winpitch, AnnotationPro...
* Automatic annotation:
    - The current state-of-the-art in Computational Linguistics allows many 
annotation tasks to be semi- or fully- automated:
        - MarsaTag (morpho-syntax for French)
        - MediaPipe
        - ...

* Each annotation that *can* be done automatically *must* be done automatically!

* Because *revising* is faster and easier than *annotating*...
if the automatic system is "good enough".

> Before using any automatic annotation tool/software, 
  it is important to consider its error rate (where applicable) 
  and to estimate how those errors will affect the purpose
  for the annotated corpora.

-------------------------------

## SPPAS is an awarded Research Software

* Research global approaches:
  * methods as language-independent as possible;
  * the possibility to adapt technologies to low-resourced languages.
* Award "Accessit" Special Jury Prize - 2022:
  * The Ministry of Higher Education, Research and Innovation presented the 
    1st Open Science Awards for Open Source Research Software.
  * ![Brigitte Bigi and Sylvie Retailleau](./etc/images/science_ouverte.jpg)


-------------------------------

## Multi-Lingual approaches to the automatic annotation of speech

* SPPAS is designed and developed to handle multiple language corpora
and/or tasks with the same algorithms in the same software environment.
* SPPAS emphasizes new practices in the methodology of tool developments:
    - considering problems with a generic multi-lingual aspect, 
    - sharing resources, 
    - putting the end-users in control of their own computing.
* Only the resources are language-specific and the approach is based on the 
simplest resources possible.

-------------------------

## Resources extend

* Phoneticians are of crucial importance for resource development
    - they can contribute to improve the resources used by automatic systems. 
* New versions are systematically released to the public and serve to 
benefit of the whole community. 

-------------------------

## Resources extend (continued)

* Resources are distributed under the terms of a public license, so that SPPAS users:
    - have free access to the application source code and the resources of the software they use, 
    - are free to share the software and resources with other people, 
    - are free to modify the software and resources,
    - are free to publish their modified versions of the software and resources.

-------------------------

## SPPAS: Main reference to cite

    Brigitte Bigi (2015).
    SPPAS - Multi-lingual Approaches to the Automatic Annotation of Speech.
    In "the Phonetician" - International Society of Phonetic Sciences, 
    ISSN 0741-6164, Number 111-112 / 2015-I-II, pages 54-69.

![](./etc/screenshots/sppas_paper.png)

-------------------------------

### You reached the end of this tutorial!

> [Click here to go back to SPPAS tutorials web page](./tutorial.html)

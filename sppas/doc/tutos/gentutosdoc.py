#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
    gentutosdoc.py
    ~~~~~~~~~~~~

    :author:       Brigitte Bigi
    :organization: Laboratoire Parole et Langage, Aix-en-Provence, France
    :contact:      contact@sppas.org
    :license:      GPL, v3
    :copyright:    Copyright (C) 2011-2022  Brigitte Bigi
    :summary:      HTML generator and hand-out for tutorials.

"""
import os
import codecs
import glob
import re
from subprocess import Popen, PIPE, STDOUT

PROGRAM = os.path.abspath(__file__)


# ---------------------------------------------------------------------------

MARKDOWN_EXT = ".md"
INDEX_EXT = ".idx"
VERSION = "4"

# ---------------------------------------------------------------------------


class sppasDocFiles(object):
    """Documentation explorer.
    
    :author:       Brigitte Bigi
    :organization: Laboratoire Parole et Langage, Aix-en-Provence, France
    :contact:      contact@sppas.org
    :license:      GPL, v3
    :copyright:    Copyright (C) 2011-2022  Brigitte Bigi

    Fix all files required to generate the documentation, and explore the
    documentation directory and folders.

    The file markdown.idx contains the list of folders of the documentation.
    It can be a 1 column file, or a two-columns file with ';' to separate
    the two columns.
    Each folder must also contain an index file with the list of markdown
    files.

    """
    # Documentation file explore
    BASE_INDEX = "markdown.idx"
    MARKDOWN_HEADER = "header.md"
    MARKDOWN_FOOTER = "footer.md"

    # Tutorials generation
    HTML_JS_HEAD = "head_js.html"

    # -----------------------------------------------------------------------

    def __init__(self, doc_dir, doc_temp):
        """Create a sppasDocFiles instance.

        :param doc_dir: (str) Name of the directory of the markdown and index files
        :param doc_temp: (str) Name of the directory of the external template files.

        """
        self._doc_dir = doc_dir
        self._doc_temp = doc_temp

    # -----------------------------------------------------------------------

    @staticmethod
    def test_file(filename):
        """Test if a file exists.

        :param filename: (str) Name of the file to test.
        :raises: IOError

        """
        if os.path.exists(filename) is False:
            raise IOError("The file {:s} is missing of the documentation."
                          "".format(filename))

    # -----------------------------------------------------------------------
    # Getters
    # -----------------------------------------------------------------------

    def get_file(self, filename):
        """Return a file of the documentation. """
        new_filename = os.path.join(self._doc_temp, filename)
        sppasDocFiles.test_file(new_filename)
        return new_filename

    # -----------------------------------------------------------------------

    def get_base_index(self):
        """Return the file indicating the list of documentation folders. """
        filename = os.path.join(self._doc_dir, sppasDocFiles.BASE_INDEX)
        sppasDocFiles.test_file(filename)
        return filename

    # -----------------------------------------------------------------------

    def get_markdown_header(self):
        """Return the header markdown file. """
        return self.get_file(sppasDocFiles.MARKDOWN_HEADER)

    # -----------------------------------------------------------------------

    def get_markdown_footer(self):
        """Return the footer markdown file. """
        return self.get_file(sppasDocFiles.MARKDOWN_FOOTER)

    # -----------------------------------------------------------------------

    def get_js_head_scripts(self):
        """Return the scripts for the <head> of an html file. """
        return self.get_file(sppasDocFiles.HTML_JS_HEAD)

    # -----------------------------------------------------------------------
    # Explore the documentation folders and files
    # -----------------------------------------------------------------------

    @staticmethod
    def get_index(dirname):
        """Return the filename of the index of a directory. """
        try:
            index_filename = os.path.join(dirname, sppasDocFiles.BASE_INDEX)
            sppasDocFiles.test_file(index_filename)
        except IOError:
            index_files = glob.glob(os.path.join(dirname, "*" + INDEX_EXT))
            if len(index_files) > 0:
                index_filename = index_files[0]
            else:
                raise IOError("No index found in {:s}.".format(dirname))

        return index_filename

    # -----------------------------------------------------------------------

    def get_idx_markdown_files(self, folder):
        """Return a list of markdown files.
        Search for an index file either in doc_dir/folder or in doc_dir.

        :param folder: (str) Folder to search for markdown files. If folder is
        None, search is performed directly in doc_dir.
        :returns: list of filenames

        """
        # Search the index file
        if folder is None:
            index_filename = self.get_index(self._doc_dir)
        else:
            index_filename = self.get_index(os.path.join(self._doc_dir, folder))

        # Get all files mentioned in the idx
        files = list()
        fp = open(index_filename, "r")
        for line in fp.readlines():
            line = line.strip()
            if ";" in line:
                tabline = line.split(';')
                filename = tabline[0]
            else:
                filename = line.strip()
            # Add its path to each file name
            if folder is not None:
                new_file = os.path.join(self._doc_dir, folder, filename)
            else:
                new_file = os.path.join(self._doc_dir, filename)

            if os.path.exists(new_file) is False:
                raise IOError("File {:s} is missing.".format(new_file))
            files.append(new_file)

        return files

    # -----------------------------------------------------------------------

    def get_doc_folders(self):
        """Return the list of folders of the documentation.

        This list is extracted from the BASE_INDEX file which should be included
        into the documentation directory.
        Instead, all 1st level folders will be explored.

        :returns: list of dirnames

        """
        folders = list()
        try:
            index_filename = self.get_base_index()
            fp = open(index_filename, "r")
            # Get all folders mentioned in the index file
            for line in fp.readlines():
                dirname = line.strip()
                # Add its path to each folder name
                new_folder = os.path.join(self._doc_dir, dirname)
                if os.path.exists(new_folder) is False:
                    raise IOError("Folder {:s} is indexed but is missing.".format(new_folder))
                folders.append(dirname)

        except IOError:
            # No base index. Explore folders
            for folder in os.listdir(self._doc_dir):
                try:
                    self.get_idx_markdown_files(folder)
                    folders.append(folder)
                except IOError:
                    pass
            folders = sorted(folders)

        return folders

    # -----------------------------------------------------------------------

    def get_all_md(self, header=True, footer=False):
        """Return the list of markdown files for a documentation directory.
        Explore 1st level folders (if any) OR the doc dir.

        :param header: (bool) Add an header markdown file into the list
        :param footer: (bool) Add a footer markdown file into the list

        :returns: list of filenames

        """
        files = list()

        # add the header file into the list
        if header is True:
            files.append(self.get_markdown_header())

        # get the list of all indexed folders in the documentation directory
        folders = self.get_doc_folders()

        # get all indexed files of each folder
        if len(folders) > 0:
            for folder in folders:
                new_files = self.get_idx_markdown_files(folder)
                files.extend(new_files)
        else:
            files.extend(self.get_idx_markdown_files(folder=None))

        # add the footer file into the list
        if footer is True:
            files.append(self.get_markdown_footer())

        return files


# ---------------------------------------------------------------------------
# Utility functions
# ---------------------------------------------------------------------------


def exec_external_command(command, result_filename):
    """Execute a command and check the output file.

    :param command: (str) The external command to be executed, with its parameters.
    :param result_filename: (str) Name of the expected output file of the command
    :raises: Exception

    """
    p = Popen(command, shell=True, stdout=PIPE, stderr=STDOUT)
    # p.wait()
    line = p.communicate()

    # Check output file
    if os.path.exists(result_filename) is False:
        raise Exception("Error: {}.".format(line[0]))

# ---------------------------------------------------------------------------
# Generate HTML files from some markdown sources
# ---------------------------------------------------------------------------


def gen_html_slides(doc, files, result_filename):
    """Execute pandoc to generate a presentation in HTML 5.

    :param doc: (sppasDocFiles) Documentation, used to get filenames.
    :param files: (list) List of filenames (markdown files)
    :param result_filename: (str) Name of the newly created HTML file

    """
    if len(files) == 0:
        raise IOError("No markdown files to generate the documentation!")

    head_html_file = doc.get_js_head_scripts()

    # Remove the existing file
    if os.path.exists(result_filename):
        os.remove(result_filename)

    # the command
    command = "pandoc"

    # the parameters
    command += " -s"
    command += " --mathjax"
    command += " -t dzslides"
    command += " --slide-level=2"
    command += " --css ./etc/styles/tuto.css"
    command += " -H " + head_html_file

    # the files and HTML options
    command += " "
    command += " ".join(files)
    command += ' -o ' + result_filename

    # Execute the command
    # -------------------
    # print("Command to be executed: {:s}".format(command))
    exec_external_command(command, result_filename)


def patch_html_file(in_filename, out_filename):
    """Apply some patches on the given HTML file.

    :param in_filename: existing HTML file
    :param out_filename: name of the file to create

    """
    # Load in_filename and put it into a string
    with codecs.open(in_filename, "r", "utf-8") as fp:
        lines = fp.readlines()
    content = "\n".join(lines)

    # Add a CR between HTML tags
    content = re.sub(">[ ]*<", ">\n\n<", content)
    # Disable autoplay of videos in DzSlides
    content = re.sub("autoplay: \"1\"", "autoplay: \"0\"", content)

    # Remove the DzSlides style of figures
    lines = list()
    in_style = False
    in_figure = False
    for line in content.split("\n"):
        if "<style>" in line:
            in_style = True
        if "</style>" in line:
            in_style = False
            in_figure = False
        if "figure " in line:
            if in_style is True:
                in_figure = True
                continue
        if in_figure is False:
            lines.append(line)

    # Add figure tag for images, if it's missing
    #     awk 'BEGIN{infigure=0}
    #     /<figure>/ {infigure=1}
    #     /<\/figure>/ {infigure=0}
    #     /<embed / {if (infigure==0) {print "<figure>\n",$0,"\n</figure>"; next;}}
    #     /<img / {if (infigure==0) {print "<figure>\n",$0; infigure=2; next;} else {print; next;}}
    #     {if (infigure==2) {print "\n</figure>\n";infigure=0;}; print}'
    content = ""
    in_figure = 0
    for i, line in enumerate(lines):
        if "<figure>" in line:
            in_figure = 1
        if "</figure>" in line:
            in_figure = 0
        if "<embed " in line:
            if in_figure == 0:
                content += "<figure>\n  %s\n</figure>" % line
                continue
        if "<img " in line:
            if in_figure == 0:
                content += "<figure>\n" + line
                in_figure = 2
                continue
            else:
                content += line
                continue

        if in_figure == 2:
            content += "\n</figure>\n"
            in_figure = 0
        content += line + "\n"

    lines = content.split("\n")
    # replace "embed" in figures, by "audio" or "video"
    # awk
    # 'BEGIN{infigure=0}\
    #     /<figure>/{infigure=1}\
    #     /<\/figure>/{infigure=0}\
    #     /<embed /{if (infigure==1) {if (match($0,".wav")){gsub("<embed ", "<audio ", $0); gsub("/>", " controls> </audio>",$0);} else {gsub("<embed ", "<video width=300 ", $0); gsub("/>", " controls> </video>",$0);}}}\
    #     {print}'
    content = ""
    in_figure = 0
    for i, line in enumerate(lines):
        if "<figure>" in line:
            in_figure = 1
        if "</figure>" in line:
            in_figure = 0
        if "<embed " in line:
            if in_figure == 1:
                if ".wav" in line:
                    line = line.replace("<embed ", "<audio ")
                    line = line.replace("/>", " controls> </audio>")
                else:
                    line = line.replace("<embed ", "<video width=300 ")
                    line = line.replace("/>", " controls> </video>")
        content += line + "\n"

    with codecs.open(out_filename, "w", "utf-8") as fp:
        fp.write("%s\n" % content)

# ---------------------------------------------------------------------------
# Main
# ---------------------------------------------------------------------------


if __name__ == "__main__":

    doc_temp = os.path.dirname(PROGRAM)

    # test if documentation directory for markdown files is ok.
    doc_dir = os.path.dirname(PROGRAM)
    print("Directory of tutos: {:s}".format(doc_dir))

    # web directory to store resulting HTML files
    result_dirname = os.path.join(doc_dir, "web")
    if os.path.exists(result_dirname) is False:
        os.mkdir(result_dirname)

    # get the list of all indexed folders in the documentation directory
    base_doc = sppasDocFiles(doc_dir, doc_temp)
    folders = base_doc.get_doc_folders()

    for folder in folders:
        print(folder)
        folder_dirname = os.path.join(doc_dir, folder)
        folder_doc = sppasDocFiles(folder_dirname, doc_temp)
        files = folder_doc.get_all_md(header=False, footer=False)
        print(files)
        tmp_filename = os.path.join(result_dirname, "tutorial_" + folder + "_tmp.html")
        result_filename = os.path.join(result_dirname, "tutorial_" + folder + ".html")
        gen_html_slides(base_doc, files, tmp_filename)
        patch_html_file(tmp_filename, result_filename)
        os.remove(tmp_filename)

# Install sphinx 
# -----------------

It is required to use Sphinx with Python 3.5+.

> apt install python3-pip
> pip3 install sphinx


# Generate rst files
# ------------------------

The following command MUST be executed from the source directory of
the packages to be documented.

> cd $SPPAS/sppas/src

Execute the following command:

> sphinx-apidoc.exe -f -o D:\myCoreCNRS\sppas\sphinx-doc\rst .\anndata\ .\anndata\tests
Creating file D:\myCoreCNRS\sppas\sphinx-doc\rst\anndata.rst.
Creating file D:\myCoreCNRS\sppas\sphinx-doc\rst\anndata.aio.rst.
Creating file D:\myCoreCNRS\sppas\sphinx-doc\rst\anndata.ann.rst.
Creating file D:\myCoreCNRS\sppas\sphinx-doc\rst\anndata.ann.annlabel.rst.
Creating file D:\myCoreCNRS\sppas\sphinx-doc\rst\anndata.ann.annlocation.rst.
Creating file D:\myCoreCNRS\sppas\sphinx-doc\rst\modules.rst.

and do the same for the next packages...
> sphinx-apidoc.exe -f -o D:\myCoreCNRS\sppas\sphinx-doc\rst .\audiodata\ .\audiodata\tests
> sphinx-apidoc.exe -f -o D:\myCoreCNRS\sppas\sphinx-doc\rst .\calculus\ .\calculus\tests
> sphinx-apidoc.exe -f -o D:\myCoreCNRS\sppas\sphinx-doc\rst .\config\
> sphinx-apidoc.exe -f -o D:\myCoreCNRS\sppas\sphinx-doc\rst .\files\ .\files\tests
> sphinx-apidoc.exe -f -o D:\myCoreCNRS\sppas\sphinx-doc\rst .\plugins\ .\plugins\tests
> sphinx-apidoc.exe -f -o D:\myCoreCNRS\sppas\sphinx-doc\rst .\resources\ .\resources\tests
> sphinx-apidoc.exe -f -o D:\myCoreCNRS\sppas\sphinx-doc\rst .\structs\ .\structs\tests
> sphinx-apidoc.exe -f -o D:\myCoreCNRS\sppas\sphinx-doc\rst .\utils\ .\utils\tests


A little bit of doc about this command:

>   sphinx-apidoc [OPTIONS] -o <OUTPUT_PATH> <MODULE_PATH> [EXCLUDE_PATTERN, …]
>    -e, --separate     Put documentation for each module on its own page.
>    -M, --module-first Put module documentation before submodule documentation.
ATTENTION : Il faut absolument mettre un '/' à la fin du chemin de -o


# Generate the HTML pages from the rst files
# ------------------------------------------


> cd $SPPAS/sppas/doc/api/

delete the file modules.rst

> sphinx-build -b html -d _build\doctrees . <DESTINATION PATH>

# sphinx-build [options] <sourcedir> <outputdir> [filenames . . . ]
# options possibles:
#   -w file Write warnings (and errors) to the given file, in addition to standard error.

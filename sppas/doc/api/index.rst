.. SPPAS documentation master file

.. image:: ../../etc/images/splash.png
    :width: 800px
    :align: center
    :alt:

Welcome to SPPAS's API documentation
====================================

http://sppas.org

SPPAS - the automatic annotation and analyses of speech, is a scientific
computer software package written and maintained by Brigitte Bigi of
the Laboratoire Parole et Langage, in Aix-en-Provence, France.


Access the modules
------------------

.. toctree::
   :maxdepth: 1

	Configuration of the application <./rst/config>
	Utilities <./rst/utils>
    Global data structures <./rst/structs>
    Calculus <./rst/calculus>

	Workspaces <./rst/wkps>
	Resources <./rst/resources>

    Annotated data <./rst/anndata>
    Audio <./rst/audiodata>
	Image <./rst/imgdata>
	Video  <./rst/videodata>

	Automatic annotations <./rst/annotations>
	Automatic analysis <./rst/analysis>
	Plugins <./rst/plugins>


Quick overview of the modules
-----------------------------

.. |package| image:: package.png


Configuration of the application, global data structures and utilities

.. automodule:: config
.. automodule:: utils
.. automodule:: calculus
.. automodule:: structs

Data structures to manage files to work with and data knowledge files

.. automodule:: wkps
.. automodule:: resources

Data structure to represent annotated data and recordings

.. automodule:: anndata
.. automodule:: audiodata
.. automodule:: imgdata
.. automodule:: videodata

The features of SPPAS

.. automodule:: annotations
.. automodule:: analysis
.. automodule:: plugins


annotations.StopWords package
=============================

Submodules
----------

annotations.StopWords.sppasstpwds module
----------------------------------------

.. automodule:: annotations.StopWords.sppasstpwds
   :members:
   :undoc-members:
   :show-inheritance:

annotations.StopWords.stpwds module
-----------------------------------

.. automodule:: annotations.StopWords.stpwds
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.StopWords
   :members:
   :undoc-members:
   :show-inheritance:

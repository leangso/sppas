annotations.TextNorm package
============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   annotations.TextNorm.num2text

Submodules
----------

annotations.TextNorm.language module
------------------------------------

.. automodule:: annotations.TextNorm.language
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.normalize module
-------------------------------------

.. automodule:: annotations.TextNorm.normalize
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2letter module
--------------------------------------

.. automodule:: annotations.TextNorm.num2letter
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.orthotranscription module
----------------------------------------------

.. automodule:: annotations.TextNorm.orthotranscription
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.splitter module
------------------------------------

.. automodule:: annotations.TextNorm.splitter
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.sppastextnorm module
-----------------------------------------

.. automodule:: annotations.TextNorm.sppastextnorm
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.tokenize module
------------------------------------

.. automodule:: annotations.TextNorm.tokenize
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.TextNorm
   :members:
   :undoc-members:
   :show-inheritance:

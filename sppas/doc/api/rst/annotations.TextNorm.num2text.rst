annotations.TextNorm.num2text package
=====================================

Submodules
----------

annotations.TextNorm.num2text.construct module
----------------------------------------------

.. automodule:: annotations.TextNorm.num2text.construct
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_asian\_lang module
-----------------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_asian_lang
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_base module
----------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_base
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_cmn module
---------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_cmn
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_europ\_lang module
-----------------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_europ_lang
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_fra module
---------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_fra
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_ita module
---------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_ita
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_jpn module
---------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_jpn
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_khm module
---------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_khm
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_pol module
---------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_pol
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_spa module
---------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_spa
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_und module
---------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_und
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.num\_vie module
---------------------------------------------

.. automodule:: annotations.TextNorm.num2text.num_vie
   :members:
   :undoc-members:
   :show-inheritance:

annotations.TextNorm.num2text.por\_num module
---------------------------------------------

.. automodule:: annotations.TextNorm.num2text.por_num
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.TextNorm.num2text
   :members:
   :undoc-members:
   :show-inheritance:

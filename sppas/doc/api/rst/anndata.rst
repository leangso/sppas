anndata package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   anndata.aio
   anndata.ann

Submodules
----------

anndata.anndataexc module
-------------------------

.. automodule:: anndata.anndataexc
   :members:
   :undoc-members:
   :show-inheritance:

anndata.ctrlvocab module
------------------------

.. automodule:: anndata.ctrlvocab
   :members:
   :undoc-members:
   :show-inheritance:

anndata.hierarchy module
------------------------

.. automodule:: anndata.hierarchy
   :members:
   :undoc-members:
   :show-inheritance:

anndata.media module
--------------------

.. automodule:: anndata.media
   :members:
   :undoc-members:
   :show-inheritance:

anndata.metadata module
-----------------------

.. automodule:: anndata.metadata
   :members:
   :undoc-members:
   :show-inheritance:

anndata.tier module
-------------------

.. automodule:: anndata.tier
   :members:
   :undoc-members:
   :show-inheritance:

anndata.transcription module
----------------------------

.. automodule:: anndata.transcription
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: anndata
   :members:
   :undoc-members:
   :show-inheritance:

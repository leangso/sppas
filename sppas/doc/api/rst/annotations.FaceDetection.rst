annotations.FaceDetection package
=================================

Submodules
----------

annotations.FaceDetection.imgfacedetect module
----------------------------------------------

.. automodule:: annotations.FaceDetection.imgfacedetect
   :members:
   :undoc-members:
   :show-inheritance:

annotations.FaceDetection.sppasfacedetect module
------------------------------------------------

.. automodule:: annotations.FaceDetection.sppasfacedetect
   :members:
   :undoc-members:
   :show-inheritance:

annotations.FaceDetection.videofacedetect module
------------------------------------------------

.. automodule:: annotations.FaceDetection.videofacedetect
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.FaceDetection
   :members:
   :undoc-members:
   :show-inheritance:

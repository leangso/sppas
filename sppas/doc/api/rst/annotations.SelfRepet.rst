annotations.SelfRepet package
=============================

Submodules
----------

annotations.SelfRepet.datastructs module
----------------------------------------

.. automodule:: annotations.SelfRepet.datastructs
   :members:
   :undoc-members:
   :show-inheritance:

annotations.SelfRepet.detectrepet module
----------------------------------------

.. automodule:: annotations.SelfRepet.detectrepet
   :members:
   :undoc-members:
   :show-inheritance:

annotations.SelfRepet.rules module
----------------------------------

.. automodule:: annotations.SelfRepet.rules
   :members:
   :undoc-members:
   :show-inheritance:

annotations.SelfRepet.sppasbaserepet module
-------------------------------------------

.. automodule:: annotations.SelfRepet.sppasbaserepet
   :members:
   :undoc-members:
   :show-inheritance:

annotations.SelfRepet.sppasrepet module
---------------------------------------

.. automodule:: annotations.SelfRepet.sppasrepet
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.SelfRepet
   :members:
   :undoc-members:
   :show-inheritance:

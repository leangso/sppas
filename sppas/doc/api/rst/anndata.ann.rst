anndata.ann package
===================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   anndata.ann.annlabel
   anndata.ann.annlocation

Submodules
----------

anndata.ann.annotation module
-----------------------------

.. automodule:: anndata.ann.annotation
   :members:
   :undoc-members:
   :show-inheritance:

anndata.ann.annset module
-------------------------

.. automodule:: anndata.ann.annset
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: anndata.ann
   :members:
   :undoc-members:
   :show-inheritance:

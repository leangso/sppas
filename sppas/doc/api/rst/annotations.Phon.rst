annotations.Phon package
========================

Submodules
----------

annotations.Phon.dagphon module
-------------------------------

.. automodule:: annotations.Phon.dagphon
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Phon.phonetize module
---------------------------------

.. automodule:: annotations.Phon.phonetize
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Phon.phonunk module
-------------------------------

.. automodule:: annotations.Phon.phonunk
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Phon.sppasphon module
---------------------------------

.. automodule:: annotations.Phon.sppasphon
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.Phon
   :members:
   :undoc-members:
   :show-inheritance:

calculus.geometry package
=========================

Submodules
----------

calculus.geometry.distances module
----------------------------------

.. automodule:: calculus.geometry.distances
   :members:
   :undoc-members:
   :show-inheritance:

calculus.geometry.linear\_fct module
------------------------------------

.. automodule:: calculus.geometry.linear_fct
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: calculus.geometry
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Align package
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   annotations.Align.aligners
   annotations.Align.models

Submodules
----------

annotations.Align.modelmixer module
-----------------------------------

.. automodule:: annotations.Align.modelmixer
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Align.sppasalign module
-----------------------------------

.. automodule:: annotations.Align.sppasalign
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Align.tracksgmt module
----------------------------------

.. automodule:: annotations.Align.tracksgmt
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Align.tracksio module
---------------------------------

.. automodule:: annotations.Align.tracksio
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.Align
   :members:
   :undoc-members:
   :show-inheritance:

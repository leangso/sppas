audiodata package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   audiodata.aio

Submodules
----------

audiodata.audio module
----------------------

.. automodule:: audiodata.audio
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.audioconvert module
-----------------------------

.. automodule:: audiodata.audioconvert
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.audiodataexc module
-----------------------------

.. automodule:: audiodata.audiodataexc
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.audioframes module
----------------------------

.. automodule:: audiodata.audioframes
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.audiopitch module
---------------------------

.. automodule:: audiodata.audiopitch
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.audiovolume module
----------------------------

.. automodule:: audiodata.audiovolume
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.autils module
-----------------------

.. automodule:: audiodata.autils
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.basevolume module
---------------------------

.. automodule:: audiodata.basevolume
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.channel module
------------------------

.. automodule:: audiodata.channel
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.channelformatter module
---------------------------------

.. automodule:: audiodata.channelformatter
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.channelframes module
------------------------------

.. automodule:: audiodata.channelframes
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.channelmfcc module
----------------------------

.. automodule:: audiodata.channelmfcc
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.channelsilence module
-------------------------------

.. automodule:: audiodata.channelsilence
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.channelsmixer module
------------------------------

.. automodule:: audiodata.channelsmixer
   :members:
   :undoc-members:
   :show-inheritance:

audiodata.channelvolume module
------------------------------

.. automodule:: audiodata.channelvolume
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: audiodata
   :members:
   :undoc-members:
   :show-inheritance:

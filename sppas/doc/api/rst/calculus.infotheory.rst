calculus.infotheory package
===========================

Submodules
----------

calculus.infotheory.entropy module
----------------------------------

.. automodule:: calculus.infotheory.entropy
   :members:
   :undoc-members:
   :show-inheritance:

calculus.infotheory.kullbackleibler module
------------------------------------------

.. automodule:: calculus.infotheory.kullbackleibler
   :members:
   :undoc-members:
   :show-inheritance:

calculus.infotheory.perplexity module
-------------------------------------

.. automodule:: calculus.infotheory.perplexity
   :members:
   :undoc-members:
   :show-inheritance:

calculus.infotheory.utilit module
---------------------------------

.. automodule:: calculus.infotheory.utilit
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: calculus.infotheory
   :members:
   :undoc-members:
   :show-inheritance:

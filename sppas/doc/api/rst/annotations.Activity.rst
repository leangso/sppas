annotations.Activity package
============================

Submodules
----------

annotations.Activity.activity module
------------------------------------

.. automodule:: annotations.Activity.activity
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Activity.sppasactivity module
-----------------------------------------

.. automodule:: annotations.Activity.sppasactivity
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.Activity
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Intsint package
===========================

Submodules
----------

annotations.Intsint.intsint module
----------------------------------

.. automodule:: annotations.Intsint.intsint
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Intsint.sppasintsint module
---------------------------------------

.. automodule:: annotations.Intsint.sppasintsint
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.Intsint
   :members:
   :undoc-members:
   :show-inheritance:

calculus package
================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   calculus.geometry
   calculus.infotheory
   calculus.scoring
   calculus.stats

Submodules
----------

calculus.calculusexc module
---------------------------

.. automodule:: calculus.calculusexc
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: calculus
   :members:
   :undoc-members:
   :show-inheritance:

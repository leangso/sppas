imgdata package
===============

Submodules
----------

imgdata.coordinates module
--------------------------

.. automodule:: imgdata.coordinates
   :members:
   :undoc-members:
   :show-inheritance:

imgdata.image module
--------------------

.. automodule:: imgdata.image
   :members:
   :undoc-members:
   :show-inheritance:

imgdata.imageutils module
-------------------------

.. automodule:: imgdata.imageutils
   :members:
   :undoc-members:
   :show-inheritance:

imgdata.imgcoordswriter module
------------------------------

.. automodule:: imgdata.imgcoordswriter
   :members:
   :undoc-members:
   :show-inheritance:

imgdata.imgdataexc module
-------------------------

.. automodule:: imgdata.imgdataexc
   :members:
   :undoc-members:
   :show-inheritance:

imgdata.imgsequence module
--------------------------

.. automodule:: imgdata.imgsequence
   :members:
   :undoc-members:
   :show-inheritance:

imgdata.imgsimilarity module
----------------------------

.. automodule:: imgdata.imgsimilarity
   :members:
   :undoc-members:
   :show-inheritance:

imgdata.objdetec module
-----------------------

.. automodule:: imgdata.objdetec
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: imgdata
   :members:
   :undoc-members:
   :show-inheritance:

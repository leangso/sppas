resources package
=================

Submodules
----------

resources.dictpron module
-------------------------

.. automodule:: resources.dictpron
   :members:
   :undoc-members:
   :show-inheritance:

resources.dictrepl module
-------------------------

.. automodule:: resources.dictrepl
   :members:
   :undoc-members:
   :show-inheritance:

resources.dumpfile module
-------------------------

.. automodule:: resources.dumpfile
   :members:
   :undoc-members:
   :show-inheritance:

resources.mapping module
------------------------

.. automodule:: resources.mapping
   :members:
   :undoc-members:
   :show-inheritance:

resources.patterns module
-------------------------

.. automodule:: resources.patterns
   :members:
   :undoc-members:
   :show-inheritance:

resources.resourcesexc module
-----------------------------

.. automodule:: resources.resourcesexc
   :members:
   :undoc-members:
   :show-inheritance:

resources.unigram module
------------------------

.. automodule:: resources.unigram
   :members:
   :undoc-members:
   :show-inheritance:

resources.vocab module
----------------------

.. automodule:: resources.vocab
   :members:
   :undoc-members:
   :show-inheritance:

resources.wordstrain module
---------------------------

.. automodule:: resources.wordstrain
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: resources
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Align.models package
================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   annotations.Align.models.acm
   annotations.Align.models.slm
   annotations.Align.models.tests

Submodules
----------

annotations.Align.models.modelsexc module
-----------------------------------------

.. automodule:: annotations.Align.models.modelsexc
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Align.models.tiermapping module
-------------------------------------------

.. automodule:: annotations.Align.models.tiermapping
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.Align.models
   :members:
   :undoc-members:
   :show-inheritance:

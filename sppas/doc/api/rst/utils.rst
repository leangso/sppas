utils package
=============

Submodules
----------

utils.compare module
--------------------

.. automodule:: utils.compare
   :members:
   :undoc-members:
   :show-inheritance:

utils.datatype module
---------------------

.. automodule:: utils.datatype
   :members:
   :undoc-members:
   :show-inheritance:

utils.fileutils module
----------------------

.. automodule:: utils.fileutils
   :members:
   :undoc-members:
   :show-inheritance:

utils.utilsexc module
---------------------

.. automodule:: utils.utilsexc
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: utils
   :members:
   :undoc-members:
   :show-inheritance:

analysis package
================

Submodules
----------

analysis.tierfilters module
---------------------------

.. automodule:: analysis.tierfilters
   :members:
   :undoc-members:
   :show-inheritance:

analysis.tierstats module
-------------------------

.. automodule:: analysis.tierstats
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: analysis
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Momel package
=========================

Submodules
----------

annotations.Momel.anchor module
-------------------------------

.. automodule:: annotations.Momel.anchor
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Momel.momel module
------------------------------

.. automodule:: annotations.Momel.momel
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Momel.momelutil module
----------------------------------

.. automodule:: annotations.Momel.momelutil
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Momel.sppasmomel module
-----------------------------------

.. automodule:: annotations.Momel.sppasmomel
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.Momel
   :members:
   :undoc-members:
   :show-inheritance:

annotations.OtherRepet package
==============================

Submodules
----------

annotations.OtherRepet.detectrepet module
-----------------------------------------

.. automodule:: annotations.OtherRepet.detectrepet
   :members:
   :undoc-members:
   :show-inheritance:

annotations.OtherRepet.rules module
-----------------------------------

.. automodule:: annotations.OtherRepet.rules
   :members:
   :undoc-members:
   :show-inheritance:

annotations.OtherRepet.sppasrepet module
----------------------------------------

.. automodule:: annotations.OtherRepet.sppasrepet
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.OtherRepet
   :members:
   :undoc-members:
   :show-inheritance:

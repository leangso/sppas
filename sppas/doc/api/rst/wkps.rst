wkps package
============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   wkps.wio

Submodules
----------

wkps.filebase module
--------------------

.. automodule:: wkps.filebase
   :members:
   :undoc-members:
   :show-inheritance:

wkps.filedatacompare module
---------------------------

.. automodule:: wkps.filedatacompare
   :members:
   :undoc-members:
   :show-inheritance:

wkps.filedatafilters module
---------------------------

.. automodule:: wkps.filedatafilters
   :members:
   :undoc-members:
   :show-inheritance:

wkps.fileref module
-------------------

.. automodule:: wkps.fileref
   :members:
   :undoc-members:
   :show-inheritance:

wkps.filestructure module
-------------------------

.. automodule:: wkps.filestructure
   :members:
   :undoc-members:
   :show-inheritance:

wkps.sppasWkps module
---------------------

.. automodule:: wkps.sppasWkps
   :members:
   :undoc-members:
   :show-inheritance:

wkps.wkpexc module
------------------

.. automodule:: wkps.wkpexc
   :members:
   :undoc-members:
   :show-inheritance:

wkps.workspace module
---------------------

.. automodule:: wkps.workspace
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: wkps
   :members:
   :undoc-members:
   :show-inheritance:

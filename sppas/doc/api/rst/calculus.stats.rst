calculus.stats package
======================

Submodules
----------

calculus.stats.central module
-----------------------------

.. automodule:: calculus.stats.central
   :members:
   :undoc-members:
   :show-inheritance:

calculus.stats.descriptivesstats module
---------------------------------------

.. automodule:: calculus.stats.descriptivesstats
   :members:
   :undoc-members:
   :show-inheritance:

calculus.stats.frequency module
-------------------------------

.. automodule:: calculus.stats.frequency
   :members:
   :undoc-members:
   :show-inheritance:

calculus.stats.linregress module
--------------------------------

.. automodule:: calculus.stats.linregress
   :members:
   :undoc-members:
   :show-inheritance:

calculus.stats.moment module
----------------------------

.. automodule:: calculus.stats.moment
   :members:
   :undoc-members:
   :show-inheritance:

calculus.stats.variability module
---------------------------------

.. automodule:: calculus.stats.variability
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: calculus.stats
   :members:
   :undoc-members:
   :show-inheritance:

calculus.scoring package
========================

Submodules
----------

calculus.scoring.kappa module
-----------------------------

.. automodule:: calculus.scoring.kappa
   :members:
   :undoc-members:
   :show-inheritance:

calculus.scoring.ubpa module
----------------------------

.. automodule:: calculus.scoring.ubpa
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: calculus.scoring
   :members:
   :undoc-members:
   :show-inheritance:

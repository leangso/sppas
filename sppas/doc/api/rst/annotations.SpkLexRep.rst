annotations.SpkLexRep package
=============================

Submodules
----------

annotations.SpkLexRep.sppaslexrep module
----------------------------------------

.. automodule:: annotations.SpkLexRep.sppaslexrep
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.SpkLexRep
   :members:
   :undoc-members:
   :show-inheritance:

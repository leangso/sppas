annotations.Syll package
========================

Submodules
----------

annotations.Syll.rules module
-----------------------------

.. automodule:: annotations.Syll.rules
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Syll.sppassyll module
---------------------------------

.. automodule:: annotations.Syll.sppassyll
   :members:
   :undoc-members:
   :show-inheritance:

annotations.Syll.syllabify module
---------------------------------

.. automodule:: annotations.Syll.syllabify
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.Syll
   :members:
   :undoc-members:
   :show-inheritance:

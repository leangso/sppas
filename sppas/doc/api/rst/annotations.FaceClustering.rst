annotations.FaceClustering package
==================================

Submodules
----------

annotations.FaceClustering.identifycoords module
------------------------------------------------

.. automodule:: annotations.FaceClustering.identifycoords
   :members:
   :undoc-members:
   :show-inheritance:

annotations.FaceClustering.kidsbuffer module
--------------------------------------------

.. automodule:: annotations.FaceClustering.kidsbuffer
   :members:
   :undoc-members:
   :show-inheritance:

annotations.FaceClustering.kidswriter module
--------------------------------------------

.. automodule:: annotations.FaceClustering.kidswriter
   :members:
   :undoc-members:
   :show-inheritance:

annotations.FaceClustering.sppasfaceid module
---------------------------------------------

.. automodule:: annotations.FaceClustering.sppasfaceid
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.FaceClustering
   :members:
   :undoc-members:
   :show-inheritance:

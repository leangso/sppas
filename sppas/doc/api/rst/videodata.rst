videodata package
=================

Submodules
----------

videodata.video module
----------------------

.. automodule:: videodata.video
   :members:
   :undoc-members:
   :show-inheritance:

videodata.videobuffer module
----------------------------

.. automodule:: videodata.videobuffer
   :members:
   :undoc-members:
   :show-inheritance:

videodata.videocoords module
----------------------------

.. automodule:: videodata.videocoords
   :members:
   :undoc-members:
   :show-inheritance:

videodata.videodataexc module
-----------------------------

.. automodule:: videodata.videodataexc
   :members:
   :undoc-members:
   :show-inheritance:

videodata.videoutils module
---------------------------

.. automodule:: videodata.videoutils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: videodata
   :members:
   :undoc-members:
   :show-inheritance:

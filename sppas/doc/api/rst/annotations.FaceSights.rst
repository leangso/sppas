annotations.FaceSights package
==============================

Submodules
----------

annotations.FaceSights.imgfacemark module
-----------------------------------------

.. automodule:: annotations.FaceSights.imgfacemark
   :members:
   :undoc-members:
   :show-inheritance:

annotations.FaceSights.sights module
------------------------------------

.. automodule:: annotations.FaceSights.sights
   :members:
   :undoc-members:
   :show-inheritance:

annotations.FaceSights.sppasfacesights module
---------------------------------------------

.. automodule:: annotations.FaceSights.sppasfacesights
   :members:
   :undoc-members:
   :show-inheritance:

annotations.FaceSights.videofacemark module
-------------------------------------------

.. automodule:: annotations.FaceSights.videofacemark
   :members:
   :undoc-members:
   :show-inheritance:

annotations.FaceSights.videosights module
-----------------------------------------

.. automodule:: annotations.FaceSights.videosights
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations.FaceSights
   :members:
   :undoc-members:
   :show-inheritance:

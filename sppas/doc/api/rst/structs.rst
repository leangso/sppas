structs package
===============

Submodules
----------

structs.basecompare module
--------------------------

.. automodule:: structs.basecompare
   :members:
   :undoc-members:
   :show-inheritance:

structs.basefilters module
--------------------------

.. automodule:: structs.basefilters
   :members:
   :undoc-members:
   :show-inheritance:

structs.basefset module
-----------------------

.. automodule:: structs.basefset
   :members:
   :undoc-members:
   :show-inheritance:

structs.baseoption module
-------------------------

.. automodule:: structs.baseoption
   :members:
   :undoc-members:
   :show-inheritance:

structs.dag module
------------------

.. automodule:: structs.dag
   :members:
   :undoc-members:
   :show-inheritance:

structs.lang module
-------------------

.. automodule:: structs.lang
   :members:
   :undoc-members:
   :show-inheritance:

structs.metainfo module
-----------------------

.. automodule:: structs.metainfo
   :members:
   :undoc-members:
   :show-inheritance:

structs.structsexc module
-------------------------

.. automodule:: structs.structsexc
   :members:
   :undoc-members:
   :show-inheritance:

structs.tips module
-------------------

.. automodule:: structs.tips
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: structs
   :members:
   :undoc-members:
   :show-inheritance:

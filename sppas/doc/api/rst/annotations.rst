annotations package
===================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   annotations.Activity
   annotations.Align
   annotations.CuedSpeech
   annotations.FaceClustering
   annotations.FaceDetection
   annotations.FaceSights
   annotations.FillIPUs
   annotations.IVA
   annotations.Intsint
   annotations.LexMetric
   annotations.Momel
   annotations.OtherRepet
   annotations.Overlaps
   annotations.Phon
   annotations.RMS
   annotations.ReOccurrences
   annotations.SearchIPUs
   annotations.SelfRepet
   annotations.SpkLexRep
   annotations.StopWords
   annotations.Syll
   annotations.TGA
   annotations.TextNorm

Submodules
----------

annotations.annotationsexc module
---------------------------------

.. automodule:: annotations.annotationsexc
   :members:
   :undoc-members:
   :show-inheritance:

annotations.autils module
-------------------------

.. automodule:: annotations.autils
   :members:
   :undoc-members:
   :show-inheritance:

annotations.baseannot module
----------------------------

.. automodule:: annotations.baseannot
   :members:
   :undoc-members:
   :show-inheritance:

annotations.diagnosis module
----------------------------

.. automodule:: annotations.diagnosis
   :members:
   :undoc-members:
   :show-inheritance:

annotations.infotier module
---------------------------

.. automodule:: annotations.infotier
   :members:
   :undoc-members:
   :show-inheritance:

annotations.log module
----------------------

.. automodule:: annotations.log
   :members:
   :undoc-members:
   :show-inheritance:

annotations.manager module
--------------------------

.. automodule:: annotations.manager
   :members:
   :undoc-members:
   :show-inheritance:

annotations.param module
------------------------

.. automodule:: annotations.param
   :members:
   :undoc-members:
   :show-inheritance:

annotations.searchtier module
-----------------------------

.. automodule:: annotations.searchtier
   :members:
   :undoc-members:
   :show-inheritance:

annotations.windowing module
----------------------------

.. automodule:: annotations.windowing
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: annotations
   :members:
   :undoc-members:
   :show-inheritance:

plugins package
===============

Submodules
----------

plugins.manager module
----------------------

.. automodule:: plugins.manager
   :members:
   :undoc-members:
   :show-inheritance:

plugins.plugin module
---------------------

.. automodule:: plugins.plugin
   :members:
   :undoc-members:
   :show-inheritance:

plugins.pluginsexc module
-------------------------

.. automodule:: plugins.pluginsexc
   :members:
   :undoc-members:
   :show-inheritance:

plugins.process module
----------------------

.. automodule:: plugins.process
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: plugins
   :members:
   :undoc-members:
   :show-inheritance:

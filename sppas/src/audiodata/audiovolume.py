# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.audiodata.audiovolume.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Estimate and store the RMS of an audio.

.. _This file is part of SPPAS: https://sppas.org/
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

from .audioframes import sppasAudioFrames
from .basevolume import sppasBaseVolume

# ----------------------------------------------------------------------------


class sppasAudioVolume(sppasBaseVolume):
    """Estimate the volume of an audio file.

    The volume is the estimation of RMS values, sampled with a window of 10ms.

    """

    def __init__(self, audio, win_len=0.01):
        """Create a sppasAudioVolume instance.

        :param audio: (sppasAudioPCM) The audio to work on.
        :param win_len: (float) Window length to estimate the volume.

        """
        super(sppasAudioVolume, self).__init__(win_len)

        # Remember current position
        pos = audio.tell()

        # Rewind to the beginning
        audio.rewind()

        # Find the rms values (explore all frames)
        nb_frames = int(win_len * audio.get_framerate())

        while audio.tell() < audio.get_nframes():
            frames = audio.read_frames(nb_frames)
            a = sppasAudioFrames(frames, audio.get_sampwidth(), audio.get_nchannels())
            self._volumes.append(a.rms())

        # Returns to the position where we was before
        audio.seek(pos)

        self._rms = audio.rms()

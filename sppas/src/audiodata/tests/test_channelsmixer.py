"""
:filename: sppas.src.audiodata.test.test_channelsmixer.py
:author:   Nicolas Chazeau, Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Test of the channel mixer.

.. _This file is part of SPPAS: https://sppas.org/
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

import unittest
import os.path

from sppas.src.config import paths

from sppas.src.audiodata.aio import open as audio_open
from sppas.src.audiodata.channelformatter import sppasChannelFormatter
from sppas.src.audiodata.channelsmixer import sppasChannelMixer

# ---------------------------------------------------------------------------

sample_1 = os.path.join(paths.samples, "samples-eng", "oriana1.wav")  # mono; 16000Hz; 16bits
sample_2 = os.path.join(paths.samples, "samples-fra", "F_F_B003_P9.wav")  # mono; 44100Hz; 32bits

# ---------------------------------------------------------------------------


class TestChannelsMixer(unittest.TestCase):

    def setUp(self):
        self._sample_1 = audio_open(sample_1)
        self._sample_2 = audio_open(sample_2)

    def tearDown(self):
        self._sample_1.close()
        self._sample_2.close()

    def test_Mix(self):
        self._sample_1.extract_channel(0)
        self._sample_2.extract_channel(0)

        formatter1 = sppasChannelFormatter(self._sample_1.get_channel(0))
        formatter1.set_framerate(16000)
        formatter1.set_sampwidth(2)
        formatter1.convert()

        formatter2 = sppasChannelFormatter(self._sample_2.get_channel(0))
        formatter2.set_framerate(16000)
        formatter2.set_sampwidth(2)
        formatter2.convert()

        mixer = sppasChannelMixer()
        mixer.append_channel(formatter1.get_channel())
        mixer.append_channel(formatter2.get_channel())
        mixer.norm_length()

        self.assertEqual(mixer.get_channel(0).get_nframes(), mixer.get_channel(1).get_nframes())

        newchannel = mixer.mix()

        self.assertEqual(newchannel.get_nframes(), mixer.get_channel(0).get_nframes())
        self.assertEqual(newchannel.get_nframes(), mixer.get_channel(1).get_nframes())

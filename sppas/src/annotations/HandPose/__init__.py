"""
:filename: sppas.src.annotations.HandPose.__init__.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: Hand & Pose detection automatic annotations of SPPAS.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

SPPAS is a wrapper for MediaPipe Hand detection and Mediapipe Pose detection.
It also proposes a custom solution in order to detect right-left hands of a
single person.


"""

import logging
from sppas.src.config import cfg
from sppas.src.config import sppasEnableFeatureError

# ---------------------------------------------------------------------------


if cfg.feature_installed("video") is False:
    # Define classes in case opencv&numpy&mediapipe are not installed.
    logging.warning("Hand&Pose is disabled because it requires video feature.")


    class sppasHandPose(object):
        def __init__(self, *args, **kwargs):
            raise sppasEnableFeatureError("video")


    class MediaPipeHandPoseDetector(object):
        def __init__(self):
            raise sppasEnableFeatureError("video")

else:
    # Import the classes in case mediapipe is installed so that
    # the automatic detections can work.
    from .sppashandpose import sppasHandPose

# ---------------------------------------------------------------------------


__all__ = (
    "sppasHandPose",
    "MediaPipeHandPoseDetector"
)

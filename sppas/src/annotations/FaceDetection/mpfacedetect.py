"""
:filename: sppas.src.annotations.FaceDetection.mpfacedetect.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  MediaPipe detector of faces in an image.

.. _This file is part of SPPAS: https://sppas.org/
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

import logging
import mediapipe as mp

from sppas.src.config import sppasError
from sppas.src.imgdata import sppasCoords
from sppas.src.imgdata import BaseObjectsDetector

# ---------------------------------------------------------------------------


class MediaPipeFaceDetector(BaseObjectsDetector):
    """SPPAS wrapper of MediaPipe Face Detection.

    See <https://google.github.io/mediapipe/solutions/face_detection>
    for details:

    "MediaPipe Face Detection is an ultrafast face detection solution that
    comes with 6 landmarks and multi-face support. It is based on BlazeFace,
    a lightweight and well-performing face detector tailored for mobile GPU
    inference."

    """

    def __init__(self):
        super(MediaPipeFaceDetector, self).__init__()
        self._extension = ""

    # -----------------------------------------------------------------------

    def load_model(self, model=None, *args):
        """Override. Instantiate a detector.

        :param model: Unused.

        """
        self._set_detector()

    # -----------------------------------------------------------------------

    def _set_detector(self, model=None):
        """Initialize the detector.

        MediaPipe has it's internal model so the given file is not used.

        :param model: (str) Un-used.
        :raises: Exception

        """
        try:
            self._detector = mp.solutions.face_detection.FaceDetection(
                min_detection_confidence=self.get_min_score()
               )
        except Exception as e:
            logging.error("MediaPipe face detection system failed to be instantiated.")
            raise sppasError(str(e))

    # -----------------------------------------------------------------------

    def set_min_score(self, value):
        """Override. Set the minimum score of a detected object to consider it.

        It means that any detected object with a score lesser than the given
        one will be ignored. The score of detected objects are supposed to
        range between 0. and 1.

        :param value: (float) Value ranging [0., 1.]
        :raise: ValueError

        """
        BaseObjectsDetector.set_min_score(self, value)
        if self._detector is not None:
            self._set_detector()

    # -----------------------------------------------------------------------

    def _detection(self, image):
        """Determine the coordinates of the detected objects.

        :param image: (sppasImage or numpy.ndarray)

        """
        # make predictions
        # Convert the BGR image to RGB before processing.
        results = self._detector.process(image.ito_rgb())

        # Convert detections into a list of sppasCoords
        if not results.detections:
            return

        w, h = image.size()
        for face in results.detections:
            # Compared to the other detectors, Media Pipe FD scores are lowers.
            # We "boost" them...
            score = min(1., 1.2*face.score[0])

            # The format of the face is a relative bounding box, ie
            # it's a box with a relative size of the image.
            # Compared to the other detectors, this box is very large around
            # the face. We lower it...
            relative_roi = face.location_data.relative_bounding_box
            x_coord = max(0, int(relative_roi.xmin * float(w)))
            y_coord = max(0, int(relative_roi.ymin * float(h)))
            w_coord = int(relative_roi.width * float(w) * 0.7)
            h_coord = int(relative_roi.height * float(h) * 0.8)
            x_coord += int(0.2 * float(w_coord))

            coord = sppasCoords(x_coord, y_coord, w_coord, h_coord)
            coord.set_confidence(score)
            self._coords.append(coord)

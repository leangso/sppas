# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.tests.test_cuedspeech.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Tests of Cued Speech automatic annotation.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import unittest
import os.path

from sppas.src.config import paths
from sppas.src.anndata import sppasTier
from sppas.src.anndata import sppasPoint
from sppas.src.anndata import sppasInterval
from sppas.src.anndata import sppasLocation
from sppas.src.anndata import sppasLabel
from sppas.src.anndata import sppasTag

from sppas.src.annotations.CuedSpeech.whatkey.keysrules import CuedSpeechCueingRules
from sppas.src.annotations.CuedSpeech.whatkey.phonestokeys import CuedSpeechKeys
from sppas.src.annotations.CuedSpeech.whatkey.genkeystier import sppasKeysGenerator
from sppas.src.annotations.CuedSpeech.whenhand.genhandtranstier import sppasHandTransitionsGenerator
from sppas.src.annotations.CuedSpeech.whenhand.transitions import sppasTransitionsPredictor
from sppas.src.annotations.CuedSpeech.wherecue.vowelcoords import sppasVowelPosition

# ---------------------------------------------------------------------------

FRA_KEYS = os.path.join(paths.resources, "cuedspeech", "cueConfig-fra.txt")

# ---------------------------------------------------------------------------


class TestWhatKeyRules(unittest.TestCase):

    def test_init(self):
        # No file is defined
        rules = CuedSpeechCueingRules()
        self.assertIsNone(rules.get_key("p"))
        self.assertIsNone(rules.get_key("#"))
        self.assertEqual(rules.get_class("p"), CuedSpeechCueingRules.PHONCLASS)
        self.assertEqual(rules.get_class("#"), CuedSpeechCueingRules.PHONCLASS)
        self.assertEqual(rules.get_class("cnone"), "N")
        self.assertEqual(rules.get_class("vnone"), "N")
        self.assertEqual(rules.get_class("cnil"), "N")
        self.assertEqual(rules.get_class("vnil"), "N")

        self.assertEqual(rules.get_key("cnone"), "0")
        self.assertEqual(rules.get_key("vnone"), "n")
        self.assertIsNone(rules.get_key("cnil"))
        self.assertIsNone(rules.get_key("vnil"))

        # A wrong file is given
        with self.assertRaises(IOError):
            rules = CuedSpeechCueingRules("toto.txt")

        # The expected file is given
        rules = CuedSpeechCueingRules(FRA_KEYS)
        self.assertEqual(rules.get_class("#"), CuedSpeechCueingRules.PHONCLASS)
        self.assertEqual(rules.get_class("e"), "V")
        self.assertEqual(rules.get_class("p"), "C")
        self.assertEqual(rules.get_class("fp"), CuedSpeechCueingRules.PHONCLASS)

        self.assertEqual(rules.get_key("p"), "1")
        self.assertEqual(rules.get_key("e"), "t")   # throat
        self.assertEqual(rules.get_key("E"), "c")   # chin
        self.assertEqual(rules.get_key("2"), "b")   # cheek bone
        self.assertEqual(rules.get_key("#"), None)
        self.assertEqual(rules.get_key("fp"), None)

    # -----------------------------------------------------------------------

    def test_load(self):
        rules = CuedSpeechCueingRules()

        # Wrong filename
        with self.assertRaises(IOError):
            rules.load("toto.txt")

        # No rule defined in the file
        result = rules.load(os.path.abspath(__file__))
        self.assertFalse(result)

        # A well-formed file
        result = rules.load(FRA_KEYS)
        self.assertTrue(result)
        self.assertEqual(rules.get_class("p"), "C")
        self.assertEqual(rules.get_class("e"), "V")
        self.assertEqual(rules.get_key("p"), "1")
        self.assertEqual(rules.get_key("e"), "t")

        self.assertEqual(rules.get_class("cnil"), "N")
        self.assertEqual(rules.get_class("vnil"), "N")
        self.assertEqual(rules.get_key("cnil"), "5")
        self.assertEqual(rules.get_key("vnil"), "s")

    # -----------------------------------------------------------------------

    def test_getters(self):
        rules = CuedSpeechCueingRules(FRA_KEYS)
        self.assertEqual(rules.get_nil_vowel(), "s")
        self.assertEqual(rules.get_nil_consonant(), "5")

        self.assertEqual(CuedSpeechCueingRules().get_neutral_consonant(), "0")
        self.assertEqual(CuedSpeechCueingRules().get_neutral_vowel(), "n")

    # -----------------------------------------------------------------------

    def test_get_keys(self):
        rules = CuedSpeechCueingRules(FRA_KEYS)

        # well-formed syllables (C-V or C- or -V)

        keys = rules.syll_to_key("p-a")
        self.assertEqual(("1", "s"), keys)

        keys = rules.syll_to_key("p")
        self.assertEqual(("1", "s"), keys)

        keys = rules.syll_to_key("a")
        self.assertEqual(("5", "s"), keys)

        # badly-formed syllables

        # with self.assertRaises(ValueError):
        #     rules.syll_to_key("") # not raised...

        with self.assertRaises(ValueError):
            rules.syll_to_key("p-a-p")

        with self.assertRaises(ValueError):
            rules.syll_to_key("p-p")

        with self.assertRaises(ValueError):
            rules.syll_to_key("a-a")

        with self.assertRaises(ValueError):
            rules.syll_to_key("a-p")

    # -----------------------------------------------------------------------

    def test_get_codes(self):
        rules = CuedSpeechCueingRules()
        vowels = rules.get_vowels_codes()
        self.assertEqual(len(vowels), 1)  # neutral=n
        self.assertTrue('n' in vowels)

        rules.load(FRA_KEYS)
        vowels = rules.get_vowels_codes()
        self.assertEqual(len(vowels), 6)  # neutral=n and 5 positions
        self.assertTrue('n' in vowels)
        self.assertEqual(vowels[0], 'n')

# ---------------------------------------------------------------------------


class TestWhatCuedSpeechKeys(unittest.TestCase):

    def setUp(self):
        self.lfpc = CuedSpeechKeys(FRA_KEYS)

    # -----------------------------------------------------------------------

    def test_annotate_breaks(self):
        phonemes = list()
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, list())

        phonemes = ["#"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, list())

        phonemes = ["fp"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, list())

    # -----------------------------------------------------------------------

    def test_annotate_consonants(self):
        phonemes = ["p"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 0)])

        phonemes = ["s", "p"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 0), (1, 1)])

        phonemes = ["s", "p", "p"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 0), (1, 1), (2, 2)])

        phonemes = ["s", "p", "#", "p"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 0), (1, 1), (3, 3)])

        phonemes = ["#", "s", "p", "#", "p"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(1, 1), (2, 2), (4, 4)])

    # -----------------------------------------------------------------------

    def test_annotate_vowels(self):
        phonemes = ["a"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 0)])

        phonemes = ["a", "e"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 0), (1, 1)])

        phonemes = ["a", "e", "u"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 0), (1, 1), (2, 2)])

        phonemes = ["a", "e", "#", "u"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 0), (1, 1), (3, 3)])

        phonemes = ["#", "a", "e", "#", "u"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(1, 1), (2, 2), (4, 4)])

    # -----------------------------------------------------------------------

    def test_annotate_unknown(self):
        # Unknown phonemes are not coded!
        phonemes = ["X"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, list())

        phonemes = ["a", "X"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 0)])

        phonemes = ["a", "X", "a"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 0), (2, 2)])

    # -----------------------------------------------------------------------

    def test_syllabify(self):
        phonemes = ['b', 'O~', 'Z', 'u', 'R']
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 1), (2, 3), (4, 4)])

        phonemes = ['b', 'O~', '#', 'Z', 'u', 'R']
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 1), (3, 4), (5, 5)])

        phonemes = ['b', 'O~', 'fp', 'Z', 'u', 'R', "#"]
        sgmts = self.lfpc.syllabify(phonemes)
        self.assertEqual(sgmts, [(0, 1), (3, 4), (5, 5)])

    # -----------------------------------------------------------------------

    def test_phonetize(self):
        phonemes = ['b', 'O~', 'Z', 'u', 'R']
        sgmts = self.lfpc.syllabify(phonemes)
        phons = self.lfpc.phonetize_syllables(phonemes, sgmts)
        self.assertEqual(phons, "b-O~.Z-u.R")

        phonemes = ['b', 'O~', 'fp', 'Z', 'u', 'R', "#"]
        sgmts = self.lfpc.syllabify(phonemes)
        phons = self.lfpc.phonetize_syllables(phonemes, sgmts)
        self.assertEqual(phons, "b-O~.Z-u.R")

    # -----------------------------------------------------------------------

    def test_keys(self):
        phonemes = ['b', 'O~', 'Z', 'u', 'R']
        sgmts = self.lfpc.syllabify(phonemes)
        phons = self.lfpc.phonetize_syllables(phonemes, sgmts)
        keys = self.lfpc.keys_phonetized(phons)
        self.assertEqual(keys, "4-m.1-c.3-s")

        phonemes = ['b', 'O~', 'fp', 'Z', 'u', 'R', "#"]
        sgmts = self.lfpc.syllabify(phonemes)
        phons = self.lfpc.phonetize_syllables(phonemes, sgmts)
        keys = self.lfpc.keys_phonetized(phons)
        self.assertEqual(keys, "4-m.1-c.3-s")

# ---------------------------------------------------------------------------


class TestWhatKeysGenerator(unittest.TestCase):

    def setUp(self):
        self.rules = CuedSpeechKeys(FRA_KEYS)
        self.lpc = sppasKeysGenerator(self.rules)

        self.tier = sppasTier("PhonAlign")
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(0.), sppasPoint(0.2))),
            sppasLabel(sppasTag("#")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(0.2), sppasPoint(0.6))),
            sppasLabel(sppasTag("v")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(0.6), sppasPoint(2.))),
            sppasLabel(sppasTag("#")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(2.), sppasPoint(2.8))),
            sppasLabel(sppasTag("j")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(2.8), sppasPoint(3.9))),
            sppasLabel(sppasTag("o")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(3.9), sppasPoint(4.))),
            sppasLabel(sppasTag("+")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(4.), sppasPoint(5.))),
            sppasLabel(sppasTag("l")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(5.), sppasPoint(6.))),
            sppasLabel(sppasTag("E")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(6.), sppasPoint(7.5))),
            sppasLabel(sppasTag("t")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(7.5), sppasPoint(10.))),
            sppasLabel(sppasTag("#")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(10.), sppasPoint(11.))),
            sppasLabel(sppasTag("v")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(11.), sppasPoint(12.))),
            sppasLabel(sppasTag("i")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(12.), sppasPoint(14.2))),
            sppasLabel(sppasTag("#")))

    # -----------------------------------------------------------------------

    def test_phons_to_key_generator(self):
        result_phons = self.lpc.phons_to_segments(self.tier)
        self.assertEqual(len(result_phons), 5)

        result_keys, result_class = self.lpc.segments_to_keys(result_phons,
                                                              self.tier.get_first_point(),
                                                              self.tier.get_last_point())
        self.assertEqual(len(result_keys), len(result_class))
        for ann1, ann2 in zip(result_keys, result_class):
            labels = ann1.get_labels()
            self.assertTrue(len(labels), 2)
            print(labels)
            labels = ann2.get_labels()
            self.assertTrue(len(labels), 2)
            print(labels)
            print()
        self.assertEqual(len(result_keys), 9)

# ---------------------------------------------------------------------------
# WHEN...
# ---------------------------------------------------------------------------


class TestWhenTransitionsPrediction(unittest.TestCase):

    def test_init(self):
        versions = sppasTransitionsPredictor.version_numbers()
        self.assertEqual(4, len(versions))
        for version_nb in versions:
            predictor = sppasTransitionsPredictor(version_nb)
            self.assertEqual(version_nb, predictor.get_version_number())

    def test_predict_base(self):
        predictor = sppasTransitionsPredictor(0)
        with self.assertRaises(Exception):
            predictor.predict_d()
        with self.assertRaises(Exception):
            predictor.predict_m()

        predictor.set_a(0, 2)
        self.assertEqual((0., 0.), predictor.predict_d())
        self.assertEqual((0., 0.), predictor.predict_m())

        predictor.set_a(2, 4)
        self.assertEqual((2., 2.), predictor.predict_d())
        self.assertEqual((2., 2.), predictor.predict_m())

    def test_predict_duchnowski(self):
        predictor = sppasTransitionsPredictor(1)
        with self.assertRaises(Exception):
            predictor.predict_d()
        with self.assertRaises(Exception):
            predictor.predict_m()

        predictor.set_a(0, 2)
        self.assertEqual((0., 0.), predictor.predict_d())
        self.assertEqual((0., 0.), predictor.predict_m())

        predictor.set_a(2, 4)
        self.assertEqual((1.9, 1.9), predictor.predict_d())
        self.assertEqual((1.9, 1.9), predictor.predict_m())

    def test_predict_attina(self):
        predictor = sppasTransitionsPredictor(2)
        with self.assertRaises(Exception):
            predictor.predict_d()
        with self.assertRaises(Exception):
            predictor.predict_m()

        predictor.set_a(0, 2)
        self.assertEqual((0., 0.), predictor.predict_d())
        self.assertEqual((0., 0.2), predictor.predict_m())

        # Predict for a CV key
        predictor.set_a(2, 4)
        m1, m2 = predictor.predict_m()
        self.assertEqual(0.76, round(m1, 2))
        self.assertEqual(2.2, round(m2, 2))

        d1, d2 = predictor.predict_d()
        self.assertEqual(0.90, round(d1, 2))
        self.assertEqual(1.98, round(d2, 2))

        # Predict for a V- key
        predictor.set_a(2, 4)
        m1, m2 = predictor.predict_m(is_nil_shape=True)
        self.assertEqual(1.08, round(m1, 2))
        self.assertEqual(2.42, round(m2, 2))

        d1, d2 = predictor.predict_d()
        self.assertEqual(0.90, round(d1, 2))   # CANT BE POSSIBLE ******* MUST: d1 >= m1
        self.assertEqual(1.98, round(d2, 2))

        # This model predicts the same values,
        # even if the current key is neutral.
        m1, m2 = predictor.predict_m(rank=0)
        self.assertEqual(0.76, round(m1, 2))
        self.assertEqual(2.2, round(m2, 2))

        d1, d2 = predictor.predict_d(is_neutral=True)
        self.assertEqual(0.90, round(d1, 2))
        self.assertEqual(1.98, round(d2, 2))

    def test_predict_rules(self):
        predictor = sppasTransitionsPredictor(3)
        with self.assertRaises(Exception):
            predictor.predict_d()
        with self.assertRaises(Exception):
            predictor.predict_m()

        predictor.set_a(0, 2)
        self.assertEqual((0., 0.), predictor.predict_d())
        self.assertEqual((0., 0.42), predictor.predict_m())

        predictor.set_a(2, 4)
        m1, m2 = predictor.predict_m()
        self.assertEqual(1.18, round(m1, 2))
        self.assertEqual(2.42, round(m2, 2))

        d1, d2 = predictor.predict_d()
        self.assertEqual(0.88, round(d1, 2))
        self.assertEqual(1.88, round(d2, 2))

        # Predict for a V- key
        predictor.set_a(2, 4)
        m1, m2 = predictor.predict_m(is_nil_shape=True)
        self.assertEqual(1.28, round(m1, 2))
        self.assertEqual(2.2, round(m2, 2))

        d1, d2 = predictor.predict_d(is_nil_shape=True)
        self.assertEqual(1., round(d1, 2))
        self.assertEqual(1.68, round(d2, 2))

        # This model predicts different values, if the current key is neutral
        m1, m2 = predictor.predict_m(rank=0)
        self.assertEqual(2.0, round(m1, 2))
        self.assertEqual(2.5, round(m2, 2))

        d1, d2 = predictor.predict_d(is_neutral=True)
        self.assertEqual(0.88, round(d1, 2))
        self.assertEqual(1.88, round(d2, 2))

        # predict d when m is known
        m1, m2 = predictor.predict_m(rank=0)
        d1, d2 = predictor.predict_d(is_neutral=True, m1=m1, m2=m2)
        self.assertEqual(2., round(d1, 2))
        self.assertEqual(2., round(d2, 2))
        predictor.set_a(2, 4)
        m1, m2 = predictor.predict_m()
        self.assertEqual(1.18, round(m1, 2))
        self.assertEqual(2.42, round(m2, 2))
        d1, d2 = predictor.predict_d(m1=m1, m2=m2)
        self.assertEqual(1.18, round(d1, 2))
        self.assertEqual(1.88, round(d2, 2))

        # predict m of the 1st key after a silence
        predictor.set_a(2, 4)
        m1, m2 = predictor.predict_m(rank=1)
        self.assertEqual(0., round(m1, 2))
        self.assertEqual(2., round(m2, 2))

# ---------------------------------------------------------------------------


class TestWhenCuedSpeechGenerator(unittest.TestCase):

    def setUp(self):
        self.rules = CuedSpeechKeys(FRA_KEYS)
        self.lpc = sppasKeysGenerator(self.rules)
        self.hand = sppasHandTransitionsGenerator(self.rules)

        self.tier = sppasTier("PhonAlign")
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(1.), sppasPoint(2.))),
            sppasLabel(sppasTag("v")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(2.), sppasPoint(2.8))),
            sppasLabel(sppasTag("j")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(2.8), sppasPoint(4.))),
            sppasLabel(sppasTag("o")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(4.), sppasPoint(5.))),
            sppasLabel(sppasTag("l")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(5.), sppasPoint(6.))),
            sppasLabel(sppasTag("E")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(6.), sppasPoint(7.5))),
            sppasLabel(sppasTag("t")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(7.5), sppasPoint(10.))),
            sppasLabel(sppasTag("#")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(10.), sppasPoint(11.))),
            sppasLabel(sppasTag("v")))
        self.tier.create_annotation(
            sppasLocation(sppasInterval(sppasPoint(11.), sppasPoint(12.))),
            sppasLabel(sppasTag("i")))

    # -----------------------------------------------------------------------

    def test_phons_to_key_generator(self):
        result_phons = self.lpc.phons_to_segments(self.tier)
        self.assertEqual(len(result_phons), 5)

        result_keys, result_class = self.lpc.segments_to_keys(result_phons,
            self.tier.get_first_point(),
            self.tier.get_last_point())
        self.assertEqual(len(result_keys), 6)
        self.assertEqual(len(result_class), 6)
        for ann in result_keys:
            labels = ann.get_labels()
            self.assertTrue(len(labels), 2)
        for ann in result_class:
            labels = ann.get_labels()
            self.assertTrue(len(labels), 2)

        print(" ---- VERSION --- {}".format(self.hand.get_generator_version()))

        result_pos = self.hand.keys_to_hand_positions(result_keys)
        for ann in result_pos:
            print(ann)
        self.assertEqual(len(result_pos), 5)

        result_shapes = self.hand.keys_to_hand_shapes(result_keys)
        for ann in result_shapes:
            print(ann)
        self.assertEqual(len(result_shapes), 5)

# ---------------------------------------------------------------------------
# WHERE...
# ---------------------------------------------------------------------------


class TestWherePredictVowelsCoords(unittest.TestCase):

    def test_get(self):
        # Default position of each vowel in a standard
        # 2D face of 1000 x 1000 pixels
        p = sppasVowelPosition()

        x, y, r = p.predict_vowel_coords('n')
        self.assertEqual((85, 2060), (x, y))
        self.assertEqual(200, r)

        x, y, r = p.predict_vowel_coords('b')
        self.assertEqual((182, 338), (x, y))

        x, y, r = p.predict_vowel_coords('m')
        self.assertEqual((240, 620), (x, y))
        self.assertEqual(110, r)

        x, y, r = p.predict_vowel_coords('c')
        self.assertEqual((479, 956), (x, y))

        x, y, r = p.predict_vowel_coords('t')
        self.assertEqual((521, 1477), (x, y))

        x, y, r = p.predict_vowel_coords('s')
        self.assertEqual((-333, 560), (x, y))


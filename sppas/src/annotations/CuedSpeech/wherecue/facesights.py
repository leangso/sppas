# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.facesights.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Sights on a face of size 100x1000px.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""


class FaceTwoDim(object):
    """All (x, y) coordinates of the 68 sights on a 1000x1000px face.

    :Example:

        >>>with FaceTwoDim() as face:
        >>>    print(face.xy0)

    This class is a solution to mimic an 'Enum' but is compatible with both
    Python 2.7 and Python 3+.

    """

    def __init__(self):
        """Create the dictionary."""
        self.__dict__ = dict(
            dim=68,
            xy0=(0, 160),
            xy1=(2, 295),
            xy2=(15, 425),
            xy3=(40, 560),
            xy4=(85, 685),
            xy7=(415, 990),
            xy8=(500, 1000),
            xy9=(585, 990),
            xy13=(960, 560),
            xy14=(985, 425),
            xy15=(1000, 295),
            xy16=(1000, 160),
            xy19=(250, 0),
            xy24=(750, 0),
            xy27=(500, 150),
            xy36=(195, 155),
            xy45=(805, 155),
            xy48=(305, 615),
            xy54=(695, 625),
            xy57=(500, 735),
            xy60=(320, 620)
        )


# ---------------------------------------------------------------------------


class FaceThreeDim(object):
    """All (x, y, z) coordinates of the 468 sights on a 1000x1000px face.

    :Example:

        >>>with FaceThreeDim() as face:
        >>>    print(face.xyz0)

    This class is a solution to mimic an 'Enum' but is compatible with both
    Python 2.7 and Python 3+.

    """

    def __init__(self):
        """Create the dictionary."""
        self.__dict__ = dict(
            dim=468,
            xyz0=(0, 160, 0)
        )

# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.vowelcoords.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Predict all the vowel positions from given sights.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

from .facesights import FaceTwoDim

# ---------------------------------------------------------------------------


class sppasVowelPosition(object):
    """Predict the coordinates of a given vowel from 2D sights.

    """

    def __init__(self, nb_sights=68):
        """Create an instance of the vowel predictor.

        :param nb_sights: (int) Number of sights.

        """
        self._f2 = FaceTwoDim()
        if nb_sights != self._f2.dim:
            raise NotImplementedError(
                "The support for vowel prediction with {:d} sights is not "
                "implemented yet. Expected {:d}.".format(nb_sights, self._f2.dim))

    # -----------------------------------------------------------------------

    def predict_vowel_coords(self, vowel='n', sights=None, **kwargs):
        """Return the relative position of the given vowel.

        :param sights: Coordinates of 68 landmarks of a real face.
        :param vowel: (char) Vowel position name
        :param kwargs: (dict) Parameters for the model to predict... [incoming]
        :return: (x, y, r) point coordinates and radius

        """
        if sights is None:
            x0 = self._f2.xy0[0]
            x4 = self._f2.xy4[0]
            x7 = self._f2.xy7[0]
            x8 = self._f2.xy8[0]
            x16 = self._f2.xy16[0]
            x36 = self._f2.xy36[0]
            x48 = self._f2.xy48[0]
            x54 = self._f2.xy54[0]
            y60 = self._f2.xy60[1]
            y1 = self._f2.xy1[1]
            y2 = self._f2.xy2[1]
            y3 = self._f2.xy3[1]
            y8 = self._f2.xy8[1]
            y19 = self._f2.xy19[1]
            y57 = self._f2.xy57[1]
        else:
            # Some positions in the real detected face (WITHOUT Z)
            x0, y0, _, s0 = sights.get_sight(0)
            x1, y1, _, s1 = sights.get_sight(1)
            x2, y2, _, s2 = sights.get_sight(2)
            x3, y3, _, s3 = sights.get_sight(3)
            x4, y4, _, s4 = sights.get_sight(4)
            x7, _, _, _ = sights.get_sight(7)
            x8, y8, _, s8 = sights.get_sight(8)
            x16, y16, _, s16 = sights.get_sight(16)
            _, y19, _, _ = sights.get_sight(19)
            x36, y36, _, s36 = sights.get_sight(36)
            x48, y48, _, s48 = sights.get_sight(48)
            x54, y54, _, s54 = sights.get_sight(54)
            x57, y57, _, s57 = sights.get_sight(57)
            x60, y60, _, s60 = sights.get_sight(60)

        radius_ratio = float((x16 - x0) + (y8 - y19)) / 2000.

        if vowel == 'b':
            x = x4 + ((abs(x36 - x0)) // 2)
            y = y1 + (abs(y2 - y1) // 3)
            # r = int(20. * radius_ratio)
            r = abs(x36 - x0) // 2

        elif vowel == 'c':
            x = x8 - ((abs(x8 - x7)) // 4)
            y = y8 - (abs(y8 - y57) // 6)
            r = int(abs(x8 - x7) / 1.5)

        elif vowel == 'm':
            x = x48 - (abs(x54-x48) // 6)
            y = y60
            r = abs(x48 - x4) // 2

        elif vowel == 's':
            x = x0 - ((2 * abs(x8 - x0)) // 3)
            y = y3
            r = int(180. * radius_ratio)

        elif vowel == 't':
            x = x8 + ((abs(x8 - x7)) // 4)
            y = y8 + int(1.8 * float(abs(y8 - y57)))
            r = int(140. * radius_ratio)

        else:
            x = x4
            y = y8 + 4 * (y8 - y57)
            r = int(200. * radius_ratio)

        return x, y, r

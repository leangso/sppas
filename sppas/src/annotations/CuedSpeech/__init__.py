# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.__init__.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Cued Speech automatic annotation.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

This package requires video feature, for opencv and numpy dependencies.

In French, Cued Speech is LfPC, the "Langue française Parlée Complétée".

The problem of cueing speech is divided into 3 tasks:

    1. WHAT? From the time-aligned phonemes, what are the keys?
    2. WHEN? From the time-aligned keys, when moving the hand (position and shape)?
    3. WHERE? Where should the hand be located, looking at the face?

What?
======

The conversion of phonemes into keys of CS is performed using a rule-based
system. This RBS phoneme-to-key segmentation system is based on the
following principle: a key is always of the form CV, a neutral position or
a neutral shape is applied when respectively no vowel or no consonant is
applicable.
System version 1:
For a given language, a position is assigned to each vowel and a shape is
assigned to each consonant. There are no exceptions.

When ?
======

System Proof of Concept (SPPAS version 3):
Each time-aligned key is exactly at the same time than the corresponding
sounds. No transition-time between keys.

System version 0, baseline:
A transition-time is estimated between the keys for both the shape (D1-D2)
and the position (M1-M2). They are both estimated from the model proposed
in the PhD (Attina, 2005, page 117 and page 136).

System version 1, baseline + rules (SPPAS version 4.2):
The position and the shape of the hand are estimated with the baseline
system, except if not reliable. Exceptions are:
- the first key of an IPU (after the hand is in neutral position and shape)
- the last key of an IPU (before the hand will be in neutral pos and shape)

Where?
======

System version 0:
To determine the trajectory of the hand, only one rule is applied:
the straight line is the shortest path!
To determine the angle of the arm, only one rule is applied:
take any picture of the hand and apply without any change.

"""

from sppas.src.config import cfg
from sppas.src.config import sppasEnableFeatureError

from .whatkey.phonestokeys import CuedSpeechKeys
from .sppascuedspeech import sppasCuedSpeech


# ---------------------------------------------------------------------------
# Define classes in case opencv&numpy are not installed.
# ---------------------------------------------------------------------------


class CuedSpeechVideoTagger(object):
    def __init__(self, *args, **kwargs):
        raise sppasEnableFeatureError("video")


# ---------------------------------------------------------------------------
# Import the classes in case the "video" feature is enabled: opencv&numpy
# are both installed and the automatic detections can work.
# ---------------------------------------------------------------------------


if cfg.feature_installed("video"):
    from sppas.src.annotations.CuedSpeech.videotagger.videocued import CuedSpeechVideoTagger

__all__ = (
    "CuedSpeechKeys",
    "CuedSpeechVideoTagger",
    "sppasCuedSpeech"
)

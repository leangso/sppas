# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.whenhand.transitions.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Hand transitions predictors. Answer the "When?" question.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""


class BaseKeysTransitionPredictor(object):
    """Base class to predict hand transition moments [D1,D2] and [M1,M2].

    """

    DELAY = 0.5

    def __init__(self, a1, a3):
        """Instantiate a moment predictor.

                    A1             A2             A3
                    | ---- C ----- | ----- V ---- |
                    | ---- C -------------------- |
                    | -------------------- V -----|

        :param a1: (float) Start time of the consonant of the key
        :param a3: (float) End time of the vowel of the key

        """
        self._a1 = float(a1)
        self._a3 = float(a3)
        self._description = "The moments [D1,D2] and [M1,M2] are predicted at" \
                            "the moment A1. It means there's no transition time."

    # -----------------------------------------------------------------------

    def get_description(self):
        """Return a brief description of the transition method, in English."""
        return self._description

    # -----------------------------------------------------------------------

    def predict_shape(self, **kwargs):
        """Predict [D1, D2] the moments when fingers are changing.

        Predict D1 - when fingers are starting to move
        Predict D2 - when fingers finished representing the expected shape

        """
        return self._a1, self._a1

    # -----------------------------------------------------------------------

    def predict_position(self, **kwargs):
        """Predict [M1, M2] the moments when the hand is moving.

        Predict M1 - when leaving the current position
        Predict M2 - when arrived to the expected position

        """
        return self._a1, self._a1

# ---------------------------------------------------------------------------


class KeysTransitionPredictorDuchnowski(BaseKeysTransitionPredictor):
    """Predict moments with the (Duchnowski et al., 1998) estimator.

    > Paul Duchnowski, Louis Braida, David Lum, Matthew Sexton,
    > Jean Krause, Smriti Banthia
    > AUTOMATIC GENERATION OF CUED SPEECH FOR THE DEAF: STATUS AND OUTLOOK
    https://www.isca-speech.org/archive/pdfs/avsp_1998/duchnowski98_avsp.pdf
    Section 2.2:
    "We found that cues are often formed before
    the corresponding sound is produced. To approxi-
    mate this effect we adjusted the start times of cues to
    begin 100 ms before the boundary determined from
    acoustic data by the cue recognizer."

    >Paul Duchnowski, Louis Braida, Maroula Bratakos, David Lum,
    > Matthew Sexton, Jean Krause
    > A SPEECHREADING AID BASED ON PHONETIC ASR
    https://isca-speech.org/archive_v0/archive_papers/icslp_1998/i98_0589.pdf
    Section 3.2:
    "We observed that human cuers often begin to form
    a cue before producing the corresponding audible sound. To ap-
    proximate this effect we adjusted the start times of the cues to
    begin 100 ms before the boundary determined by the cue recognizer."

    """

    def __init__(self, a1, a3):
        """Instantiate a moment predictor.

        :param a1: (float) Start time of the first phoneme of the key
        :param a3: (float) End time of the second phoneme of the key

        """
        super(KeysTransitionPredictorDuchnowski, self).__init__(a1, a3)
        self._description = "The moments [D1,D2] and [M1,M2] are predicted 100ms" \
                            "before the moment A1. It means there's no transition time."

    # -----------------------------------------------------------------------

    def predict_shape(self, **kwargs):
        """Predict [D1, D2] the moments when fingers are changing.

        """
        # Predict D1 - when the shape started to change
        d1 = max(0., self._a1 - 0.100)

        # Predict D2 - when the shape is well-formed
        d2 = max(0., self._a1 - 0.100)

        return d1, d2

    # -----------------------------------------------------------------------

    def predict_position(self, **kwargs):
        """Predict [M1, M2] the moments when the hand is moving.

        """
        # Predict M1 - when movement started
        m1 = max(0., self._a1 - 0.100)

        # Predict M2 - when movement ended
        m2 = max(0., self._a1 - 0.100)

        return m1, m2

# ---------------------------------------------------------------------------


class KeysTransitionPredictorAttina(BaseKeysTransitionPredictor):
    """Predict moments with a baseline estimator.

    Page 117 and page 136:
    https://tel.archives-ouvertes.fr/file/index/docid/384080/filename/these_attina.pdf

    """

    def __init__(self, a1, a3):
        """Instantiate a moment predictor.

        :param a1: (float) Start time of the first phoneme of the key
        :param a3: (float) End time of the second phoneme of the key

        """
        super(KeysTransitionPredictorAttina, self).__init__(a1, a3)
        self._description = "The moments [D1,D2] and [M1,M2] are estimated"\
                            "proportionally to the duration [A1,A3]. For example," \
                            "M2 is 10% of [A1,A3] later than A1 in a CV sequence."

    # -----------------------------------------------------------------------

    def predict_position(self, is_nil_shape=False, **kwargs):
        """Predict [M1, M2] the moments when the hand is moving.

        Predict M1 - when leaving the current position
        Predict M2 - when arrived to the expected position

        To predict M2:
            - CV key; both page 136 and page 117, M2 is 10% later than A1.
            - -V key: page 117, M2 is 21% later than A1.

        To predict M1:
            - CV key: page 136 M1 is 62-65% of A1A3, but page 117, M1 is 60% of A1A3
            - -V key: page 117, M1 is 46% of A1A3

        """
        a3a1 = self._a3 - self._a1

        # Predict M2 - when movement ended
        if is_nil_shape is False:
            # both page 136 and page 117: in a CV key, M2 is 10% later than A1
            a1m2 = a3a1 * 0.10
        else:
            # page 117: in a -V key, M2 is 21% later than A1
            a1m2 = a3a1 * 0.21
        m2 = max(0., self._a1 + a1m2)

        # Predict M1 - when movement started
        if is_nil_shape is False:
            # page 136: in a CV key, M1 is 62-65% of A1A3
            # but page 117: in a CV key, M1 is 60% of A1A3
            m2m1 = a3a1 * 0.62
        else:
            # page 117: in a -V key, M1 is 46% of A1A3
            m2m1 = a3a1 * 0.46
        m1 = max(0., self._a1 - m2m1)

        return m1, m2

    # -----------------------------------------------------------------------

    def predict_shape(self, **kwargs):
        """Predict [D1, D2] the moments when fingers are changing.

        D1 and D2 are predicted from the key duration only.
        Following the model proposed in the PhD (Attina, 2005), page 136:
        - estimates A1A3: the duration of the key;
        - D2 position is just before A1 (1%)
        - estimates D1D2 interval: 55% of A3A1 => gives D1 position related to D2

        """
        a3a1 = self._a3 - self._a1

        # Predict D2 - when the shape is well-formed
        # page 136: in a CV key, D2 is 1% before than A1
        a1d2 = a3a1 * 0.01
        d2 = max(0., self._a1 - a1d2)

        # Predict D1 - when the shape started to change
        d2d1 = a3a1 * 0.55
        d1 = max(0., self._a1 - d2d1)

        return d1, d2

# ---------------------------------------------------------------------------


class KeysTransitionPredictorRules(BaseKeysTransitionPredictor):
    """Predict moments with a rule-based estimator.

    Improvement of the system proposed by Attina (2005).
    We modified [D1,D2] with the following rule:
        - the transition of the shape must be completed before the transition
          of the position.
    We modified [M1,M2] with the following rule:
        - the transition to neutral (i.e. a long silence) is not proportional
        to [A1,A3]. We fixed an empirical value.
        - the first position transition after a neutral one is faster (rank)

    A critical delay to display the hand cue is +165ms. The max delay is
    +100ms. A non-significant delay is +33ms. See:
      > Maroula S. Bratakos, Paul Duchnowski and Louis D. Braida
      > Toward the Automatic Generation of Cued Speech
      > Cued Speech Journal VI 1998 p1-37
      > (c) 1998 National Cued Speech Association, Inc.
    From this study, we established the following rule:
        - the transition must always be completed when the phoneme begins

    """

    def __init__(self, a1, a3):
        """Instantiate a moment predictor.

        :param a1: (float) Start time of the first phoneme of the key
        :param a3: (float) End time of the second phoneme of the key

        """
        super(KeysTransitionPredictorRules, self).__init__(a1, a3)
        self._description = "The moments [D1,D2] and [M1,M2] are estimated"\
                            "proportionally to the duration [A1,A3] with some" \
                            "exception rules. "

    # -----------------------------------------------------------------------

    def predict_position(self, rank=3, is_nil_shape=False, **kwargs):
        """Predict [M1, M2] the moments when the hand is moving.

        Predict M1 - when leaving the current position
        Predict M2 - when arrived to the expected position

        :param rank: (int) The rank of the key. 0=silence, 1=1st key after a silence, etc
        :param is_nil_shape: (bool) The shape is a neutral one (no consonant)

        """
        a3a1 = self._a3 - self._a1
        d = BaseKeysTransitionPredictor.DELAY

        if rank == 0:
            # Both the position and the shape are neutral.
            # Not in the above-mentioned reference, we fix it empirically...
            # because a neutral position means a very long silence, then
            # the Attina model clearly predicts wrong values.
            # [a1-a3] is a silence, so after speech
            m1 = min(self._a1 + (d // 5), self._a1 + (a3a1 * 0.1))
            m2 = min(m1 + d, m1 + (a3a1 * 0.5))

        elif rank == 1:
            # the 1st key after a silence. It seems that the position
            # is reach just before the phoneme starts.
            m2 = self._a1 - (d // 5)
            m2m1 = a3a1 * 1.52
            m1 = max(0., m2 - m2m1)

        else:
            if is_nil_shape is False:
                # This is a CV key
                if rank == 2:
                    # the 2nd key after a silence. It is also reaching the
                    # target earlier than the other keys.
                    m2 = self._a1
                    m2m1 = a3a1 * 0.72
                else:
                    # (Attina, 2005) page 117:
                    # in a -V key, M2 is 21% later than A1
                    a1m2 = a3a1 * 0.21
                    # (Attina, 2005) page 136: in a CV key, M2 is 62-65% of A1A3
                    # but page 117: in a CV key, M2 is 60% of A1A3
                    m2 = max(0., self._a1 + a1m2)
                    m2m1 = a3a1 * 0.62
            else:
                # This is a -V key.
                # the 1st key after a silence starts before
                if rank == 2:
                    m2 = self._a1
                    m2m1 = a3a1 * 0.72
                else:
                    # (Attina, 2005) both page 136 and page 117:
                    # in a CV key, M2 is 10% later than A1
                    a1m2 = a3a1 * 0.10
                    # (Attina, 2005) page 117: in a -V key, M2 is 46% of A1A3
                    m2 = max(0., self._a1 + a1m2)
                    m2m1 = a3a1 * 0.46

            m1 = max(0., m2 - m2m1)

        # Do not go too far away...
        m2 = min(m2, self._a3)
        m1 = min(m1, m2)

        return m1, m2

    # -----------------------------------------------------------------------

    def predict_shape(self, rank=3, is_nil_shape=False, m1=None, m2=None, **kwargs):
        """Predict [D1, D2] the moments when fingers are changing.

        :param rank: (int) The rank of the key. 0=silence, 1=1st key after a silence, etc
        :param is_nil_shape: (bool) The shape is a neutral one (no consonant)
        :param m1: (float) The M1 value
        :param m2: (float) The M2 value

        """
        a3a1 = self._a3 - self._a1
        d = BaseKeysTransitionPredictor.DELAY

        # Predict D1 - when the shape started to change
        if rank == 0:
            # Both the position and the shape are neutral.
            # [a1-a3] is a (long) silence, so after speech
            d1 = min(self._a1 + (d // 4), self._a1 + (a3a1 * 0.2))
            d2 = min(d1 + d, d1 + (a3a1 * 0.4))

        else:
            if rank == 1:
                # the 1st key after a silence. It seems that the shape
                # is formed very early before the phoneme starts.
                # d2 must be (clearly) before m2
                d2 = self._a1 - (d // 2)
                d2d1 = a3a1 * 1.02
                # d1 must be after m1
                d1 = max(0., min(m1 + (d // 5), d2 - d2d1))

            elif rank == 2:
                # the 2nd key after a silence. It is also reaching the
                # target earlier than the other keys.
                d2 = self._a1 - (d // 4)
                d2d1 = a3a1 * 0.72
                d1 = max(0., d2 - d2d1)

            else:
                # (Attina 2005) page 136:
                # in a CV key, D2 is 1% before A1
                if is_nil_shape is False:
                    # (Attina 2005) page 136:
                    # In the tested version (alpc, august 2022): a1d2 = a3a1 * 0.01
                    # Transition was finishing too late... Adjusted is:
                    a1d2 = a3a1 * 0.10
                    d2d1 = a3a1 * 0.55
                else:
                    # (Attina 2005) page 136:
                    # In the tested version (alpc, august 2022): a1d2 = a3a1 * 0.11
                    # Transition was finishing too late... Adjusted is:
                    a1d2 = a3a1 * 0.21
                    # but we adjust transition duration because d1 must be after m1
                    d2d1 = a3a1 * 0.39
                d2 = max(0., self._a1 - a1d2)
                d1 = max(0., d2 - d2d1)

            # But... reduce d2 when [d1, d2] is very long...
            d2d1 = d2 - d1
            if d2d1 > BaseKeysTransitionPredictor.DELAY:
                reduce = a3a1 * 0.05
                d2 = max(d1, d2 - reduce)

        # Do not go too far away...
        d2 = min(d2, self._a3)
        # Do not end shape transition after position transition
        if m2 is not None:
            d2 = min(d2, m2)
        d1 = min(d1, d2)
        # Do not start shape transition before position transition
        if m1 is not None:
            d1 = max(d1, m1)
            d2 = max(d1, d2)

        return d1, d2

# ---------------------------------------------------------------------------


class sppasTransitionsPredictor(object):
    """Hand Transitions predictor for both shape and position.

    """
    # A dictionary to associate a version number and a class to instantiate.
    HAND_TRANSITIONS = dict()
    HAND_TRANSITIONS[0] = BaseKeysTransitionPredictor
    HAND_TRANSITIONS[1] = KeysTransitionPredictorDuchnowski
    HAND_TRANSITIONS[2] = KeysTransitionPredictorAttina
    HAND_TRANSITIONS[3] = KeysTransitionPredictorRules

    # The default version number used to define a prediction system.
    DEFAULT = 3

    # -----------------------------------------------------------------------

    @staticmethod
    def version_numbers():
        """Return the whole list of supported version numbers."""
        return list(sppasTransitionsPredictor.HAND_TRANSITIONS.keys())

    # -----------------------------------------------------------------------

    def __init__(self, version_number=DEFAULT):
        """Create a hand transitions predictor.

        :param version_number: (int)

        """
        self.__version = sppasTransitionsPredictor.DEFAULT
        self.set_version_number(version_number)
        self.__predictor = None

    # -----------------------------------------------------------------------

    def get_version_number(self):
        """Return the filename."""
        return self.__version

    # -----------------------------------------------------------------------

    def set_version_number(self, version_number):
        """Set a new version number.

        It invalidates the current values of A1 and A3.

        :param version_number: (int) One of the supported versions

        """
        authorized = self.version_numbers()
        v = int(version_number)
        if v not in authorized:
            raise Exception("Invalid version number {:d}. Expected one of: {}"
                            "".format(v, authorized))
        self.__version = v
        self.__predictor = None

    # -----------------------------------------------------------------------

    def set_a(self, a1, a3):
        """Set [A1, A3] the moments of the sounds of a key.

        A1 - when the 1st phoneme starts in the audio
        A3 - when the 2nd phoneme ends in the audio

        """
        self.__predictor = sppasTransitionsPredictor.HAND_TRANSITIONS[self.__version](a1, a3)

    # -----------------------------------------------------------------------

    def predict_m(self, **kwargs):
        """Predict [M1, M2] the moments when the hand is moving.

        Predict M1 - when leaving the current position
        Predict M2 - when arrived to the expected position

        Neutral means it's not a phoneme: the key is 0-n.
        A nil shape means it's a key of type -V.

        Possible arguments are:
         - rank: (int) The rank of the key after a silence (sil=0, 1st key=1, ...)
         - is_nil_shape: (bool) The shape is nil, i.e. there's no consonant in the syllable (N-V)

        """
        if self.__predictor is None:
            raise Exception("A1 and A3 must be known in order to predict the "
                            "position transition moments.")
        return self.__predictor.predict_position(**kwargs)

    # -----------------------------------------------------------------------

    def predict_d(self, **kwargs):
        """Predict [D1, D2] the moments when fingers are changing.

        Possible arguments are:
         - rank: (int) The rank of the key after a silence (sil=0, 1st key=1, ...)
         - is_nil_shape: (bool) The shape is nil, i.e. there's no consonant in the syllable (N-V)

        """
        if self.__predictor is None:
            raise Exception("A1 and A3 must be known in order to predict the "
                            "shape transition moments.")
        return self.__predictor.predict_shape(**kwargs)


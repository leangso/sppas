# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.whenhand.genhandtranstier.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  CS hand transitions generator. Answer the "When?" question.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

import logging

from sppas.src.config import sppasTypeError
from sppas.src.anndata import sppasTier
from sppas.src.anndata import sppasInterval
from sppas.src.anndata import sppasPoint
from sppas.src.anndata import sppasLocation
from sppas.src.anndata import sppasTag
from sppas.src.anndata import sppasLabel

from ..whatkey.keysrules import CuedSpeechCueingRules

from .transitions import sppasTransitionsPredictor

# ---------------------------------------------------------------------------


class sppasHandTransitionsGenerator(object):
    """Create the CS coding scheme from time-aligned phonemes.

    """

    def __init__(self, cue_rules, generator_version=sppasTransitionsPredictor.DEFAULT):
        """Instantiate a CS generator.

        :param cue_rules: (CuedSpeechKeys) Rules to convert phonemes => keys

        """
        if isinstance(cue_rules, CuedSpeechCueingRules) is False:
            raise sppasTypeError("cue_rules", "CuedSpeechCueingRules")

        # Rule-based system to convert a sequence of phonemes into keys
        self.__cued = cue_rules

        # Hand transition prediction system: predict [D1, D2] and [M1, M2]
        self.__transitions = sppasTransitionsPredictor()

        # Version of the generator
        self.set_generator_version(generator_version)

    # -----------------------------------------------------------------------

    def get_generator_version(self):
        """Return the version number of the generation system."""
        return self.__transitions.get_version_number()

    # -----------------------------------------------------------------------

    @staticmethod
    def get_generator_versions():
        """Return the list of version numbers of the generation system."""
        return sppasTransitionsPredictor.version_numbers()

    # -----------------------------------------------------------------------

    def set_generator_version(self, value):
        """Set the generation version.

        - 0: no time estimation.
        - 1: system based on Paul Duchnowski et al. (1998): shift -100ms
        - 2: system based on Virginie Attina (2005) synchronization model
        - 3: (2) + empirical rules from B. Bigi & Datha
        - 4: not implemented -> (3) with rules deduced from CLeLfPC statistics

        """
        value = int(value)
        self.__transitions = sppasTransitionsPredictor(value)

    # -----------------------------------------------------------------------

    def keys_to_hand_positions(self, tier_keys):
        """Create a tier with the periods of hand movements (vowels).

        Each annotation of the returned tier CS-HandShapes contains
        the period [M1, M2] when the hand is moving. In that case, the
        annotations contain two labels: the start hand position and the
        target one.

        :param tier_keys: (sppasTier)
        :return: (sppasTier) Tier with name 'CS-HandPositions'

        """
        pos_tier = sppasTier("CS-HandPositions")
        pos_tier.set_meta('cued_speech_position_of_tier', tier_keys.get_name())

        # No transition has to be estimated for the first key.
        ann = tier_keys[0]
        labels = ann.get_labels()
        prev_shp = labels[0]
        prev_pos = labels[1]
        interval = ann.get_location()
        prev_m2_point = interval.get_lowest_localization()
        a3 = interval.get_highest_localization().get_midpoint()
        key_rank = 1

        for ii in range(1, len(tier_keys)):
            ann = tier_keys[ii]
            labels = ann.get_labels()
            if len(labels) != 2:
                raise ValueError("A LPC-key should contain 2 labels (C-V)."
                                 "Got {:d} instead.".format(len(labels)))

            # The hand position of this annotation = the vowel = 2nd label
            cur_shp = labels[0]
            cur_pos = labels[1]

            # A3A1 duration value
            interval = ann.get_location()
            a1 = interval.get_lowest_localization().get_midpoint()
            if a1 > a3:
                # a hole between this key and the previous one
                key_rank = 2
            a3 = interval.get_highest_localization().get_midpoint()
            self.__transitions.set_a(a1, a3)

            # The key is speech: move from previous position to current one
            # or a long silence: move from previous position to neutral one.
            is_neutral = self.position_is_neutral(cur_pos.get_best().get_content())
            neutral_consonant = self.shape_is_neutral(cur_pos.get_best().get_content())
            if is_neutral is True:
                # This is a long silence because of the neutral key position.
                key_rank = 0
            same_key = (prev_shp == cur_shp) & (prev_pos == cur_pos)
            m1, m2 = self.__transitions.predict_m(rank=key_rank,
                                                  is_nil_shape=neutral_consonant,
                                                  is_same=same_key)

            # Create the interval and add an annotation
            interval = self.__moments_to_interval(m1, m2, prev_m2_point, a1, a3)
            if interval is not None:
                pos_tier.create_annotation(sppasLocation(interval), [prev_pos, cur_pos])
            else:
                logging.error("The key {:s} can't have a hand position transition.".format(ann))

            # prepare for the next key...
            prev_pos = cur_pos
            prev_shp = cur_shp
            prev_m2_point = interval.get_end()
            key_rank += 1

        return pos_tier

    # -----------------------------------------------------------------------

    def keys_to_hand_shapes(self, tier_keys, tier_pos=None):
        """Create a tier with the periods of finger movements (shapes).

        Each annotation of the returned tier CS-HandShapes contains the
        period [D1, D2] when the fingers are changing.
        The annotations contain two labels: the start hand shape and the
        target hand shape.

        :param tier_keys: (sppasTier) A tier with the keys.
        :param tier_pos: (sppasTier) A tier with the position of each key.

        """
        if tier_pos is None:
            tier_pos = self.keys_to_hand_positions(tier_keys)
        if len(tier_pos)+1 != len(tier_keys):
            raise Exception("Expected the same nb of keys and pos. Got {:d} and {:d}"
                            "".format(len(tier_keys), len(tier_pos)))

        shapes_tier = sppasTier("CS-HandShapes")
        shapes_tier.set_meta('cued_speech_shape_of_tier', tier_keys.get_name())
        if len(tier_keys) == 0:
            return shapes_tier

        # No transition has to be estimated for the first key.
        ann = tier_keys[0]
        labels = ann.get_labels()
        prev_shape = labels[0]
        interval = ann.get_location()
        prev_d2_point = interval.get_lowest_localization()
        a3 = interval.get_highest_localization().get_midpoint()
        key_rank = 1     # The rank of the key after a silence.

        for ii in range(1, len(tier_keys)):
            labels = tier_keys[ii].get_labels()
            if len(labels) != 2:
                raise ValueError("A key should contain 2 labels (C-V)."
                                 "Got {:d} instead.".format(len(labels)))

            # The hand shape of this annotation = the consonant = 1st label
            cur_shape = labels[0]
            # Two consecutive keys with the same hand shape: no shape transition
            if cur_shape == prev_shape:
                continue

            # [A1,A3] duration value
            interval = tier_keys[ii].get_location()
            a1 = interval.get_lowest_localization().get_midpoint()
            if a1 > a3:
                # There were a hole between this key and the previous one.
                # There's a hole between keys if laughter, dummy, noise, etc.
                key_rank = 2
            a3 = interval.get_highest_localization().get_midpoint()
            self.__transitions.set_a(a1, a3)

            pos_interval = tier_pos[ii-1].get_location()
            m1 = pos_interval.get_lowest_localization().get_midpoint()
            m2 = pos_interval.get_highest_localization().get_midpoint()

            # Move fingers from the previous shape to the current one
            is_neutral = self.shape_is_neutral(cur_shape.get_best().get_content())
            if is_neutral is True:
                # This is a long silence because of the neutral key position.
                key_rank = 0
            d1, d2 = self.__transitions.predict_d(rank=key_rank, m1=m1, m2=m2)

            # Create the interval and add an annotation
            shape_interval = self.__moments_to_interval(d1, d2, prev_d2_point, a1, a3)
            if shape_interval is not None:
                shapes_tier.create_annotation(sppasLocation(shape_interval), [prev_shape, cur_shape])
            else:
                logging.error("The key {:s} can't have an hand shape transition."
                              "".format(tier_keys[ii]))

            # prepare for the next key...
            prev_shape = cur_shape
            prev_d2_point = shape_interval.get_end()
            key_rank += 1

        return shapes_tier

    # -----------------------------------------------------------------------

    def shape_is_neutral(self, s):
        return s == self.__cued.get_neutral_consonant()

    # -----------------------------------------------------------------------

    def position_is_neutral(self, p):
        return p == self.__cued.get_neutral_vowel()

    # -----------------------------------------------------------------------

    @staticmethod
    def __moments_to_interval(m1, m2, prev_m2_point, a1, a3):
        """Create a sppasInterval from 2 time values [m1, m2].

        prev_m2 is used to not allow overlaps with the previous interval.
        in that case, m1 is set to the previous m2.

        a1 and a3 are used in case of m1 changed, in order to estimate a
        new m2 value if needed.

        """
        a3a1 = a3 - a1

        # Estimate a proportional radius -- but in a range of [0.005, 0.020]
        radius = min(max(0.005, a3a1 * 0.05), 0.02)

        # Create the interval points
        m1_point = sppasPoint(m1, radius)
        m2_point = sppasPoint(m2, radius)
        # ... but the degenerated interval is forbidden
        if m1_point == m2_point:
            m1_point = sppasPoint(max(0., m1-radius), radius / 2.)
            m2_point = sppasPoint(m2+radius, radius / 2.)

        # The current m1 must not be before m2 of the previous annotation
        success = True
        if prev_m2_point >= m1_point:
            m1_point = prev_m2_point.copy()
            # if we changed m1, we must be sure m2 is still ok!
            ratio = 0.25
            radius = min(0.02, a3a1 * 0.02)
            while m2_point <= m1_point:
                if ratio >= 0.9:
                    success = False
                    break
                a1m2 = a3a1 * ratio
                m2 = a1 + a1m2
                m2_point = sppasPoint(m2, radius)
                ratio += 0.1

        if success is True:
            return sppasInterval(m1_point, m2_point)
        return None


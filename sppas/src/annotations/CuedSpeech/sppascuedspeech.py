# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.sppascuedspeech.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  SPPAS integration of the Cued Speech automatic annotation.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

import os
import logging

from sppas.src.config import cfg
from sppas.src.config import annots
from sppas.src.config import sppasEnableFeatureError

from sppas.src.anndata import sppasTrsRW
from sppas.src.anndata import sppasTranscription
from sppas.src.anndata import sppasMedia

from ..baseannot import sppasBaseAnnotation
from ..searchtier import sppasFindTier
from ..annotationsexc import NoInputError
from ..annotationsexc import AnnotationOptionError
from ..annotationsexc import EmptyOutputError
from ..autils import SppasFiles

from .whatkey.phonestokeys import CuedSpeechKeys
from .whatkey.genkeystier import sppasKeysGenerator
from .whenhand.genhandtranstier import sppasHandTransitionsGenerator
from .wherecue.gencoordstier import sppasHandCoordsGenerator

if cfg.feature_installed("video"):
    from sppas.src.annotations.CuedSpeech.videotagger.videocued import CuedSpeechVideoTagger
else:
    class CuedSpeechVideoTagger(object):
        def __init__(self, *args, **kwargs):
            raise sppasEnableFeatureError("video")

# ---------------------------------------------------------------------------


class sppasCuedSpeech(sppasBaseAnnotation):
    """SPPAS integration of the automatic Cued Speech key-code generation.

    """

    def __init__(self, log=None):
        """Create a new instance.

        Log is used for a better communication of the annotation process and its
        results. If None, logs are redirected to the default logging system.

        :param log: (sppasLog) Human-readable logs.

        """
        super(sppasCuedSpeech, self).__init__("cuedspeech.json", log)
        self.__lang = "und"

        # A rule-based system to convert a sequence of phonemes into keys
        self.__cued = CuedSpeechKeys()

        # Tiers generator of keys.
        # It answers the "What?" question.
        self.__gencue = sppasKeysGenerator(cue_rules=self.__cued)

        # Tiers generator of hand shapes and hand positions.
        # It answers the "When?" question.
        self.__genhand = sppasHandTransitionsGenerator(cue_rules=self.__cued,
                                                       generator_version=self._options["handtrans"])

        # Tiers generator of vowels & hand coordinates.
        # It answers the "Where?" question. Requires sights.
        self.__gencoords = sppasHandCoordsGenerator(cue_rules=self.__cued)

        # Video tagger: instantiated when running if video tag option is enabled.
        self.__tagger = None

    # -----------------------------------------------------------------------

    def load_resources(self, config_filename, lang="und", **kwargs):
        """Fix the keys from a configuration file.

        :param config_filename: Name of the configuration file with the keys
        :param lang: (str) Iso639-3 of the language or "und" if unknown.

        """
        self.__lang = lang
        if lang != "und":
            self.__cued.load(config_filename)

        self.__gencue = sppasKeysGenerator(cue_rules=self.__cued)
        self.__genhand = sppasHandTransitionsGenerator(cue_rules=self.__cued,
                                                       generator_version=self._options["handtrans"])
        self.__gencoords = sppasHandCoordsGenerator(cue_rules=self.__cued)
        if len(self._options['handset']) > 0:
            # A set of hand was fixed before the
            self.set_handset(self._options['handset'])

    # -----------------------------------------------------------------------
    # Methods to fix options
    # -----------------------------------------------------------------------

    def fix_options(self, options):
        """Fix all options.

        Available options are:

            - createvideo

        :param options: (sppasOption)

        """
        for opt in options:

            key = opt.get_key()
            if "createvideo" == key:
                self.set_create_video(opt.get_value())

            elif "pattern" in key:
                self._options[key] = opt.get_value()

            elif "handtrans" == key:
                self.set_when_handtrans_version(opt.get_value())

            elif "handset" == key:
                self.set_handset(opt.get_value())
            else:
                raise AnnotationOptionError(key)

    # -----------------------------------------------------------------------

    def set_create_video(self, create=True):
        """Fix the createvideo option.

        :param create: (bool)

        """
        self._options['createvideo'] = create

    # -----------------------------------------------------------------------

    def set_handset(self, set_name):
        """Set the name of hand pictures and load them.

        :param set_name: (str) Name of the set of hand pictures

        """
        try:
            self._options['handset'] = set_name
        except IOError as e:
            self.logfile.print_message(str(e), status=annots.error)
            self._options['handset'] = ""

    # -----------------------------------------------------------------------

    def set_when_handtrans_version(self, version=3):
        """Fix the version of the hand transition generator.

        :param version: (int)

        """
        all_versions = self.__genhand.get_generator_versions()
        version = int(version)
        if version not in all_versions:
            msg = "Invalid version number '{}'. Expected one of {}"\
                  "".format(version, all_versions)
            self.logfile.print_message(msg, status=annots.error)
            version = 3

        self.__genhand.set_generator_version(version)
        self._options['handtrans'] = version

    # -----------------------------------------------------------------------
    # CS-Syllabification of time-aligned phonemes stored into a tier
    # -----------------------------------------------------------------------

    def convert(self, phonemes):
        """Syllabify labels of a time-aligned phones tier.

        :param phonemes: (sppasTier) time-aligned phonemes tier
        :returns: (sppasTier)

        """
        # What? Create the tiers with the CS keys from the phonemes
        cs_segments = self.__gencue.phons_to_segments(phonemes)
        cs_keys, cs_class = self.__gencue.segments_to_keys(cs_segments, phonemes.get_first_point(), phonemes.get_last_point())

        # When? Predict hand shapes and hand positions
        cs_pos = self.__genhand.keys_to_hand_positions(cs_keys)
        cs_shapes = self.__genhand.keys_to_hand_shapes(cs_keys, cs_pos)

        return cs_segments, cs_keys, cs_class, cs_shapes, cs_pos

    # -----------------------------------------------------------------------

    def make_video(self, video_file, trs, output):
        """Create a video with the tagged keys.

        :param video_file: (str) Filename of the video
        :param trs: (sppasTranscription) All required tiers to draw in a video

        """
        if cfg.feature_installed("video") is True:
            self.logfile.print_message("Create the tagged video", status=annots.info)
            self.__tagger.load(video_file)
            self.__tagger.tag_with_keys(trs, output)
            self.__tagger.close()
        else:
            self.logfile.print_message(
                "To tag a video, the video support feature must be enabled."
                "", status=annots.error)

    # -----------------------------------------------------------------------

    def get_inputs(self, input_files):
        """Return the media and the annotated filenames.

        :param input_files: (list)
        :raise: NoInputError
        :return: (str, str) Names of the 3 expected files

        """
        ext = self.get_input_extensions()
        media_ext = [e.lower() for e in ext[1]]
        phons_ext = [e.lower() for e in ext[0]]
        sights_ext = [e.lower() for e in ext[2]]
        media = None
        annot_phons = None
        annot_sights = None
        pphones = self._options["inputpattern1"]
        psights = self._options["inputpattern3"]

        for filename in input_files:
            fn, fe = os.path.splitext(filename)
            if media is None and fe in media_ext:
                media = filename
            elif annot_phons is None and fe.lower() in phons_ext and fn.endswith(pphones):
                annot_phons = filename
            elif annot_sights is None and fe.lower() in sights_ext and fn.endswith(psights):
                annot_sights = filename

        if annot_phons is None:
            logging.error("The annotated file with time-aligned phonemes was not found.")
            raise NoInputError

        return media, annot_phons, annot_sights

    # -----------------------------------------------------------------------
    # Apply the annotation on one given file
    # -----------------------------------------------------------------------

    def run(self, input_files, output=None):
        """Run the automatic annotation process on an input.

        :param input_files: (list of str) time-aligned phonemes, and optionally video, csv files
        :param output: (str) the output name
        :returns: (sppasTranscription)

        """
        file_video, file_phons, file_sights = self.get_inputs(input_files)

        # Get the tier from which we'll generate LPC keys
        parser = sppasTrsRW(file_phons)
        trs_input = parser.read()
        tier_input = sppasFindTier.aligned_phones(trs_input)

        # Create the transcription result
        trs_output = sppasTranscription(self.name)
        trs_output.set_meta('annotation_result_of', file_phons)
        trs_output.set_meta('language_iso', "iso639-3")
        trs_output.set_meta('language_name_0', "Undetermined")
        if len(self.__lang) == 3:
            trs_output.set_meta('language_code_0', self.__lang)
            trs_output.set_meta('language_url_0', "https://iso639-3.sil.org/code/" + self.__lang)
        else:
            trs_output.set_meta('language_code_0', "und")
            trs_output.set_meta('language_url_0', "https://iso639-3.sil.org/code/und")

        # Predict what and when cueing the given phonemes
        tier_cs, tier_key, tier_class, tier_shapes_transitions, tier_pos_transitions = self.convert(tier_input)
        trs_output.append(tier_cs)
        trs_output.append(tier_key)
        trs_output.append(tier_class)
        trs_output.append(tier_shapes_transitions)
        trs_output.append(tier_pos_transitions)

        # Add sppasMedia into the output transcription
        media = None
        if file_video is not None:
            extm = os.path.splitext(file_video.lower())[1]
            media = sppasMedia(os.path.abspath(file_video), mime_type="video/" + extm)
            trs_output.add_media(media)

        # Predict where are the vowels relatively to the given sights
        if file_sights is not None:
            trs_coords = sppasTranscription(self.name)
            if media is not None:
                trs_coords.add_media(media)
            tier_vowels = self.__gencoords.keys_to_vowels_coords(file_sights)
            trs_coords.append(tier_vowels)
            tier_pos_probas, tier_shapes_probas = self.__gencoords.keys_to_target_probas(tier_shapes_transitions, tier_pos_transitions, tier_vowels)
            tier_pos_coords = self.__gencoords.hands_to_target_coords(tier_pos_probas, tier_vowels)
            tier_angles = self.__gencoords.hands_to_angles(tier_pos_probas, tier_vowels)
            trs_coords.append(tier_shapes_probas)
            trs_coords.append(tier_pos_probas)
            trs_coords.append(tier_pos_coords)
            trs_coords.append(tier_angles)
            trs_coords.append(tier_input)

            # Create a video with the keys
            if self._options['createvideo']:
                # Video tagger of cued keys
                self.__tagger = CuedSpeechVideoTagger(cue_rules=self.__cued)
                self.__tagger.load_hands(self._options['handset'])

                do_vid = False
                if len(input_files) > 2:
                    if file_video is not None and file_sights is not None:
                        do_vid = True
                        self.make_video(file_video, trs_coords, output)
                if do_vid is False:
                    self.logfile.print_message(
                        "The option to tag the video was enabled but no video/csv "
                        "was found corresponding to the annotated file {:s}"
                        "".format(input_files[0]), status=-1)

        # Add sppasMedia into the output tiers
        if media is not None:
            for tier in trs_output:
                tier.set_media(media)

        # Save in a file
        if output is not None:
            if len(trs_output) > 0:
                output_file = self.fix_out_file_ext(output)
                parser = sppasTrsRW(output_file)
                parser.write(trs_output)
                return [output_file]
            else:
                raise EmptyOutputError

        return trs_output

    # ----------------------------------------------------------------------

    def get_output_pattern(self):
        """Pattern this annotation uses in an output filename."""
        return self._options.get("outputpattern", "-cuedspeech")

    def get_input_patterns(self):
        """Pattern this annotation expects for its input filename."""
        return [
            self._options.get("inputpattern1", "-palign"),
            self._options.get("inputpattern2", ""),          # video
            self._options.get("inputpattern3", "-sights"),   # csv/xra
            ]

    # -----------------------------------------------------------------------

    @staticmethod
    def get_input_extensions():
        """Extensions that the annotation expects for its input filename."""
        return [
            SppasFiles.get_informat_extensions("ANNOT_ANNOT"),
            SppasFiles.get_informat_extensions("VIDEO"),
            [".xra", ".csv"]
            ]

# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.keysrules.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Parse a file of Cued Speech to manage a set of cueing rules.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

import os

from sppas.src.config import symbols
from sppas.src.config import separators
from sppas.src.config import error
from sppas.src.config import sppasUnicode
from sppas.src.config import sppasIOError

# ----------------------------------------------------------------------------


class sppasCuedRulesValueError(ValueError):
    """:ERROR 1320:.

    A Cued Speech syllable must be a sequence of C-V phonemes. Got {} instead.

    """

    def __init__(self, value):
        self._status = 1322
        self.parameter = error(self._status) + \
                         (error(self._status, "annotations")).format(value)

    def __str__(self):
        return repr(self.parameter)

    def get_status(self):
        return self._status

    status = property(get_status, None)

# ----------------------------------------------------------------------------


class sppasCuedRulesMinValueError(ValueError):
    """:ERROR 1321:.

    A Cued Speech syllable should contain at least one phoneme. Got {} instead.

    """

    def __init__(self, value):
        self._status = 1321
        self.parameter = error(self._status) + \
                         (error(self._status, "annotations")).format(value)

    def __str__(self):
        return repr(self.parameter)

    def get_status(self):
        return self._status

    status = property(get_status, None)

# ----------------------------------------------------------------------------


class sppasCuedRulesMaxValueError(ValueError):
    """:ERROR 1322:.

    A Cued Speech syllable should contain a maximum of two phoneme. Got {} instead.

    """

    def __init__(self, value):
        self._status = 1322
        self.parameter = error(self._status) + \
                         (error(self._status, "annotations")).format(value)

    def __str__(self):
        return repr(self.parameter)

    def get_status(self):
        return self._status

    status = property(get_status, None)

# ----------------------------------------------------------------------------


class CuedSpeechCueingRules(object):
    """Rules-based system to predict Cued Speech keys.

    Format of the rules:

    PHONCLASS i V
    PHONCLASS e V
    ...
    PHONCLASS p C
    PHONCLASS t C
    ...
    PHONKEY i m
    PHONKEY e t
    PHONKEY p 1
    PHONKEY t 5
    ...

    PHONCLASS cnil N
    PHONCLASS vnil N
    PHONKEY cnil 5
    PHONKEY vnil s

    """

    # A class for a non-phoneme sound or a pause or a silence
    PHONCLASS = "N"

    # -----------------------------------------------------------------------

    def __init__(self, filename=None):
        """Create a new instance.

        :param filename: (str) Name of the file with the rules.

        """
        # key = phoneme or symbol;
        # value = tuple(class, key code)
        self.__phon = dict()

        if filename is not None:
            self.load(filename)
        else:
            self.reset()

    # ------------------------------------------------------------------------

    def reset(self):
        """Reset the set of rules."""
        self.__phon = dict()
        for phone in symbols.all:
            self.__phon[phone] = (CuedSpeechCueingRules.PHONCLASS, None)

        # neutral position and shape is a system-dependent coding scheme
        # (but not language-dependent)
        self.__phon["vnone"] = (CuedSpeechCueingRules.PHONCLASS, "n")
        self.__phon["cnone"] = (CuedSpeechCueingRules.PHONCLASS, "0")

        # a missing consonant or vowel is a language-dependent key code
        # but the class has to be added.
        self.__phon["vnil"] = (CuedSpeechCueingRules.PHONCLASS, None)
        self.__phon["cnil"] = (CuedSpeechCueingRules.PHONCLASS, None)

    # ------------------------------------------------------------------------

    def load(self, filename):
        """Load the rules from a file.

        :param filename: (str) Name of the file with the rules.
        :returns: (bool) Rules were appended or not

        """
        self.reset()
        if os.path.exists(filename):
            with open(filename, "r") as f:
                lines = f.readlines()
                f.close()
        else:
            raise sppasIOError("Unknown Cued Speech rules file {}".format(filename))

        added = False
        for line_nb, line in enumerate(lines, 1):
            sp = sppasUnicode(line)
            line = sp.to_strip()
            wds = line.split()
            if len(wds) == 3:
                # The string representing the phoneme is the key of the self.__phon dict.
                p = wds[1]
                if p not in self.__phon:
                    # create a new entry in the dictionary of phonemes
                    self.__phon[p] = (None, None)

                # fill-in the value of the phoneme entry in the dict
                tup = self.__phon[p]
                if wds[0] == "PHONCLASS":
                    # PHONCLASS allows to fill-in the phoneme class.
                    if wds[1] not in ("cnone", "vnone"):
                        self.__phon[wds[1]] = (wds[2], tup[1])
                        added = True

                elif wds[0] == "PHONKEY":
                    # PHONKEY allows to fill-in the phoneme key
                    if wds[1] not in ("cnone", "vnone"):
                        self.__phon[wds[1]] = (tup[0], wds[2])
                        added = True

        return added

    # ------------------------------------------------------------------------
    # Getters: about the classes
    # ------------------------------------------------------------------------

    def get_class(self, phoneme):
        """Return the class identifier of the phoneme.

        If the phoneme is unknown, the neutral class is returned.

        :param phoneme: (str) A phoneme
        :returns: class of the phoneme or neutral class

        """
        tup = self.__phon.get(phoneme, None)
        if tup is None:
            return CuedSpeechCueingRules.PHONCLASS
        return tup[0]

    # ------------------------------------------------------------------------
    # Getters about the keys: shape and position
    # ------------------------------------------------------------------------

    def get_vowels_codes(self):
        """Return the list of key codes of all the positions (vowels)."""
        vowels_codes = list()
        for phon in self.__phon:
            phonkey = self.get_key(phon)
            if self.get_class(phon) == "V" and phonkey is not None:
                vowels_codes.append(phonkey)

        # tricks: reverse is true in order to have 'b' of the French LfPC
        # at the last position because it does not exist in English CS.
        # Then, English and French will have the vowels sorted the same...
        return [self.get_neutral_vowel()] + \
               sorted(list(set(vowels_codes)), reverse=True)

    # ------------------------------------------------------------------------

    def get_consonants_codes(self):
        """Return the list of key codes of all the shapes (consonants)."""
        vowels_codes = list()
        for phon in self.__phon:
            phonkey = self.get_key(phon)
            if self.get_class(phon) == "C" and phonkey is not None:
                vowels_codes.append(phonkey)

        return [self.get_neutral_consonant()] + sorted(list(set(vowels_codes)))

    # ------------------------------------------------------------------------

    def get_key(self, phoneme):
        """Return the key identifier of the phoneme.

        None is returned if the phoneme is unknown or if it is a break.
        If the phoneme is known but no key was defined for this phoneme,
        the "nil" key is returned.

        :param phoneme: (str) A phoneme
        :returns: key of the phoneme or None

        """
        tup = self.__phon.get(phoneme, None)
        if tup is None:
            return None

        # The key like defined in the config file
        key = tup[1]

        # If no key was defined for this phoneme, use nil.
        if key is None:
            if tup[0] == "V":
                key = self.__phon["vnil"][1]
            elif tup[0] == "C":
                key = self.__phon["cnil"][1]

        return key

    # ------------------------------------------------------------------------

    def get_nil_consonant(self):
        """Return the key code for a missing consonant."""
        return self.__phon['cnil'][1]

    # ------------------------------------------------------------------------

    def get_nil_vowel(self):
        """Return the key code for a missing vowel."""
        return self.__phon['vnil'][1]

    # ------------------------------------------------------------------------

    def get_neutral_vowel(self):
        """Return the key code of the neutral position (vowel)."""
        return self.__phon['vnone'][1]

    # ------------------------------------------------------------------------

    def get_neutral_consonant(self):
        """Return the key code of the neutral shape (consonant)."""
        return self.__phon['cnone'][1]

    # ------------------------------------------------------------------------

    def syll_to_key(self, syll):
        """Return the key codes matching the given syllable.

        The given entry can be either of the form CV or C or V.

        :param syll: (str) A syllable like "p-a", or "p" or "a".
        :returns: tuple(str, str) representing a CV key code
        :raises: ValueError

        """
        phons = syll.split(separators.phonemes)
        if len(phons) == 0:
            raise sppasCuedRulesMinValueError(syll)
        if len(phons) > 2:
            raise sppasCuedRulesMaxValueError(syll)

        if len(phons) == 1:
            if self.get_class(phons[0]) == "V":
                phons.insert(0, "cnil")
            elif self.get_class(phons[0]) == "C":
                phons.append("vnil")
            else:
                phons.insert(0, "unknown")

        if self.get_class(phons[0]) not in ("N", "C") or self.get_class(phons[1]) not in ("N", "V"):
            raise sppasCuedRulesValueError(syll)

        return self.get_key(phons[0]), self.get_key(phons[1])

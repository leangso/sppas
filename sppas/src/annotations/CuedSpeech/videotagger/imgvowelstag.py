# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.videotagger.imgvowelstag.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Tag an image with the vowels positions.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

import numpy
import cv2

from sppas.src.config import sppasTypeError
from sppas.src.anndata.ann.annlabel.tagtypes import sppasFuzzyPoint
from sppas.src.imgdata import sppasImage
from sppas.src.annotations.CuedSpeech.whatkey.phonestokeys import CuedSpeechKeys

# ---------------------------------------------------------------------------


class sppasImageVowelsTagger(object):
    """Tag the image with vowels.

    Six colors are fixed to draw the vowels positions:
        - Key of vowels "n" is grey   (128, 128, 128)
        - Key of vowels "t" is green  (0, 175, 0)
        - Key of vowels "s" is blue   (0, 128, 255)
        - Key of vowels "m" is red    (205, 0, 0)
        - Key of vowels "c" is orange (255, 128, 0)
        - Key of vowels "b" is pink   (200, 0, 100)

    """

    def __init__(self, cue_rules=CuedSpeechKeys()):
        """Create a new instance.

        :param cue_rules: (CuedSpeechKeys) Rules and codes for vowel positions and hand shapes

        """
        # Rule-based system to convert a sequence of phonemes into keys
        if isinstance(cue_rules, CuedSpeechKeys) is False:
            raise sppasTypeError(type(cue_rules), "CuedSpeechKeys")
        self.__cued = cue_rules

        # The colors for the vowels
        self.__colors = dict()
        self.__colors['b'] = [128,   0,   0, 205, 255, 200]
        self.__colors['g'] = [128, 175, 128,   0, 128,   0]
        self.__colors['r'] = [128,   0, 255,   0,   0, 100]

        # Add the name of the vowel inside the circle
        self.__vname = False
        self.__thickness = 2

    # -----------------------------------------------------------------------
    # Getters and setters
    # -----------------------------------------------------------------------

    def enable_vowel_name(self, value=True):
        """Set the option to tag the name of each vowel in its circle.

        :param value: (bool)

        """
        value = bool(value)
        self.__vname = value

    # -----------------------------------------------------------------------

    def set_thickness(self, value=1):
        """Set the thickness of the pen to draw the text into vowel circles.

        :param value: (int)

        """
        value = int(value)
        if value < 1:
            value = 1
        self.__thickness = value

    # -----------------------------------------------------------------------

    def get_vowel_text(self, idx):
        """Return the text code for the given vowel index.

        :param idx: (int) Vowel rank
        :return: (str)

        """
        idx = int(idx)
        # Vowels key codes
        vc = self.__cued.get_vowels_codes()
        # Code of the i-th vowel
        if 0 <= idx < len(vc):
            return vc[idx]
        return ""

    # -----------------------------------------------------------------------

    def get_vowel_color(self, idx):
        """Return the color for the given vowel index.

        :param idx: (int) Vowel rank
        :return: (r, g, b)

        """
        rgb = (128, 128, 128)
        if idx < len(self.__colors['r']):
            r = self.__colors['r'][idx]
            g = self.__colors['g'][idx]
            b = self.__colors['b'][idx]
            rgb = (r, g, b)

        return rgb

    # -----------------------------------------------------------------------
    # Image tagging
    # -----------------------------------------------------------------------

    def slap_on(self, image, fpoints) -> None:
        """Tag the given vowels to the image.

        :param image: (sppasImage or numpy.ndarray)
        :param fpoints: (list of sppasFuzzyPoint) The coordinates of vowels
        :raise: TypeError

        """
        # Set the image to be tagged
        if isinstance(image, sppasImage) is True:
            img = image.copy()
        elif isinstance(image, numpy.ndarray) is True:
            img = sppasImage(input_aray=image)
        else:
            raise sppasTypeError(type(image), "sppasImage/numpy.ndarray")

        # Draw a circle for each given vowel
        for i, c in enumerate(fpoints):
            # Get the coordinates of the vowel: (x,y) and radius.
            if c is None or isinstance(c, sppasFuzzyPoint) is False:
                continue
            x, y = c.get_midpoint()
            radius = max(5, c.get_radius())

            # Fix the color of the circle of the i-th vowel
            rgb = self.get_vowel_color(i)

            # Draw the circle -- gradient color
            for cr in range(1, radius, 2):
                r = min(255, rgb[0] + (cr*3))
                g = min(255, rgb[1] + (cr*3))
                b = min(255, rgb[2] + (cr*3))
                cv2.circle(img, (x, y), cr, (r, g, b), 2)

        img = sppasImage(input_array=img)
        for i, c in enumerate(fpoints):
            # Get the coordinates of the vowel: (x,y) and radius.
            if c is None or isinstance(c, sppasFuzzyPoint) is False:
                continue
            x, y = c.get_midpoint()
            radius = max(5, c.get_radius())

            # Draw the vowel name in the middle of the circle
            if self.__vname is True:
                text = self.get_vowel_text(i)
                if x >= 0 and y >= 0 and len(text) > 0:
                    img.put_text((x - (radius // 2), y + (radius // 2)), (250, 250, 250), self.__thickness, text)

        return img

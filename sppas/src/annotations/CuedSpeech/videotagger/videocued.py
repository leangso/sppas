# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.videocued.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Tag a video with the Cued Speech keys.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

import logging
import traceback

from sppas.src.config import sppasError
from sppas.src.config import sppasTypeError
from sppas.src.anndata import sppasTier
from sppas.src.anndata import sppasPoint
from sppas.src.anndata import serialize_labels
from sppas.src.videodata import sppasVideoReaderBuffer
from sppas.src.videodata import sppasBufferVideoWriter
from sppas.src.annotations.CuedSpeech.whatkey.phonestokeys import CuedSpeechKeys

from .imgvowelstag import sppasImageVowelsTagger
from .imghandtag import sppasImageHandTagger

# ---------------------------------------------------------------------------


class CuedSpeechVideoTagger(object):
    """Create a video with hands tagged on the face of a video.

    """

    def __init__(self, cue_rules=CuedSpeechKeys()):
        """Create a new instance.

        :param cue_rules: (CuedSpeechKeys) Rules and codes for vowel positions and hand shapes

        """
        # A buffer of images in order to read/write a video
        self.__video_buffer = sppasVideoReaderBuffer()
        self.__video_writer = sppasBufferVideoWriter()

        # Rule-based system to convert a sequence of phonemes into keys
        if isinstance(cue_rules, CuedSpeechKeys) is False:
            raise sppasTypeError(type(cue_rules), "CuedSpeechKeys")
        self.__cued = cue_rules

        self._img_vtag = sppasImageVowelsTagger(self.__cued)
        self._img_htag = sppasImageHandTagger(self.__cued)

    # -----------------------------------------------------------------------

    def load_hands(self, set_name):
        """Load the hand pictures with given name.

        :param set_name: (str) A hand set name (handcue, ...)
        :raise: IOError

        """
        success = self._img_htag.load_hands(prefix=set_name, pattern="-hands")
        if success is False:
            raise IOError("Hand pictures with name {} not loaded.".format(set_name))

    # -----------------------------------------------------------------------

    def load(self, video):
        """Open the video.

        :param video: (str) Filename of the input video

        """
        self.close()

        # Open the video file
        self.__video_buffer.open(video)

        # Adjust the video writer
        self.__video_writer.set_fps(self.__video_buffer.get_framerate())

    # -----------------------------------------------------------------------

    def is_loaded(self):
        """Return True if the buffer is currently opened."""
        if self.__video_buffer is None:
            return False
        return self.__video_buffer.is_opened()

    # -----------------------------------------------------------------------

    def close(self):
        """Release video streams."""
        self.__video_buffer.close()
        self.__video_writer.close()

    # -----------------------------------------------------------------------

    def is_opened(self):
        """Return True if the writer is currently opened."""
        return self.__video_writer.is_opened()

    # -----------------------------------------------------------------------

    def __del__(self):
        self.close()

    # -----------------------------------------------------------------------

    def tag_with_keys(self, transcription, output):
        """Tag the video with the given keys.

        The possible tiers of the given transcription are:
            - CS-PosCoords: Coordinates of the target position
            - CS-Angles: Angle of the hand
            - CS-ShapeProbas: probabilities of the shapes of the hand
            - CS-PosProbas: probabilities of the positions of the hand
            - CS-VowelsCoords: Coordinates of the position of the vowels
            - PhonAlign: Time-aligned phonemes

        The video streams (buffer/writer) are not released... Use close().

        :param transcription: (sppasTranscription)
        :param output: Output video filename
        :return: list of created files -- expected 1

        """
        # Check the buffer
        # ----------------
        if self.is_loaded() is False:
            raise sppasError("No video in the buffer. Nothing can be tagged.")

        # Get the tiers indicating hand shape, hand position, hand angle,
        # vowels & phoneme
        # --------------------------------------------------------------------
        pos_coords_tier = transcription.find("CS-PosCoords")
        shapes_tier = transcription.find("CS-ShapeProbas")
        nb_tiers = 6
        if pos_coords_tier is None or shapes_tier is None:
            logging.warning("Missing tier with hand coords and/or shape probabilities.")
            pos_coords_tier = None
            shapes_tier = None
            nb_tiers -= 2

        angles_tier = transcription.find("CS-Angles")
        if angles_tier is None:
            logging.warning("Missing tier with hand angles.")
            nb_tiers -= 1

        positions_tier = transcription.find("CS-PosProbas")
        if positions_tier is None:
            logging.warning("Missing tier with position probabilities.")
            nb_tiers -= 1

        vowels_tier = transcription.find("CS-VowelsCoords")
        if vowels_tier is None:
            logging.warning("Missing tier with vowels coords.")
            nb_tiers -= 1

        infos_tier = transcription.find("PhonAlign")
        if infos_tier is None:
            logging.warning("Missing tier with time-aligned phonemes.")
            nb_tiers -= 1

        if nb_tiers == 0:
            raise sppasError("None of the expected tiers was found in the "
                             "transcription. Video cuing aborted.")

        # Browse the video, one buffer at a time
        # --------------------------------------
        result = list()
        i = 0             # index of the first image of each buffer
        nb = 0            # buffer number
        read_next = True  # reached the end of the video or not
        try:
            self.__video_buffer.seek_buffer(0)
            image_duration = 1. / self.__video_buffer.get_framerate()

            while read_next is True:
                logging.info(" ... buffer number {:d}".format(nb + 1))

                # Fill-in the buffer with images
                read_next = self.__video_buffer.next()
                start_time = float(i) * image_duration

                # Tag the images of the buffer with hands and/or vowel coords
                self._tag_buffer(start_time,
                                 pos_coords_tier, angles_tier,
                                 positions_tier, shapes_tier,
                                 vowels_tier, infos_tier)

                # Save the current result in a video
                if output is not None:
                    new_files = self.__video_writer.write_video(self.__video_buffer, output, "")
                    result.extend(new_files)

                nb += 1
                i += len(self.__video_buffer)

        except:
            traceback.print_exc()
            raise

        return result

    # -----------------------------------------------------------------------

    def _tag_buffer(self, start_time, pos_coords_tier, angle_tier, pos_tier, shp_tier, vowels_tier, info_tier):
        """ Browse the buffer and tag the images.

        """
        image_duration = 1. / self.__video_buffer.get_framerate()
        radius = 0.0005  # image_duration / 4.
        sb, eb = self.__video_buffer.get_buffer_range()
        pos_idx = -1
        shp_idx = -1
        vowels_idx = -1
        info_idx = -1
        ang_idx = -1

        iter_images = self.__video_buffer.__iter__()
        for i in range(self.__video_buffer.__len__()):
            # Get the current frame number in the video stream
            video_image_number = sb + i

            # Get the current image, and add alpha channel.
            img = next(iter_images)
            img = img.ialpha(254)

            # Get the middle time of the current image
            s = start_time + (i * image_duration)
            middle = s + (image_duration / 2.)
            point = sppasPoint(middle, radius)

            # Get the annotations during the image
            pos_idx = self.get_next_ann_idx(pos_coords_tier, point, pos_idx)
            shp_idx = self.get_next_ann_idx(shp_tier, point, shp_idx)
            vowels_idx = self.get_next_ann_idx(vowels_tier, point, vowels_idx)
            info_idx = self.get_next_ann_idx(info_tier, point, info_idx)
            ang_idx = self.get_next_ann_idx(angle_tier, point, ang_idx)

            shp_text = "Shape: "
            shapes = list()
            if shp_idx != -1:
                # One or two tags are expected: the shapes of the hand
                for shape_label in shp_tier[shp_idx].get_labels():
                    for tag, score in shape_label:
                        shp = tag.get_typed_content()
                        if score is None:
                            score = 1.
                        shapes.append((shp, score))
                        shp_text += shp

            pos_text = "Pos: "
            fpoints = list()
            if pos_idx != -1:
                if pos_tier is not None:
                    for pos_label in pos_tier[pos_idx].get_labels():
                        for tag, score in pos_label:
                            pos_text += tag.get_typed_content()

                # One or two tags are expected: the coords of the target
                for pos_label in pos_coords_tier[pos_idx].get_labels():
                    # Get the sppasFuzzyPoint() of the coords
                    for tag, score in pos_label:
                        point = tag.get_typed_content()
                        if score is None:
                            score = 1.
                        fpoints.append((point, score))

            angle = 330
            angles = list()
            if ang_idx != -1:
                if angle_tier is not None:
                    for ang_label in angle_tier[ang_idx].get_labels():
                        for tag, score in ang_label:
                            angles.append(tag.get_typed_content())
                    angle = angles[0]

            # Write the information
            info_text = "Info: "
            if info_idx != -1:
                info_text += str(round(info_tier[info_idx].get_lowest_localization().get_midpoint(), 3))
                info_text += " "
                info_text += serialize_labels(info_tier[info_idx].get_labels())

            vowels_positions = list()
            if shp_idx != -1 and pos_idx != -1:
                # logging.debug(" - ANGLE = {}".format(angles_tier[pos_idx].get_labels()[0].get_best().get_typed_content()))
                if vowels_idx != -1:
                    for vowel_label in vowels_tier[vowels_idx].get_labels():
                        # Get the sppasFuzzyPoint() of the vowel
                        pos = vowel_label.get_best().get_typed_content()
                        vowels_positions.append(pos)

                # ---------- Slap the hand on the face
                img = self._img_htag.slap_on(img, shapes, fpoints, angle=angle, vowels_pos=vowels_positions)

                # ----------- Draw vowels' positions -------------
                #if logging.getLogger().getEffectiveLevel() == 0:
                #    if len(vowels_positions) > 0:
                #        img = self._img_vtag.slap_on(img, vowels_positions)

                # ----------- Put text of debug information -------------
                if logging.getLogger().getEffectiveLevel() == 0:
                    # Tag the image with the information (frame/shape/position/key)
                    frame_text = "Frame: " + str(video_image_number) + " (" + str(round(middle, 3)) + str(
                        round(radius, 3)) + ")"
                    self._put_debug_texts(img, (frame_text, shp_text, pos_text, info_text))

            self.__video_buffer.set_at(img.ibgra_to_bgr(), i)

    # -----------------------------------------------------------------------

    def _put_debug_texts(self, img, texts):
        """Put debug information on the given image."""
        step = 30
        for i, text in enumerate(texts):
            img.put_text((100, (i+1)*step), (10, 10, 10), 1, text)

    # -----------------------------------------------------------------------

    @staticmethod
    def get_ann_idx(tier, timepoint):
        """Return the index corresponding to the given time point.

        :param tier: (sppasTier)
        :param timepoint: (float or sppasPoint) The time to search for the index
        :return: (int)

        """
        if tier is not None:
            if tier.is_point():
                ann_idx = tier.index(timepoint)
            else:
                # bound=2 : include begin, include end
                ann_idx = tier.mindex(timepoint, bound=2)
            if ann_idx != -1:
                return ann_idx
        return -1

    # -----------------------------------------------------------------------

    @staticmethod
    def get_next_ann_idx(tier, timepoint, ann_idx):
        """Return the next index corresponding to the given time point.

        :param tier: (sppasTier)
        :param timepoint: (float or sppasPoint) The time to search for the index
        :param ann_idx: (int) The index to start to search for the index
        :return: (int)

        """
        if tier is None:
            return -1
        if ann_idx >= len(tier):
            return -1

        new_ann_idx = -1
        if tier is not None:
            if ann_idx == -1:
                return CuedSpeechVideoTagger.get_ann_idx(tier, timepoint)

            new_ann_idx = ann_idx
            while new_ann_idx != -1:
                if tier[new_ann_idx].get_lowest_localization() <= timepoint <= tier[new_ann_idx].get_highest_localization():
                    break
                else:
                    new_ann_idx += 1
                    if new_ann_idx == len(tier):
                        new_ann_idx = -1

                    # I went too far, do not continue with the next annotations.
                    if timepoint > tier[new_ann_idx].get_highest_localization():
                        new_ann_idx = -1

        return new_ann_idx

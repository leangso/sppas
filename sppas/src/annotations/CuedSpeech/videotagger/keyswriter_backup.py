# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.keyswriter_backup.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Video buffer and writer for the Cued Speech keys.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

import logging
import collections
import os
import cv2
import numpy as np
import wx

from sppas.src.config import cfg
from sppas.src.config import paths
from sppas.src.config import annots
from sppas.src.config import sppasTrash
from sppas.src.config import sppasExtensionWriteError
from sppas.src.calculus.geometry.linear_fct import slope_intercept
from sppas.src.calculus.geometry.linear_fct import linear_fct
from sppas.src.imgdata import sppasImage
from sppas.src.imgdata import sppasCoords
from sppas.src.imgdata import sppasCoordsImageWriter
from sppas.src.videodata.video import sppasVideoWriter

video_extensions = tuple(sppasVideoWriter.FOURCC.keys())

# ----------------------------------------------------------------------------
# The solution used to cartoonize is proposed here:
# https://medium.com/nerd-for-tech/cartoonize-images-with-python-10e2a466b5fb
# ----------------------------------------------------------------------------


def icartoon(img, colorize=True):
    """Apply a cartoonizer effect.

    :param img: (sppasImage or nb.array)
    :param colorize: (bool) Colorized result.
    :return: (sppasImage)

    """
    # Apply some Median blur on the image
    img_blur = cv2.medianBlur(img, 3)
    # Apply a bilateral filter on the image
    # d – Diameter of each pixel neighborhood that is used during filtering.
    # sigmaColor – Filter sigma in the color space. A larger value of the
    #   parameter means that farther colors within the pixel neighborhood
    #   will be mixed together, resulting in larger areas of semi-equal color.
    # sigmaSpace – Filter sigma in the coordinate space. A larger value of
    #   the parameter means that farther pixels will influence each other as
    #   long as their colors are close enough.
    # img_bf = cv2.bilateralFilter(img_blur, d=5, sigmaColor=80, sigmaSpace=80)
    img_bf = cv2.bilateralFilter(img_blur, d=3, sigmaColor=50, sigmaSpace=50)

    # Use the laplace filter to detect edges.
    # For each of the Laplacian filters we use a kernel size of 5.
    # 7 => Too many edges; 3 => not enough edges
    # 'CV_8U' means that we are using 8 bit values (0–255).
    img_lp_al = cv2.Laplacian(img_bf, cv2.CV_8U, ksize=5)

    # Laplacian of the original image detected a lot of noise.
    # The image with all the filters is the sharpest, which comes in handy in
    # a bit. This is however not yet what we want. We need an image preferably
    # black and white that we can use as a mask.
    # Convert the image to greyscale (1D)
    img_lp_al_grey = cv2.cvtColor(img_lp_al, cv2.COLOR_BGR2GRAY)

    # Each variable now contains a 1-dimensional array instead of a 3-dimensional
    # array. Next we are going to use image thresholding to set values that are
    # near black to black and set values that are near white to white.
    # Manual image thresholding
    # _, tresh_al = cv2.threshold(img_lp_al_grey, 127, 255, cv2.THRESH_BINARY)
    # Remove some additional noise
    blur_al = cv2.GaussianBlur(img_lp_al_grey, (3, 3), 0)
    # Apply a threshold (Otsu)
    _, tresh_al = cv2.threshold(blur_al, 245, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # We now have a black image with white edges, we only have to invert the black
    # and the white for our mask.
    # Invert the black and the white
    inverted_Bilateral = cv2.subtract(255, tresh_al)

    if colorize is True:
        # Turn colors of the image into cartoon style
        img_color = cv2.bilateralFilter(img, d=5, sigmaColor=220, sigmaSpace=220)
        # slow: img_color = cv2.edgePreservingFilter(img, flags=2, sigma_s=50, sigma_r=0.4)
        # img_color = color_quantization_kmeans(img.copy())
        return sppasImage(input_array=cv2.bitwise_and(img_color, img_color, mask=inverted_Bilateral))

    return sppasImage(input_array=inverted_Bilateral)

# ---------------------------------------------------------------------------


class sppasKeysImageWriter(sppasCoordsImageWriter):
    """Tag&Write an image.

    In order to draw the 6 position on the face, the 6 first colors are fixed,
    so they won't be randomly picked up at the moment of writing the image.

    """

    def __init__(self):
        """Create a new sppasSightsImageWriter instance.

        Write the given image in the given filename.
        Five colors are fixed to draw the vowels positions:
            - Key of vowels "n" is grey   (128, 128, 128)
            - Key of vowels "t" is green  (0, 175, 0)
            - Key of vowels "s" is blue   (0, 128, 255)
            - Key of vowels "m" is red    (205, 0, 0)
            - Key of vowels "c" is orange (255, 128, 0)
            - Key of vowels "b" is pink   (200, 0, 100)

        """
        super(sppasKeysImageWriter, self).__init__()

        # Reset the colors to fix custom ones
        self._colors = dict()
        self._colors['b'] = [128,   0,   0, 205, 255, 200]
        self._colors['g'] = [128, 175, 128,   0, 128,   0]
        self._colors['r'] = [128,   0, 255,   0,   0, 100]

# ---------------------------------------------------------------------------


class sppasKeysVideoWriter(object):
    """Write a video with keys.

    """

    def __init__(self, image_writer=None):
        """Create a new instance.

        """
        # Manage options and write images if needed
        self._img_writer = sppasKeysImageWriter()
        if image_writer is not None:
            if isinstance(image_writer, sppasKeysImageWriter) is True:
                self._img_writer = image_writer

        # Apply some filters to the images of the video
        self.__cartoonize = False
        self.__colorize = False

        # sppasVideoWriter() for the output videos - if tag/video options.
        self._video_writer = None
        self._fps = 25.       # default video framerate -- important
        self._video_ext = annots.video_extension

        # The sights of all images in the video
        self.__sights = list()

        # Vowel code index. Loaded from the config file and set later on.
        self._vrank = ('n',)
        self._crank = ('0',)

        # Consonants: sppasImage of the hands and options
        self.__hand_prefix = "handcue"
        self.__hands = self.__hand_pictures()

    # -----------------------------------------------------------------------
    # Getters and setters for the options
    # -----------------------------------------------------------------------

    def get_video_extension(self):
        """Return the extension for video files."""
        return self._video_ext

    # -----------------------------------------------------------------------

    def set_video_extension(self, ext):
        """Set the extension of video files."""
        ext = str(ext)
        if ext.startswith(".") is False:
            ext = "." + ext
        if ext not in video_extensions:
            raise sppasExtensionWriteError(ext)

        self._video_ext = ext

    # -----------------------------------------------------------------------

    def get_fps(self):
        """Return the defined fps value to write video files (float)."""
        return self._fps

    # -----------------------------------------------------------------------

    def set_fps(self, value):
        """Fix the framerate of the output video.

        :param value: (float) Number of frames per seconds
        :raise: NegativeValueError, IntervalRangeError

        """
        # if the value isn't correct, sppasVideoWriter() will raise an exc.
        w = sppasVideoWriter()
        w.set_fps(value)
        self._fps = value

    # -----------------------------------------------------------------------

    def get_tag_output(self):
        """Return True if faces of the images will be surrounded."""
        return self._img_writer.options.get_tag_output()

    # -----------------------------------------------------------------------

    def get_crop_output(self):
        """Return True if the option to crop faces is enabled."""
        return self._img_writer.options.get_crop_output()

    # -----------------------------------------------------------------------

    def get_output_width(self):
        """Return the width of the output image files."""
        return self._img_writer.options.get_width()

    # -----------------------------------------------------------------------

    def get_output_height(self):
        """Return the height of the outputs files."""
        return self._img_writer.options.get_height()

    # -----------------------------------------------------------------------

    def set_options(self, tag=None, crop=None, cartoon=None, width=None, height=None):
        """Set any/some/all of the options."""
        self._img_writer.set_options(csv=False, xra=False, tag=tag, crop=crop, width=width, height=height)
        if cartoon is not None:
            self.__cartoonize = cartoon
            # self.__hand_prefix = "cartoonhandcue"
            # self.__hand_prefix = "justhandcue"
            self.__hand_prefix = "handcue"
            self.__hands = self.__hand_pictures()

    # -----------------------------------------------------------------------

    def set_vowels(self, vrank):
        """Set the rank of the vowels like used in the video buffer.

        :param vrank: (list, tuple) Each char is the key of a vowel position.

        """
        self._vrank = tuple(vrank)

    # -----------------------------------------------------------------------

    def set_consonants(self, crank):
        """Set the consonants.

        :param crank: (list, tuple) Each char is the key of a consonant shape.

        """
        self._crank = tuple(crank)
        self.__hands = self.__hand_pictures()

    # -----------------------------------------------------------------------

    def get_vowel_rank(self, vowel_code):
        """Return an index from the code of a vowel or -1.

        :param vowel_code: (char) One of n, b, c, s, m, t for French.

        """
        if vowel_code in self._vrank:
            return self._vrank.index(vowel_code)

        return -1

    # -----------------------------------------------------------------------

    def get_image_size(self, image):
        """Return the size of the image depending on the image and options."""
        return image.get_proportional_size(
            width=self._img_writer.options.get_width(),
            height=self._img_writer.options.get_height()
        )

    # -----------------------------------------------------------------------
    # Writer is here
    # -----------------------------------------------------------------------

    def create_video_writer(self, out_name, image, pattern=""):
        """Create a sppasVideoWriter().

        :raise: PermissionError

        """
        # Fix width and height of the video
        w, h = self.get_image_size(image)

        # Fix the video filename
        filename = "{:s}{:s}".format(out_name, pattern) + self._video_ext
        logging.debug("Create a video writer {:s}. Size {:d}, {:d}"
                      "".format(filename, w, h))

        if os.path.exists(filename) is True:
            logging.warning("A file with name {:s} is already existing.".format(filename))
            trash_filename = sppasTrash().put_file_into(filename)
            logging.info("The file was moved into the Trash of SPPAS "
                         "with name: {:s}".format(trash_filename))

        # Create a writer
        try:
            writer = sppasVideoWriter()
            writer.set_size(w, h)
            writer.set_fps(self._fps)
            writer.set_aspect("extend")
            writer.open(filename)
        except Exception as e:
            logging.error("OpenCV failed to open the VideoWriter for file "
                          "{}: {}".format(filename, str(e)))
            return None

        return writer, filename

    # -----------------------------------------------------------------------

    def close(self):
        """Close all currently used sppasVideoWriter().

        It has to be invoked when writing buffers is finished in order to
        release the video writers.

        """
        if self._video_writer is not None:
            self._video_writer.close()
            self._video_writer = None

    # -----------------------------------------------------------------------

    def set_sights(self, all_sights):
        """A list of N sights of all images of the video."""
        # len(all_sights) must match the nb of images of the video to write
        self.__sights = all_sights

    # -----------------------------------------------------------------------

    def write_video(self, video_buffer, out_name, pattern):
        """Save the result in video format.

        :param video_buffer: (sppasImage) The images to write
        :param out_name: (str) The filename of the output video file
        :param pattern: (str) Pattern to add to cropped video filename(s)
        :return: list of newly created video file names

        """
        # Fix the coords of the hand position and fix the hand shape for each image.
        position_coords = self.__eval_vowel_coords(video_buffer)
        shape_probas = self.__eval_consonants_probas(video_buffer)
        if len(position_coords) != len(video_buffer):
            raise Exception("hum... I should have as many vowel positions as images in the buffer..."
                            "but got {} and {}.".format(len(position_coords), len(video_buffer)))
        if len(shape_probas) != len(video_buffer):
            raise Exception("hum... I should have as many consonant shapes as images in the buffer..."
                            "but got {} and {}.".format(len(shape_probas), len(video_buffer)))

        # Browse the buffer to tag each image (sb = start buffer idx)
        sb, eb = video_buffer.get_buffer_range()
        prev_coord = None
        new_files = list()
        iter_images = video_buffer.__iter__()
        for i in range(video_buffer.__len__()):
            video_image_number = sb + i
            video_image_sights = list()
            if video_image_number <= len(self.__sights):
                video_image_sights = self.__sights[video_image_number-1]

            # Get the current image.
            # Create the sppasVideoWriter() if it wasn't already done.
            # An image is required to properly fix the video size.
            image = next(iter_images)
            if self._video_writer is None:
                self._video_writer, fn = self.create_video_writer(out_name, image, pattern)
                new_files.append(fn)
            img = image.copy()

            # Crop the face and turn the rest of the image into white color
            if self.get_crop_output() is True:
                img = self.__crop_face(img, video_image_sights)

            # Cartoonize the image if the option is enabled
            if self.__cartoonize is True:
                img = icartoon(img, colorize=self.__colorize)

            # Tag the image either with a badge or a hand picture
            # ---------------------------------------------------
            consonants, vowels = video_buffer.get_key(i)
            if self.__hands is None:
                tagged = self.__tag_image_with_badges(img, vowels, shape_probas[i], position_coords[i], prev_coord)
                prev_coord = None
                if i > 0 and len(consonants) == 2:
                    prev_coord = position_coords[i]
            else:
                tagged = self.__tag_image_with_hands(img, shape_probas[i], position_coords[i], prev_coord)
                prev_coord = None
                if i > 0 and len(consonants) == 2:
                    prev_coord = position_coords[i]

            # Turn the tagged image into BGR (remove alpha channel)
            img = tagged.ibgra_to_bgr()

            # Tag the position of each vowel with circles, their coordinates
            # are stored into the list of coordinates of the buffer
            if self.get_tag_output() is True:
                coords = video_buffer.get_coordinates(i)
                img = self._img_writer.tag_image(img, coords)

            # Tag the image with the information for debug
            # --------------------------------------------
            # Tag the img index, the vowels and the consonants, at top-left
            cv2.putText(img, "Frame: " + str(video_image_number), (100, 50), cv2.FONT_HERSHEY_SIMPLEX, 1., wx.BLACK, 1)
            cv2.putText(img, "Shape: " + "".join(consonants), (100, 80), cv2.FONT_HERSHEY_SIMPLEX, 1., wx.BLACK, 1)
            cv2.putText(img, "Pos: " + "".join(vowels), (100, 110), cv2.FONT_HERSHEY_SIMPLEX, 1., wx.BLACK, 1)
            cv2.putText(img, "Info: " + "".join(video_buffer.get_info(i)), (100, 140), cv2.FONT_HERSHEY_SIMPLEX, 1., wx.BLACK, 1)

            # Write the tagged image into the video
            self._video_writer.write(img)

        return new_files

    # -----------------------------------------------------------------------

    def __crop_face(self, image, image_sights, mode="emoji"):
        """Return the image with only the cropped face, or only the sights.

        :param mode: crop, sights or emoji

        """
        if image_sights is None:
            return image
        if isinstance(image_sights, list) is False:
            return image
        if len(image_sights) == 0:
            return image
        # Get sights of the first kid (should be the only one!)
        sights = image_sights[0]
        if len(sights) < 68:
            return image
        width, height = image.size()

        if mode == "sights":
            # Turn the image into white
            image[:, :, :3][True] = [255, 255, 255]

            # Draw the face from the sights. Not closed polylines
            thickness = 1
            color = (0, 0, 0)
            points1 = list()
            for i in range(17):
                points1.append((sights.x(i), sights.y(i)))
            points2 = list()
            for i in range(17, 22):
                points2.append((sights.x(i), sights.y(i)))
            points3 = list()
            for i in range(22, 27):
                points3.append((sights.x(i), sights.y(i)))
            points4 = list()
            for i in range(27, 31):
                points4.append((sights.x(i), sights.y(i)))
            points5 = list()
            for i in range(31, 36):
                points5.append((sights.x(i), sights.y(i)))
            allpoints = [np.array(points1), np.array(points2), np.array(points3), np.array(points4), np.array(points5)]
            cv2.polylines(image, allpoints, False, color, thickness, cv2.LINE_AA, shift=0)

            # Draw the face from the sights. Closed polylines
            points1 = list()
            for i in range(36, 42):
                points1.append((sights.x(i), sights.y(i)))
            points2 = list()
            for i in range(42, 48):
                points2.append((sights.x(i), sights.y(i)))
            points3 = list()
            for i in range(48, 60):
                points3.append((sights.x(i), sights.y(i)))
            points4 = list()
            for i in range(60, 68):
                points4.append((sights.x(i), sights.y(i)))
            allpoints = [np.array(points1), np.array(points2), np.array(points3), np.array(points4)]
            cv2.polylines(image, allpoints, True, color, thickness, cv2.LINE_AA, shift=0)

            return image

        elif mode == "emoji":
            # Turn the image into white
            image[:, :, :3][True] = [255, 255, 255]

            # face background: a filled circle
            center_xy = (sights.x(30), sights.y(30))
            radius = int((sights.x(30) - sights.x(0)) * 1.2)
            image = cv2.circle(image, center_xy, radius, (0, 250, 250), -1)

            points2 = list()
            for i in range(17, 22):
                points2.append((sights.x(i), sights.y(i)))
            points3 = list()
            for i in range(22, 27):
                points3.append((sights.x(i), sights.y(i)))
            allpoints = [np.array(points2), np.array(points3)]
            cv2.polylines(image, allpoints, False, (44, 154, 212), 2, cv2.LINE_AA, shift=0)

            # eyes
            radius = (sights.x(39) - sights.x(36)) // 2
            center_x = sights.x(36) + radius
            center_y = sights.y(36)
            image = cv2.circle(image, (center_x, center_y), radius, (255, 255, 255), -1)
            image = cv2.circle(image, (center_x, center_y), radius//3, (47, 65, 87), -1)
            radius = (sights.x(45) - sights.x(42)) // 2
            center_x = sights.x(42) + radius
            center_y = sights.y(42)
            image = cv2.circle(image, (center_x, center_y), radius, (255, 255, 255), -1)
            image = cv2.circle(image, (center_x, center_y), radius//3, (47, 65, 87), -1)

            # Lips
            points = list()
            for i in range(48, 60):
                points.append((sights.x(i), sights.y(i)))
            cv2.fillConvexPoly(image, np.array(points), (47, 65, 87))  # brown

            points = list()
            for i in range(60, 68):
                points.append((sights.x(i), sights.y(i)))
            cv2.fillConvexPoly(image, np.array(points), (200, 180, 255))  # pink

            return image

        else:
            # Crop the face in the existing image. Make the rest white.
            # Create a polygon from the sights
            area_points = list()
            for i in range(17):
                area_points.append((sights.x(i), sights.y(i)))
            area_points.append((min(width, sights.x(26)+10), max(0, (sights.y(26) - 10))))
            area_points.append((sights.x(24), max(0, (sights.y(24) - 10))))
            area_points.append((sights.x(19), max(0, (sights.y(19) - 10))))
            area_points.append((max(0, sights.x(17)-10), max(0, (sights.y(17) - 10))))
            points = np.array(area_points)

            # Make mask
            mask = np.zeros((height, width), dtype=np.uint8)

            # Solution 1:
            # cv2.drawContours(mask, [points], -1, (255, 255, 255), -1, cv2.LINE_AA)
            # # Do bit-op
            # dst = cv2.bitwise_and(image, image, mask=mask)
            # # Add the white background
            # bg = np.ones_like(image, np.uint8) * 255
            # cv2.bitwise_not(bg, bg, mask=mask)
            # facekid = bg + dst

            # Solution 2
            cv2.fillConvexPoly(mask, points, (255))
            facekid = cv2.bitwise_and(image, image, mask=mask)
            # Turn black to white
            red, green, blue = facekid[:, :, 0], facekid[:, :, 1], facekid[:, :, 2]
            mask = (red == 0) & (green == 0) & (blue == 0)
            facekid[:, :, :3][mask] = [255, 255, 255]
            return sppasImage(input_array=facekid)

    # -----------------------------------------------------------------------

    def __tag_image_with_hands(self, back_image, shapes, coord, prev_coord=None):
        """

        """
        if back_image.channel == 3:
            # Add alpha channel to the self-copied image
            img = back_image.ialpha(254)
        else:
            img = back_image.copy()

        # Sort shapes: from the lowest to the highest proba
        scores = dict()
        for c, p in shapes:
            scores[c] = p
        scores = collections.Counter(scores)
        sorted_scores = scores.most_common()

        for c, p in reversed(sorted_scores):
            hand_img = self.__hands[c].copy()

            # HAND RE-SIZE SHOULD BE ESTIMATED RELATIVELY TO THE SIZE OF THE FACE
            hand_img = hand_img.iresize(img.width // 2, img.height / 2)

            # ARM ANGLE (should be predicted by a model)
            hand_img = hand_img.irotate(angle=-60)

            # shift coords because of the finger (x, y) in our hand pictures
            x = max(0, coord.x - int(0.65 * float(hand_img.width)))
            y = max(0, coord.y - int(0.30 * float(hand_img.height)))

            # When the shape is changing and the position is changing
            if len(shapes) > 1:
                if prev_coord is not None:
                    hand_img = hand_img.iblur(value=3)
                else:
                    hand_img = hand_img.iblur(value=1)

            # When the hand is moving...
            if prev_coord is not None:
                # create a movement effect: put 3 times the hand in transparent
                # from the previous coord to the current one
                px = max(0, prev_coord.x - int(0.65 * float(hand_img.width)))
                py = max(0, prev_coord.y - int(0.30 * float(hand_img.height)))
                tr_step_x = max(0, (x - px) // 4)
                tr_step_y = max(0, (y - py) // 4)
                if tr_step_x + tr_step_y > 0:
                    tr_hand = hand_img.ialpha(20, direction=-1)
                    # img = img.ioverlay(tr_hand, (px + 3*tr_step_x, py + 3*tr_step_y))
                    img = img.ioverlay(tr_hand, (px + 2*tr_step_x, py + 2*tr_step_y))
                    # img = img.ioverlay(tr_hand, (px + tr_step_x, py + tr_step_y))

            # The transparency of the hand is proportional to the proba
            if len(shapes) > 1:
                hand_img = hand_img.ialpha(int(p * 255.), direction=-1)

            img = img.ioverlay(hand_img, sppasCoords(x, y))

        return img

    # -----------------------------------------------------------------------

    def __tag_image_with_badges(self, back_image, vowels, shapes, coord, prev_coord):
        """

        """
        if back_image.channel == 3:
            # Add alpha channel to the self-copied image
            img = back_image.ialpha(254)
        else:
            img = back_image.copy()

        # Get which vowel we are considering. Use the current if vowels has
        # 1 vowel, or the target if vowels has 2 vowels.
        v = self._vrank.index(vowels[-1])
        # Choose the color matching the given vowel
        colors = self._img_writer.get_colors()

        # Sort shapes: from the lowest to the highest proba
        scores = dict()
        for c, p in shapes:
            scores[c] = p
        scores = collections.Counter(scores)
        sorted_scores = scores.most_common()

        x = coord.x
        y = coord.y
        for c, p in reversed(sorted_scores):
            # create an image to put the badge
            hand_img = sppasImage().blank_image(img.width, img.height, alpha=1)
            color = (colors['r'][v], colors['g'][v], colors['b'][v], int(p * 255.))

            # When the hand is moving...
            if prev_coord is not None:
                # create a movement effect: put 3 times the text
                # from the previous coord to the current one
                px = prev_coord.x
                py = prev_coord.y
                tr_step_x = max(0, (x - px) // 4)
                tr_step_y = max(0, (y - py) // 4)
                if tr_step_x + tr_step_y > 0:
                    hand_img.put_text((px + 3*tr_step_x, py + 3*tr_step_y), color, thickness=2, text=c)
                    hand_img.put_text((px + 2*tr_step_x, py + 2*tr_step_y), color, thickness=2, text=c)
                    hand_img.put_text((px + tr_step_x, py + tr_step_y), color, thickness=2, text=c)

            # put the text at the expected coords and adjust the transparency
            # which is proportional to the proba of the shape
            hand_img.put_text(coord, color, thickness=2, text=c)

            # Overlay the badge on the original back image
            img = img.ioverlay(hand_img, sppasCoords(0, 0))

        return img

    # -----------------------------------------------------------------------

    def __eval_vowel_coords(self, video_buffer):
        """Return a list of coords of the hand position for each image.

        ... THIS METHOD SHOULD BE PART OF A PREDICTION MODEL and RESULTING
        COORDS SHOULD BE STORED INTO THE VIDEO BUFFER ...

        Linear prediction of position transitions.
        *** BUT (Attina 2005) showed that the hand is accelerating during the
        first half part of the transition and then decelerating during the
        second half part. ***

        :return: list of len(video_buffer) sppasCoords()

        """
        coords = [None] * len(video_buffer)
        coord1 = None
        i = 0
        while i < len(video_buffer):
            # vowels is a list of 1 or 2 positions
            consonants, vowels = video_buffer.get_key(i)
            from_vowel_idx = self._vrank.index(vowels[0])
            try:
                coord1 = video_buffer.get_coordinate(i, from_vowel_idx)
            except ValueError:
                # There's no coordinate at the given index
                pass

            if len(vowels) == 1:
                coords[i] = coord1
                i += 1

            else:
                to_vowel_idx = self._vrank.index(vowels[1])
                new_vowels = vowels
                nb_img = 0
                # the hand is moving... on how many images?
                while vowels == new_vowels:
                    if (i + nb_img) == len(video_buffer):
                        break
                    nb_img += 1
                    if (i + nb_img) == len(video_buffer):
                        break
                    consonants, new_vowels = video_buffer.get_key(i + nb_img)

                # there are nb images to move from the position of vowels[0]
                # to the one of vowels[1]
                coord2 = None
                try:
                    coord2 = video_buffer.get_coordinate(i + nb_img - 1, to_vowel_idx)
                except ValueError:
                    while coord2 is None:
                        try:
                            coord2 = video_buffer.get_coordinate(i + nb_img, to_vowel_idx)
                        except ValueError:
                            coord2 = None
                        nb_img += 1
                        if i + nb_img == len(video_buffer):
                            break
                if i + nb_img == len(video_buffer):
                    break
                coord2 = video_buffer.get_coordinate(i + nb_img - 1, to_vowel_idx)

                # the hand have to move from "coord1" to "coord2"
                # by tagging "nb" images.
                if nb_img > 2:
                    a, b = slope_intercept((coord1.x, coord1.y), (coord2.x, coord2.y))
                    step_x = (coord2.x - coord1.x) / float(nb_img)
                    step_y = (coord2.y - coord1.y) / float(nb_img)
                    for j in range(nb_img):
                        # coords where to put other in self
                        x = max(0, coord1.x + int(step_x * (j+1)))
                        if coord1.x != coord2.x:
                            y = max(0, int(linear_fct(x, a, b)))
                        else:
                            y = max(0, coord1.y + int(step_y * (j+1)))
                        coords[i] = sppasCoords(x, y)
                        i += 1

                elif nb_img == 2:
                    coords[i] = coord1
                    i += 1
                    coords[i] = coord2
                    i += 1

                else:
                    # It can happen at the end of a buffer, if only one image is
                    # remaining while pos is changing. Or at the beginning...
                    if i > 0:
                        coords[i] = coords[i-1].copy()
                    else:
                        coords[i] = coord1
                    i += 1

        for i, c in enumerate(coords):
            if c is None:
                for j in range(i):
                    consonants, vowels = video_buffer.get_key(j)
                    logging.error(" -> {:d} - {} ".format(j, vowels))
                raise Exception("The image at index {:d} does not have coordinates "
                                "for the hand position.".format(i))
        return coords

    # -----------------------------------------------------------------------

    def __eval_consonants_probas(self, video_buffer):
        """Return a list of tuples of the hand shape + proba for each image.

        Prediction of shape transitions.

        """
        shapes = [None] * len(video_buffer)
        i = 0
        while i < len(video_buffer):
            # consonants is a list of 1 or 2 shapes
            consonants, vowels = video_buffer.get_key(i)
            c1 = consonants[0]

            if len(consonants) == 1:
                shapes[i] = [(c1, 1.)]
                i += 1

            else:
                new_consonants = consonants
                nb_img = 0
                # the hand shape is changing
                while consonants == new_consonants:
                    if (i + nb_img) == len(video_buffer):
                        break
                    nb_img += 1
                    if (i + nb_img) == len(video_buffer):
                        break
                    new_consonants, vowels = video_buffer.get_key(i + nb_img)

                # there are nb images to change the shape from consonants[0]
                # to the one of consonants[1]
                c2 = consonants[1]
                if nb_img == 1:
                    p = 0.7
                elif nb_img == 2:
                    p = 0.6
                elif nb_img == 3:
                    p = 0.5
                else:
                    p = 0.4
                shapes[i] = [(c1, 1. - p), (c2, p)]
                i += 1
                proba_step = (1. - p) / float(nb_img+1)
                for j in range(1, nb_img):
                    proba_shape1 = p + round((j+1) * proba_step, 2)
                    proba_shape0 = 1. - proba_shape1
                    shapes[i] = [(c1, proba_shape0), (c2, proba_shape1)]
                    i += 1

        return shapes

    # -----------------------------------------------------------------------

    def __hand_pictures(self):
        """Return the dict of picture filenames of the hand shapes.

        *** This is a temporary solution in order to debug the shape and
        position predictors. ***

        """
        # We are not in debug mode, so we don't use the hand pictures...
        if cfg.log_level > 0:
            return None

        pict = dict()
        for c in self._crank:
            filename = os.path.join(paths.resources, "cuedspeech", self.__hand_prefix + "-" + c + ".png")
            if os.path.exists(filename):
                hand_img = sppasImage(filename=filename)
                logging.debug("Hand image file: {}".format(filename))
            else:
                logging.warning(
                    "Can't find file {}. With no hand pictures, images will "
                    "be tagged with badges instead..".format(filename))
                # try with the default prefix
                filename = os.path.join(paths.resources, "cuedspeech", "handcue-" + c + ".png")
                if os.path.exists(filename):
                    hand_img = sppasImage(filename=filename)
                    logging.info("The default image is used instead")
                else:
                    logging.error("No image found. Hand-tagging is disabled.")
                    return None

            # Cartoonize the image if the option is enabled
            if self.__cartoonize is True:
                # Remove alpha-channel. transparency is turned into white.
                bgr_img = hand_img.ibgra_to_bgr()
                # Turn black into white
                data = np.array(bgr_img)
                r1, g1, b1 = 0, 0, 0        # Original value
                r2, g2, b2 = 255, 255, 255  # Value that we want to replace it with
                red, green, blue = data[:, :, 0], data[:, :, 1], data[:, :, 2]
                mask = (red == r1) & (green == g1) & (blue == b1)
                data[:, :, :3][mask] = [r2, g2, b2]
                # Cartoonize
                cartoon_img = icartoon(data, colorize=self.__colorize)
                # Get back the alpha channel
                hand_img = sppasImage(input_array=cv2.cvtColor(cartoon_img, cv2.COLOR_RGB2RGBA))
                # Turn back white to transparency
                mask = (red == r2) & (green == g2) & (blue == b2)
                hand_img[:, :, :4][mask] = [r2, g2, b2, 0]

            pict[c] = hand_img

        return pict

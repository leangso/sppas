# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.videotagger.test_videocuer.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Tests of Video tagging cues.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import unittest
import os.path

from sppas.src.config import paths
from sppas.src.anndata.ann.annlabel.tagtypes import sppasFuzzyPoint
from sppas.src.anndata import sppasTrsRW
from sppas.src.anndata import sppasTranscription
from sppas.src.anndata import sppasPoint
from sppas.src.imgdata import sppasImage
from sppas.src.imgdata import sppasSights
from sppas.src.annotations.CuedSpeech import CuedSpeechKeys

from .videocued import CuedSpeechVideoTagger
from .imgvowelstag import sppasImageVowelsTagger
from .imghandtag import sppasImageHandTagger
from .imghandtag import sppasHandProperties
from .imghandtag import sppasHands

# ---------------------------------------------------------------------------

FRA_KEYS = os.path.join(paths.resources, "cuedspeech", "cueConfig-fra.txt")
VIDEO = os.path.join(paths.demo, "demo.mkv")
TRS = os.path.join(paths.demo, "demo-palign.xra")

# ---------------------------------------------------------------------------


class TestTagHandsImage(unittest.TestCase):

    def setUp(self) -> None:
        self.cues = CuedSpeechKeys(FRA_KEYS)

    def test_hands(self):
        hands = sppasHands(self.cues)
        self.assertEqual(0, len(hands))
        hands.load(prefix="handcue", pattern="-hands")
        self.assertEqual(9, len(hands))

    def test_tag(self):
        hands = sppasHands(self.cues)
        hands.load(prefix="handcue", pattern="-hands")

        # create a background image and define a position point in it
        x = 300
        y = 400
        bg_img = sppasImage(0).blank_image(1024, 768, white=True, alpha=0)
        bg_img.surround_coord((x, y), (128, 128, 128, 254), 20)
        bg_img.write("test_bg.png")

        # get a hand image
        hand_img = hands.image("5")
        ohw, ohh = hand_img.size()
        ix, iy = hands.target_coords("5")
        print("Original Hand image size: {} {}".format(ohw, ohh))
        print("Original Target coords: {} {}".format(ix, iy))

        # Resize proportional to the size of the face
        scale_factor = 0.15
        hand_img = hand_img.iresize(int(float(ohw) * scale_factor),
                                    int(float(ohh) * scale_factor))
        rhw, rhh = hand_img.size()
        print("Hand image resized: {} {}".format(rhw, rhh))
        ix = int(float(ix) * scale_factor)
        iy = int(float(iy) * scale_factor)
        print("Resized Target coords: {} {}".format(ix, iy))

        hand_img = hand_img.icenter(width=1024, height=768)
        print("Hand image centered: {} {}".format(hand_img.width, hand_img.height))
        ix += (1024 - rhw) // 2
        iy += (768 - rhh) // 2
        print("Centered Target coords: {} {}".format(ix, iy))

        hand_img = hand_img.irotate(angle=300, center=(ix, iy), redimension=False)
        print("Rotated hand image size: {} {}".format(hand_img.width, hand_img.height))

        cx = x - ix
        cy = y - iy
        print("Top-Left corner of hand: {} {}".format(cx, cy))

        crop_x = 0
        crop_y = 0
        if cx < 0:
            crop_x = -cx
            ix = ix - crop_x
            cx = 0
        if cy < 0:
            crop_y = -cy
            iy = iy - crop_y
            cy = 0
        if crop_x > 0 or crop_y > 0:
            print("Crop coords: {} {} {} {} ".format(crop_x, crop_y, hand_img.width - crop_x, hand_img.height - crop_y))
            hand_img = hand_img.icrop((crop_x, crop_y, hand_img.width - crop_x, hand_img.height - crop_y))

        hand_img.surround_coord((ix, iy), (250, 128, 0), 10, text="")

        bg_img = bg_img.ioverlay(hand_img, (cx, cy))
        bg_img.write("test.png")


class TestHandProperties(unittest.TestCase):

    def test_init(self):
        # invalid image
        with self.assertRaises(TypeError):
            sppasHandProperties("image", "sights", 0)

        # valid image, invalid sights
        s = sppasSights(10)
        img = sppasImage(0).blank_image(w=320, h=200)
        with self.assertRaises(Exception):
            sppasHandProperties(img, s, 0)

        # valid image, valid sights, invalid target
        s = sppasSights(4)
        with self.assertRaises(ValueError):
            sppasHandProperties(img, s, 12)

        # valid image, valid sights, valid target
        s = sppasSights(4)
        p = sppasHandProperties(img, s, 2)
        self.assertEqual(0, p.distance())
        self.assertEqual(0, p.angle())

    def test_estimation(self):
        img = sppasImage(0).blank_image(w=320, h=200)
        s = sppasSights(4)
        s.set_sight(0, 250, 400)   # S0 = (250, 400)

        s.set_sight(1, 200, 200)  # S9 = (200, 200)
        s.set_sight(2, 200, 200)
        s.set_sight(3, 200, 200)
        p = sppasHandProperties(img, s, 2)
        self.assertEqual(206, p.distance())
        self.assertEqual(14, p.angle())

        s.set_sight(1, 300, 200)   # S9 = (300, 200)
        s.set_sight(2, 300, 200)
        s.set_sight(3, 300, 200)
        p = sppasHandProperties(img, s, 2)
        self.assertEqual(206, p.distance())
        self.assertEqual(-14, p.angle())

# ---------------------------------------------------------------------------


class TestImageVowelsTagger(unittest.TestCase):

    def setUp(self) -> None:
        self.img = sppasImage(filename=os.path.join(paths.samples, "faces", "BrigitteBigiSlovenie2016.jpg"))
        self.cues = CuedSpeechKeys(FRA_KEYS)

    def test_init(self):
        it = sppasImageVowelsTagger()
        it = sppasImageVowelsTagger(self.cues)
        with self.assertRaises(TypeError):
            sppasImageVowelsTagger("filename")

    def test_get_vowel_text(self):
        it = sppasImageVowelsTagger()
        self.assertEqual("n", it.get_vowel_text(0))
        self.assertEqual("", it.get_vowel_text(1))
        self.assertEqual("", it.get_vowel_text(-1))
        it = sppasImageVowelsTagger(self.cues)
        self.assertEqual("n", it.get_vowel_text(0))
        self.assertEqual("t", it.get_vowel_text(1))
        self.assertEqual("b", it.get_vowel_text(5))
        self.assertEqual("", it.get_vowel_text(-1))

    def test_slap_on(self):
        it = sppasImageVowelsTagger(self.cues)
        f1 = sppasFuzzyPoint((500, 400), 30)
        f2 = sppasFuzzyPoint((600, 400), 30)
        f3 = sppasFuzzyPoint((700, 400), 30)
        f4 = sppasFuzzyPoint((800, 400), 30)
        f5 = sppasFuzzyPoint((900, 400), 30)
        f6 = sppasFuzzyPoint((1000, 400), 30)

        with self.assertRaises(TypeError):
            it.slap_on("filename", [f1, f2])

        ii = self.img.copy()
        it.slap_on(ii, [None, f1, None, f2])

        ii = self.img.copy()
        it.enable_vowel_name()
        it.slap_on(ii, [f1, f2, f3, f4, f5, f6])
        ii.write("test.jpg")

# ---------------------------------------------------------------------------


class TestImageHandTagger(unittest.TestCase):

    def setUp(self) -> None:
        self.img = sppasImage(filename=os.path.join(paths.samples, "faces", "BrigitteBigiSlovenie2016.jpg"))
        self.cues = CuedSpeechKeys(FRA_KEYS)

    def test_init(self):
        it = sppasImageHandTagger()
        it = sppasImageHandTagger(self.cues)
        with self.assertRaises(TypeError):
            sppasImageHandTagger("filename")

    def test_tag_with_badge(self):
        it = sppasImageHandTagger()
        it.enable_hand_mode(False)
        fpoint = sppasFuzzyPoint((900, 250), 30)
        ii = self.img.copy()

        it._tag_with_badge(ii, fpoint, "n")
        ii.write("test.jpg")

    def test_tag_with(self):
        it = sppasImageHandTagger(self.cues)
        fpoints = list()
        fpoints.append(sppasFuzzyPoint((900, 250), 30))
        fpoints.append(sppasFuzzyPoint((100, 100), 30))
        fpoints.append(sppasFuzzyPoint((300, 100), 30))
        fpoints.append(sppasFuzzyPoint((500, 100), 30))
        fpoints.append(sppasFuzzyPoint((700, 100), 30))
        fpoints.append(sppasFuzzyPoint((100, 400), 30))
        fpoints.append(sppasFuzzyPoint((300, 400), 30))
        fpoints.append(sppasFuzzyPoint((500, 400), 30))
        fpoints.append(sppasFuzzyPoint((700, 400), 30))

        ii = self.img.copy()
        codes = self.cues.get_consonants_codes()
        for i in range(len(codes)):
            if i in range(3, 6):
                it._tag_with_badge(ii, [(codes[i], 0.7), (codes[i+1], 0.3)], [fpoints[i]])
            else:
                it._tag_with_badge(ii, [(codes[i], 1.)], [fpoints[i]])
        ii.write("test_badges.jpg")

        ii = self.img.copy()
        it.enable_hand_mode(True)
        codes = self.cues.get_consonants_codes()
        for i in range(len(codes)):
            if i in range(3, 6):
                ii = it._tag_with_hand(ii, [(codes[i], 0.7), (codes[i + 1], 0.3)], [fpoints[i]])
            else:
                ii = it._tag_with_hand(ii, [(codes[i], 1.)], [fpoints[i]])
        ii.write("test_hands.jpg")

    def test_slap_on(self):
        it = sppasImageHandTagger(self.cues)
        it.enable_hand_mode()
        fpoints = list()
        fpoints.append(sppasFuzzyPoint((900, 250), 30))
        fpoints.append(sppasFuzzyPoint((100, 100), 30))
        fpoints.append(sppasFuzzyPoint((300, 100), 30))
        fpoints.append(sppasFuzzyPoint((500, 100), 30))
        fpoints.append(sppasFuzzyPoint((700, 100), 30))
        fpoints.append(sppasFuzzyPoint((100, 400), 30))
        fpoints.append(sppasFuzzyPoint((300, 400), 30))
        fpoints.append(sppasFuzzyPoint((500, 400), 30))
        fpoints.append(sppasFuzzyPoint((700, 400), 30))

        with self.assertRaises(TypeError):
            it.slap_on("filename", [], [])

# ---------------------------------------------------------------------------


class TestVideoCued(unittest.TestCase):

    def test_init(self):
        # No file is defined
        vt = CuedSpeechVideoTagger()
        self.assertFalse(vt.is_loaded())
        self.assertFalse(vt.is_opened())

        # A file with rules is defined
        with self.assertRaises(TypeError):
            vt = CuedSpeechVideoTagger(FRA_KEYS)
        # and properly loaded in an instance of CuedSpeechKeys
        vt = CuedSpeechVideoTagger(CuedSpeechKeys(FRA_KEYS))

    def test_load_close(self):
        vt = CuedSpeechVideoTagger()
        vt.load(VIDEO)
        self.assertTrue(vt.is_loaded())
        self.assertFalse(vt.is_opened())
        vt.close()
        self.assertFalse(vt.is_loaded())
        self.assertFalse(vt.is_opened())

    def test_not_tag(self):
        vt = CuedSpeechVideoTagger()
        # No video in the buffer.
        with self.assertRaises(Exception):
            vt.tag_with_keys(sppasTranscription(), "output")

        # No tiers in the transcription
        vt.load(VIDEO)
        with self.assertRaises(Exception):
            vt.tag_with_keys(sppasTranscription(), "output")
        vt.close()

    def test_get_ann_idx_interval_tier(self):
        vt = CuedSpeechVideoTagger()
        parser = sppasTrsRW(TRS)
        trs = parser.read()
        phons = trs.find("PhonAlign")
        self.assertIsNotNone(phons)

        ann_idx = vt.get_ann_idx(phons, sppasPoint(0.2))
        self.assertEqual(0, ann_idx)
        ann_idx = vt.get_ann_idx(phons, 0.2)
        self.assertEqual(0, ann_idx)
        ann_idx = vt.get_ann_idx(phons, 0.)
        self.assertEqual(0, ann_idx)
        ann_idx = vt.get_ann_idx(phons, 0.520914)
        self.assertEqual(0, ann_idx)
        ann_idx = vt.get_ann_idx(phons, 1.)
        self.assertEqual(5, ann_idx)

        ann_idx = vt.get_next_ann_idx(phons, 1., 5)
        self.assertEqual(5, ann_idx)
        ann_idx = vt.get_next_ann_idx(phons, 1.1, 5)
        self.assertEqual(6, ann_idx)
        ann_idx = vt.get_next_ann_idx(phons, 1., 10)
        self.assertEqual(-1, ann_idx)
        ann_idx = vt.get_next_ann_idx(phons, 1.1, -1)
        self.assertEqual(6, ann_idx)

# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.videotagger.handsightsreader.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Read a CSV or XRA and load sights of a hand.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

import codecs
import os

from sppas.src.imgdata import sppasSights
from sppas.src.anndata.aio import sppasXRA

# ---------------------------------------------------------------------------


class sppasImageSightsReader(object):
    """Read&create sights from a CSV/XRA file.

    The CSV file must have the following columns:

        - 1
        - index of the sights in the image;
        - success -- like in OpenFace2 CSV results
        - number of sights
        - all x values
        - all y values
        - all z values -- if not None
        - image name

    """

    def __init__(self, input_file, csv_separator=";"):
        """Set the list of sights defined in the given file.

        :param input_file: (str) coords from a sppasCoordsImageWriter
        :param csv_separator: (char) Columns separator in the CSV file

        """
        # List of coordinates, and corresponding image filenames
        self.sights = list()

        fn, fe = os.path.splitext(input_file)
        if fe.lower() == ".csv":
            self.__load_from_csv(input_file, csv_separator)
        elif fe.lower() == ".xra":
            self.__load_from_xra(input_file)
        else:
            raise Exception("Unrecognized extension, expected .csv or .xra."
                            "Got {} instead.".format(fe))

    # -----------------------------------------------------------------------

    def __load_from_csv(self, input_file, separator):
        with codecs.open(input_file, "r") as csv:
            lines = csv.readlines()

        # Browse the (expected only one) line(s) to get sights.
        if len(lines) > 0:
            for line in lines:
                content = line.split(separator)
                # column to indicate a success: 1 if yes
                if content[2] == "1":
                    # number of sight values
                    nb = int(content[3])
                    s = sppasSights(nb)
                    # extract all (x, y, score)
                    for i in range(3, 3+nb):
                        x = int(content[i-3])
                        y = int(content[i-3+nb])
                        s.set_sight(i-3, x, y, None)
                    self.sights.append(s)

    # -----------------------------------------------------------------------

    def __load_from_xra(self, input_file):
        trs = sppasXRA("HandImageSights")
        trs.read(input_file)

        # Get the tier with the expected type:
        # location is point and labels are fuzzy points
        tier = None
        for t in trs:
            if t.is_point() is True and t.is_fuzzypoint() is True:
                tier = t
                break
        if tier is None:
            raise Exception("No valid tier in XRA. Cant load sights.")

        # Browse the (expected only one) annotation(s) to get sights.
        for ann in tier:
            data = list()
            for i, label in enumerate(ann.get_labels()):
                tag = label.get_best()
                score = label.get_score(tag)
                fuzzy_point = tag.get_typed_content()
                x, y = fuzzy_point.get_midpoint()
                data.append((x, y, score))
            s = sppasSights(nb=len(data))
            for i in range(len(data)):
                s.set_sight(i, data[i][0], data[i][1], None, data[i][2])

            self.sights.append(s)

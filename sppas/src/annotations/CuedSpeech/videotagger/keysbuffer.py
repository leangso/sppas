# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.keysbuffer.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Video buffer for the Cued Speech keys.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

from sppas.src.videodata import sppasCoordsVideoBuffer

# ---------------------------------------------------------------------------


class sppasKeysVideoBuffer(sppasCoordsVideoBuffer):
    """A video buffer with lists of coordinates, ids, sights, and keys.

    For each image of the buffer, there are:
    - the coordinates in the buffer represented as a list of the positions
      of all the vowels + neutral;
    - the key is a tuple made of a consonant and a vowel identifier.

    """

    def __init__(self, video=None, size=-1):
        """Create a new instance.

        :param video: (str) The video filename
        :param size: (int) Number of images of the buffer or -1 for auto

        """
        super(sppasKeysVideoBuffer, self).__init__(video, size=size)

        self.__infos = list()
        self.__conson = list()
        self.__vowel = list()
        self.__init_keys()

    # -----------------------------------------------------------------------

    def __init_keys(self):
        self.__conson = [None] * self.get_buffer_size()
        self.__vowel = [None] * self.get_buffer_size()
        self.__infos = [""] * self.get_buffer_size()

    # -----------------------------------------------------------------------

    def reset(self):
        """Override. Reset all the info related to the buffer content."""
        sppasCoordsVideoBuffer.reset(self)
        self.__init_keys()

    # -----------------------------------------------------------------------

    def next(self):
        """Override. Fill in the buffer with the next images & reset keys.

        """
        ret = sppasCoordsVideoBuffer.next(self)
        self.__init_keys()
        return ret

    # -----------------------------------------------------------------------

    def get_info(self, buffer_index):
        """Return the information text of a given image.

        :param buffer_index: (int) Index of the image in the buffer
        :return: (str)

        """
        buffer_index = self.check_buffer_index(buffer_index)
        return self.__infos[buffer_index]

    # -----------------------------------------------------------------------

    def set_info(self, buffer_index, info):
        """Set an information text to a given image index.

        :param buffer_index: (int) Index of the image in the buffer
        :param info: (str) the given information text

        """
        buffer_index = self.check_buffer_index(buffer_index)
        self.__infos[buffer_index] = info

    # -----------------------------------------------------------------------

    def get_key(self, buffer_index):
        """Return the (consonant, vowel) key of a given image.

        :param buffer_index: (int) Index of the image in the buffer
        :return: tuple(str, str)

        """
        buffer_index = self.check_buffer_index(buffer_index)
        return self.__conson[buffer_index], self.__vowel[buffer_index]

    # -----------------------------------------------------------------------

    def set_key(self, buffer_index, consonant, vowel):
        """Set the key to a given image index.

        :param buffer_index: (int) Index of the image in the buffer
        :param consonant: (str) the given consonant
        :param vowel: (str) the given vowel

        """
        buffer_index = self.check_buffer_index(buffer_index)
        self.__conson[buffer_index] = consonant
        self.__vowel[buffer_index] = vowel

    # -----------------------------------------------------------------------

    def set_shape(self, buffer_index, consonant):
        """Set the shape (consonant) to a given image index.

        :param buffer_index: (int) Index of the image in the buffer
        :param consonant: (str) the given consonant

        """
        buffer_index = self.check_buffer_index(buffer_index)
        self.__conson[buffer_index] = consonant

    # -----------------------------------------------------------------------

    def set_position(self, buffer_index, vowel):
        """Set the position (vowel) to a given image index.

        :param buffer_index: (int) Index of the image in the buffer
        :param vowel: (str) the given vowel

        """
        buffer_index = self.check_buffer_index(buffer_index)
        self.__vowel[buffer_index] = vowel


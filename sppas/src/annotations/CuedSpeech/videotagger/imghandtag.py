# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.annotations.CuedSpeech.videotagger.imghandtag.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Tag an image with the hand.

.. _This file is part of SPPAS: <https://sppas.org/>
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2022  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

import numpy
import cv2
import os
import collections
import logging
import math

from sppas.src.config import cfg
from sppas.src.config import paths
from sppas.src.config import sppasError
from sppas.src.config import sppasTypeError
from sppas.src.config import IntervalRangeException
from sppas.src.anndata.ann.annlabel.tagtypes import sppasFuzzyPoint
from sppas.src.imgdata import sppasImage
from sppas.src.imgdata import sppasCoords
from sppas.src.imgdata import sppasSights
from sppas.src.annotations.CuedSpeech.whatkey.phonestokeys import CuedSpeechKeys

from .handsightsreader import sppasImageSightsReader

# ----------------------------------------------------------------------------
# The solution used to cartoonize is proposed here:
# https://medium.com/nerd-for-tech/cartoonize-images-with-python-10e2a466b5fb
# ----------------------------------------------------------------------------


def icartoon(img, colorize=True):
    """Apply a cartoonizer effect.

    :param img: (sppasImage or nb.array)
    :param colorize: (bool) Colorized result.
    :return: (sppasImage)

    """
    # Apply some Median blur on the image
    img_blur = cv2.medianBlur(img, 3)
    # Apply a bilateral filter on the image
    # d – Diameter of each pixel neighborhood that is used during filtering.
    # sigmaColor – Filter sigma in the color space. A larger value of the
    #   parameter means that farther colors within the pixel neighborhood
    #   will be mixed together, resulting in larger areas of semi-equal color.
    # sigmaSpace – Filter sigma in the coordinate space. A larger value of
    #   the parameter means that farther pixels will influence each other as
    #   long as their colors are close enough.
    # img_bf = cv2.bilateralFilter(img_blur, d=5, sigmaColor=80, sigmaSpace=80)
    img_bf = cv2.bilateralFilter(img_blur, d=3, sigmaColor=50, sigmaSpace=50)

    # Use the laplace filter to detect edges.
    # For each of the Laplacian filters we use a kernel size of 5.
    # 7 => Too many edges; 3 => not enough edges
    # 'CV_8U' means that we are using 8 bit values (0–255).
    img_lp_al = cv2.Laplacian(img_bf, cv2.CV_8U, ksize=5)

    # Laplacian of the original image detected a lot of noise.
    # The image with all the filters is the sharpest, which comes in handy in
    # a bit. This is however not yet what we want. We need an image preferably
    # black and white that we can use as a mask.
    # Convert the image to greyscale (1D)
    img_lp_al_grey = cv2.cvtColor(img_lp_al, cv2.COLOR_BGR2GRAY)

    # Each variable now contains a 1-dimensional array instead of a 3-dimensional
    # array. Next we are going to use image thresholding to set values that are
    # near black to black and set values that are near white to white.
    # Manual image thresholding
    # _, tresh_al = cv2.threshold(img_lp_al_grey, 127, 255, cv2.THRESH_BINARY)
    # Remove some additional noise
    blur_al = cv2.GaussianBlur(img_lp_al_grey, (3, 3), 0)
    # Apply a threshold (Otsu)
    _, tresh_al = cv2.threshold(blur_al, 245, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # We now have a black image with white edges, we only have to invert the black
    # and the white for our mask.
    # Invert the black and the white
    inverted_Bilateral = cv2.subtract(255, tresh_al)

    if colorize is True:
        # Turn colors of the image into cartoon style
        img_color = cv2.bilateralFilter(img, d=5, sigmaColor=220, sigmaSpace=220)
        # slow: img_color = cv2.edgePreservingFilter(img, flags=2, sigma_s=50, sigma_r=0.4)
        # img_color = color_quantization_kmeans(img.copy())
        return sppasImage(input_array=cv2.bitwise_and(img_color, img_color, mask=inverted_Bilateral))

    return sppasImage(input_array=inverted_Bilateral)

# ---------------------------------------------------------------------------


class sppasHandProperties(object):
    """Data storage of a hand.

    Angle estimation is performed thanks to both sights S0 (wrist) and S9
    (middle finger mcp).
       - angle is 0 degree if: x0 = x9 ; y0 <= y9
       - angle is 180 degrees if: x0 = x9 ; y0 > y9
    Size estimation is performed thanks to both sights S0 and S9 too.

    Estimations from coordinates in a picture:

            A---------------------S9
            |                   /
            |               /
            |          /
            |     /
            S0

        S0 = (x0, y0)
        S9 = (x9, y9)
        A = (x0, y9)

        Let's introduce:
        - a: the distance between S0 and S9 to be estimated
        - b: the distance between A and S9: abs(y9 - y0)
        - c: the distance between A and S0: abs(x9 - x0)
        - alpha = angle A = 90 degrees
        - beta = angle S0 to be estimated

        then:
        a = sqrt(b*b - c*c) [pythagore]
        beta = arccos( (c*c + a*a - b*b) / (2*c*a) )

    """

    def __init__(self, image, sights, target_idx):
        """Create a data structure for a hand.

        :param image: (sppasImage)
        :param sights: (sppasSights)
        :param target_idx: (int) Index of the target sight

        """
        self.__angle = 0
        self.__dist = 0
        self.__target = (0, 0)

        # Set the image
        if isinstance(image, sppasImage) is False:
            raise sppasTypeError(type(image), "sppasImage")
        self.__image = image

        # Set the sights
        if isinstance(sights, sppasSights) is False:
            raise sppasTypeError(type(sights), "sppasSights")
        self.__sights = sights

        # Set the target (x,y) from the sights
        if 0 <= target_idx < len(sights):
            x, y, _, _ = sights.get_sight(target_idx)
            self.__target = (x, y)
        else:
            raise IntervalRangeException(target_idx, 0, len(sights))

        # Estimate the distance and the angle from the sights
        self.__estimation()

    # -----------------------------------------------------------------------
    # Geometry
    # -----------------------------------------------------------------------

    @staticmethod
    def euclidian_distance(x1, x2):
        """Return the distance between 2 points x1 and x2."""
        return abs(x2 - x1)

    @staticmethod
    def pythagore(b, c):
        """Estimate Pythagore of 'a' from 'b' and 'c'."""
        b = float(b)
        c = float(c)
        return math.sqrt((b*b) + (c*c))

    @staticmethod
    def beta_angle(a, b, c):
        """Return the angle beta in degrees."""
        a = float(a)
        b = float(b)
        c = float(c)
        if c*a != 0.:
            nominateur = ((c*c) + (a*a) - (b*b))
            denominateur = (2*c*a)
            radian = math.acos(nominateur/denominateur)
            return math.degrees(radian)
        return 0.

    # -----------------------------------------------------------------------
    # Getters and setters
    # -----------------------------------------------------------------------

    def distance(self):
        """Return the [s0,s9] estimated distance value (float)."""
        return self.__dist

    def angle(self):
        """Return the estimated 'beta' angle value (float)."""
        return self.__angle

    def nb_sights(self):
        """Return the number of sights of the hand (int)."""
        return len(self.__sights)

    def image(self):
        """Return the image of the hand (sppasImage)."""
        return self.__image

    def image_size(self):
        """Return the image size of the hand (w, h)."""
        return self.__image.size()

    def target_coords(self):
        """Return (x, y) of the target point of the hand (int, int)."""
        return self.__target

    # -----------------------------------------------------------------------
    # Private
    # -----------------------------------------------------------------------

    def __estimation(self):
        if len(self.__sights) == 21:
            # The result of an automatic hand detection
            x0, y0, _, _ = self.__sights.get_sight(0)
            x9, y9, _, _ = self.__sights.get_sight(9)

        elif len(self.__sights) == 4:
            # The result of an automatic pose detection
            x0, y0, _, _ = self.__sights.get_sight(0)
            x1, y1, _, _ = self.__sights.get_sight(1)
            x3, y3, _, _ = self.__sights.get_sight(3)
            x9 = (x1 + int((x3 - x1) / 2.))
            y9 = (y1 + int((y3 - y1) / 2.))

        else:
            raise sppasError("21 or 4 sights were expected. "
                             "Got {:d} instead.".format(len(self.__sights)))

        # Estimate the distance "a" between S0 and S9
        b = sppasHandProperties.euclidian_distance(x9, x0)
        c = sppasHandProperties.euclidian_distance(y9, y0)
        a = sppasHandProperties.pythagore(b, c)

        # Estimate the angle beta
        beta = sppasHandProperties.beta_angle(a, b, c)
        if y0 >= y9:
            if x0 < x9:
                beta = 360 - beta
        else:
            if x0 <= x9:
                beta = 180 + beta
            else:
                beta = 90 + beta

        # There's no need of a high precision!
        self.__angle = int(round(beta, 0))
        self.__dist = int(round(a, 0))

# ---------------------------------------------------------------------------


class sppasHands(object):
    """Data structure to load and store all hands.

    """

    def __init__(self, cue_rules=CuedSpeechKeys()):
        """Create an instance to store a hand for each given consonant.

        :param cue_rules: (CuedSpeechKeys) Cued speech rules

        """
        # Rule-based system to convert a sequence of phonemes into keys
        # Rules are needed to get both the consonant codes and their target point
        if isinstance(cue_rules, CuedSpeechKeys) is False:
            raise sppasTypeError(type(cue_rules), "CuedSpeechKeys")
        self.__cued = cue_rules

        # The property of each hand. key=code value=sppasHandProperties()
        self.__handps = dict()

    # -----------------------------------------------------------------------

    def load(self, prefix, pattern=""):
        """Load the hand images matching the given prefix.

        :param prefix: (str) Prefix in hand image filenames.
        :param pattern: (str) Pattern in hand image filenames.
        :return: (bool) successfully loaded hand pictures

        """
        codes = self.__cued.get_consonants_codes()
        loaded_hands = self.__load_hand_pictures(str(prefix), pattern)

        if len(loaded_hands) != len(codes):
            logging.error("Hand pictures not properly loaded.")
            self.__handps = dict()
        else:
            self.__handps = loaded_hands

        return len(self.__handps)

    # -----------------------------------------------------------------------

    def image(self, code):
        """Return a deep copy of the image matching the given code, or None.

        :param code: (str) Image code name.
        :return: (sppasImage or None)

        """
        if code in self.__handps:
            return self.__handps[code].image().copy()
        return None

    # -----------------------------------------------------------------------

    def target_coords(self, code):
        """Return target coords of the image matching the given code, or None.

        :param code: (str) Image code name.
        :return: (sppasImage or None)

        """
        if code in self.__handps:
            return self.__handps[code].target_coords()
        return None

    # -----------------------------------------------------------------------

    def angle(self, code):
        """Return the angle of the image matching the given code, or 0.

        :param code: (str) Image code name.
        :return: (int)

        """
        if code in self.__handps:
            return self.__handps[code].angle()
        return 0

    # -----------------------------------------------------------------------

    def distance(self, code):
        """Return the distance of the image matching the given code, or 0.

        :param code: (str) Image code name.
        :return: (int)

        """
        if code in self.__handps:
            return self.__handps[code].distance()
        return 0

    # -----------------------------------------------------------------------

    def __load_hand_pictures(self, prefix, pattern=""):
        """Return the dict of picture filenames of the hand shapes.

        """
        pict = dict()
        for c in self.__cued.get_consonants_codes():
            filename = os.path.join(paths.resources, "cuedspeech", prefix + "_" + c + ".png")
            if os.path.exists(filename):
                hand_img = sppasImage(filename=filename)
                filename = os.path.join(paths.resources, "cuedspeech", prefix + "_" + c + pattern + ".xra")
                if os.path.exists(filename):
                    try:
                        data = sppasImageSightsReader(filename)
                        if len(data.sights) == 1:
                            tgt = 12
                            if c == "0":
                                tgt = 9
                            elif c in ("1", "6"):
                                tgt = 8
                            pict[c] = sppasHandProperties(hand_img, data.sights[0], tgt)
                            logging.debug("Successfully read hand image and its sights: "
                                          "{:s}".format(filename))
                        else:
                            raise IOError("Invalid sights file {:s}. ({:d} != 1)".format(filename, len(data.sights)))
                    except Exception as e:
                        logging.error("Error while reading hand sights: {:s}".format(str(e)))
                else:
                    logging.warning("Can't find hand sights file {}.".format(filename))

            else:
                logging.warning("Can't find hand picture file {}.".format(filename))

        return pict

    # -----------------------------------------------------------------------
    # Overloads
    # -----------------------------------------------------------------------

    def __len__(self):
        return len(self.__handps)

# ---------------------------------------------------------------------------


class sppasImageHandTagger(object):
    """Tag the image with hand picture.

    """

    def __init__(self, cue_rules=CuedSpeechKeys()):
        """Create a new instance.

        :param cue_rules: (CuedSpeechKeys) Rules and codes for vowel positions and hand shapes

        """
        # Rule-based system to convert a sequence of phonemes into keys
        if isinstance(cue_rules, CuedSpeechKeys) is False:
            raise sppasTypeError(type(cue_rules), "CuedSpeechKeys")
        self.__cued = cue_rules

        # Vowel code index. Loaded from the config file.
        self._vrank = tuple(self.__cued.get_vowels_codes())

        # Options
        self.__cartoonize = False
        self.__colorize = False
        self.__badge_color = (64, 64, 64)

        # Pictures of the hands and their properties -- need to be loaded
        self.__hands = sppasHands(cue_rules)
        self.__hand_mode = False

    # -----------------------------------------------------------------------

    def get_vowel_rank(self, vowel_code):
        """Return an index from the code of a vowel or -1.

        :param vowel_code: (char) One of n, b, c, s, m, t for French.

        """
        if vowel_code in self._vrank:
            return self._vrank.index(vowel_code)

        return -1

    # -----------------------------------------------------------------------

    def load_hands(self, prefix, pattern=""):
        """Load the set of pictures of the hands to represent shapes.

        The hand mode is automatically enabled if all hand pictures and sights
        are properly loaded.

        :param prefix: (str) A prefix of the hand filenames
        :param pattern: (str) A pattern of the hand filenames

        """
        self.__hands.load(prefix, pattern)
        self.__hand_mode = True if len(self.__hands) > 0 else False

    # -----------------------------------------------------------------------
    # Getters and setters
    # -----------------------------------------------------------------------

    def enable_hand_mode(self, value=True):
        """Enable to add the pictures of a hand.

        :param value: (bool) True to add a hand, False to draw a badge.

        """
        value = bool(value)
        if value is True and len(self.__hands) == 0:
            raise sppasError("Hand mode can't be enable: no hand pictures loaded.")
        self.__hand_mode = value

    # -----------------------------------------------------------------------
    # Image tagging
    # -----------------------------------------------------------------------

    def slap_on(self, image, shapes, target_points, angle=330, vowels_pos=None):
        """Tag a hand to the image.

        :param image: (sppasImage or numpy.ndarray)
        :param shapes: one or two consonant names and their probabilities
        :param target_points: (sppasFuzzyPoint) The target positions of the hand (one or two)
        :param angle: (int) Expected angle value for the hand. It depends on the vowel position...
        :param vowels_pos: (list) List of all vowels coordinates

        :raise: TypeError

        """
        if vowels_pos is None:
            vowels_pos = []
        if len(shapes) * len(target_points) == 0:
            return image

        # Until the version 1 of the CuedSpeech generator is released,
        # the hand mode is available only in debug level.
        tag_with_hand = bool(self.__hand_mode is True and logging.getLogger().getEffectiveLevel() == 0)

        # Set the image to be tagged
        if isinstance(image, sppasImage) is True:
            img = image.copy()
        elif isinstance(image, numpy.ndarray) is True:
            img = sppasImage(input_aray=image)
        else:
            raise sppasTypeError(type(image), "sppasImage/numpy.ndarray")

        # Only the shape is changing, not the position
        if len(shapes) > len(target_points):
            # Sort shapes: from the highest to the lowest probability
            scores = dict()
            for code, proba in shapes:
                scores[code] = proba
            scores = collections.Counter(scores)
            sorted_shapes_scores = scores.most_common()
            for i in reversed(range(len(sorted_shapes_scores))):
                point, _ = target_points[0]
                x, y = point.get_midpoint()
                c, p = sorted_shapes_scores[i]
                if tag_with_hand:
                    scale_factor = self.__eval_hand_scale(c, vowels_pos)
                    img = self.__tag_image_with_hand(img, c, p, x, y, angle, scale_factor=scale_factor)
                else:
                    img = self.__tag_image_with_badge(img, c, p, x, y, point.get_radius())

        # Only the position is changing, not the shape
        # Draw the shape at the position with the highest proba
        elif len(shapes) < len(target_points):
            # Sort positions: from the highest to the lowest probability
            scores = dict()
            for point, proba in target_points:
                scores[point] = proba
            scores = collections.Counter(scores)
            sorted_points_scores = scores.most_common()
            point, pp = sorted_points_scores[0]
            x, y = point.get_midpoint()
            c, p = shapes[0]
            if tag_with_hand:
                scale_factor = self.__eval_hand_scale(c, vowels_pos)
                img = self.__tag_image_with_hand(img, c, p, x, y, angle, scale_factor=scale_factor)
            else:
                img = self.__tag_image_with_badge(img, c, p, x, y, point.get_radius(), point_proba=pp)

        else:
            # Both hand shape and hand position are changing
            for i in reversed(range(len(shapes))):
                point, pp = target_points[i]
                x, y = point.get_midpoint()
                code, proba = shapes[i]
                if tag_with_hand:
                    scale_factor = self.__eval_hand_scale(code, vowels_pos)
                    img = self.__tag_image_with_hand(img, code, proba, x, y, angle, scale_factor=scale_factor)
                else:
                    img = self.__tag_image_with_badge(img, code, proba, x, y, point.get_radius(), point_proba=pp)

        return img

    # -----------------------------------------------------------------------

    def __eval_hand_scale(self, shape_code, vowels_pos):
        """Estimate the scale factor for the image of the hand.

        IMPORTANT:
        It is supposed that the sights S0 and S9 of the hand pictures are
        properly fixed.

        """
        # The distance between s0 and s9 in the picture of the hand.
        hand_dist = self.__hands.distance(shape_code)

        # The distance between "c" and "t" midpoints.
        n_idx = self.get_vowel_rank("n")
        t_idx = self.get_vowel_rank("t")
        assert n_idx != -1 and t_idx != -1
        xn, yn = vowels_pos[n_idx].get_midpoint()
        xt, yt = vowels_pos[t_idx].get_midpoint()

        b = sppasHandProperties.euclidian_distance(xn, xt)
        c = sppasHandProperties.euclidian_distance(yn, yt)
        a = round(float(sppasHandProperties.pythagore(b, c)) * 0.8, 0)

        hand_factor = round(a / float(hand_dist), 2)

        # AND WE DONT HAVE MANUALLY MODIFIED OUR HAND SIGHTS...
        # *********************** TEMPORARY SOLUTION ******************
        # **************** WORKS ONLY FOR 'brigitte' hands ************
        if logging.getLogger().getEffectiveLevel() < 10:
            if shape_code == "0":
                return 0.22
            else:
                return 0.29

        return hand_factor

    # -----------------------------------------------------------------------

    def __tag_image_with_hand(self, img, shape_code, shape_proba, x, y, angle, point_proba=1., scale_factor=0.20):
        hand_img = self.__hands.image(shape_code)
        ohw, ohh = hand_img.size()
        ix, iy = self.__hands.target_coords(shape_code)

        hand_img = hand_img.iresize(int(float(ohw) * scale_factor),
                                    int(float(ohh) * scale_factor))
        ix = int(float(ix) * scale_factor)
        iy = int(float(iy) * scale_factor)
        rhw, rhh = hand_img.size()

        # When the position is changing, the hand is supposed to move. Mimic with blur.
        # if point_proba < 1.:
        #    hand_img = hand_img.iblur(value=1)

        # The transparency of the hand is proportional to the proba of the shape
        if shape_proba < 1:
            hand_img = hand_img.ialpha(int(shape_proba * 255.), direction=-1)

        # Enlarge the image to ensure that the hand won't be cropped
        # at the time of rotating it
        hand_img = hand_img.icenter(width=img.width, height=img.height)
        ix = ix + ((img.width - rhw) // 2)
        iy = iy + ((img.height - rhh) // 2)

        # Rotate of a given angle.
        # By default, the rotation center is the center of the image.
        # In our case, the rotation center is the target finger in order that
        # its coordinates don't change.
        rotation = self.__hands.angle(shape_code) - angle
        hand_img = hand_img.irotate(rotation, center=(ix, iy), redimension=False)

        # Shift coords to match the target point with the one in our hand picture
        cx = x - ix
        cy = y - iy
        crop_x = 0
        crop_y = 0
        if cx < 0:
            crop_x = -cx
            ix = ix - crop_x
            cx = 0
        if cy < 0:
            crop_y = -cy
            iy = iy - crop_y
            cy = 0
        if crop_x > 0 or crop_y > 0:
            hand_img = hand_img.icrop((crop_x, crop_y, max(0, hand_img.width - crop_x), max(0, hand_img.height - crop_y)))

        # The image of the hand is ready. Overlays on the back image.
        img = img.ioverlay(hand_img, sppasCoords(cx, cy))
        # img = self.overlay_transparent(img, hand_img, cx, cy)

        return img

    # -----------------------------------------------------------------------

    def __tag_image_with_badge(self, img, shape_code, shape_proba, x, y, radius, point_proba=1.):
        """Tag the image with a circle and a text inside.

        """
        # a circle filled in gray and surrounded by a white line
        cv2.circle(img, (x, y), radius - 1, self.__badge_color, -1)
        if point_proba < 1:
            # define the color of the text and put it in the middle of the circle
            b = int(250. * shape_proba)
            cv2.circle(img, (x, y), radius, (255-b, 255-b, 255-b), 1)
        else:
            cv2.circle(img, (x, y), radius, (5, 5, 5), 1)

        if shape_proba < 1.:
            # define the color of the text and put it in the middle of the circle
            b = int(250. * shape_proba)
            img.put_text((x - (radius // 2), y + (radius // 2)), (b, b, b), 1, shape_code)
        else:
            img.put_text((x - (radius // 2), y + (radius // 2)), (250, 250, 250), 2, shape_code)

        return img

    # -----------------------------------------------------------------------
    # Private
    # -----------------------------------------------------------------------

    @staticmethod
    def overlay_transparent(background_img, img_to_overlay_t, x, y, overlay_size=None):
        """TO BE TESTED...
        @brief      Overlays a transparent PNG onto another image using CV2

        @param      background_img    The background image
        @param      img_to_overlay_t  The transparent image to overlay (has alpha channel)
        @param      x                 x location to place the top-left corner of our overlay
        @param      y                 y location to place the top-left corner of our overlay
        @param      overlay_size      The size to scale our overlay to (tuple), no scaling if None

        @return     Background image with overlay on top

        """
        bg_img = background_img.copy()

        if overlay_size is not None:
            img_to_overlay_t = cv2.resize(img_to_overlay_t.copy(), overlay_size)

        # Extract the alpha mask of the RGBA image, convert to RGB
        b, g, r, a = cv2.split(img_to_overlay_t)
        overlay_color = cv2.merge((b, g, r))

        # Apply some simple filtering to remove edge noise
        mask = cv2.medianBlur(a, 5)

        h, w, _ = overlay_color.shape
        roi = bg_img[y:y + h, x:x + w]

        # Black-out the area behind the logo in our original ROI
        img1_bg = cv2.bitwise_and(roi.copy(), roi.copy(), mask=cv2.bitwise_not(mask))

        # Mask out the logo from the logo image.
        img2_fg = cv2.bitwise_and(overlay_color, overlay_color, mask=mask)

        # Update the original image with our new ROI
        bg_img[y:y + h, x:x + w] = cv2.add(img1_bg, img2_fg)

        return bg_img

    # -----------------------------------------------------------------------

    def __do_cartoonize(self, image):
        # Remove alpha-channel. transparency is turned into white.
        bgr_img = image.ibgra_to_bgr()
        # Turn black into white
        data = numpy.array(bgr_img)
        r1, g1, b1 = 0, 0, 0  # Original value
        r2, g2, b2 = 255, 255, 255  # Value that we want to replace it with
        red, green, blue = data[:, :, 0], data[:, :, 1], data[:, :, 2]
        mask = (red == r1) & (green == g1) & (blue == b1)
        data[:, :, :3][mask] = [r2, g2, b2]
        # Cartoonize
        cartoon_img = icartoon(data, colorize=self.__colorize)
        # Get back the alpha channel
        img = sppasImage(input_array=cv2.cvtColor(cartoon_img, cv2.COLOR_RGB2RGBA))
        # Turn back white to transparency
        mask = (red == r2) & (green == g2) & (blue == b2)
        img[:, :, :4][mask] = [r2, g2, b2, 0]

        return img

"""
:filename: sppas.src.__init__.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  SPPAS API source code.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

Import all source packages of the API except those of the UI.

"""

# Configuration of the application, global data structures and utilities.
from .config import *
from .utils import *
from .structs import *
from .calculus import *

# Data structures to manage files to work with and data knowledge files
from .wkps import *
from .resources import *

# Data structure to represent annotated data and recordings
from .anndata import *
from .audiodata import *
from .imgdata import *
from .videodata import *

# The features of SPPAS
from .annotations import *
from .analysis import *
from .plugins import *
from sppas.src.annotations.Align.models import *

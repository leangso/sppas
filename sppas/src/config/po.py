# -*- coding: UTF-8 -*-
"""
:filename: sppas.src.config.po.py
:author: Brigitte Bigi
:contact: develop@sppas.org
:summary: Translation system of SPPAS.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import os
import sys
import gettext
import locale
import logging

from .settings import sppasPathSettings
from .settings import sppasGlobalSettings

# ----------------------------------------------------------------------------


class T:
    """Utility class to mimic the GNUTranslations class.

    Backward compatibility of Python 2.7.

    """

    @staticmethod
    def gettext(message):
        """Return msg in unicode."""
        if sys.version_info >= (3, 0):
            return message
        else:
            with sppasGlobalSettings() as sg:
                return message.decode(sg.__encoding__)

    @staticmethod
    def ugettext(message):
        """Return msg."""
        if sys.version_info >= (3, 0):
            return message
        else:
            with sppasGlobalSettings() as sg:
                return message.decode(sg.__encoding__)

# ----------------------------------------------------------------------------


class sppasTranslate(object):
    """Fix the domain to translate messages and to activate the gettext method.

    sppasTranslate is useful for the internationalization of texts.
    The locale is used to set the language but English is the default.
    The path to search for a domain translation is the one of the 'po' folder.

    :Example:

        >>> _ = sppasTranslate().translation("domain").gettext
        >>> my_string = _("Some string in the domain.")

    """

    DEFAULT_LOCALE = ["en"]

    def __init__(self):
        """Create a sppasTranslate instance.

         Fix languages.

        """
        self.__lang = sppasTranslate.get_lang_list()

    # ------------------------------------------------------------------------

    @staticmethod
    def get_lang_list():
        """Return the list of languages depending on the default locale.

        At a first stage, the language is fixed with the default locale.
        English is then either appended to the list or used by default.

        """
        lc = sppasTranslate.DEFAULT_LOCALE
        try:
            sys_locale, encoding = locale.getdefaultlocale()
            if sys_locale is None:
                # Under MacOS, the locale is defined differently compared to
                # other systems, then Python cannot capture a valid value.
                # so, try something else:
                sys_locale = os.getenv("LANG")

            if sys_locale is not None:
                if "_" in sys_locale:
                    sys_locale = sys_locale[:sys_locale.index("_")]
                lc.insert(0, sys_locale)
            else:
                logging.warning("The OS didn't defined a valid default locale.")

        except Exception as e:
            logging.error("Can't get the system default locale: {}".format(e))

        return lc

    # ------------------------------------------------------------------------

    def translation(self, domain):
        """Create the GNUTranslations for a given domain.

        A domain corresponds to a .po file of the language in the 'po' folder
        of the SPPAS package.

        :param domain: (str) Name of the domain.
        :returns: (GNUTranslations)

        """
        try:
            # Install translation for the local language + English
            with sppasPathSettings() as path:
                t = gettext.translation(domain, path.po, self.__lang)
                t.install()
                return t

        except:
            try:
                # Install translation for English only
                with sppasPathSettings() as path:
                    t = gettext.translation(domain, path.po, sppasTranslate.DEFAULT_LOCALE)
                    t.install()
                    return t

            except IOError:
                pass

        # No language installed. The messages won't be translated;
        # at least they are simply returned.
        return T()

# ---------------------------------------------------------------------------


def info(msg_id, domain=None):
    """Return the info message from gettext.

    :param msg_id: (str or int) Info id
    :param domain: (str) Name of the domain

    """
    # Format the input message
    if isinstance(msg_id, int):
        msg = "{:04d}".format(msg_id)
    else:
        msg = str(msg_id)
    msg = ":INFO " + msg + ": "

    if domain is not None:
        try:
            st = sppasTranslate()
            translation = st.translation(domain)
            return translation.gettext(msg)
        except:
            pass

    return msg

# ---------------------------------------------------------------------------


def error(msg_id, domain=None):
    """Return the error message from gettext.

    :param msg_id: (str or int) Error id
    :param domain: (str) Name of the domain

    """
    # Format the input message
    if isinstance(msg_id, int):
        msg = "{:04d}".format(msg_id)
    else:
        msg = str(msg_id)
    msg = ":ERROR " + msg + ": "

    if domain is not None:
        try:
            st = sppasTranslate()
            translation = st.translation(domain)
            return translation.gettext(msg)
        except:
            pass

    return msg

# ---------------------------------------------------------------------------


def msg(msg, domain=None):
    """Return the message from gettext.

    :param msg: (str) Message
    :param domain: (str) Name of the domain

    """
    if domain is not None:
        try:
            st = sppasTranslate()
            translation = st.translation(domain)
            return translation.gettext(msg)
        except:
            pass

    return msg


"""
:filename: sppas.src.imgdata.tests.test_sights.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Tests of the sights class.

.. _This file is part of SPPAS: https://sppas.org/
..
    -------------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    -------------------------------------------------------------------------

"""

import os
import unittest

from sppas.src.config import paths
from sppas.src.imgdata.sights import sppasSights

# ---------------------------------------------------------------------------


class TestSights(unittest.TestCase):

    def test_init(self):
        s = sppasSights()
        self.assertEqual(68, len(s))

        s = sppasSights(5)
        self.assertEqual(5, len(s))
        for i in range(5):
            self.assertEqual((0, 0, None, None), s.get_sight(0))

        with self.assertRaises(TypeError):
            sppasSights(-1)
        with self.assertRaises(TypeError):
            sppasSights(1.6)

    # ------------------------------------------------------------------------

    def test_get_set(self):
        s = sppasSights(5)
        s.set_sight(0, 10, 20)
        self.assertEqual((10, 20, None, None), s.get_sight(0))
        s.set_sight_score(0, 0.123)
        self.assertEqual(0.123, s.score(0))
        s.set_sight(0, 39, 12)
        self.assertEqual((39, 12, None, None), s.get_sight(0))

        with self.assertRaises(Exception):
            s.get_sight(-1)
        with self.assertRaises(Exception):
            s.get_sight(12)
        with self.assertRaises(Exception):
            s.get_sight(0.5)

        basic_sights = sppasSights(5)
        for i, s in enumerate(basic_sights):
            x, y, z, score = s
            x += i + 10
            y += i + 20
            basic_sights.set_sight(i, x, y)

    # ------------------------------------------------------------------------

    def test_overloads(self):
        s = sppasSights(68)
        self.assertEqual((0, 0, None, None), s[0])
        self.assertEqual((0, 0, None, None), s[0])
        # with slice:
        self.assertEqual([(0, 0, None, None)]*10, s[0:10])

        for item in s:
            self.assertEqual((0, 0, None, None), item)

        s.set_sight(1, 10, 20)
        self.assertTrue((10, 20) in s)
        self.assertTrue((10, 20, 0.123) in s)
        self.assertFalse((10, 10) in s)

    # ------------------------------------------------------------------------

    def test_get_list(self):
        s = sppasSights(5)
        s.set_sight(0, 10, 20)

        x = s.get_x()
        self.assertEqual(5, len(x))
        self.assertTrue(isinstance(x, tuple))
        self.assertEqual(10, x[0])
        self.assertEqual((10, 20, None, None), s[0])

        # we expect that if we modify 'x', it won't change 's'
        x = list(x)
        x[0] = "anything"
        self.assertEqual("anything", x[0])
        self.assertEqual((10, 20, None, None), s[0])

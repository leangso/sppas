# -*- coding : UTF-8 -*-
"""
:filename: sppas.src.imgdata.sights.py
:author:   Brigitte Bigi
:contact:  develop@sppas.org
:summary:  Data structure to store the n sights of an object.

.. _This file is part of SPPAS: https://sppas.org/
..
    ---------------------------------------------------------------------

     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

"""

from sppas.src.config import NegativeValueError
from sppas.src.config import IndexRangeException
from sppas.src.config import sppasTypeError
from sppas.src.imgdata import sppasCoords

# ---------------------------------------------------------------------------


class sppasSights(object):
    """Data structure to store sights.

    This class is storing nb sights; each sight is made of 4 values:
        - x: coordinate on the x-axis, initialized to 0
        - y: coordinate on the y-axis, initialized to 0
        - z: an optional coordinate on the z-axis, initialized to None
        - an optional confidence score, initialized to None

    Notice that each of the sight parameter is stored into a list of 'nb'
    values, instead of storing a single list of 'nb' lists of values, because:

    - 2 lists of 'nb' int and 1 of float = [x1,x2,...] [y1,y2,...] [s1,s2,...]
        3*64 + 2*68*24 + 1*68*24 = 5088
    - 1 list of 'nb' lists of 2 int and 1 float: [[x1,y1,s1], [x2,y2,s2]...]
        64 + 68*64 + 2*68*24 + 1*68*24 = 9312

    """

    def __init__(self, nb=68):
        """Create a new instance.

        :param nb: (int) Number of expected sights.

        """
        # Number of sights to store
        self.__nb = sppasCoords.to_dtype(nb, int, unsigned=True)

        # 2D-axis values
        self.__x = [0]*nb
        self.__y = [0]*nb
        # 3D-axis -- save memory when not used
        self.__z = None
        # Confidence scores -- save memory when not used
        self.__confidence = None

    # -----------------------------------------------------------------------
    # All sights stored in this class
    # -----------------------------------------------------------------------

    def reset(self):
        """Reset all values."""
        # 2D-axis values
        self.__x = [0]*self.__nb
        self.__y = [0]*self.__nb
        # 3D-axis -- save memory when not used
        self.__z = None
        # Confidence scores -- save memory when not used
        self.__confidence = None

    # -----------------------------------------------------------------------

    def copy(self):
        """Return a deep copy of the current sppasSights()."""
        copied = sppasSights(nb=self.__nb)
        for i in range(self.__nb):
            x, y, z, s = self.get_sight(i)
            copied.set_sight(i, x, y, z, s)
        return copied

    # -----------------------------------------------------------------------

    def get_x(self):
        """Return the list of x values."""
        # returning self.__x allows it to be modified: a list is mutable.
        # here we return a copy in a tuple so self.__x won't change.
        return tuple(self.__x)

    # -----------------------------------------------------------------------

    def get_y(self):
        """Return the list of y values."""
        return tuple(self.__y)

    # -----------------------------------------------------------------------

    def get_z(self):
        """Return the list of z values or None."""
        if self.__z is None:
            return None
        return tuple(self.__y)

    # -----------------------------------------------------------------------

    def get_s(self):
        """Return the list of confidence score values or None."""
        if self.__confidence is None:
            return None
        return tuple(self.__confidence)

    # -----------------------------------------------------------------------
    # An individual sight
    # -----------------------------------------------------------------------

    def get_sight(self, idx):
        """Return the (x, y, z, s) of the given sight.

        :param idx: (int) Index of the sight
        :return: tuple(x, y, z, confidence)

        """
        return self.__x[idx], self.__y[idx], self.z(idx), self.score(idx)

    # -----------------------------------------------------------------------

    def set_sight(self, idx, x, y, z=None, s=None):
        """Set the sight at the given index.

        :param idx: (int) Index of the sight
        :param x: (int) pixel position on the x axis (width)
        :param y: (int) pixel position on the y axis (height)
        :param z: (int or None) pixel position on the z axis
        :param s: (float or None) An optional confidence score

        """
        # Check the given parameters
        idx = self.check_index(idx)
        x = sppasCoords.to_dtype(x, int, unsigned=True)
        y = sppasCoords.to_dtype(y, int, unsigned=True)

        # Assign values to our data structures
        self.__x[idx] = x
        self.__y[idx] = y
        self.set_sight_z(idx, z)
        self.set_sight_score(idx, s)

    # -----------------------------------------------------------------------

    def x(self, idx):
        """Return the x-axe value of the sight at the given index.

        :param idx: (int) Index of the sight
        :return: (int)

        """
        idx = self.check_index(idx)
        return self.__x[idx]

    # -----------------------------------------------------------------------

    def y(self, idx):
        """Return the y-axe value of the sight at the given index.

        :param idx: (int) Index of the sight
        :return: (int)

        """
        idx = self.check_index(idx)
        return self.__y[idx]

    # -----------------------------------------------------------------------

    def z(self, idx):
        """Return the z-axe value of the sight at the given index or None.

        :param idx: (int) Index of the sight
        :return: (int or None)

        """
        if self.__z is None:
            return None

        idx = self.check_index(idx)
        return self.__z[idx]

    # -----------------------------------------------------------------------

    def set_sight_z(self, idx, z):
        """Set a z value to the sight at the given index.

        :param idx: (int) Index of the sight
        :param z: (int or None) An optional z value

        """
        idx = self.check_index(idx)
        # If a score is assigned
        if z is not None:
            s = sppasCoords.to_dtype(z, int, unsigned=False)
            if self.__z is None:
                # hum... we never assigned a z... create the list now
                self.__z = [None] * self.__nb
            self.__z[idx] = s
        else:
            if self.__z is not None:
                # A z is not set but we already have some. Clear the
                # one that is already existing.
                self.__z[idx] = None

    # -----------------------------------------------------------------------

    def score(self, idx):
        """Return the score of the sight at the given index or None.

        :param idx: (int) Index of the sight or None to get the average score
        :return: (int or None)

        """
        if self.__confidence is None:
            return None

        idx = self.check_index(idx)
        return self.__confidence[idx]

    # -----------------------------------------------------------------------

    def get_mean_score(self):
        """Return the mean score or None."""
        if self.__confidence is None:
            return None
        values = [v for v in self.__confidence if v is not None]
        if len(values) == 0:
            return None
        return sum(values) / len(values)

    # -----------------------------------------------------------------------

    def set_sight_score(self, idx, s):
        """Set a score to the sight at the given index.

        :param idx: (int) Index of the sight
        :param s: (float or None) An optional confidence score

        """
        idx = self.check_index(idx)
        # If a score is assigned
        if s is not None:
            s = sppasCoords.to_dtype(s, float, unsigned=False)
            if self.__confidence is None:
                # hum... we never assigned a score... create the list now
                self.__confidence = [None] * self.__nb
            self.__confidence[idx] = s
        else:
            if self.__confidence is not None:
                # A score is not set but we already have some. Clear the
                # one that is already existing.
                self.__confidence[idx] = None

    # -----------------------------------------------------------------------
    # Utilities
    # -----------------------------------------------------------------------

    def intermediate(self, other):
        """Return the sights with the intermediate positions.

        :param other: (sppasSights)
        :return: (sppasSights)

        """
        if isinstance(other, sppasSights) is False:
            raise sppasTypeError(other, "sppasSights")

        if len(other) != self.__nb:
            raise ValueError("Intermediate estimation expected {:d} sights. "
                             "Got {:d} instead.".format(self.__nb, len(other)))
        s = sppasSights(self.__nb)
        i = 0
        for s1, s2 in zip(self, other):  # s1=(x1,y1,z1,c1) and s2=(x2,y2,z2,c2)
            # estimate the (x,y) middle point
            x = s1.x(i) + ((s2.x(i) - s1.x(i)) // 2)
            y = s1.y(i) + ((s2.y(i) - s1.y(i)) // 2)
            # estimate the z middle point
            z = None
            if s1.z(i) is not None and s1.z(i) is not None:
                z = s1.z(i) + ((s2.z(i) - s1.z(i)) // 2)
            # estimate the average score
            c = None
            if s1.score(i) is not None and s2.score(i) is not None:
                c = (s1.score(i) + s2.score(i)) / 2.
            # Then set the sight position and score
            s.set_sight(i, x, y, z, c)
            i += 1

        return s

    # -----------------------------------------------------------------------

    def check_index(self, value):
        """Raise an exception if the given index is not valid.

        :param value: (int)
        :raise: sppasTypeError, NegativeValueError, IndexRangeException

        """
        # Check if the given value is an integer
        try:
            value = int(value)
        except ValueError:
            raise sppasTypeError(value, "int")

        # Check if the given value is in the range [0,nb]
        if value < 0:
            raise NegativeValueError(value)
        if self.__nb < value:
            raise IndexRangeException(value, 0, self.__nb)

        # The given value is good
        return value

    # -----------------------------------------------------------------------
    # Overloads
    # -----------------------------------------------------------------------

    def __str__(self):
        s = ""
        for i in range(self.__nb):
            s += "({:d},{:d}".format(self.__x[i], self.__y[i])
            if self.__z is not None:
                s += ",{}".format(self.__z[i])
            if self.__confidence is not None:
                if self.__confidence[i] is not None:
                    s += ": {}".format(self.__confidence[i])
            s += ") "
        return s

    # -----------------------------------------------------------------------

    def __repr__(self):
        return self.__class__.__name__

    # -----------------------------------------------------------------------

    def __format__(self, fmt):
        return str(self).__format__(fmt)

    # ------------------------------------------------------------------------

    def __len__(self):
        """Return the number of sights."""
        return self.__nb

    # ------------------------------------------------------------------------

    def __iter__(self):
        """Browse the current sights."""
        for i in range(self.__nb):
            yield self.get_sight(i)

    # ------------------------------------------------------------------------

    def __getitem__(self, item):
        if isinstance(item, slice):
            # Get the start, stop, and step from the slice
            return [self.get_sight(ii) for ii in range(*item.indices(len(self)))]

        return self.get_sight(item)

    # -----------------------------------------------------------------------

    def __contains__(self, other):
        """Return true if value in sights -- score is ignored.

        :param other: a list/tuple of (x,y,...)

        """
        if isinstance(other, (list, tuple)) is True:
            if len(other) < 2:
                return False
            c = sppasSights(1)
            c.set_sight(0, other[0], other[1])
            if len(other) > 2:
                if isinstance(other[2], int):
                    c.set_sight_z(0, other[2])
            other = c

        if isinstance(other, sppasSights) is False:
            return False

        for i in range(self.__nb):
            if self.__x[i] == other.x(0) and self.__y[i] == other.y(0) and self.z(i) == other.z(0):
                return True
        return False

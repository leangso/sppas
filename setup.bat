GOTO EndHeader

:filename:     setup.bat
:author:       Brigitte Bigi
:contact:      develop@sppas.org
:summary:      Launch the setup of SPPAS for Windows.

.. _This file is part of SPPAS: https://sppas.org/
..
    ---------------------------------------------------------------------
     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2023  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    Use of this software is governed by the GNU Public License, version 3.

    SPPAS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPPAS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPPAS. If not, see <http://www.gnu.org/licenses/>.

    This banner notice must not be removed.

    ---------------------------------------------------------------------

:EndHeader

@echo off
title "SPPAS Setup"

SET PYTHONIOENCODING=UTF-8
SET PYENV=".sppaspyenv~"
SET ACTIVATE=".sppaspyenv~\Scripts\activate.bat"
SET PYTHON=".sppaspyenv~\Scripts\python.exe"

echo ----------------------------------------------------------------------------------
echo Welcome to SPPAS setup.
echo a program written by Brigitte Bigi (PhD), Researcher at
echo Laboratoire Parole et Langage, Aix-en-Provence, France.
echo ----------------------------------------------------------------------------------
echo This setup creates a Python Virtual Environment for SPPAS in %PYENV%
echo It will then install wxPython and launch the Graphical User Installer
echo in order to install some external programs.
echo ----------------------------------------------------------------------------------

if not exist %PYENV% (
    echo Search for python.exe command
    WHERE python.exe
    if %ERRORLEVEL% EQU 0 (
        echo ... python.exe found.
        python.exe .\sppas\bin\checkpy.py
        if %ERRORLEVEL% EQU 0 (
            color 1E
            echo Please wait while Python virtual environment is being created
            python.exe -m venv %PYENV%
            echo ... ok
        ) else (
			color 4E
			if %ERRORLEVEL% EQU 9009 (
				echo The python.exe program exists but it can't be executed.
				echo It probably means that it was installed and desinstalled.
				echo The python.exe remains but does not work.
            ) else (
				echo ... invalid python version.
			)
        )
   ) else (
        echo ... python.exe not found.
   )
)

if not exist %PYENV% (
    echo Search for python3.exe command
    WHERE python3.exe >nul 2>nul
    if %ERRORLEVEL% EQU 0 (
        echo ... python3.exe found.
        python3.exe .\sppas\bin\checkpy.py
        if %ERRORLEVEL% EQU 0 (
            color 1E
            echo Please wait while Python virtual environment is being created
            python3.exe -m venv %PYENV%
            echo ... ok
        ) else (
			color 4E
			if %ERRORLEVEL% EQU 9009 (
				echo The python3.exe program exists but it can't be executed.
				echo It probably means that it was installed and desinstalled 
				echo but the file remains. You need to re-install it.
            ) else (
				echo ... invalid python version.
			)
		)
   )
)

REM we need to wait that Windows realizes that the pyenv was created...
timeout /t 3

REM Launch the setup within the python virtual environment
if exist %PYENV% (
    REM ------- until SPPAS 4.9, wxPython required to be installed first.
    REM echo Check if wxPython is installed or needs update...
    REM %PYTHON% .\sppas\ui\wxapp\preinstall.py --wxpython
    REM %PYTHON% -m pip install wxpython
    REM add a required timeout in order to let some time to Windows OS to take changed into account!!!!
    REM timeout /t 10
    REM echo ... done
    REM echo Launch installation GUI program
    REM start "" %PYTHON% .\sppas\bin\preinstallgui.py
    REM Close the windows with a delay to let some time to read messages
    REM timeout /t 5
	color F1
    echo Launch the installation program in web browser
    echo * * *  Closing this windows will kill the application  * * *
    start "SPPAS Web-Setup" /b /high %PYTHON% .\sppas\ui\swapp\__main__.py --setup
    REM ... /b Starts an application without opening a new Command Prompt window
    REM ... /high Starts an application in the specified priority class.
	echo ----------------------------------------------------------------------

) else (
    color 4E
	echo ********************************************************
    echo      No valid python command was found or 
    echo      the virtual environment failed to be created.
    echo      Install Python 3 from the Windows Store,
	echo      and re-start this setup.
	echo ********************************************************
    timeout /t 120
)

import os
import sys
import csv
import shutil
import subprocess

from os import path

PROGRAM = os.path.abspath(__file__)
SPPAS = os.path.dirname(os.path.dirname(PROGRAM))
sys.path.append(SPPAS)


###################################


def scan_dir(path):
    dirs = list(os.scandir(path))
    dirs.sort(key=lambda x: x.name)
    return dirs


def get_utt_id_info(utt_id):
    parts = utt_id.split('-')
    spk_id = parts[0]
    session_id = parts[1]
    trans_id = parts[2]
    return spk_id, session_id, trans_id


def prepare_datafiles(dataset_dir, output_dir):
    # read dataset
    dataset = []
    wav_dir = path.join(dataset_dir, 'wav')
    trans_file = path.join(dataset_dir, 'trans.csv')

    print('>>> Read dataset: ', trans_file)

    with open(trans_file, 'r', encoding='utf-8') as file:
        csv_reader = csv.reader(file, delimiter='\t')
        for row in csv_reader:
            utt_id = row[0]
            trans = row[1]

            spk_id, session_id, trans_id = get_utt_id_info(utt_id)
            wav_file = path.join(wav_dir, spk_id, '%s-%s.wav' % (session_id, trans_id))

            record = (utt_id, spk_id, session_id, trans_id, trans, wav_file)
            dataset.append(record)

    os.makedirs(output_dir, exist_ok=True)

    print('>>> Generate data files')

    prev_spk = None
    for (utt_id, spk_id, session_id, trans_id, trans, wav_file) in dataset:
        if prev_spk != spk_id:
            out_spk_dir = path.join(output_dir, spk_id)
            os.makedirs(out_spk_dir, exist_ok=True)

            print('Speaker: ', spk_id)
            prev_spk = spk_id

        # prepare files: transcript + wav
        trans_fname = '%s_%s.txt' % (session_id, trans_id)
        with open(path.join(out_spk_dir, trans_fname), 'w', encoding='utf-8') as writer:
            writer.write(trans)

        wav_fname = '%s_%s.wav' % (session_id, trans_id)
        out_wav_file = path.join(out_spk_dir, wav_fname)
        shutil.copyfile(wav_file, out_wav_file)


def palign(data_dir, lang='fra', ext='.csv', clean_output=False, timeout=60):
    for file in scan_dir(data_dir):
        if not file.name.endswith('.wav'):
                continue

        # generate annotations
        script = str(path.join(SPPAS, 'sppas', 'bin', 'annotation.py'))
        command = ['python', script, '-l', lang, '-e', ext, '-I', file.path, '--fillipus', '--textnorm', '--phonetize', '--alignment']

        try:
            p = subprocess.run(command, timeout=timeout, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        except FileNotFoundError as exc:
            print('!!! FileNotFound: ', file.path)
        except subprocess.TimeoutExpired as exc:
            print('!!! Timeout: ', file.name)
        except subprocess.CalledProcessError as exc:
            print('!!! Error: ', file.name)

        if clean_output:
            fname = os.path.splitext(path.basename(file.name))[0]
            try:
                os.remove(os.path.join(data_dir, '%s-token%s' % (fname, ext)))
                os.remove(os.path.join(data_dir, '%s-phon%s' % (fname, ext)))
                os.remove(os.path.join(data_dir, '%s%s' % (fname, ext)))
            except FileNotFoundError as e:
                pass


def perform_palign(dataset_dir, output_dir, lang='fra', ext='.csv', clean_output=False, clean=True, timeout=60):
    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    # prepare data files for aligment
    prepare_datafiles(dataset_dir=dataset_dir, output_dir=output_dir)

    # perform aligment
    print('>>> Generate alignment')

    for spk_dir in scan_dir(output_dir):
        if not spk_dir.is_dir() or spk_dir.name.startswith('.') or spk_dir.name.startswith('_'):
                continue
        
        print('Speaker: ', spk_dir.name)
        palign(spk_dir.path, lang=lang, ext=ext, clean_output=clean_output, timeout=timeout)
        

###################################


if __name__ == "__main__":
    dataset_dir = r'/home/sotheara.leang/phd/exp/sppas/braf100'
    output_dir = r'/home/sotheara.leang/phd/exp/sppas/braf100_palign'
    
    lang = 'fra'
    ext = '.csv'
    clean_output = False
    clean = True
    timeout = 20

    perform_palign(dataset_dir=dataset_dir, output_dir=output_dir, lang=lang, ext=ext, clean_output=clean_output, clean=clean, timeout=timeout)

    # spk_dir = r'C:\Users\Sotheara Leang\Downloads\braf100_palign\FAJ_Train'
    # palign(spk_dir, lang=lang, ext=ext, clean_output=clean_output, timeout=timeout)

#!/bin/bash
# -*- coding: UTF-8 -*-
# ---------------------------------------------------------------------------
#                   This banner notice must not be removed
# ---------------------------------------------------------------------------
# Filename: sppas.command
# Author:   Brigitte Bigi
# Contact:  develop@sppas.org
# Summary:  Launch the GUI of SPPAS for MacOS/Linux.
# ---------------------------------------------------------------------------
#            ___   __    __    __    ___
#           /     |  \  |  \  |  \  /              the automatic
#           \__   |__/  |__/  |___| \__             annotation and
#              \  |     |     |   |    \             analysis
#           ___/  |     |     |   | ___/              of speech
#
#           https://sppas.org/
#
# ---------------------------------------------------------------------------
#                   Copyright (C) 2011-2023  Brigitte Bigi
#            Laboratoire Parole et Langage, Aix-en-Provence, France
# ---------------------------------------------------------------------------
# Use of this software is governed by the GNU Public License, version 3.
#
# SPPAS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SPPAS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SPPAS. If not, see <http://www.gnu.org/licenses/>.
#
# ---------------------------------------------------------------------------


# ===========================================================================
# Fix global & environment variables
# ===========================================================================

PROGRAM_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
export PYTHONIOENCODING=UTF-8
export SPPAS=$PROGRAM_DIR

# Python virtual environment path
PYENV="$PROGRAM_DIR/.sppaspyenv~"
PYTHON="$PYENV/bin/python"


# ===========================================================================
# FUNCTIONS
# ===========================================================================

command_exists () {
    type "$1" &> /dev/null ;
}


# ===========================================================================
# MAIN
# ===========================================================================
cd $SPPAS

# Write useful information
unamestr=`uname | cut -f1 -d'_'`;
echo "This program starts with: ";
echo "  - System:  $unamestr";
echo "  - Display:  $DISPLAY";
echo "  - Location: $PROGRAM_DIR";
echo "  - Command: $PYTHON";

if command_exists "$PYTHON"; then
    echo -e "  - Python: version ";
    "$PYTHON" --version;

    # Get the name of the system
    if [ "$unamestr" == "CYGWIN" ]; then
        if [ -z $DISPLAY ]; then
           echo "[ ERROR ] Unable to access the X Display.";
           echo "Did you enabled XWin server?";
           exit -1;
        fi
    fi

    echo "SPPAS Graphical User Interface...";
    $PYTHON sppas;
    exit $?;

else
    echo "[ ERROR ] Python virtual environment for SPPAS does not exists.";
    echo "          ... did you forget to read the installation instructions?";
    exit -1;
fi

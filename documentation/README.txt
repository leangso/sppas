    -------------------------------------------------------------------------
     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    -------------------------------------------------------------------------

	SPPAS documentation

    -------------------------------------------------------------------------

    This folder contains the PDF files describing SPPAS and/or files helping
    in understanding/using it.

    Except for the main reference, permission is granted to copy, distribute
    and/or modify these documents under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.

    A copy of the license is included in this folder.

    -------------------------------------------------------------------------

    -------------------------------------------------------------------------
     ___   __    __    __    ___
    /     |  \  |  \  |  \  /              the automatic
    \__   |__/  |__/  |___| \__             annotation and
       \  |     |     |   |    \             analysis
    ___/  |     |     |   | ___/              of speech

    Copyright (C) 2011-2021  Brigitte Bigi
    Laboratoire Parole et Langage, Aix-en-Provence, France

    -------------------------------------------------------------------------

	SPPAS samples
	
    -------------------------------------------------------------------------

    This is a set of recorded files in order to test the automatic annotations
	of SPPAS. The folder "faces" contains video files to test the face detection,
	face sights and cued speech. The other folders are audio recordings and the
	corresponding orthographic transcription of various languages. The language
	name is coded in iso639-3, i.e. it is identified with 3 characters. They
	allow to test all annotations of the STANDALONE type (see documentation): 
	text normalization, phonetization, alignment, etc.
	
	It is highly recommended to test SPPAS on these samples!
	
	All the files are licensed under a Creative Commons 
	Attribution-NonCommercial-ShareAlike 4.0 International License.
	
    -------------------------------------------------------------------------
